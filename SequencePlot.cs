﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace OpticsBuilder
{
    public partial class SequencePlot : Form
    {
        public int startR, endR;
        public DataGridView grid;
        public SequencePlot()
        {
            InitializeComponent();
        }

        internal void DrawSequence(DataGridView sequenceGrid, int startRow, int endRow)
        {
            startR=startRow; endR=endRow;
            grid = sequenceGrid;

            sequenceChart.ChartAreas[0].AxisY.IsStartedFromZero = false; // This allows the Y-axis to not necessarily start from 0
            //sequenceChart.ChartAreas[0].AxisY.Minimum = Double.NaN; // Set to NaN to auto-scale
            //sequenceChart.ChartAreas[0].AxisY.Maximum = Double.NaN; // Set to NaN to auto-scale


            for (int i = startR; i <= endR; i++) 
            {
                DataGridViewRow r = grid.Rows[i];
                if (r.Cells["startX"].Value != null && r.Cells["startZ"].Value != null)
                {
                    try
                    {
                        double x = Convert.ToDouble(r.Cells["startX"].Value);
                        double z = Convert.ToDouble(r.Cells["startZ"].Value);
                        
                        sequenceChart.Series[0].Points.AddXY(x, z);          
                    }
                    catch
                    {
                        continue;
                    }
                }
            }
        }

        private void apertureChart_MouseMove(object sender, MouseEventArgs e)
        {

        }

        private void apertureChart_MouseMove_1(object sender, MouseEventArgs e)
        {
            try
            {
                var sourceChart = sender as System.Windows.Forms.DataVisualization.Charting.Chart;
                //var result = sourceChart.HitTest(e.X, e.Y);
                var chartAreas = sourceChart.ChartAreas[0];


                chartAreas.CursorX.Position = chartAreas.AxisX.PixelPositionToValue(e.X);
                chartAreas.CursorY.Position = chartAreas.AxisY.PixelPositionToValue(e.Y);

                cursorCoord.Text = "X=" + chartAreas.CursorX.Position.ToString() + " Y=" + chartAreas.CursorY.Position.ToString();
            }
            catch
            {
                return;
            }
            if (_prevPosition.HasValue)
            {
                _rect.Width = e.X - _rect.X;
                _rect.Height = e.Y - _rect.Y;

                sequenceChart.Invalidate(); // Redraw the chart to show the zoom rectangle
            }
        }

        private void apertureChart_Click(object sender, EventArgs e)
        {

        }

        private void cursorCoord_Click(object sender, EventArgs e)
        {

        }

        private Point? _prevPosition = null;
        private Rectangle _rect = new Rectangle();

        private void sequenceChart_MouseDown(object sender, MouseEventArgs e)
        {
            _prevPosition = e.Location;
            _rect.Location = e.Location;
        }

        private void sequenceChart_MouseUp(object sender, MouseEventArgs e)
        {
            try
            {
                if (_prevPosition.HasValue)
                {
                    if (_rect.Width > 0 && _rect.Height > 0)
                    {
                        double xStart = sequenceChart.ChartAreas[0].AxisX.PixelPositionToValue(_rect.X);
                        double xEnd = sequenceChart.ChartAreas[0].AxisX.PixelPositionToValue(_rect.X + _rect.Width);
                        double yStart = sequenceChart.ChartAreas[0].AxisY.PixelPositionToValue(_rect.Y + _rect.Height);
                        double yEnd = sequenceChart.ChartAreas[0].AxisY.PixelPositionToValue(_rect.Y);

                        sequenceChart.ChartAreas[0].AxisX.ScaleView.Zoom(xStart, xEnd);
                        sequenceChart.ChartAreas[0].AxisY.ScaleView.Zoom(yStart, yEnd);
                    }

                    _prevPosition = null;
                    sequenceChart.Invalidate();
                }
            }
            catch
            {
                //Autoscale bounds out of range, ignore
            }
        }

        private void resetZoomButton_Click(object sender, EventArgs e)
        {
            sequenceChart.ChartAreas[0].AxisX.ScaleView.ZoomReset();
            sequenceChart.ChartAreas[0].AxisY.ScaleView.ZoomReset();
        }

        private void sequenceChart_Paint(object sender, PaintEventArgs e)
        {
            if (_prevPosition.HasValue)
            {
                using (Pen pen = new Pen(Color.Black))
                {
                    pen.DashStyle = System.Drawing.Drawing2D.DashStyle.Dash;
                    e.Graphics.DrawRectangle(pen, _rect);
                }
            }
        }

        private void displayNamesCheckbox_CheckedChanged(object sender, EventArgs e)
        {
            if (displayNamesCheckbox.Checked && (endR-startR)>500)
            {
                displayNamesCheckbox.Checked = false;
                MessageBox.Show("Max 500 labels can be displayed");
                return;
            }

            int pointId=0;
            for (int i = startR; i <= endR; i++)
            {
                DataGridViewRow r = grid.Rows[i];
                if (r.Cells["startX"].Value != null && r.Cells["startZ"].Value != null)
                {
                    try
                    {
                        if (!displayNamesCheckbox.Checked)
                        {
                            sequenceChart.Series[0].Points[pointId].Label = "";
                            sequenceChart.Series[0].Points[pointId].LabelToolTip = "";
                        }
                        else
                        {
                            double x = Convert.ToDouble(r.Cells["startX"].Value);
                            double z = Convert.ToDouble(r.Cells["startZ"].Value);



                            if (r.Cells["AVName"].Value != null)
                            {
                                try
                                {
                                    string label = r.Cells["AVName"].Value.ToString();
                                    sequenceChart.Series[0].Points[pointId].Label = label;
                                    sequenceChart.Series[0].Points[pointId].LabelToolTip =
                                        "Row " + (i + 1) + " X=" + r.Cells["startX"].Value.ToString() + " Z=" +
                                        r.Cells["startZ"].Value.ToString() + " theta=" +
                                        r.Cells["thetaStart"].Value.ToString();
                                }
                                catch
                                {

                                }
                            }
                        }
                    }
                    catch
                    {
                    }
                }
                pointId++;
            }
        }
    }
}

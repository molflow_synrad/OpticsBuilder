﻿namespace OpticsBuilder
{
    partial class AperturePlot
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.apertureChart = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.cursorCoord = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.apertureChart)).BeginInit();
            this.SuspendLayout();
            // 
            // apertureChart
            // 
            chartArea1.AxisX.Title = "X [m]";
            chartArea1.AxisY.Title = "Y [m]";
            chartArea1.Name = "ChartArea1";
            this.apertureChart.ChartAreas.Add(chartArea1);
            this.apertureChart.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.apertureChart.Dock = System.Windows.Forms.DockStyle.Fill;
            this.apertureChart.Location = new System.Drawing.Point(0, 0);
            this.apertureChart.Name = "apertureChart";
            series1.BorderWidth = 2;
            series1.ChartArea = "ChartArea1";
            series1.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series1.Legend = "Legend1";
            series1.MarkerColor = System.Drawing.Color.Red;
            series1.MarkerSize = 7;
            series1.MarkerStyle = System.Windows.Forms.DataVisualization.Charting.MarkerStyle.Circle;
            series1.Name = "Series1";
            series1.XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Double;
            series1.YValuesPerPoint = 2;
            series1.YValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Double;
            this.apertureChart.Series.Add(series1);
            this.apertureChart.Size = new System.Drawing.Size(800, 446);
            this.apertureChart.TabIndex = 0;
            this.apertureChart.Text = "aperture";
            this.apertureChart.Click += new System.EventHandler(this.apertureChart_Click);
            this.apertureChart.MouseMove += new System.Windows.Forms.MouseEventHandler(this.apertureChart_MouseMove);
            // 
            // cursorCoord
            // 
            this.cursorCoord.AutoSize = true;
            this.cursorCoord.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.cursorCoord.Location = new System.Drawing.Point(0, 433);
            this.cursorCoord.Name = "cursorCoord";
            this.cursorCoord.Size = new System.Drawing.Size(97, 13);
            this.cursorCoord.TabIndex = 1;
            this.cursorCoord.Text = "Mouse coordinates";
            // 
            // AperturePlot
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 446);
            this.Controls.Add(this.cursorCoord);
            this.Controls.Add(this.apertureChart);
            this.Name = "AperturePlot";
            this.Text = "AperturePlot";
            ((System.ComponentModel.ISupportInitialize)(this.apertureChart)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataVisualization.Charting.Chart apertureChart;
        private System.Windows.Forms.Label cursorCoord;
    }
}
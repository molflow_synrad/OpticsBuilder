﻿namespace OpticsBuilder
{
    partial class OpticsBuilderForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            this.tabControl = new System.Windows.Forms.TabControl();
            this.resultTab = new System.Windows.Forms.TabPage();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.sMinText = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.exportButton = new System.Windows.Forms.Button();
            this.createDriftsCheckbox = new System.Windows.Forms.CheckBox();
            this.sMaxText = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.readSeqButton = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.dispInsertDriftsCheckbox = new System.Windows.Forms.CheckBox();
            this.readDispButton = new System.Windows.Forms.Button();
            this.surveyLabel = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.seqSurveyDataView = new System.Windows.Forms.DataGridView();
            this.instanceName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.instanceObject = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.instanceType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.instancePos = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.survLength = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.hasDispData = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.seqObjectsDataView = new System.Windows.Forms.DataGridView();
            this.name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.type = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.seqValuesDataView = new System.Windows.Forms.DataGridView();
            this.valueName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ValueValue = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.inputTab = new System.Windows.Forms.TabPage();
            this.paramsPanel = new System.Windows.Forms.Panel();
            this.getResultButton = new System.Windows.Forms.Button();
            this.prg = new System.Windows.Forms.ProgressBar();
            this.beginsWithCheckbox = new System.Windows.Forms.CheckBox();
            this.nameDefinesTypeCheckbox = new System.Windows.Forms.CheckBox();
            this.analyzeButton = new System.Windows.Forms.Button();
            this.resetTableButton = new System.Windows.Forms.Button();
            this.pasteButton = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.addLineButton = new System.Windows.Forms.Button();
            this.defineNameColButton = new System.Windows.Forms.Button();
            this.remLineButton = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.defineTypeButton = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.defineSButton = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.sModeBox = new System.Windows.Forms.GroupBox();
            this.sType1 = new System.Windows.Forms.RadioButton();
            this.sType0 = new System.Windows.Forms.RadioButton();
            this.button2 = new System.Windows.Forms.Button();
            this.refLocX = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.quadNameText = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.dipoleNameText = new System.Windows.Forms.TextBox();
            this.driftNameText = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.refDirZ = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.refDirY = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.defineDipoleButton = new System.Windows.Forms.Button();
            this.refDirX = new System.Windows.Forms.TextBox();
            this.defineQuadButton = new System.Windows.Forms.Button();
            this.refLocZ = new System.Windows.Forms.TextBox();
            this.refLocY = new System.Windows.Forms.TextBox();
            this.defineReferenceRowButton = new System.Windows.Forms.Button();
            this.quadSameColumnCheckbox = new System.Windows.Forms.CheckBox();
            this.defineBetaXButton = new System.Windows.Forms.Button();
            this.quadModeBox = new System.Windows.Forms.GroupBox();
            this.quadGradientModeRadio = new System.Windows.Forms.RadioButton();
            this.quadK1LmodeRadio = new System.Windows.Forms.RadioButton();
            this.defineBetaYButton = new System.Windows.Forms.Button();
            this.dipoleModeBox = new System.Windows.Forms.GroupBox();
            this.beamEnergyTextbox = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.dipoleStrecgthModeRadio = new System.Windows.Forms.RadioButton();
            this.dipoleAngleModeRadio = new System.Windows.Forms.RadioButton();
            this.defineEtaXButton = new System.Windows.Forms.Button();
            this.defineAlphaYButton = new System.Windows.Forms.Button();
            this.defineEtaPrimeButton = new System.Windows.Forms.Button();
            this.defineAlphaXButton = new System.Windows.Forms.Button();
            this.inputDataView = new System.Windows.Forms.DataGridView();
            this.dbTab = new System.Windows.Forms.TabPage();
            this.queryView = new System.Windows.Forms.DataGridView();
            this.queryName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.querySstart = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.querySend = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.queryShape = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.queryDiamU = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.queryDiamV = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.queryEllipseA = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.queryEllipseB = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.queryEllipseC = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.queryEllipseD = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.queryNote = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.machineCodeCombo = new System.Windows.Forms.ComboBox();
            this.connResultText = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.queryResultText = new System.Windows.Forms.Label();
            this.dbTableText = new System.Windows.Forms.TextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.button8 = new System.Windows.Forms.Button();
            this.passwordText = new System.Windows.Forms.TextBox();
            this.usernameText = new System.Windows.Forms.TextBox();
            this.serverText = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.queryButton = new System.Windows.Forms.Button();
            this.exportTab = new System.Windows.Forms.TabPage();
            this.taperCombo = new System.Windows.Forms.ComboBox();
            this.label34 = new System.Windows.Forms.Label();
            this.curveStepText = new System.Windows.Forms.TextBox();
            this.label33 = new System.Windows.Forms.Label();
            this.readApertureButton = new System.Windows.Forms.Button();
            this.circleSidesText = new System.Windows.Forms.TextBox();
            this.label32 = new System.Windows.Forms.Label();
            this.writeGeomButton = new System.Windows.Forms.Button();
            this.browseGeomButton = new System.Windows.Forms.Button();
            this.geomFileTextBox = new System.Windows.Forms.TextBox();
            this.label31 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.searchResultLabel = new System.Windows.Forms.Label();
            this.searchEndButton = new System.Windows.Forms.Button();
            this.searchBeginningButton = new System.Windows.Forms.Button();
            this.searchTermTextbox = new System.Windows.Forms.TextBox();
            this.label28 = new System.Windows.Forms.Label();
            this.suffixCheckbox = new System.Windows.Forms.CheckBox();
            this.readTwissButton = new System.Windows.Forms.Button();
            this.writeBXYfileButton = new System.Windows.Forms.Button();
            this.getEndLineButton = new System.Windows.Forms.Button();
            this.getStartLineButton = new System.Windows.Forms.Button();
            this.endLineText = new System.Windows.Forms.TextBox();
            this.startLineText = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.startLineLabel = new System.Windows.Forms.Label();
            this.bAngleButton = new System.Windows.Forms.Button();
            this.prg_new = new System.Windows.Forms.ProgressBar();
            this.resetTableButton_new = new System.Windows.Forms.Button();
            this.pasteFromClipboardButton_new = new System.Windows.Forms.Button();
            this.addLineButton_new = new System.Windows.Forms.Button();
            this.remLineButton_new = new System.Windows.Forms.Button();
            this.refThetaText = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.refZText = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.refXText = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.getRefRowButton = new System.Windows.Forms.Button();
            this.refRowTextBox = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.prefixCheckbox = new System.Windows.Forms.CheckBox();
            this.eMaxText = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.eMinText = new System.Windows.Forms.TextBox();
            this.eMinLabel = new System.Windows.Forms.Label();
            this.dLtext = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.energySpreadText = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.beamEText = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.analyzedView = new System.Windows.Forms.DataGridView();
            this.hasApertureInfo = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.AVname = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.length = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.angle = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.B = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.grad = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.K1L = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.twissS = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.s = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.startX = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.thetaStart = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.startZ = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.zlimit = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.xOffset = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.yOffset = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.xAngle = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.yAngle = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.betaX = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.betaY = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.etaX = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.etaPX = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.alphaX = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.alphaY = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AptType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Aperture1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Aperture2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Aperture3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Aperture4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.beamELabel = new System.Windows.Forms.Label();
            this.pMassText = new System.Windows.Forms.TextBox();
            this.browseBXYbutton = new System.Windows.Forms.Button();
            this.bxyFileNameText = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.currentText = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.emittYtext = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.emittXtext = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.writeFilesButton = new System.Windows.Forms.Button();
            this.dirTextBox = new System.Windows.Forms.TextBox();
            this.browseButton = new System.Windows.Forms.Button();
            this.apertureTab = new System.Windows.Forms.TabPage();
            this.tabControl.SuspendLayout();
            this.resultTab.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.seqSurveyDataView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.seqObjectsDataView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.seqValuesDataView)).BeginInit();
            this.inputTab.SuspendLayout();
            this.paramsPanel.SuspendLayout();
            this.sModeBox.SuspendLayout();
            this.quadModeBox.SuspendLayout();
            this.dipoleModeBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.inputDataView)).BeginInit();
            this.dbTab.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.queryView)).BeginInit();
            this.exportTab.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.analyzedView)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl
            // 
            this.tabControl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl.Controls.Add(this.resultTab);
            this.tabControl.Controls.Add(this.inputTab);
            this.tabControl.Controls.Add(this.dbTab);
            this.tabControl.Controls.Add(this.exportTab);
            this.tabControl.Controls.Add(this.apertureTab);
            this.tabControl.Location = new System.Drawing.Point(12, 12);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(914, 643);
            this.tabControl.TabIndex = 4;
            // 
            // resultTab
            // 
            this.resultTab.Controls.Add(this.groupBox3);
            this.resultTab.Controls.Add(this.groupBox2);
            this.resultTab.Controls.Add(this.groupBox1);
            this.resultTab.Controls.Add(this.surveyLabel);
            this.resultTab.Controls.Add(this.label9);
            this.resultTab.Controls.Add(this.label8);
            this.resultTab.Controls.Add(this.seqSurveyDataView);
            this.resultTab.Controls.Add(this.seqObjectsDataView);
            this.resultTab.Controls.Add(this.seqValuesDataView);
            this.resultTab.Location = new System.Drawing.Point(4, 22);
            this.resultTab.Name = "resultTab";
            this.resultTab.Padding = new System.Windows.Forms.Padding(3);
            this.resultTab.Size = new System.Drawing.Size(906, 617);
            this.resultTab.TabIndex = 1;
            this.resultTab.Text = "MadX SEQ/DISP/TWISS file import";
            this.resultTab.UseVisualStyleBackColor = true;
            // 
            // groupBox3
            // 
            this.groupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.groupBox3.Controls.Add(this.sMinText);
            this.groupBox3.Controls.Add(this.label10);
            this.groupBox3.Controls.Add(this.label11);
            this.groupBox3.Controls.Add(this.exportButton);
            this.groupBox3.Controls.Add(this.createDriftsCheckbox);
            this.groupBox3.Controls.Add(this.sMaxText);
            this.groupBox3.Location = new System.Drawing.Point(500, 539);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(339, 72);
            this.groupBox3.TabIndex = 18;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Export result";
            // 
            // sMinText
            // 
            this.sMinText.Location = new System.Drawing.Point(189, 24);
            this.sMinText.Name = "sMinText";
            this.sMinText.Size = new System.Drawing.Size(56, 20);
            this.sMinText.TabIndex = 11;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(142, 27);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(41, 13);
            this.label10.TabIndex = 9;
            this.label10.Text = "from s=";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(251, 27);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(16, 13);
            this.label11.TabIndex = 10;
            this.label11.Text = "to";
            // 
            // exportButton
            // 
            this.exportButton.Enabled = false;
            this.exportButton.Location = new System.Drawing.Point(6, 22);
            this.exportButton.Name = "exportButton";
            this.exportButton.Size = new System.Drawing.Size(130, 23);
            this.exportButton.TabIndex = 8;
            this.exportButton.Text = "Feed result to Exporter tab";
            this.exportButton.UseVisualStyleBackColor = true;
            this.exportButton.Click += new System.EventHandler(this.exportBXYButton_Click);
            // 
            // createDriftsCheckbox
            // 
            this.createDriftsCheckbox.AutoSize = true;
            this.createDriftsCheckbox.Location = new System.Drawing.Point(144, 50);
            this.createDriftsCheckbox.Name = "createDriftsCheckbox";
            this.createDriftsCheckbox.Size = new System.Drawing.Size(143, 17);
            this.createDriftsCheckbox.TabIndex = 13;
            this.createDriftsCheckbox.Text = "Auto-create missing drifts";
            this.createDriftsCheckbox.UseVisualStyleBackColor = true;
            // 
            // sMaxText
            // 
            this.sMaxText.Location = new System.Drawing.Point(273, 24);
            this.sMaxText.Name = "sMaxText";
            this.sMaxText.Size = new System.Drawing.Size(56, 20);
            this.sMaxText.TabIndex = 12;
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.groupBox2.Controls.Add(this.readSeqButton);
            this.groupBox2.Location = new System.Drawing.Point(9, 539);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(145, 72);
            this.groupBox2.TabIndex = 17;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Import sequence";
            // 
            // readSeqButton
            // 
            this.readSeqButton.Location = new System.Drawing.Point(6, 20);
            this.readSeqButton.Name = "readSeqButton";
            this.readSeqButton.Size = new System.Drawing.Size(130, 28);
            this.readSeqButton.TabIndex = 1;
            this.readSeqButton.Text = "Read SEQ file";
            this.readSeqButton.UseVisualStyleBackColor = true;
            this.readSeqButton.Click += new System.EventHandler(this.readSeqButton_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.groupBox1.Controls.Add(this.dispInsertDriftsCheckbox);
            this.groupBox1.Controls.Add(this.readDispButton);
            this.groupBox1.Location = new System.Drawing.Point(160, 539);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(197, 72);
            this.groupBox1.TabIndex = 16;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Import lattice functions";
            // 
            // dispInsertDriftsCheckbox
            // 
            this.dispInsertDriftsCheckbox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.dispInsertDriftsCheckbox.AutoSize = true;
            this.dispInsertDriftsCheckbox.Checked = true;
            this.dispInsertDriftsCheckbox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.dispInsertDriftsCheckbox.Location = new System.Drawing.Point(6, 49);
            this.dispInsertDriftsCheckbox.Name = "dispInsertDriftsCheckbox";
            this.dispInsertDriftsCheckbox.Size = new System.Drawing.Size(187, 17);
            this.dispInsertDriftsCheckbox.TabIndex = 14;
            this.dispInsertDriftsCheckbox.Text = "Read and insert markers and drifts";
            this.dispInsertDriftsCheckbox.UseVisualStyleBackColor = true;
            // 
            // readDispButton
            // 
            this.readDispButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.readDispButton.Enabled = false;
            this.readDispButton.Location = new System.Drawing.Point(6, 19);
            this.readDispButton.Name = "readDispButton";
            this.readDispButton.Size = new System.Drawing.Size(88, 28);
            this.readDispButton.TabIndex = 2;
            this.readDispButton.Text = "Read DISP file";
            this.readDispButton.UseVisualStyleBackColor = true;
            this.readDispButton.Click += new System.EventHandler(this.readDispButton_Click);
            // 
            // surveyLabel
            // 
            this.surveyLabel.AutoSize = true;
            this.surveyLabel.Location = new System.Drawing.Point(10, 366);
            this.surveyLabel.Name = "surveyLabel";
            this.surveyLabel.Size = new System.Drawing.Size(43, 13);
            this.surveyLabel.TabIndex = 7;
            this.surveyLabel.Text = "Survey:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(6, 185);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(46, 13);
            this.label9.TabIndex = 6;
            this.label9.Text = "Objects:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(6, 8);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(42, 13);
            this.label8.TabIndex = 5;
            this.label8.Text = "Values:";
            // 
            // seqSurveyDataView
            // 
            this.seqSurveyDataView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.seqSurveyDataView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.seqSurveyDataView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.instanceName,
            this.instanceObject,
            this.instanceType,
            this.instancePos,
            this.survLength,
            this.hasDispData});
            this.seqSurveyDataView.Location = new System.Drawing.Point(6, 382);
            this.seqSurveyDataView.Name = "seqSurveyDataView";
            this.seqSurveyDataView.Size = new System.Drawing.Size(833, 151);
            this.seqSurveyDataView.TabIndex = 4;
            // 
            // instanceName
            // 
            this.instanceName.HeaderText = "Name";
            this.instanceName.Name = "instanceName";
            this.instanceName.ReadOnly = true;
            this.instanceName.Width = 150;
            // 
            // instanceObject
            // 
            this.instanceObject.HeaderText = "Object";
            this.instanceObject.Name = "instanceObject";
            this.instanceObject.ReadOnly = true;
            this.instanceObject.Width = 150;
            // 
            // instanceType
            // 
            this.instanceType.HeaderText = "Type";
            this.instanceType.Name = "instanceType";
            this.instanceType.ReadOnly = true;
            this.instanceType.Width = 150;
            // 
            // instancePos
            // 
            this.instancePos.HeaderText = "S (start) [m]";
            this.instancePos.Name = "instancePos";
            this.instancePos.ReadOnly = true;
            this.instancePos.Width = 150;
            // 
            // survLength
            // 
            this.survLength.HeaderText = "Length [m]";
            this.survLength.Name = "survLength";
            this.survLength.ReadOnly = true;
            // 
            // hasDispData
            // 
            this.hasDispData.HeaderText = "Dispersion data";
            this.hasDispData.Name = "hasDispData";
            this.hasDispData.ReadOnly = true;
            // 
            // seqObjectsDataView
            // 
            this.seqObjectsDataView.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.seqObjectsDataView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.seqObjectsDataView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.name,
            this.type});
            this.seqObjectsDataView.Location = new System.Drawing.Point(6, 201);
            this.seqObjectsDataView.Name = "seqObjectsDataView";
            this.seqObjectsDataView.Size = new System.Drawing.Size(833, 159);
            this.seqObjectsDataView.TabIndex = 3;
            // 
            // name
            // 
            this.name.HeaderText = "Name";
            this.name.Name = "name";
            this.name.ReadOnly = true;
            // 
            // type
            // 
            this.type.HeaderText = "Type";
            this.type.Name = "type";
            this.type.ReadOnly = true;
            // 
            // seqValuesDataView
            // 
            this.seqValuesDataView.AllowUserToResizeRows = false;
            this.seqValuesDataView.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.seqValuesDataView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.seqValuesDataView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.valueName,
            this.ValueValue});
            this.seqValuesDataView.Location = new System.Drawing.Point(6, 24);
            this.seqValuesDataView.Name = "seqValuesDataView";
            this.seqValuesDataView.Size = new System.Drawing.Size(833, 151);
            this.seqValuesDataView.TabIndex = 0;
            // 
            // valueName
            // 
            this.valueName.HeaderText = "Name";
            this.valueName.Name = "valueName";
            this.valueName.ReadOnly = true;
            this.valueName.Width = 300;
            // 
            // ValueValue
            // 
            this.ValueValue.HeaderText = "Value";
            this.ValueValue.Name = "ValueValue";
            this.ValueValue.ReadOnly = true;
            this.ValueValue.Width = 300;
            // 
            // inputTab
            // 
            this.inputTab.Controls.Add(this.paramsPanel);
            this.inputTab.Controls.Add(this.inputDataView);
            this.inputTab.Location = new System.Drawing.Point(4, 22);
            this.inputTab.Name = "inputTab";
            this.inputTab.Padding = new System.Windows.Forms.Padding(3);
            this.inputTab.Size = new System.Drawing.Size(906, 617);
            this.inputTab.TabIndex = 0;
            this.inputTab.Text = "Manual table input";
            this.inputTab.UseVisualStyleBackColor = true;
            // 
            // paramsPanel
            // 
            this.paramsPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.paramsPanel.BackColor = System.Drawing.Color.White;
            this.paramsPanel.Controls.Add(this.getResultButton);
            this.paramsPanel.Controls.Add(this.prg);
            this.paramsPanel.Controls.Add(this.beginsWithCheckbox);
            this.paramsPanel.Controls.Add(this.nameDefinesTypeCheckbox);
            this.paramsPanel.Controls.Add(this.analyzeButton);
            this.paramsPanel.Controls.Add(this.resetTableButton);
            this.paramsPanel.Controls.Add(this.pasteButton);
            this.paramsPanel.Controls.Add(this.button6);
            this.paramsPanel.Controls.Add(this.addLineButton);
            this.paramsPanel.Controls.Add(this.defineNameColButton);
            this.paramsPanel.Controls.Add(this.remLineButton);
            this.paramsPanel.Controls.Add(this.button5);
            this.paramsPanel.Controls.Add(this.defineTypeButton);
            this.paramsPanel.Controls.Add(this.button4);
            this.paramsPanel.Controls.Add(this.defineSButton);
            this.paramsPanel.Controls.Add(this.button3);
            this.paramsPanel.Controls.Add(this.sModeBox);
            this.paramsPanel.Controls.Add(this.button2);
            this.paramsPanel.Controls.Add(this.refLocX);
            this.paramsPanel.Controls.Add(this.button1);
            this.paramsPanel.Controls.Add(this.quadNameText);
            this.paramsPanel.Controls.Add(this.label6);
            this.paramsPanel.Controls.Add(this.dipoleNameText);
            this.paramsPanel.Controls.Add(this.driftNameText);
            this.paramsPanel.Controls.Add(this.label2);
            this.paramsPanel.Controls.Add(this.refDirZ);
            this.paramsPanel.Controls.Add(this.label3);
            this.paramsPanel.Controls.Add(this.refDirY);
            this.paramsPanel.Controls.Add(this.label4);
            this.paramsPanel.Controls.Add(this.label5);
            this.paramsPanel.Controls.Add(this.defineDipoleButton);
            this.paramsPanel.Controls.Add(this.refDirX);
            this.paramsPanel.Controls.Add(this.defineQuadButton);
            this.paramsPanel.Controls.Add(this.refLocZ);
            this.paramsPanel.Controls.Add(this.refLocY);
            this.paramsPanel.Controls.Add(this.defineReferenceRowButton);
            this.paramsPanel.Controls.Add(this.quadSameColumnCheckbox);
            this.paramsPanel.Controls.Add(this.defineBetaXButton);
            this.paramsPanel.Controls.Add(this.quadModeBox);
            this.paramsPanel.Controls.Add(this.defineBetaYButton);
            this.paramsPanel.Controls.Add(this.dipoleModeBox);
            this.paramsPanel.Controls.Add(this.defineEtaXButton);
            this.paramsPanel.Controls.Add(this.defineAlphaYButton);
            this.paramsPanel.Controls.Add(this.defineEtaPrimeButton);
            this.paramsPanel.Controls.Add(this.defineAlphaXButton);
            this.paramsPanel.Location = new System.Drawing.Point(10, 259);
            this.paramsPanel.Name = "paramsPanel";
            this.paramsPanel.Size = new System.Drawing.Size(876, 336);
            this.paramsPanel.TabIndex = 42;
            // 
            // getResultButton
            // 
            this.getResultButton.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.getResultButton.Location = new System.Drawing.Point(430, 277);
            this.getResultButton.Name = "getResultButton";
            this.getResultButton.Size = new System.Drawing.Size(438, 47);
            this.getResultButton.TabIndex = 45;
            this.getResultButton.Text = "Feed to exporter";
            this.getResultButton.UseVisualStyleBackColor = true;
            this.getResultButton.Click += new System.EventHandler(this.getResultButton_Click);
            // 
            // prg
            // 
            this.prg.Location = new System.Drawing.Point(431, 8);
            this.prg.Name = "prg";
            this.prg.Size = new System.Drawing.Size(247, 23);
            this.prg.TabIndex = 44;
            this.prg.Visible = false;
            // 
            // beginsWithCheckbox
            // 
            this.beginsWithCheckbox.AutoSize = true;
            this.beginsWithCheckbox.Location = new System.Drawing.Point(135, 77);
            this.beginsWithCheckbox.Name = "beginsWithCheckbox";
            this.beginsWithCheckbox.Size = new System.Drawing.Size(89, 17);
            this.beginsWithCheckbox.TabIndex = 43;
            this.beginsWithCheckbox.Text = "Begins with...";
            this.beginsWithCheckbox.UseVisualStyleBackColor = true;
            // 
            // nameDefinesTypeCheckbox
            // 
            this.nameDefinesTypeCheckbox.AutoSize = true;
            this.nameDefinesTypeCheckbox.Location = new System.Drawing.Point(9, 77);
            this.nameDefinesTypeCheckbox.Name = "nameDefinesTypeCheckbox";
            this.nameDefinesTypeCheckbox.Size = new System.Drawing.Size(106, 17);
            this.nameDefinesTypeCheckbox.TabIndex = 42;
            this.nameDefinesTypeCheckbox.Text = "Also defines type";
            this.nameDefinesTypeCheckbox.UseVisualStyleBackColor = true;
            this.nameDefinesTypeCheckbox.CheckedChanged += new System.EventHandler(this.nameDefinesTypeCheckbox_CheckedChanged);
            // 
            // analyzeButton
            // 
            this.analyzeButton.Location = new System.Drawing.Point(133, 220);
            this.analyzeButton.Name = "analyzeButton";
            this.analyzeButton.Size = new System.Drawing.Size(126, 20);
            this.analyzeButton.TabIndex = 41;
            this.analyzeButton.Text = "Apply and Analyze";
            this.analyzeButton.UseVisualStyleBackColor = true;
            this.analyzeButton.Click += new System.EventHandler(this.analyzeButton_Click);
            // 
            // resetTableButton
            // 
            this.resetTableButton.Location = new System.Drawing.Point(680, 8);
            this.resetTableButton.Name = "resetTableButton";
            this.resetTableButton.Size = new System.Drawing.Size(188, 23);
            this.resetTableButton.TabIndex = 4;
            this.resetTableButton.Text = "Reset table";
            this.resetTableButton.UseVisualStyleBackColor = true;
            this.resetTableButton.Click += new System.EventHandler(this.button1_Click);
            // 
            // pasteButton
            // 
            this.pasteButton.Location = new System.Drawing.Point(226, 8);
            this.pasteButton.Name = "pasteButton";
            this.pasteButton.Size = new System.Drawing.Size(199, 23);
            this.pasteButton.TabIndex = 3;
            this.pasteButton.Text = "Paste from clipboard";
            this.pasteButton.UseVisualStyleBackColor = true;
            this.pasteButton.Click += new System.EventHandler(this.pasteButton_Click);
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(835, 168);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(33, 23);
            this.button6.TabIndex = 40;
            this.button6.Text = "Clr";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // addLineButton
            // 
            this.addLineButton.Enabled = false;
            this.addLineButton.Location = new System.Drawing.Point(9, 8);
            this.addLineButton.Name = "addLineButton";
            this.addLineButton.Size = new System.Drawing.Size(95, 23);
            this.addLineButton.TabIndex = 1;
            this.addLineButton.Text = "Add row(s)";
            this.addLineButton.UseVisualStyleBackColor = true;
            this.addLineButton.Click += new System.EventHandler(this.addButton_Click);
            // 
            // defineNameColButton
            // 
            this.defineNameColButton.Location = new System.Drawing.Point(3, 48);
            this.defineNameColButton.Name = "defineNameColButton";
            this.defineNameColButton.Size = new System.Drawing.Size(124, 23);
            this.defineNameColButton.TabIndex = 5;
            this.defineNameColButton.Text = "Define Name column";
            this.defineNameColButton.UseVisualStyleBackColor = true;
            this.defineNameColButton.Click += new System.EventHandler(this.defineNameColButton_Click);
            // 
            // remLineButton
            // 
            this.remLineButton.Enabled = false;
            this.remLineButton.Location = new System.Drawing.Point(110, 8);
            this.remLineButton.Name = "remLineButton";
            this.remLineButton.Size = new System.Drawing.Size(110, 23);
            this.remLineButton.TabIndex = 2;
            this.remLineButton.Text = "Remove row(s)";
            this.remLineButton.UseVisualStyleBackColor = true;
            this.remLineButton.Click += new System.EventHandler(this.remButton_Click);
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(835, 144);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(33, 23);
            this.button5.TabIndex = 39;
            this.button5.Text = "Clr";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click_1);
            // 
            // defineTypeButton
            // 
            this.defineTypeButton.Location = new System.Drawing.Point(133, 48);
            this.defineTypeButton.Name = "defineTypeButton";
            this.defineTypeButton.Size = new System.Drawing.Size(124, 23);
            this.defineTypeButton.TabIndex = 6;
            this.defineTypeButton.Text = "Define Type column";
            this.defineTypeButton.UseVisualStyleBackColor = true;
            this.defineTypeButton.Click += new System.EventHandler(this.defineTypeButton_Click);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(835, 120);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(33, 23);
            this.button4.TabIndex = 38;
            this.button4.Text = "Clr";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // defineSButton
            // 
            this.defineSButton.Location = new System.Drawing.Point(275, 48);
            this.defineSButton.Name = "defineSButton";
            this.defineSButton.Size = new System.Drawing.Size(124, 23);
            this.defineSButton.TabIndex = 7;
            this.defineSButton.Text = "Define S column";
            this.defineSButton.UseVisualStyleBackColor = true;
            this.defineSButton.Click += new System.EventHandler(this.defineSButton_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(835, 96);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(33, 23);
            this.button3.TabIndex = 37;
            this.button3.Text = "Clr";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // sModeBox
            // 
            this.sModeBox.Controls.Add(this.sType1);
            this.sModeBox.Controls.Add(this.sType0);
            this.sModeBox.Location = new System.Drawing.Point(275, 77);
            this.sModeBox.Name = "sModeBox";
            this.sModeBox.Size = new System.Drawing.Size(124, 67);
            this.sModeBox.TabIndex = 9;
            this.sModeBox.TabStop = false;
            this.sModeBox.Text = "S definiton mode";
            // 
            // sType1
            // 
            this.sType1.AutoSize = true;
            this.sType1.Location = new System.Drawing.Point(7, 42);
            this.sType1.Name = "sType1";
            this.sType1.Size = new System.Drawing.Size(84, 17);
            this.sType1.TabIndex = 9;
            this.sType1.Text = "Element end";
            this.sType1.UseVisualStyleBackColor = true;
            // 
            // sType0
            // 
            this.sType0.AutoSize = true;
            this.sType0.Checked = true;
            this.sType0.Location = new System.Drawing.Point(6, 19);
            this.sType0.Name = "sType0";
            this.sType0.Size = new System.Drawing.Size(86, 17);
            this.sType0.TabIndex = 8;
            this.sType0.TabStop = true;
            this.sType0.Text = "Element start";
            this.sType0.UseVisualStyleBackColor = true;
            this.sType0.CheckedChanged += new System.EventHandler(this.sType0_CheckedChanged);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(835, 72);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(33, 23);
            this.button2.TabIndex = 36;
            this.button2.Text = "Clr";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click_1);
            // 
            // refLocX
            // 
            this.refLocX.Location = new System.Drawing.Point(275, 278);
            this.refLocX.Name = "refLocX";
            this.refLocX.Size = new System.Drawing.Size(46, 20);
            this.refLocX.TabIndex = 10;
            this.refLocX.Text = "0";
            this.refLocX.TextChanged += new System.EventHandler(this.textBox2_TextChanged);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(835, 48);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(33, 23);
            this.button1.TabIndex = 35;
            this.button1.Text = "Clr";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // quadNameText
            // 
            this.quadNameText.Location = new System.Drawing.Point(135, 194);
            this.quadNameText.Name = "quadNameText";
            this.quadNameText.Size = new System.Drawing.Size(124, 20);
            this.quadNameText.TabIndex = 11;
            this.quadNameText.Text = "QUAD,QUADRUPOLE";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(132, 102);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(69, 13);
            this.label6.TabIndex = 34;
            this.label6.Text = "Drift name(s):";
            // 
            // dipoleNameText
            // 
            this.dipoleNameText.Location = new System.Drawing.Point(135, 155);
            this.dipoleNameText.Name = "dipoleNameText";
            this.dipoleNameText.Size = new System.Drawing.Size(124, 20);
            this.dipoleNameText.TabIndex = 12;
            this.dipoleNameText.Text = "LBEND,RBEND";
            this.dipoleNameText.TextChanged += new System.EventHandler(this.dipoleNameText_TextChanged);
            // 
            // driftNameText
            // 
            this.driftNameText.Location = new System.Drawing.Point(135, 117);
            this.driftNameText.Name = "driftNameText";
            this.driftNameText.Size = new System.Drawing.Size(124, 20);
            this.driftNameText.TabIndex = 33;
            this.driftNameText.Text = "DRIFT,SEXTUPOLE";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(143, 283);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(123, 13);
            this.label2.TabIndex = 13;
            this.label2.Text = "Ref. location (X,Y,Z) [m]:";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // refDirZ
            // 
            this.refDirZ.Location = new System.Drawing.Point(379, 304);
            this.refDirZ.Name = "refDirZ";
            this.refDirZ.Size = new System.Drawing.Size(45, 20);
            this.refDirZ.TabIndex = 32;
            this.refDirZ.Text = "1";
            this.refDirZ.TextChanged += new System.EventHandler(this.textBox7_TextChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(132, 140);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(80, 13);
            this.label3.TabIndex = 14;
            this.label3.Text = "Dipole name(s):";
            this.label3.Click += new System.EventHandler(this.label3_Click);
            // 
            // refDirY
            // 
            this.refDirY.Location = new System.Drawing.Point(327, 304);
            this.refDirY.Name = "refDirY";
            this.refDirY.Size = new System.Drawing.Size(46, 20);
            this.refDirY.TabIndex = 31;
            this.refDirY.Text = "0";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(132, 178);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(105, 13);
            this.label4.TabIndex = 15;
            this.label4.Text = "Quadrupole name(s):";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(143, 307);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(126, 13);
            this.label5.TabIndex = 30;
            this.label5.Text = "Ref. direction (X,Y,Z) [m]:";
            // 
            // defineDipoleButton
            // 
            this.defineDipoleButton.Location = new System.Drawing.Point(405, 48);
            this.defineDipoleButton.Name = "defineDipoleButton";
            this.defineDipoleButton.Size = new System.Drawing.Size(126, 42);
            this.defineDipoleButton.TabIndex = 16;
            this.defineDipoleButton.Text = "Define dipole strength column";
            this.defineDipoleButton.UseVisualStyleBackColor = true;
            this.defineDipoleButton.Click += new System.EventHandler(this.defineDipoleButton_Click);
            // 
            // refDirX
            // 
            this.refDirX.Location = new System.Drawing.Point(275, 304);
            this.refDirX.Name = "refDirX";
            this.refDirX.Size = new System.Drawing.Size(46, 20);
            this.refDirX.TabIndex = 29;
            this.refDirX.Text = "0";
            // 
            // defineQuadButton
            // 
            this.defineQuadButton.Location = new System.Drawing.Point(537, 48);
            this.defineQuadButton.Name = "defineQuadButton";
            this.defineQuadButton.Size = new System.Drawing.Size(126, 42);
            this.defineQuadButton.TabIndex = 17;
            this.defineQuadButton.Text = "Define quadrupole strength column";
            this.defineQuadButton.UseVisualStyleBackColor = true;
            this.defineQuadButton.Click += new System.EventHandler(this.button2_Click);
            // 
            // refLocZ
            // 
            this.refLocZ.Location = new System.Drawing.Point(378, 278);
            this.refLocZ.Name = "refLocZ";
            this.refLocZ.Size = new System.Drawing.Size(46, 20);
            this.refLocZ.TabIndex = 28;
            this.refLocZ.Text = "0";
            this.refLocZ.TextChanged += new System.EventHandler(this.textBox6_TextChanged);
            // 
            // refLocY
            // 
            this.refLocY.Location = new System.Drawing.Point(327, 278);
            this.refLocY.Name = "refLocY";
            this.refLocY.Size = new System.Drawing.Size(46, 20);
            this.refLocY.TabIndex = 27;
            this.refLocY.Text = "0";
            // 
            // defineReferenceRowButton
            // 
            this.defineReferenceRowButton.Location = new System.Drawing.Point(9, 277);
            this.defineReferenceRowButton.Name = "defineReferenceRowButton";
            this.defineReferenceRowButton.Size = new System.Drawing.Size(126, 47);
            this.defineReferenceRowButton.TabIndex = 19;
            this.defineReferenceRowButton.Text = "Define reference row";
            this.defineReferenceRowButton.UseVisualStyleBackColor = true;
            this.defineReferenceRowButton.Click += new System.EventHandler(this.defineReferenceRowButton_Click);
            // 
            // quadSameColumnCheckbox
            // 
            this.quadSameColumnCheckbox.AutoSize = true;
            this.quadSameColumnCheckbox.Location = new System.Drawing.Point(539, 97);
            this.quadSameColumnCheckbox.Name = "quadSameColumnCheckbox";
            this.quadSameColumnCheckbox.Size = new System.Drawing.Size(135, 17);
            this.quadSameColumnCheckbox.TabIndex = 26;
            this.quadSameColumnCheckbox.Text = "Same column as dipole";
            this.quadSameColumnCheckbox.UseVisualStyleBackColor = true;
            this.quadSameColumnCheckbox.CheckedChanged += new System.EventHandler(this.quadSameColumnCheckbox_CheckedChanged);
            // 
            // defineBetaXButton
            // 
            this.defineBetaXButton.Location = new System.Drawing.Point(680, 48);
            this.defineBetaXButton.Name = "defineBetaXButton";
            this.defineBetaXButton.Size = new System.Drawing.Size(152, 23);
            this.defineBetaXButton.TabIndex = 20;
            this.defineBetaXButton.Text = "Define BetaX column";
            this.defineBetaXButton.UseVisualStyleBackColor = true;
            this.defineBetaXButton.Click += new System.EventHandler(this.button5_Click);
            // 
            // quadModeBox
            // 
            this.quadModeBox.Controls.Add(this.quadGradientModeRadio);
            this.quadModeBox.Controls.Add(this.quadK1LmodeRadio);
            this.quadModeBox.Location = new System.Drawing.Point(539, 120);
            this.quadModeBox.Name = "quadModeBox";
            this.quadModeBox.Size = new System.Drawing.Size(124, 67);
            this.quadModeBox.TabIndex = 11;
            this.quadModeBox.TabStop = false;
            this.quadModeBox.Text = "Format";
            this.quadModeBox.Enter += new System.EventHandler(this.groupBox2_Enter);
            // 
            // quadGradientModeRadio
            // 
            this.quadGradientModeRadio.AutoSize = true;
            this.quadGradientModeRadio.Location = new System.Drawing.Point(7, 42);
            this.quadGradientModeRadio.Name = "quadGradientModeRadio";
            this.quadGradientModeRadio.Size = new System.Drawing.Size(94, 17);
            this.quadGradientModeRadio.TabIndex = 9;
            this.quadGradientModeRadio.Text = "Gradient [T/m]";
            this.quadGradientModeRadio.UseVisualStyleBackColor = true;
            // 
            // quadK1LmodeRadio
            // 
            this.quadK1LmodeRadio.AutoSize = true;
            this.quadK1LmodeRadio.Checked = true;
            this.quadK1LmodeRadio.Location = new System.Drawing.Point(6, 19);
            this.quadK1LmodeRadio.Name = "quadK1LmodeRadio";
            this.quadK1LmodeRadio.Size = new System.Drawing.Size(44, 17);
            this.quadK1LmodeRadio.TabIndex = 8;
            this.quadK1LmodeRadio.TabStop = true;
            this.quadK1LmodeRadio.Text = "K1L";
            this.quadK1LmodeRadio.UseVisualStyleBackColor = true;
            // 
            // defineBetaYButton
            // 
            this.defineBetaYButton.Location = new System.Drawing.Point(680, 72);
            this.defineBetaYButton.Name = "defineBetaYButton";
            this.defineBetaYButton.Size = new System.Drawing.Size(152, 23);
            this.defineBetaYButton.TabIndex = 21;
            this.defineBetaYButton.Text = "Define BetaY column";
            this.defineBetaYButton.UseVisualStyleBackColor = true;
            this.defineBetaYButton.Click += new System.EventHandler(this.defineBetaYButton_Click);
            // 
            // dipoleModeBox
            // 
            this.dipoleModeBox.Controls.Add(this.beamEnergyTextbox);
            this.dipoleModeBox.Controls.Add(this.label7);
            this.dipoleModeBox.Controls.Add(this.dipoleStrecgthModeRadio);
            this.dipoleModeBox.Controls.Add(this.dipoleAngleModeRadio);
            this.dipoleModeBox.Location = new System.Drawing.Point(407, 96);
            this.dipoleModeBox.Name = "dipoleModeBox";
            this.dipoleModeBox.Size = new System.Drawing.Size(124, 118);
            this.dipoleModeBox.TabIndex = 10;
            this.dipoleModeBox.TabStop = false;
            this.dipoleModeBox.Text = "Format";
            // 
            // beamEnergyTextbox
            // 
            this.beamEnergyTextbox.Location = new System.Drawing.Point(9, 84);
            this.beamEnergyTextbox.Name = "beamEnergyTextbox";
            this.beamEnergyTextbox.Size = new System.Drawing.Size(99, 20);
            this.beamEnergyTextbox.TabIndex = 46;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 68);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(102, 13);
            this.label7.TabIndex = 45;
            this.label7.Text = "Beam energy [GeV]:";
            // 
            // dipoleStrecgthModeRadio
            // 
            this.dipoleStrecgthModeRadio.AutoSize = true;
            this.dipoleStrecgthModeRadio.Location = new System.Drawing.Point(7, 42);
            this.dipoleStrecgthModeRadio.Name = "dipoleStrecgthModeRadio";
            this.dipoleStrecgthModeRadio.Size = new System.Drawing.Size(81, 17);
            this.dipoleStrecgthModeRadio.TabIndex = 9;
            this.dipoleStrecgthModeRadio.Text = "Strength [T]";
            this.dipoleStrecgthModeRadio.UseVisualStyleBackColor = true;
            // 
            // dipoleAngleModeRadio
            // 
            this.dipoleAngleModeRadio.AutoSize = true;
            this.dipoleAngleModeRadio.Checked = true;
            this.dipoleAngleModeRadio.Location = new System.Drawing.Point(6, 19);
            this.dipoleAngleModeRadio.Name = "dipoleAngleModeRadio";
            this.dipoleAngleModeRadio.Size = new System.Drawing.Size(76, 17);
            this.dipoleAngleModeRadio.TabIndex = 8;
            this.dipoleAngleModeRadio.TabStop = true;
            this.dipoleAngleModeRadio.Text = "Angle [rad]";
            this.dipoleAngleModeRadio.UseVisualStyleBackColor = true;
            // 
            // defineEtaXButton
            // 
            this.defineEtaXButton.Location = new System.Drawing.Point(680, 96);
            this.defineEtaXButton.Name = "defineEtaXButton";
            this.defineEtaXButton.Size = new System.Drawing.Size(152, 23);
            this.defineEtaXButton.TabIndex = 22;
            this.defineEtaXButton.Text = "Define EtaX column";
            this.defineEtaXButton.UseVisualStyleBackColor = true;
            this.defineEtaXButton.Click += new System.EventHandler(this.defineEtaXButton_Click);
            // 
            // defineAlphaYButton
            // 
            this.defineAlphaYButton.Location = new System.Drawing.Point(680, 168);
            this.defineAlphaYButton.Name = "defineAlphaYButton";
            this.defineAlphaYButton.Size = new System.Drawing.Size(152, 23);
            this.defineAlphaYButton.TabIndex = 25;
            this.defineAlphaYButton.Text = "Define alphaY column";
            this.defineAlphaYButton.UseVisualStyleBackColor = true;
            this.defineAlphaYButton.Click += new System.EventHandler(this.defineAlphaYButton_Click);
            // 
            // defineEtaPrimeButton
            // 
            this.defineEtaPrimeButton.Location = new System.Drawing.Point(680, 120);
            this.defineEtaPrimeButton.Name = "defineEtaPrimeButton";
            this.defineEtaPrimeButton.Size = new System.Drawing.Size(152, 23);
            this.defineEtaPrimeButton.TabIndex = 23;
            this.defineEtaPrimeButton.Text = "Define EtaPrimeX column";
            this.defineEtaPrimeButton.UseVisualStyleBackColor = true;
            this.defineEtaPrimeButton.Click += new System.EventHandler(this.defineEtaPrimeButton_Click);
            // 
            // defineAlphaXButton
            // 
            this.defineAlphaXButton.Location = new System.Drawing.Point(680, 144);
            this.defineAlphaXButton.Name = "defineAlphaXButton";
            this.defineAlphaXButton.Size = new System.Drawing.Size(152, 23);
            this.defineAlphaXButton.TabIndex = 24;
            this.defineAlphaXButton.Text = "Define alphaX column";
            this.defineAlphaXButton.UseVisualStyleBackColor = true;
            this.defineAlphaXButton.Click += new System.EventHandler(this.button9_Click);
            // 
            // inputDataView
            // 
            this.inputDataView.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.NullValue = null;
            this.inputDataView.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.inputDataView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.inputDataView.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.inputDataView.CausesValidation = false;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.inputDataView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.inputDataView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.inputDataView.DefaultCellStyle = dataGridViewCellStyle3;
            this.inputDataView.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.inputDataView.EnableHeadersVisualStyles = false;
            this.inputDataView.Location = new System.Drawing.Point(10, 7);
            this.inputDataView.Name = "inputDataView";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.inputDataView.RowHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.inputDataView.Size = new System.Drawing.Size(876, 246);
            this.inputDataView.TabIndex = 0;
            this.inputDataView.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.inputDataView_CellContentClick);
            this.inputDataView.SelectionChanged += new System.EventHandler(this.songsDataGridView_SelectionChanged);
            // 
            // dbTab
            // 
            this.dbTab.Controls.Add(this.queryView);
            this.dbTab.Controls.Add(this.machineCodeCombo);
            this.dbTab.Controls.Add(this.connResultText);
            this.dbTab.Controls.Add(this.label29);
            this.dbTab.Controls.Add(this.queryResultText);
            this.dbTab.Controls.Add(this.dbTableText);
            this.dbTab.Controls.Add(this.label27);
            this.dbTab.Controls.Add(this.button8);
            this.dbTab.Controls.Add(this.passwordText);
            this.dbTab.Controls.Add(this.usernameText);
            this.dbTab.Controls.Add(this.serverText);
            this.dbTab.Controls.Add(this.label26);
            this.dbTab.Controls.Add(this.label25);
            this.dbTab.Controls.Add(this.label24);
            this.dbTab.Controls.Add(this.queryButton);
            this.dbTab.Location = new System.Drawing.Point(4, 22);
            this.dbTab.Name = "dbTab";
            this.dbTab.Size = new System.Drawing.Size(906, 617);
            this.dbTab.TabIndex = 3;
            this.dbTab.Text = "From LHC layout database";
            this.dbTab.UseVisualStyleBackColor = true;
            // 
            // queryView
            // 
            this.queryView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.queryView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.queryView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.queryName,
            this.querySstart,
            this.querySend,
            this.queryShape,
            this.queryDiamU,
            this.queryDiamV,
            this.queryEllipseA,
            this.queryEllipseB,
            this.queryEllipseC,
            this.queryEllipseD,
            this.queryNote});
            this.queryView.Location = new System.Drawing.Point(15, 184);
            this.queryView.Name = "queryView";
            this.queryView.Size = new System.Drawing.Size(883, 376);
            this.queryView.TabIndex = 16;
            // 
            // queryName
            // 
            this.queryName.HeaderText = "Name";
            this.queryName.Name = "queryName";
            this.queryName.ReadOnly = true;
            // 
            // querySstart
            // 
            this.querySstart.HeaderText = "S start (m)";
            this.querySstart.Name = "querySstart";
            this.querySstart.ReadOnly = true;
            // 
            // querySend
            // 
            this.querySend.HeaderText = "S end (m)";
            this.querySend.Name = "querySend";
            this.querySend.ReadOnly = true;
            // 
            // queryShape
            // 
            this.queryShape.HeaderText = "Shape";
            this.queryShape.Name = "queryShape";
            this.queryShape.ReadOnly = true;
            // 
            // queryDiamU
            // 
            this.queryDiamU.HeaderText = "Inner Diameter U";
            this.queryDiamU.Name = "queryDiamU";
            this.queryDiamU.ReadOnly = true;
            // 
            // queryDiamV
            // 
            this.queryDiamV.HeaderText = "Inner Diameter V";
            this.queryDiamV.Name = "queryDiamV";
            this.queryDiamV.ReadOnly = true;
            // 
            // queryEllipseA
            // 
            this.queryEllipseA.HeaderText = "Ellipse Param A";
            this.queryEllipseA.Name = "queryEllipseA";
            this.queryEllipseA.ReadOnly = true;
            // 
            // queryEllipseB
            // 
            this.queryEllipseB.HeaderText = "Ellipse Param B";
            this.queryEllipseB.Name = "queryEllipseB";
            this.queryEllipseB.ReadOnly = true;
            // 
            // queryEllipseC
            // 
            this.queryEllipseC.HeaderText = "Ellipse Param C";
            this.queryEllipseC.Name = "queryEllipseC";
            this.queryEllipseC.ReadOnly = true;
            // 
            // queryEllipseD
            // 
            this.queryEllipseD.HeaderText = "Ellipse Param D";
            this.queryEllipseD.Name = "queryEllipseD";
            this.queryEllipseD.ReadOnly = true;
            // 
            // queryNote
            // 
            this.queryNote.HeaderText = "Note";
            this.queryNote.Name = "queryNote";
            this.queryNote.ReadOnly = true;
            this.queryNote.Width = 200;
            // 
            // machineCodeCombo
            // 
            this.machineCodeCombo.FormattingEnabled = true;
            this.machineCodeCombo.Location = new System.Drawing.Point(104, 139);
            this.machineCodeCombo.Name = "machineCodeCombo";
            this.machineCodeCombo.Size = new System.Drawing.Size(461, 21);
            this.machineCodeCombo.TabIndex = 15;
            // 
            // connResultText
            // 
            this.connResultText.AutoSize = true;
            this.connResultText.Location = new System.Drawing.Point(771, 45);
            this.connResultText.Name = "connResultText";
            this.connResultText.Size = new System.Drawing.Size(0, 13);
            this.connResultText.TabIndex = 14;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(12, 142);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(75, 13);
            this.label29.TabIndex = 12;
            this.label29.Text = "Machine code";
            // 
            // queryResultText
            // 
            this.queryResultText.AutoSize = true;
            this.queryResultText.Location = new System.Drawing.Point(771, 142);
            this.queryResultText.Name = "queryResultText";
            this.queryResultText.Size = new System.Drawing.Size(0, 13);
            this.queryResultText.TabIndex = 10;
            // 
            // dbTableText
            // 
            this.dbTableText.Location = new System.Drawing.Point(104, 110);
            this.dbTableText.Name = "dbTableText";
            this.dbTableText.Size = new System.Drawing.Size(461, 20);
            this.dbTableText.TabIndex = 9;
            this.dbTableText.Text = "layout_apertures_v";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(12, 113);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(56, 13);
            this.label27.TabIndex = 8;
            this.label27.Text = "Data table";
            // 
            // button8
            // 
            this.button8.Location = new System.Drawing.Point(571, 15);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(181, 72);
            this.button8.TabIndex = 7;
            this.button8.Text = "Connect";
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Click += new System.EventHandler(this.button8_Click);
            // 
            // passwordText
            // 
            this.passwordText.Location = new System.Drawing.Point(104, 67);
            this.passwordText.Name = "passwordText";
            this.passwordText.PasswordChar = '*';
            this.passwordText.Size = new System.Drawing.Size(461, 20);
            this.passwordText.TabIndex = 6;
            // 
            // usernameText
            // 
            this.usernameText.Location = new System.Drawing.Point(104, 41);
            this.usernameText.Name = "usernameText";
            this.usernameText.Size = new System.Drawing.Size(461, 20);
            this.usernameText.TabIndex = 5;
            this.usernameText.Text = "LAYOUT_APP_VAC_ANALYSIS";
            // 
            // serverText
            // 
            this.serverText.Location = new System.Drawing.Point(104, 15);
            this.serverText.Name = "serverText";
            this.serverText.Size = new System.Drawing.Size(461, 20);
            this.serverText.TabIndex = 4;
            this.serverText.Text = "pdb-s.cern.ch:10121/PDB_CERNDB1.cern.ch";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(12, 70);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(53, 13);
            this.label26.TabIndex = 3;
            this.label26.Text = "Password";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(12, 44);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(29, 13);
            this.label25.TabIndex = 2;
            this.label25.Text = "User";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(12, 18);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(77, 13);
            this.label24.TabIndex = 1;
            this.label24.Text = "Server/service";
            // 
            // queryButton
            // 
            this.queryButton.Enabled = false;
            this.queryButton.Location = new System.Drawing.Point(571, 110);
            this.queryButton.Name = "queryButton";
            this.queryButton.Size = new System.Drawing.Size(181, 50);
            this.queryButton.TabIndex = 0;
            this.queryButton.Text = "Query machine";
            this.queryButton.UseVisualStyleBackColor = true;
            this.queryButton.Click += new System.EventHandler(this.button7_Click);
            // 
            // exportTab
            // 
            this.exportTab.Controls.Add(this.taperCombo);
            this.exportTab.Controls.Add(this.label34);
            this.exportTab.Controls.Add(this.curveStepText);
            this.exportTab.Controls.Add(this.label33);
            this.exportTab.Controls.Add(this.readApertureButton);
            this.exportTab.Controls.Add(this.circleSidesText);
            this.exportTab.Controls.Add(this.label32);
            this.exportTab.Controls.Add(this.writeGeomButton);
            this.exportTab.Controls.Add(this.browseGeomButton);
            this.exportTab.Controls.Add(this.geomFileTextBox);
            this.exportTab.Controls.Add(this.label31);
            this.exportTab.Controls.Add(this.label30);
            this.exportTab.Controls.Add(this.searchResultLabel);
            this.exportTab.Controls.Add(this.searchEndButton);
            this.exportTab.Controls.Add(this.searchBeginningButton);
            this.exportTab.Controls.Add(this.searchTermTextbox);
            this.exportTab.Controls.Add(this.label28);
            this.exportTab.Controls.Add(this.suffixCheckbox);
            this.exportTab.Controls.Add(this.readTwissButton);
            this.exportTab.Controls.Add(this.writeBXYfileButton);
            this.exportTab.Controls.Add(this.getEndLineButton);
            this.exportTab.Controls.Add(this.getStartLineButton);
            this.exportTab.Controls.Add(this.endLineText);
            this.exportTab.Controls.Add(this.startLineText);
            this.exportTab.Controls.Add(this.label13);
            this.exportTab.Controls.Add(this.startLineLabel);
            this.exportTab.Controls.Add(this.bAngleButton);
            this.exportTab.Controls.Add(this.prg_new);
            this.exportTab.Controls.Add(this.resetTableButton_new);
            this.exportTab.Controls.Add(this.pasteFromClipboardButton_new);
            this.exportTab.Controls.Add(this.addLineButton_new);
            this.exportTab.Controls.Add(this.remLineButton_new);
            this.exportTab.Controls.Add(this.refThetaText);
            this.exportTab.Controls.Add(this.label12);
            this.exportTab.Controls.Add(this.refZText);
            this.exportTab.Controls.Add(this.label1);
            this.exportTab.Controls.Add(this.refXText);
            this.exportTab.Controls.Add(this.label14);
            this.exportTab.Controls.Add(this.getRefRowButton);
            this.exportTab.Controls.Add(this.refRowTextBox);
            this.exportTab.Controls.Add(this.label15);
            this.exportTab.Controls.Add(this.prefixCheckbox);
            this.exportTab.Controls.Add(this.eMaxText);
            this.exportTab.Controls.Add(this.label16);
            this.exportTab.Controls.Add(this.eMinText);
            this.exportTab.Controls.Add(this.eMinLabel);
            this.exportTab.Controls.Add(this.dLtext);
            this.exportTab.Controls.Add(this.label17);
            this.exportTab.Controls.Add(this.energySpreadText);
            this.exportTab.Controls.Add(this.label18);
            this.exportTab.Controls.Add(this.beamEText);
            this.exportTab.Controls.Add(this.label19);
            this.exportTab.Controls.Add(this.analyzedView);
            this.exportTab.Controls.Add(this.beamELabel);
            this.exportTab.Controls.Add(this.pMassText);
            this.exportTab.Controls.Add(this.browseBXYbutton);
            this.exportTab.Controls.Add(this.bxyFileNameText);
            this.exportTab.Controls.Add(this.label20);
            this.exportTab.Controls.Add(this.currentText);
            this.exportTab.Controls.Add(this.label21);
            this.exportTab.Controls.Add(this.emittYtext);
            this.exportTab.Controls.Add(this.label22);
            this.exportTab.Controls.Add(this.emittXtext);
            this.exportTab.Controls.Add(this.label23);
            this.exportTab.Controls.Add(this.writeFilesButton);
            this.exportTab.Controls.Add(this.dirTextBox);
            this.exportTab.Controls.Add(this.browseButton);
            this.exportTab.Location = new System.Drawing.Point(4, 22);
            this.exportTab.Name = "exportTab";
            this.exportTab.Padding = new System.Windows.Forms.Padding(3);
            this.exportTab.Size = new System.Drawing.Size(906, 617);
            this.exportTab.TabIndex = 2;
            this.exportTab.Text = "Export result to Synrad format";
            this.exportTab.UseVisualStyleBackColor = true;
            // 
            // taperCombo
            // 
            this.taperCombo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.taperCombo.FormattingEnabled = true;
            this.taperCombo.Items.AddRange(new object[] {
            "Taper apt chg",
            "Step apt chg"});
            this.taperCombo.Location = new System.Drawing.Point(677, 593);
            this.taperCombo.Name = "taperCombo";
            this.taperCombo.Size = new System.Drawing.Size(108, 21);
            this.taperCombo.TabIndex = 128;
            this.taperCombo.Text = "Taper apt chg";
            // 
            // label34
            // 
            this.label34.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(630, 597);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(41, 13);
            this.label34.TabIndex = 127;
            this.label34.Text = "radians";
            // 
            // curveStepText
            // 
            this.curveStepText.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.curveStepText.Location = new System.Drawing.Point(569, 594);
            this.curveStepText.Name = "curveStepText";
            this.curveStepText.Size = new System.Drawing.Size(55, 20);
            this.curveStepText.TabIndex = 126;
            this.curveStepText.Text = "0.001";
            // 
            // label33
            // 
            this.label33.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(477, 597);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(86, 13);
            this.label33.TabIndex = 125;
            this.label33.Text = "curve step every";
            // 
            // readApertureButton
            // 
            this.readApertureButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.readApertureButton.Location = new System.Drawing.Point(542, 348);
            this.readApertureButton.Name = "readApertureButton";
            this.readApertureButton.Size = new System.Drawing.Size(98, 23);
            this.readApertureButton.TabIndex = 124;
            this.readApertureButton.Text = "Read aperture";
            this.readApertureButton.UseVisualStyleBackColor = true;
            this.readApertureButton.Click += new System.EventHandler(this.readApertureButton_Click);
            // 
            // circleSidesText
            // 
            this.circleSidesText.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.circleSidesText.Location = new System.Drawing.Point(425, 594);
            this.circleSidesText.Name = "circleSidesText";
            this.circleSidesText.Size = new System.Drawing.Size(37, 20);
            this.circleSidesText.TabIndex = 123;
            this.circleSidesText.Text = "36";
            // 
            // label32
            // 
            this.label32.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(356, 597);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(63, 13);
            this.label32.TabIndex = 122;
            this.label32.Text = "Circle sides:";
            // 
            // writeGeomButton
            // 
            this.writeGeomButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.writeGeomButton.Location = new System.Drawing.Point(787, 592);
            this.writeGeomButton.Name = "writeGeomButton";
            this.writeGeomButton.Size = new System.Drawing.Size(103, 23);
            this.writeGeomButton.TabIndex = 121;
            this.writeGeomButton.Text = "write geom.";
            this.writeGeomButton.UseVisualStyleBackColor = true;
            this.writeGeomButton.Click += new System.EventHandler(this.writeGeomButton_Click);
            // 
            // browseGeomButton
            // 
            this.browseGeomButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.browseGeomButton.Location = new System.Drawing.Point(315, 592);
            this.browseGeomButton.Name = "browseGeomButton";
            this.browseGeomButton.Size = new System.Drawing.Size(32, 23);
            this.browseGeomButton.TabIndex = 120;
            this.browseGeomButton.Text = "...";
            this.browseGeomButton.UseVisualStyleBackColor = true;
            this.browseGeomButton.Click += new System.EventHandler(this.browseGeomButton_Click);
            // 
            // geomFileTextBox
            // 
            this.geomFileTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.geomFileTextBox.Location = new System.Drawing.Point(102, 594);
            this.geomFileTextBox.Name = "geomFileTextBox";
            this.geomFileTextBox.Size = new System.Drawing.Size(207, 20);
            this.geomFileTextBox.TabIndex = 119;
            this.geomFileTextBox.Text = "Geometry.txt";
            // 
            // label31
            // 
            this.label31.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(14, 597);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(81, 13);
            this.label31.TabIndex = 118;
            this.label31.Text = "geom. filename:";
            // 
            // label30
            // 
            this.label30.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(14, 545);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(56, 13);
            this.label30.TabIndex = 117;
            this.label30.Text = "Output dir:";
            // 
            // searchResultLabel
            // 
            this.searchResultLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.searchResultLabel.AutoSize = true;
            this.searchResultLabel.Location = new System.Drawing.Point(546, 406);
            this.searchResultLabel.Name = "searchResultLabel";
            this.searchResultLabel.Size = new System.Drawing.Size(0, 13);
            this.searchResultLabel.TabIndex = 116;
            // 
            // searchEndButton
            // 
            this.searchEndButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.searchEndButton.Location = new System.Drawing.Point(425, 401);
            this.searchEndButton.Name = "searchEndButton";
            this.searchEndButton.Size = new System.Drawing.Size(115, 23);
            this.searchEndButton.TabIndex = 115;
            this.searchEndButton.Text = "Search end";
            this.searchEndButton.UseVisualStyleBackColor = true;
            this.searchEndButton.Click += new System.EventHandler(this.searchEndButton_Click);
            // 
            // searchBeginningButton
            // 
            this.searchBeginningButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.searchBeginningButton.Location = new System.Drawing.Point(307, 401);
            this.searchBeginningButton.Name = "searchBeginningButton";
            this.searchBeginningButton.Size = new System.Drawing.Size(112, 23);
            this.searchBeginningButton.TabIndex = 114;
            this.searchBeginningButton.Text = "Search beginning";
            this.searchBeginningButton.UseVisualStyleBackColor = true;
            this.searchBeginningButton.Click += new System.EventHandler(this.searchBeginningButton_Click);
            // 
            // searchTermTextbox
            // 
            this.searchTermTextbox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.searchTermTextbox.Location = new System.Drawing.Point(82, 403);
            this.searchTermTextbox.Name = "searchTermTextbox";
            this.searchTermTextbox.Size = new System.Drawing.Size(219, 20);
            this.searchTermTextbox.TabIndex = 113;
            this.searchTermTextbox.TextChanged += new System.EventHandler(this.searchTermTextbox_TextChanged);
            // 
            // label28
            // 
            this.label28.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(12, 406);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(64, 13);
            this.label28.TabIndex = 112;
            this.label28.Text = "Search text:";
            // 
            // suffixCheckbox
            // 
            this.suffixCheckbox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.suffixCheckbox.Location = new System.Drawing.Point(680, 544);
            this.suffixCheckbox.Name = "suffixCheckbox";
            this.suffixCheckbox.Size = new System.Drawing.Size(105, 17);
            this.suffixCheckbox.TabIndex = 111;
            this.suffixCheckbox.Text = "s position suffix";
            this.suffixCheckbox.UseVisualStyleBackColor = true;
            this.suffixCheckbox.CheckedChanged += new System.EventHandler(this.suffixCheckbox_CheckedChanged);
            // 
            // readTwissButton
            // 
            this.readTwissButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.readTwissButton.Location = new System.Drawing.Point(425, 348);
            this.readTwissButton.Name = "readTwissButton";
            this.readTwissButton.Size = new System.Drawing.Size(115, 23);
            this.readTwissButton.TabIndex = 110;
            this.readTwissButton.Text = "Read TWISS file";
            this.readTwissButton.UseVisualStyleBackColor = true;
            this.readTwissButton.Click += new System.EventHandler(this.readTwissButton_Click);
            // 
            // writeBXYfileButton
            // 
            this.writeBXYfileButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.writeBXYfileButton.Location = new System.Drawing.Point(787, 566);
            this.writeBXYfileButton.Name = "writeBXYfileButton";
            this.writeBXYfileButton.Size = new System.Drawing.Size(103, 23);
            this.writeBXYfileButton.TabIndex = 109;
            this.writeBXYfileButton.Text = "write BXY file";
            this.writeBXYfileButton.UseVisualStyleBackColor = true;
            this.writeBXYfileButton.Click += new System.EventHandler(this.writeBXYfileButton_Click);
            // 
            // getEndLineButton
            // 
            this.getEndLineButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.getEndLineButton.Location = new System.Drawing.Point(787, 509);
            this.getEndLineButton.Name = "getEndLineButton";
            this.getEndLineButton.Size = new System.Drawing.Size(103, 21);
            this.getEndLineButton.TabIndex = 108;
            this.getEndLineButton.Text = "Get Selected";
            this.getEndLineButton.UseVisualStyleBackColor = true;
            this.getEndLineButton.Click += new System.EventHandler(this.getEndLineButton_Click);
            // 
            // getStartLineButton
            // 
            this.getStartLineButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.getStartLineButton.Location = new System.Drawing.Point(787, 485);
            this.getStartLineButton.Name = "getStartLineButton";
            this.getStartLineButton.Size = new System.Drawing.Size(103, 21);
            this.getStartLineButton.TabIndex = 107;
            this.getStartLineButton.Text = "Get Selected";
            this.getStartLineButton.UseVisualStyleBackColor = true;
            this.getStartLineButton.Click += new System.EventHandler(this.getStartLineButton_Click);
            // 
            // endLineText
            // 
            this.endLineText.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.endLineText.Location = new System.Drawing.Point(732, 509);
            this.endLineText.Name = "endLineText";
            this.endLineText.Size = new System.Drawing.Size(37, 20);
            this.endLineText.TabIndex = 106;
            // 
            // startLineText
            // 
            this.startLineText.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.startLineText.Location = new System.Drawing.Point(732, 486);
            this.startLineText.Name = "startLineText";
            this.startLineText.Size = new System.Drawing.Size(37, 20);
            this.startLineText.TabIndex = 105;
            // 
            // label13
            // 
            this.label13.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(677, 512);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(49, 13);
            this.label13.TabIndex = 104;
            this.label13.Text = "End row:";
            // 
            // startLineLabel
            // 
            this.startLineLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.startLineLabel.AutoSize = true;
            this.startLineLabel.Location = new System.Drawing.Point(677, 489);
            this.startLineLabel.Name = "startLineLabel";
            this.startLineLabel.Size = new System.Drawing.Size(52, 13);
            this.startLineLabel.TabIndex = 103;
            this.startLineLabel.Text = "Start row:";
            // 
            // bAngleButton
            // 
            this.bAngleButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.bAngleButton.Location = new System.Drawing.Point(787, 379);
            this.bAngleButton.Name = "bAngleButton";
            this.bAngleButton.Size = new System.Drawing.Size(103, 47);
            this.bAngleButton.TabIndex = 102;
            this.bAngleButton.Text = "B<->Angle and Grad<->K1L";
            this.bAngleButton.UseVisualStyleBackColor = true;
            this.bAngleButton.Click += new System.EventHandler(this.bAngleButton_Click);
            // 
            // prg_new
            // 
            this.prg_new.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.prg_new.Location = new System.Drawing.Point(646, 348);
            this.prg_new.Name = "prg_new";
            this.prg_new.Size = new System.Drawing.Size(123, 23);
            this.prg_new.TabIndex = 101;
            this.prg_new.Visible = false;
            // 
            // resetTableButton_new
            // 
            this.resetTableButton_new.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.resetTableButton_new.Location = new System.Drawing.Point(787, 348);
            this.resetTableButton_new.Name = "resetTableButton_new";
            this.resetTableButton_new.Size = new System.Drawing.Size(103, 23);
            this.resetTableButton_new.TabIndex = 100;
            this.resetTableButton_new.Text = "Reset table";
            this.resetTableButton_new.UseVisualStyleBackColor = true;
            this.resetTableButton_new.Click += new System.EventHandler(this.resetTableButton_new_Click);
            // 
            // pasteFromClipboardButton_new
            // 
            this.pasteFromClipboardButton_new.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.pasteFromClipboardButton_new.Location = new System.Drawing.Point(220, 348);
            this.pasteFromClipboardButton_new.Name = "pasteFromClipboardButton_new";
            this.pasteFromClipboardButton_new.Size = new System.Drawing.Size(199, 23);
            this.pasteFromClipboardButton_new.TabIndex = 99;
            this.pasteFromClipboardButton_new.Text = "Paste from clipboard";
            this.pasteFromClipboardButton_new.UseVisualStyleBackColor = true;
            this.pasteFromClipboardButton_new.Click += new System.EventHandler(this.pasteFromClipboardButton_new_Click);
            // 
            // addLineButton_new
            // 
            this.addLineButton_new.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.addLineButton_new.Location = new System.Drawing.Point(8, 348);
            this.addLineButton_new.Name = "addLineButton_new";
            this.addLineButton_new.Size = new System.Drawing.Size(90, 23);
            this.addLineButton_new.TabIndex = 97;
            this.addLineButton_new.Text = "Add row(s)";
            this.addLineButton_new.UseVisualStyleBackColor = true;
            this.addLineButton_new.Click += new System.EventHandler(this.addLineButton_new_Click);
            // 
            // remLineButton_new
            // 
            this.remLineButton_new.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.remLineButton_new.Location = new System.Drawing.Point(104, 348);
            this.remLineButton_new.Name = "remLineButton_new";
            this.remLineButton_new.Size = new System.Drawing.Size(110, 23);
            this.remLineButton_new.TabIndex = 98;
            this.remLineButton_new.Text = "Remove row(s)";
            this.remLineButton_new.UseVisualStyleBackColor = true;
            this.remLineButton_new.Click += new System.EventHandler(this.remLineButton_new_Click);
            // 
            // refThetaText
            // 
            this.refThetaText.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.refThetaText.Location = new System.Drawing.Point(630, 379);
            this.refThetaText.Name = "refThetaText";
            this.refThetaText.Size = new System.Drawing.Size(54, 20);
            this.refThetaText.TabIndex = 95;
            this.refThetaText.Text = "0";
            // 
            // label12
            // 
            this.label12.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(546, 382);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(78, 13);
            this.label12.TabIndex = 94;
            this.label12.Text = "Ref.theta [rad]:";
            // 
            // refZText
            // 
            this.refZText.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.refZText.Location = new System.Drawing.Point(486, 379);
            this.refZText.Name = "refZText";
            this.refZText.Size = new System.Drawing.Size(54, 20);
            this.refZText.TabIndex = 93;
            this.refZText.Text = "0";
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(426, 382);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(54, 13);
            this.label1.TabIndex = 92;
            this.label1.Text = "Ref.Z [m]:";
            // 
            // refXText
            // 
            this.refXText.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.refXText.Location = new System.Drawing.Point(364, 379);
            this.refXText.Name = "refXText";
            this.refXText.Size = new System.Drawing.Size(54, 20);
            this.refXText.TabIndex = 91;
            this.refXText.Text = "0";
            // 
            // label14
            // 
            this.label14.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(304, 382);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(54, 13);
            this.label14.TabIndex = 90;
            this.label14.Text = "Ref.X [m]:";
            // 
            // getRefRowButton
            // 
            this.getRefRowButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.getRefRowButton.Location = new System.Drawing.Point(181, 377);
            this.getRefRowButton.Name = "getRefRowButton";
            this.getRefRowButton.Size = new System.Drawing.Size(75, 23);
            this.getRefRowButton.TabIndex = 89;
            this.getRefRowButton.Text = "Set selected";
            this.getRefRowButton.UseVisualStyleBackColor = true;
            this.getRefRowButton.Click += new System.EventHandler(this.getRefRowButton_Click);
            // 
            // refRowTextBox
            // 
            this.refRowTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.refRowTextBox.Location = new System.Drawing.Point(138, 379);
            this.refRowTextBox.Name = "refRowTextBox";
            this.refRowTextBox.Size = new System.Drawing.Size(37, 20);
            this.refRowTextBox.TabIndex = 88;
            // 
            // label15
            // 
            this.label15.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(12, 382);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(120, 13);
            this.label15.TabIndex = 87;
            this.label15.Text = "Reference element row:";
            // 
            // prefixCheckbox
            // 
            this.prefixCheckbox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.prefixCheckbox.Checked = true;
            this.prefixCheckbox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.prefixCheckbox.Location = new System.Drawing.Point(569, 544);
            this.prefixCheckbox.Name = "prefixCheckbox";
            this.prefixCheckbox.Size = new System.Drawing.Size(105, 17);
            this.prefixCheckbox.TabIndex = 86;
            this.prefixCheckbox.Text = "file number prefix";
            this.prefixCheckbox.UseVisualStyleBackColor = true;
            // 
            // eMaxText
            // 
            this.eMaxText.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.eMaxText.Location = new System.Drawing.Point(307, 516);
            this.eMaxText.Name = "eMaxText";
            this.eMaxText.Size = new System.Drawing.Size(100, 20);
            this.eMaxText.TabIndex = 85;
            this.eMaxText.Text = "50E6";
            // 
            // label16
            // 
            this.label16.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(217, 519);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(92, 13);
            this.label16.TabIndex = 84;
            this.label16.Text = "Photon eMax (eV)";
            // 
            // eMinText
            // 
            this.eMinText.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.eMinText.Location = new System.Drawing.Point(102, 516);
            this.eMinText.Name = "eMinText";
            this.eMinText.Size = new System.Drawing.Size(100, 20);
            this.eMinText.TabIndex = 83;
            this.eMinText.Text = "4";
            // 
            // eMinLabel
            // 
            this.eMinLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.eMinLabel.AutoSize = true;
            this.eMinLabel.Location = new System.Drawing.Point(12, 519);
            this.eMinLabel.Name = "eMinLabel";
            this.eMinLabel.Size = new System.Drawing.Size(89, 13);
            this.eMinLabel.TabIndex = 82;
            this.eMinLabel.Text = "Photon Emin (eV)";
            // 
            // dLtext
            // 
            this.dLtext.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.dLtext.Location = new System.Drawing.Point(102, 490);
            this.dLtext.Name = "dLtext";
            this.dLtext.Size = new System.Drawing.Size(100, 20);
            this.dLtext.TabIndex = 81;
            this.dLtext.Text = "1";
            // 
            // label17
            // 
            this.label17.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(12, 493);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(82, 13);
            this.label17.TabIndex = 80;
            this.label17.Text = "step length (cm)";
            // 
            // energySpreadText
            // 
            this.energySpreadText.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.energySpreadText.Location = new System.Drawing.Point(307, 463);
            this.energySpreadText.Name = "energySpreadText";
            this.energySpreadText.Size = new System.Drawing.Size(100, 20);
            this.energySpreadText.TabIndex = 78;
            this.energySpreadText.Text = "0.1";
            // 
            // label18
            // 
            this.label18.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(217, 466);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(91, 13);
            this.label18.TabIndex = 77;
            this.label18.Text = "energy spread (%)";
            // 
            // beamEText
            // 
            this.beamEText.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.beamEText.Location = new System.Drawing.Point(504, 464);
            this.beamEText.Name = "beamEText";
            this.beamEText.Size = new System.Drawing.Size(100, 20);
            this.beamEText.TabIndex = 76;
            this.beamEText.Text = "1";
            // 
            // label19
            // 
            this.label19.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(217, 443);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(82, 13);
            this.label19.TabIndex = 73;
            this.label19.Text = "pMass(GeV/c2)";
            // 
            // analyzedView
            // 
            this.analyzedView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.Silver;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.analyzedView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle5;
            this.analyzedView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.analyzedView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.hasApertureInfo,
            this.AVname,
            this.length,
            this.angle,
            this.B,
            this.grad,
            this.K1L,
            this.twissS,
            this.s,
            this.startX,
            this.thetaStart,
            this.startZ,
            this.zlimit,
            this.xOffset,
            this.yOffset,
            this.xAngle,
            this.yAngle,
            this.betaX,
            this.betaY,
            this.etaX,
            this.etaPX,
            this.alphaX,
            this.alphaY,
            this.AptType,
            this.Aperture1,
            this.Aperture2,
            this.Aperture3,
            this.Aperture4});
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle12.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle12.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle12.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle12.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle12.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.analyzedView.DefaultCellStyle = dataGridViewCellStyle12;
            this.analyzedView.Location = new System.Drawing.Point(8, 6);
            this.analyzedView.Name = "analyzedView";
            dataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle13.BackColor = System.Drawing.Color.Silver;
            dataGridViewCellStyle13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle13.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle13.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle13.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle13.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.analyzedView.RowHeadersDefaultCellStyle = dataGridViewCellStyle13;
            this.analyzedView.Size = new System.Drawing.Size(894, 336);
            this.analyzedView.TabIndex = 60;
            // 
            // hasApertureInfo
            // 
            this.hasApertureInfo.HeaderText = "Aperture";
            this.hasApertureInfo.Name = "hasApertureInfo";
            this.hasApertureInfo.Width = 60;
            // 
            // AVname
            // 
            this.AVname.HeaderText = "Name";
            this.AVname.Name = "AVname";
            // 
            // length
            // 
            this.length.HeaderText = "length (m)";
            this.length.Name = "length";
            // 
            // angle
            // 
            this.angle.HeaderText = "DIPOLE: Bending angle [rad]";
            this.angle.Name = "angle";
            // 
            // B
            // 
            this.B.HeaderText = "DIPOLE: Field (T)";
            this.B.Name = "B";
            // 
            // grad
            // 
            this.grad.HeaderText = "QUADRUPOLE: Gradient (T/m)";
            this.grad.Name = "grad";
            // 
            // K1L
            // 
            this.K1L.HeaderText = "QUADRUPOLE: K1L";
            this.K1L.Name = "K1L";
            // 
            // twissS
            // 
            dataGridViewCellStyle6.BackColor = System.Drawing.Color.WhiteSmoke;
            this.twissS.DefaultCellStyle = dataGridViewCellStyle6;
            this.twissS.HeaderText = "Twiss s [m]";
            this.twissS.Name = "twissS";
            this.twissS.ReadOnly = true;
            // 
            // s
            // 
            dataGridViewCellStyle7.BackColor = System.Drawing.Color.WhiteSmoke;
            this.s.DefaultCellStyle = dataGridViewCellStyle7;
            this.s.HeaderText = "start s [m]";
            this.s.Name = "s";
            this.s.ReadOnly = true;
            // 
            // startX
            // 
            dataGridViewCellStyle8.BackColor = System.Drawing.Color.WhiteSmoke;
            this.startX.DefaultCellStyle = dataGridViewCellStyle8;
            this.startX.HeaderText = "start X (m)";
            this.startX.Name = "startX";
            this.startX.ReadOnly = true;
            // 
            // thetaStart
            // 
            dataGridViewCellStyle9.BackColor = System.Drawing.Color.WhiteSmoke;
            this.thetaStart.DefaultCellStyle = dataGridViewCellStyle9;
            this.thetaStart.HeaderText = "start Theta [rad]";
            this.thetaStart.Name = "thetaStart";
            this.thetaStart.ReadOnly = true;
            // 
            // startZ
            // 
            dataGridViewCellStyle10.BackColor = System.Drawing.Color.WhiteSmoke;
            this.startZ.DefaultCellStyle = dataGridViewCellStyle10;
            this.startZ.HeaderText = "start Z (m)";
            this.startZ.Name = "startZ";
            this.startZ.ReadOnly = true;
            // 
            // zlimit
            // 
            dataGridViewCellStyle11.BackColor = System.Drawing.Color.WhiteSmoke;
            this.zlimit.DefaultCellStyle = dataGridViewCellStyle11;
            this.zlimit.HeaderText = "Z end (m)";
            this.zlimit.Name = "zlimit";
            this.zlimit.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // xOffset
            // 
            this.xOffset.HeaderText = "dX [m]";
            this.xOffset.Name = "xOffset";
            // 
            // yOffset
            // 
            this.yOffset.HeaderText = "dY [m]";
            this.yOffset.Name = "yOffset";
            // 
            // xAngle
            // 
            this.xAngle.HeaderText = "dX/dS";
            this.xAngle.Name = "xAngle";
            // 
            // yAngle
            // 
            this.yAngle.HeaderText = "dY/dS";
            this.yAngle.Name = "yAngle";
            // 
            // betaX
            // 
            this.betaX.HeaderText = "beta X [m]";
            this.betaX.Name = "betaX";
            // 
            // betaY
            // 
            this.betaY.HeaderText = "beta Y [m]";
            this.betaY.Name = "betaY";
            // 
            // etaX
            // 
            this.etaX.HeaderText = "eta X [m]";
            this.etaX.Name = "etaX";
            // 
            // etaPX
            // 
            this.etaPX.HeaderText = "eta prime X";
            this.etaPX.Name = "etaPX";
            // 
            // alphaX
            // 
            this.alphaX.HeaderText = "alpha X";
            this.alphaX.Name = "alphaX";
            // 
            // alphaY
            // 
            this.alphaY.HeaderText = "alpha Y";
            this.alphaY.Name = "alphaY";
            // 
            // AptType
            // 
            this.AptType.HeaderText = "Aperture Type";
            this.AptType.Name = "AptType";
            // 
            // Aperture1
            // 
            this.Aperture1.HeaderText = "Aperture 1 [m]";
            this.Aperture1.Name = "Aperture1";
            // 
            // Aperture2
            // 
            this.Aperture2.HeaderText = "Aperture 2 [m]";
            this.Aperture2.Name = "Aperture2";
            // 
            // Aperture3
            // 
            this.Aperture3.HeaderText = "Aperture 3 [m]";
            this.Aperture3.Name = "Aperture3";
            // 
            // Aperture4
            // 
            this.Aperture4.HeaderText = "Aperture 4 [m]";
            this.Aperture4.Name = "Aperture4";
            // 
            // beamELabel
            // 
            this.beamELabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.beamELabel.AutoSize = true;
            this.beamELabel.Location = new System.Drawing.Point(414, 467);
            this.beamELabel.Name = "beamELabel";
            this.beamELabel.Size = new System.Drawing.Size(74, 13);
            this.beamELabel.TabIndex = 75;
            this.beamELabel.Text = "Beam E (GeV)";
            // 
            // pMassText
            // 
            this.pMassText.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.pMassText.Location = new System.Drawing.Point(307, 440);
            this.pMassText.Name = "pMassText";
            this.pMassText.Size = new System.Drawing.Size(100, 20);
            this.pMassText.TabIndex = 74;
            this.pMassText.Text = "-0.00051099891";
            // 
            // browseBXYbutton
            // 
            this.browseBXYbutton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.browseBXYbutton.Location = new System.Drawing.Point(315, 565);
            this.browseBXYbutton.Name = "browseBXYbutton";
            this.browseBXYbutton.Size = new System.Drawing.Size(32, 23);
            this.browseBXYbutton.TabIndex = 72;
            this.browseBXYbutton.Text = "...";
            this.browseBXYbutton.UseVisualStyleBackColor = true;
            this.browseBXYbutton.Click += new System.EventHandler(this.browseBXYbutton_Click);
            // 
            // bxyFileNameText
            // 
            this.bxyFileNameText.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.bxyFileNameText.Location = new System.Drawing.Point(102, 568);
            this.bxyFileNameText.Name = "bxyFileNameText";
            this.bxyFileNameText.Size = new System.Drawing.Size(207, 20);
            this.bxyFileNameText.TabIndex = 71;
            this.bxyFileNameText.Text = "beam.bxy";
            // 
            // label20
            // 
            this.label20.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(14, 571);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(68, 13);
            this.label20.TabIndex = 70;
            this.label20.Text = "bxy filename:";
            // 
            // currentText
            // 
            this.currentText.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.currentText.Location = new System.Drawing.Point(504, 440);
            this.currentText.Name = "currentText";
            this.currentText.Size = new System.Drawing.Size(100, 20);
            this.currentText.TabIndex = 69;
            this.currentText.Text = "1";
            // 
            // label21
            // 
            this.label21.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(414, 443);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(64, 13);
            this.label21.TabIndex = 68;
            this.label21.Text = "current (mA)";
            // 
            // emittYtext
            // 
            this.emittYtext.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.emittYtext.Location = new System.Drawing.Point(102, 463);
            this.emittYtext.Name = "emittYtext";
            this.emittYtext.Size = new System.Drawing.Size(100, 20);
            this.emittYtext.TabIndex = 67;
            this.emittYtext.Text = "1E-9";
            // 
            // label22
            // 
            this.label22.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(12, 466);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(84, 13);
            this.label22.TabIndex = 66;
            this.label22.Text = "Emittance_Y (m)";
            // 
            // emittXtext
            // 
            this.emittXtext.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.emittXtext.Location = new System.Drawing.Point(102, 440);
            this.emittXtext.Name = "emittXtext";
            this.emittXtext.Size = new System.Drawing.Size(100, 20);
            this.emittXtext.TabIndex = 65;
            this.emittXtext.Text = "1E-9";
            // 
            // label23
            // 
            this.label23.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(12, 443);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(84, 13);
            this.label23.TabIndex = 64;
            this.label23.Text = "Emittance_X (m)";
            // 
            // writeFilesButton
            // 
            this.writeFilesButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.writeFilesButton.Location = new System.Drawing.Point(787, 539);
            this.writeFilesButton.Name = "writeFilesButton";
            this.writeFilesButton.Size = new System.Drawing.Size(103, 23);
            this.writeFilesButton.TabIndex = 63;
            this.writeFilesButton.Text = "write param file(s)";
            this.writeFilesButton.UseVisualStyleBackColor = true;
            this.writeFilesButton.Click += new System.EventHandler(this.writeFilesButton_Click);
            // 
            // dirTextBox
            // 
            this.dirTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.dirTextBox.Location = new System.Drawing.Point(102, 542);
            this.dirTextBox.Name = "dirTextBox";
            this.dirTextBox.Size = new System.Drawing.Size(420, 20);
            this.dirTextBox.TabIndex = 62;
            // 
            // browseButton
            // 
            this.browseButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.browseButton.Location = new System.Drawing.Point(528, 540);
            this.browseButton.Name = "browseButton";
            this.browseButton.Size = new System.Drawing.Size(33, 23);
            this.browseButton.TabIndex = 61;
            this.browseButton.Text = "...";
            this.browseButton.UseVisualStyleBackColor = true;
            this.browseButton.Click += new System.EventHandler(this.browseButton_Click);
            // 
            // apertureTab
            // 
            this.apertureTab.Location = new System.Drawing.Point(4, 22);
            this.apertureTab.Name = "apertureTab";
            this.apertureTab.Padding = new System.Windows.Forms.Padding(3);
            this.apertureTab.Size = new System.Drawing.Size(906, 617);
            this.apertureTab.TabIndex = 4;
            this.apertureTab.Text = "Aperture list";
            this.apertureTab.UseVisualStyleBackColor = true;
            // 
            // OpticsBuilderForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(926, 657);
            this.Controls.Add(this.tabControl);
            this.Name = "OpticsBuilderForm";
            this.Text = "Survey file to SynRad+ converter";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.Resize += new System.EventHandler(this.Form1_Resize);
            this.tabControl.ResumeLayout(false);
            this.resultTab.ResumeLayout(false);
            this.resultTab.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.seqSurveyDataView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.seqObjectsDataView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.seqValuesDataView)).EndInit();
            this.inputTab.ResumeLayout(false);
            this.paramsPanel.ResumeLayout(false);
            this.paramsPanel.PerformLayout();
            this.sModeBox.ResumeLayout(false);
            this.sModeBox.PerformLayout();
            this.quadModeBox.ResumeLayout(false);
            this.quadModeBox.PerformLayout();
            this.dipoleModeBox.ResumeLayout(false);
            this.dipoleModeBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.inputDataView)).EndInit();
            this.dbTab.ResumeLayout(false);
            this.dbTab.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.queryView)).EndInit();
            this.exportTab.ResumeLayout(false);
            this.exportTab.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.analyzedView)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.TabPage inputTab;
        private System.Windows.Forms.DataGridView inputDataView;
        private System.Windows.Forms.Button pasteButton;
        private System.Windows.Forms.TabPage resultTab;
        private System.Windows.Forms.DataGridView seqValuesDataView;
        private System.Windows.Forms.Button resetTableButton;
        private System.Windows.Forms.Button defineNameColButton;
        private System.Windows.Forms.Button defineSButton;
        private System.Windows.Forms.Button defineTypeButton;
        private System.Windows.Forms.GroupBox sModeBox;
        private System.Windows.Forms.RadioButton sType1;
        private System.Windows.Forms.RadioButton sType0;
        private System.Windows.Forms.TextBox refLocX;
        private System.Windows.Forms.Button defineQuadButton;
        private System.Windows.Forms.Button defineDipoleButton;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox dipoleNameText;
        private System.Windows.Forms.TextBox quadNameText;
        private System.Windows.Forms.Button defineReferenceRowButton;
        private System.Windows.Forms.Button defineAlphaXButton;
        private System.Windows.Forms.Button defineEtaPrimeButton;
        private System.Windows.Forms.Button defineEtaXButton;
        private System.Windows.Forms.Button defineBetaYButton;
        private System.Windows.Forms.Button defineBetaXButton;
        private System.Windows.Forms.Button defineAlphaYButton;
        private System.Windows.Forms.CheckBox quadSameColumnCheckbox;
        private System.Windows.Forms.GroupBox quadModeBox;
        private System.Windows.Forms.RadioButton quadGradientModeRadio;
        private System.Windows.Forms.RadioButton quadK1LmodeRadio;
        private System.Windows.Forms.GroupBox dipoleModeBox;
        private System.Windows.Forms.RadioButton dipoleStrecgthModeRadio;
        private System.Windows.Forms.RadioButton dipoleAngleModeRadio;
        private System.Windows.Forms.TextBox refLocZ;
        private System.Windows.Forms.TextBox refLocY;
        private System.Windows.Forms.TextBox refDirZ;
        private System.Windows.Forms.TextBox refDirY;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox refDirX;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox driftNameText;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button analyzeButton;
        private System.Windows.Forms.Panel paramsPanel;
        private System.Windows.Forms.CheckBox beginsWithCheckbox;
        private System.Windows.Forms.CheckBox nameDefinesTypeCheckbox;
        private System.Windows.Forms.ProgressBar prg;
        private System.Windows.Forms.TabPage exportTab;
        private System.Windows.Forms.Button addLineButton;
        private System.Windows.Forms.Button remLineButton;
        private System.Windows.Forms.Button getResultButton;
        private System.Windows.Forms.TextBox beamEnergyTextbox;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button readDispButton;
        private System.Windows.Forms.Button readSeqButton;
        private System.Windows.Forms.Label surveyLabel;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.DataGridView seqSurveyDataView;
        private System.Windows.Forms.DataGridView seqObjectsDataView;
        private System.Windows.Forms.DataGridViewTextBoxColumn valueName;
        private System.Windows.Forms.DataGridViewTextBoxColumn ValueValue;
        private System.Windows.Forms.DataGridViewTextBoxColumn name;
        private System.Windows.Forms.DataGridViewTextBoxColumn type;
        private System.Windows.Forms.Button exportButton;
        private System.Windows.Forms.TextBox sMaxText;
        private System.Windows.Forms.TextBox sMinText;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button writeBXYfileButton;
        private System.Windows.Forms.Button getEndLineButton;
        private System.Windows.Forms.Button getStartLineButton;
        private System.Windows.Forms.TextBox endLineText;
        private System.Windows.Forms.TextBox startLineText;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label startLineLabel;
        private System.Windows.Forms.Button bAngleButton;
        private System.Windows.Forms.ProgressBar prg_new;
        private System.Windows.Forms.Button resetTableButton_new;
        private System.Windows.Forms.Button pasteFromClipboardButton_new;
        private System.Windows.Forms.Button addLineButton_new;
        private System.Windows.Forms.Button remLineButton_new;
        private System.Windows.Forms.TextBox refThetaText;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox refZText;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox refXText;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Button getRefRowButton;
        private System.Windows.Forms.TextBox refRowTextBox;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.CheckBox prefixCheckbox;
        private System.Windows.Forms.TextBox eMaxText;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox eMinText;
        private System.Windows.Forms.Label eMinLabel;
        private System.Windows.Forms.TextBox dLtext;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox energySpreadText;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox beamEText;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.DataGridView analyzedView;
        private System.Windows.Forms.Label beamELabel;
        private System.Windows.Forms.TextBox pMassText;
        private System.Windows.Forms.Button browseBXYbutton;
        private System.Windows.Forms.TextBox bxyFileNameText;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox currentText;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox emittYtext;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox emittXtext;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Button writeFilesButton;
        private System.Windows.Forms.TextBox dirTextBox;
        private System.Windows.Forms.Button browseButton;
        private System.Windows.Forms.CheckBox createDriftsCheckbox;
        private System.Windows.Forms.CheckBox dispInsertDriftsCheckbox;
        private System.Windows.Forms.DataGridViewTextBoxColumn instanceName;
        private System.Windows.Forms.DataGridViewTextBoxColumn instanceObject;
        private System.Windows.Forms.DataGridViewTextBoxColumn instanceType;
        private System.Windows.Forms.DataGridViewTextBoxColumn instancePos;
        private System.Windows.Forms.DataGridViewTextBoxColumn survLength;
        private System.Windows.Forms.DataGridViewCheckBoxColumn hasDispData;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button readTwissButton;
        private System.Windows.Forms.TabPage dbTab;
        private System.Windows.Forms.TextBox serverText;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Button queryButton;
        private System.Windows.Forms.TextBox passwordText;
        private System.Windows.Forms.TextBox usernameText;
        private System.Windows.Forms.ComboBox machineCodeCombo;
        private System.Windows.Forms.Label connResultText;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label queryResultText;
        private System.Windows.Forms.TextBox dbTableText;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.DataGridView queryView;
        private System.Windows.Forms.DataGridViewTextBoxColumn queryName;
        private System.Windows.Forms.DataGridViewTextBoxColumn querySstart;
        private System.Windows.Forms.DataGridViewTextBoxColumn querySend;
        private System.Windows.Forms.DataGridViewTextBoxColumn queryShape;
        private System.Windows.Forms.DataGridViewTextBoxColumn queryDiamU;
        private System.Windows.Forms.DataGridViewTextBoxColumn queryDiamV;
        private System.Windows.Forms.DataGridViewTextBoxColumn queryEllipseA;
        private System.Windows.Forms.DataGridViewTextBoxColumn queryEllipseB;
        private System.Windows.Forms.DataGridViewTextBoxColumn queryEllipseC;
        private System.Windows.Forms.DataGridViewTextBoxColumn queryEllipseD;
        private System.Windows.Forms.DataGridViewTextBoxColumn queryNote;
        private System.Windows.Forms.CheckBox suffixCheckbox;
        private System.Windows.Forms.Button searchEndButton;
        private System.Windows.Forms.Button searchBeginningButton;
        private System.Windows.Forms.TextBox searchTermTextbox;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label searchResultLabel;
        private System.Windows.Forms.Button readApertureButton;
        private System.Windows.Forms.TextBox circleSidesText;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Button writeGeomButton;
        private System.Windows.Forms.Button browseGeomButton;
        private System.Windows.Forms.TextBox geomFileTextBox;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.TextBox curveStepText;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.ComboBox taperCombo;
        private System.Windows.Forms.DataGridViewCheckBoxColumn hasApertureInfo;
        private System.Windows.Forms.DataGridViewTextBoxColumn AVname;
        private System.Windows.Forms.DataGridViewTextBoxColumn length;
        private System.Windows.Forms.DataGridViewTextBoxColumn angle;
        private System.Windows.Forms.DataGridViewTextBoxColumn B;
        private System.Windows.Forms.DataGridViewTextBoxColumn grad;
        private System.Windows.Forms.DataGridViewTextBoxColumn K1L;
        private System.Windows.Forms.DataGridViewTextBoxColumn twissS;
        private System.Windows.Forms.DataGridViewTextBoxColumn s;
        private System.Windows.Forms.DataGridViewTextBoxColumn startX;
        private System.Windows.Forms.DataGridViewTextBoxColumn thetaStart;
        private System.Windows.Forms.DataGridViewTextBoxColumn startZ;
        private System.Windows.Forms.DataGridViewTextBoxColumn zlimit;
        private System.Windows.Forms.DataGridViewTextBoxColumn xOffset;
        private System.Windows.Forms.DataGridViewTextBoxColumn yOffset;
        private System.Windows.Forms.DataGridViewTextBoxColumn xAngle;
        private System.Windows.Forms.DataGridViewTextBoxColumn yAngle;
        private System.Windows.Forms.DataGridViewTextBoxColumn betaX;
        private System.Windows.Forms.DataGridViewTextBoxColumn betaY;
        private System.Windows.Forms.DataGridViewTextBoxColumn etaX;
        private System.Windows.Forms.DataGridViewTextBoxColumn etaPX;
        private System.Windows.Forms.DataGridViewTextBoxColumn alphaX;
        private System.Windows.Forms.DataGridViewTextBoxColumn alphaY;
        private System.Windows.Forms.DataGridViewTextBoxColumn AptType;
        private System.Windows.Forms.DataGridViewTextBoxColumn Aperture1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Aperture2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Aperture3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Aperture4;
        private System.Windows.Forms.TabPage apertureTab;
    }
}


﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Numerics;
using System.IO;
//using Oracle.ManagedDataAccess;
//using Oracle.ManagedDataAccess.Client;

namespace OpticsBuilder
{
    public partial class OpticsBuilderForm : Form
    {
        StaticData formData;

        public OpticsBuilderForm()
        {
            InitializeComponent();
            System.Globalization.CultureInfo customCulture = (System.Globalization.CultureInfo)System.Threading.Thread.CurrentThread.CurrentCulture.Clone();
            customCulture.NumberFormat.NumberDecimalSeparator = "."; //force DOT as decimal separator
            System.Threading.Thread.CurrentThread.CurrentCulture = customCulture;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            
            formData = new StaticData();
            inputDataView.ColumnCount = 1;
            inputDataView.RowHeadersWidth = 30;
            analyzedView.RowHeadersWidth = 90;
            //RefreshRowHeaders();
            RefreshColumnHeaders();
            tabControl.SelectedIndex = 3;
        }

        private void addButton_Click(object sender, EventArgs e)
        {
            if (this.inputDataView.SelectedRows.Count > 0)
            {
                this.inputDataView.Rows.Insert(Math.Min(this.inputDataView.SelectedRows[0].Index, this.inputDataView.SelectedRows[this.inputDataView.SelectedRows.Count - 1].Index), this.inputDataView.SelectedRows.Count);
            }
            //RefreshRowHeaders();
        }

        private void RefreshRowHeaders()
        {
            prg.Value = 0;
            prg.Maximum = inputDataView.Rows.Count;
            prg.Visible = true;

            for (int i = 0; i < inputDataView.Rows.Count; i++)
            {
                prg.Value = i;
                prg.PerformStep();
                string headerText;
                Font font = new Font(inputDataView.ColumnHeadersDefaultCellStyle.Font, FontStyle.Bold);
                /*if (i == formData.datastartRowId)
                {
                    headerText = "Data start ->";
                }
                else*/
                if (i == formData.referenceRowId)
                {
                    headerText = "Ref. point ->";
                }
                else
                {
                    headerText = "Row" + (i + 1).ToString();
                    font = new Font(font, FontStyle.Regular);
                }
                inputDataView.Rows[i].HeaderCell.Value = headerText;
                inputDataView.Rows[i].HeaderCell.Style.Font = font;
            }
            prg.Visible = false;
        }

        private void RefreshColumnHeaders()
        {
            prg.Visible = true;
            prg.Value = 0;
            prg.Maximum = inputDataView.Columns.Count;
            for (int i = 0; i < inputDataView.Columns.Count; i++)
            {
                prg.Value = i;
                prg.PerformStep();
                inputDataView.Columns[i].SortMode = DataGridViewColumnSortMode.NotSortable;
                string headerText;
                Font font = new Font(inputDataView.ColumnHeadersDefaultCellStyle.Font, FontStyle.Bold);
                if (i == formData.nameColumnId)
                {
                    headerText = "Name";
                }
                else if (i == formData.typeColumnId)
                {
                    headerText = "Type";
                }
                else if (i == formData.sColumnId)
                {
                    headerText = "S [m]";
                }
                else if (i == formData.alphaXColumn)
                {
                    headerText = "AlphaX";
                }
                else if (i == formData.alphaYColumn)
                {
                    headerText = "AlphaY";
                }
                else if (i == formData.betaXColumn)
                {
                    headerText = "BetaX [m]";
                }
                else if (i == formData.betaYColumn)
                {
                    headerText = "BetaY [m]";
                }
                else if (i == formData.dipoleStrengthColumnId)
                {
                    headerText = "Dipole param";
                }
                else if (i == formData.etaPXColumn)
                {
                    headerText = "EtaPrime X";
                }
                else if (i == formData.etaXColumn)
                {
                    headerText = "Eta X [m]";
                }
                else if (i == formData.quadStrengthColumnId)
                {
                    headerText = "Quad param";
                }
                else
                {
                    headerText = "Col" + (i + 1).ToString();
                    font = new Font(font, FontStyle.Regular);
                }
                inputDataView.Columns[i].HeaderCell.Value = headerText;
                inputDataView.Columns[i].HeaderCell.Style.Font = font;
            }
            prg.Visible = false;
        }

        private void remButton_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewRow row in this.inputDataView.SelectedRows)
            {
                if (this.inputDataView.SelectedRows[0].Index != this.inputDataView.Rows.Count - 1)
                {
                    this.inputDataView.Rows.Remove(row);
                }
            }
            //RefreshRowHeaders();
        }

        // PasteInData pastes clipboard data into the grid passed to it.
        private void PasteInData(ref DataGridView dgv)
        {

            char[] rowSplitter = { '\n', '\r' };  // Cr and Lf.
            char columnSplitter = '\t';         // Tab.
            char[] columnSplitters = { columnSplitter };

            IDataObject dataInClipboard = Clipboard.GetDataObject();

            var clipboardData = dataInClipboard.GetData(DataFormats.Text);
            if (clipboardData == null) return;
            string stringInClipboard = clipboardData.ToString();

            string[] rowsInClipboard = stringInClipboard.Split(rowSplitter,
                StringSplitOptions.RemoveEmptyEntries);
            string[] columnsInClipboard = rowsInClipboard[0].Split(columnSplitters/*,
                StringSplitOptions.RemoveEmptyEntries*/);

            int r = 0;
            int c = 0;

            if (dgv.SelectedCells.Count > 0)
            {
                r = dgv.SelectedCells[0].RowIndex;
                c = dgv.SelectedCells[0].ColumnIndex;
            }


            prg.Visible = true;
            prg.Minimum = 0;
            prg.Maximum = rowsInClipboard.Length;
            prg.Value = 0;

            if (dgv.Rows.Count < (r + rowsInClipboard.Length))
            {
                dgv.Rows.Add(r + rowsInClipboard.Length + 1 - dgv.Rows.Count);
                prg.Value += 1;
                prg.PerformStep();
            }
            if (dgv.Columns.Count < (c + columnsInClipboard.Length))
                for (int iCol = 0; iCol < (c + columnsInClipboard.Length + 1 - dgv.Columns.Count); iCol++)
                {
                    dgv.Columns.Add("", "");
                    dgv.Columns[dgv.Columns.Count - 1].SortMode = DataGridViewColumnSortMode.NotSortable;

                }

            // Loop through lines:

            int iRow = 0;
            prg.Value = 0;
            while (iRow < rowsInClipboard.Length)
            {
                // Split up rows to get individual cells:

                string[] valuesInRow =
                    rowsInClipboard[iRow].Split(columnSplitter);

                // Cycle through cells.
                // Assign cell value only if within columns of grid:

                int jCol = 0;
                while (jCol < valuesInRow.Length)
                {
                    if ((dgv.ColumnCount - 1) >= (c + jCol))
                    {
                        if (!String.IsNullOrEmpty(valuesInRow[jCol]))
                        {
                            if (dgv.Columns[c + jCol] is DataGridViewTextBoxColumn)
                            { //Prevent pasting into checkbox
                                dgv.Rows[r + iRow].Cells[c + jCol].Value =
                                valuesInRow[jCol];
                            }
                            else
                            {
                                if (valuesInRow[jCol].ToLower() == "true")
                                {
                                    dgv.Rows[r + iRow].Cells[c + jCol].Value = true;
                                }
                            }
                        } else
                        {
                            dgv.Rows[r + iRow].Cells[c + jCol].Value = null; //Instead of empty string
                        }
                    }

                    jCol += 1;
                } // end while
                prg.Value = iRow;
                prg.PerformStep();
                iRow += 1;
            } // end while
            prg.Visible = false;
        } // PasteInData

        private void pasteButton_Click(object sender, EventArgs e)
        {
            PasteInData(ref inputDataView);
            //RefreshRowHeaders();
            RefreshColumnHeaders();
        }

        private void Form1_Resize(object sender, EventArgs e)
        {
            /*
            tabControl.Width = Width - 40;
            inputDataView.Width = inputTab.Width - 25;

            Form f1 = sender as Form;

            tabControl.Height = this.Height - 70;

            inputDataView.Height = inputTab.Height - paramsPanel.Height - 30;
            paramsPanel.Location = new Point(paramsPanel.Location.X, inputDataView.Height + 20);
            */
        }

        private void songsDataGridView_SelectionChanged(object sender, EventArgs e)
        {
            bool hasSelectedRow = this.inputDataView.SelectedRows.Count > 0;
            addLineButton.Enabled = remLineButton.Enabled = hasSelectedRow;
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            resetTableButton.Text = "Resizing table...";

            inputDataView.ColumnCount = 1;
            inputDataView.RowCount = 1;
            inputDataView.Rows[0].Cells[0].Value = "";

            //Default values
            formData.nameColumnId = -1;
            formData.typeColumnId = -1;
            formData.sColumnId = -1;
            formData.dipoleStrengthColumnId = -1;
            formData.quadStrengthColumnId = -1;

            formData.betaXColumn = -1;
            formData.betaYColumn = -1;
            formData.etaXColumn = -1;
            formData.etaPXColumn = -1;
            formData.alphaXColumn = -1;
            formData.alphaYColumn = -1;

            resetTableButton.Text = "Reset table";
        }

        private void defineNameColButton_Click(object sender, EventArgs e)
        {
            formData.nameColumnId = SetColumnId();
            RefreshColumnHeaders();
        }

        private int SetColumnId()
        {
            if (inputDataView.SelectedCells.Count != 1)
            {
                System.Windows.Forms.MessageBox.Show("Select exactly one cell");
                return -1;
            }
            else
            {
                int colIndex = inputDataView.SelectedCells[0].ColumnIndex;
                return colIndex;
            }
        }

        private int SetRowId(DataGridView dw)
        {
            if (dw.SelectedRows.Count != 1)
            {
                if (dw.SelectedCells.Count != 1)
                {
                    System.Windows.Forms.MessageBox.Show("Select exactly one cell or one row");
                    return -1;
                }
                else
                {
                    return dw.SelectedCells[0].RowIndex;
                }
            }
            else
            {
                return dw.SelectedRows[0].Index;
            }

        }

        private void defineTypeButton_Click(object sender, EventArgs e)
        {
            formData.typeColumnId = SetColumnId();
            RefreshColumnHeaders();
        }

        private void defineSButton_Click(object sender, EventArgs e)
        {
            formData.sColumnId = SetColumnId();
            RefreshColumnHeaders();
        }

        private void sType0_CheckedChanged(object sender, EventArgs e)
        {
            RadioButton rb = sender as RadioButton;
            if (rb != null)
                if (sType1.Checked)
                    formData.sMode = 1;
                else
                    formData.sMode = 0;
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            formData.quadStrengthColumnId = SetColumnId();
            RefreshColumnHeaders();
        }

        private void button9_Click(object sender, EventArgs e)
        {
            formData.alphaXColumn = SetColumnId();
            RefreshColumnHeaders();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            formData.betaXColumn = SetColumnId();
            RefreshColumnHeaders();
        }

        private void groupBox2_Enter(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void textBox7_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox6_TextChanged(object sender, EventArgs e)
        {

        }

        private void inputDataView_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void defineDipoleButton_Click(object sender, EventArgs e)
        {
            formData.dipoleStrengthColumnId = SetColumnId();
            RefreshColumnHeaders();
        }

        private void quadSameColumnCheckbox_CheckedChanged(object sender, EventArgs e)
        {

            defineQuadButton.Enabled = !(quadSameColumnCheckbox.Checked);
        }

        private void defineBetaYButton_Click(object sender, EventArgs e)
        {
            formData.betaYColumn = SetColumnId();
            RefreshColumnHeaders();
        }

        private void defineEtaXButton_Click(object sender, EventArgs e)
        {
            formData.etaXColumn = SetColumnId();
            RefreshColumnHeaders();
        }

        private void defineEtaPrimeButton_Click(object sender, EventArgs e)
        {
            formData.etaPXColumn = SetColumnId();
            RefreshColumnHeaders();
        }

        private void defineAlphaYButton_Click(object sender, EventArgs e)
        {
            formData.alphaYColumn = SetColumnId();
            RefreshColumnHeaders();
        }



        private void defineReferenceRowButton_Click(object sender, EventArgs e)
        {
            if (formData.referenceRowId >= 0 && formData.referenceRowId < inputDataView.RowCount)
            {
                inputDataView.Rows[formData.referenceRowId].DefaultCellStyle.BackColor = Color.FromArgb(224, 224, 224);
            }
            formData.referenceRowId = SetRowId(inputDataView);
            inputDataView.Rows[formData.referenceRowId].DefaultCellStyle.BackColor = Color.OrangeRed;
            //RefreshRowHeaders();
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            formData.betaXColumn = -1;
            RefreshColumnHeaders();
        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            formData.betaYColumn = -1;
            RefreshColumnHeaders();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            formData.etaXColumn = -1;
            RefreshColumnHeaders();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            formData.etaPXColumn = -1;
            RefreshColumnHeaders();
        }

        private void button5_Click_1(object sender, EventArgs e)
        {
            formData.alphaXColumn = -1;
            RefreshColumnHeaders();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            formData.alphaYColumn = -1;
            RefreshColumnHeaders();
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void dipoleNameText_TextChanged(object sender, EventArgs e)
        {

        }

        private void analyzeButton_Click(object sender, EventArgs e)
        {

            formData.driftNames = driftNameText.Text.Split(',');
            formData.dipoleNames = dipoleNameText.Text.Split(',');
            formData.quadNames = quadNameText.Text.Split(',');

            int localTypeColumnId;
            if (nameDefinesTypeCheckbox.Checked)
            {
                localTypeColumnId = formData.nameColumnId;
            }
            else
            {
                localTypeColumnId = formData.typeColumnId;
            }

            if (localTypeColumnId >= 0 && localTypeColumnId < inputDataView.ColumnCount)
            {
                //int startRowId = Math.Max(0, formData.datastartRowId);
                for (int i = 0/*startRowId*/; i < inputDataView.Rows.Count; i++)
                {
                    ElementType rowType = GetRowType(i, localTypeColumnId);
                    if (rowType == ElementType.Unknown)
                    {
                        inputDataView.Rows[i].DefaultCellStyle.BackColor = Color.FromArgb(230, 230, 230);
                    }
                    else if (rowType == ElementType.Drift)
                    {
                        inputDataView.Rows[i].DefaultCellStyle.BackColor = Color.White;
                    }
                    else if (rowType == ElementType.Dipole)
                    {
                        inputDataView.Rows[i].DefaultCellStyle.BackColor = Color.FromArgb(230, 255, 230);
                    }
                    else if (rowType == ElementType.Quadrupole)
                    {
                        inputDataView.Rows[i].DefaultCellStyle.BackColor = Color.FromArgb(230, 230, 255);
                    }
                    else if (rowType == ElementType.Reference)
                    {
                        inputDataView.Rows[i].DefaultCellStyle.BackColor = Color.OrangeRed;
                    }
                }
            }
            else
            {
                System.Windows.Forms.MessageBox.Show("Type column not defined");
            }
        }

        private void nameDefinesTypeCheckbox_CheckedChanged(object sender, EventArgs e)
        {
            defineTypeButton.Enabled = !nameDefinesTypeCheckbox.Checked;
        }

        private bool ContainsBeginning(string stringToAnalyze, string[] possibleBeginnings)
        {
            foreach (var beginning in possibleBeginnings)
            {
                if (stringToAnalyze.StartsWith(beginning)) return true;
            }
            return false;
        }

        private ElementType GetRowType(int i, int columnId)
        {
            if (i == formData.referenceRowId)
            {
                return ElementType.Reference;
            }
            else
            {
                var cellValue = inputDataView.Rows[i].Cells[columnId].Value;
                if (cellValue != null)
                {
                    string typeText = cellValue.ToString();

                    if ((!beginsWithCheckbox.Checked && formData.driftNames.Contains(typeText))
                        || (beginsWithCheckbox.Checked && ContainsBeginning(typeText, formData.driftNames)))
                    {
                        return ElementType.Drift;
                    }
                    else if ((!beginsWithCheckbox.Checked && formData.dipoleNames.Contains(typeText))
                        || (beginsWithCheckbox.Checked && ContainsBeginning(typeText, formData.dipoleNames)))
                    {
                        return ElementType.Dipole;
                    }
                    else if ((!beginsWithCheckbox.Checked && formData.quadNames.Contains(typeText))
                        || (beginsWithCheckbox.Checked && ContainsBeginning(typeText, formData.quadNames)))
                    {
                        return ElementType.Quadrupole;
                    }
                    else
                    {
                        return ElementType.Unknown;
                    }
                }
                else return ElementType.Unknown;
            }
        }

        private void readSeqButton_Click(object sender, EventArgs e)
        {
            System.IO.Stream myStream = null;
            OpenFileDialog openFileDialog1 = new OpenFileDialog();

            //openFileDialog1.InitialDirectory = "c:\\";
            openFileDialog1.Filter = "SEQ files (*.seq)|*.seq|All files (*.*)|*.*";
            openFileDialog1.FilterIndex = 0;
            openFileDialog1.RestoreDirectory = true;

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {

                    seqValuesDataView.Rows.Clear();
                    seqObjectsDataView.Rows.Clear();
                    seqSurveyDataView.Rows.Clear();
                    surveyLabel.Text = "Survey:";
                    formData.storedValues = new Dictionary<string, double>();
                    formData.storedObjects = new Dictionary<string, SurveyObject>();
                    formData.storedSequence = new SurveySequence();

                    Dictionary<string, double> values = new Dictionary<string, double>();
                    List<SurveyObject> objects = new List<SurveyObject>();
                    using (myStream = openFileDialog1.OpenFile())
                    {
                        System.IO.StreamReader reader = new System.IO.StreamReader(myStream);
                        string line;
                        bool sequenceMode = false;
                        while ((line = reader.ReadLine()) != null)
                        {
                            if (line.StartsWith("!")) continue; //skip comments
                            if (line.Length == 0)
                            {
                                continue; //skip empty lines
                            }
                            string[] separators = { ";" };
                            string[] commands = line.Split(separators, StringSplitOptions.RemoveEmptyEntries);


                            foreach (string command in commands)
                            {
                                if (!sequenceMode)
                                {
                                    //Look for value definition
                                    Match match_value_def = Regex.Match(command, @"\s*(\w+)\s*=\s*([-+]?[0-9]*\.?[0-9]+(?:[eE][-+]?[0-9]+)?)\s*");
                                    if (match_value_def.Value == command) //Full command match
                                    {
                                        string name = match_value_def.Groups[1].Value;
                                        double value = Convert.ToDouble(match_value_def.Groups[2].Value);
                                        values.Add(name, value);
                                        DataGridViewRow row = (DataGridViewRow)seqValuesDataView.Rows[0].Clone();
                                        row.Cells[0].Value = name;
                                        row.Cells[1].Value = value;
                                        seqValuesDataView.Rows.Add(row);
                                    }
                                }

                                if (!sequenceMode)
                                {
                                    //Look for object definition
                                    Match match_object_def = Regex.Match(command, @"\s*(\w+)\s*:\s*(\w+)\s*((?:,\s*\w+:=\s*(?:(?:[-+]?[0-9]*\.?[0-9]+(?:[eE][-+]?[0-9]+)?)\s*\*\s*)?\w+)*)");
                                    if (match_object_def.Value == command) //Full command match
                                    {
                                        SurveyObject newObject = new SurveyObject();
                                        newObject.name = match_object_def.Groups[1].Value;
                                        newObject.type = match_object_def.Groups[2].Value;
                                        if (match_object_def.Groups[3].Length != 0)
                                        { //has parameters
                                            string[] splitParams = match_object_def.Groups[3].Value.Split(',');
                                            foreach (string param in splitParams)
                                            {
                                                if (param.Length == 0) continue;
                                                Match match_param_def = Regex.Match(param, @"\s*(\w+):=\s*(?:([-+]?[0-9]*\.?[0-9]+(?:[eE][-+]?[0-9]+)?)\s*\*\s*)?(\w+)\s*");
                                                if (match_param_def.Value == param)
                                                { //Full param match
                                                    string paramName = match_param_def.Groups[1].Value;
                                                    double paramValue = 0;

                                                    string key = match_param_def.Groups[3].Value;
                                                    if (values.ContainsKey(key))
                                                    {
                                                        double multiplier = 1.0;
                                                        if (match_param_def.Groups[2].Length != 0) multiplier = Convert.ToDouble(match_param_def.Groups[2].Value);
                                                        paramValue = multiplier * values[key];
                                                    }
                                                    else
                                                        throw new Exception(key + " (not found)");

                                                    newObject.properties.Add(paramName, paramValue);
                                                }
                                            }
                                        }
                                        formData.storedObjects.Add(match_object_def.Groups[1].Value, newObject);

                                        //Add to data view
                                        seqObjectsDataView.Rows.Add();

                                        DataGridViewRow lastRow = seqObjectsDataView.Rows[seqObjectsDataView.RowCount - 2];
                                        lastRow.Cells["Name"].Value = match_object_def.Groups[1].Value;

                                        lastRow.Cells["Type"].Value = newObject.type;
                                        foreach (KeyValuePair<string, double> entry in newObject.properties)
                                        {
                                            if (!seqObjectsDataView.Columns.Contains(entry.Key))
                                            {
                                                seqObjectsDataView.Columns.Add(entry.Key, entry.Key);
                                                seqObjectsDataView.Columns[seqObjectsDataView.ColumnCount - 1].ReadOnly = true;
                                            }

                                            lastRow.Cells[entry.Key].Value = entry.Value;
                                        }

                                    }
                                }

                                //Look for sequence
                                if (!sequenceMode)
                                {
                                    Match match_sequenceStart_def = Regex.Match(command, @"(\w+):\s*SEQUENCE\s*,\s*REFER\s*=\s*(\w+)\s*,\s*L\s*=\s*([-+]?[0-9]*\.?[0-9]+(?:[eE][-+]?[0-9]+)?)");
                                    if (match_sequenceStart_def.Value == command) //Full command match
                                    { //Enter sequence mode
                                        formData.storedSequence.name = match_sequenceStart_def.Groups[1].Value;
                                        formData.storedSequence.referenceType = match_sequenceStart_def.Groups[2].Value;
                                        formData.storedSequence.length = Convert.ToDouble(match_sequenceStart_def.Groups[3].Value);
                                        formData.storedSequence.instanceList = new List<SurveyInstance>();
                                        surveyLabel.Text = "Sequence: " + formData.storedSequence.name + "    Reference: " + formData.storedSequence.referenceType + "    Length: " + formData.storedSequence.length.ToString("G10");
                                        sequenceMode = true;
                                        continue; //next command
                                    }
                                }

                                if (sequenceMode)
                                {
                                    //Look for sequence end
                                    if (command == "ENDSEQUENCE")
                                    {
                                        sequenceMode = false;
                                        continue;
                                    }

                                    //Look for instance
                                    Match match_instance_def = Regex.Match(command, @"([\w.]+):\s*(\w+),\s*at\s*=\s*([-+]?[0-9]*\.?[0-9]+(?:[eE][-+]?[0-9]+)?)");
                                    if (match_instance_def.Value == command) //Full command match
                                    { //New instance
                                        SurveyInstance newInstance = new SurveyInstance();
                                        newInstance.instanceName = match_instance_def.Groups[1].Value;
                                        newInstance.surveyObject = formData.storedObjects[match_instance_def.Groups[2].Value];
                                        newInstance.centerLocation = Convert.ToDouble(match_instance_def.Groups[3].Value);
                                        formData.storedSequence.instanceList.Add(newInstance);
                                    }
                                }
                            }
                        }
                        myStream.Close();
                        RefreshStoredSequenceView();
                        readDispButton.Enabled = true;
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error: " + ex.Message);
                }
                exportButton.Enabled = true;
            }
        }

        private void RefreshStoredSequenceView()
        {
            seqSurveyDataView.Rows.Clear();
            int nbRows = 0;
            if (formData.storedSequence.instanceList != null)
            {
                nbRows = formData.storedSequence.instanceList.Count;
            }
            seqSurveyDataView.RowCount = Math.Max(1, nbRows);
            for (int i = 0; i < nbRows; i++)
            {
                seqSurveyDataView.Rows[i].Cells["instanceName"].Value = formData.storedSequence.instanceList[i].instanceName;
                seqSurveyDataView.Rows[i].Cells["instanceObject"].Value = formData.storedSequence.instanceList[i].surveyObject.name;
                seqSurveyDataView.Rows[i].Cells["instanceType"].Value = formData.storedSequence.instanceList[i].surveyObject.type;
                double length = 0.0;
                if (formData.storedSequence.instanceList[i].surveyObject.properties.ContainsKey("L")) length = Convert.ToDouble(formData.storedSequence.instanceList[i].surveyObject.properties["L"]);
                seqSurveyDataView.Rows[i].Cells["instancePos"].Value = formData.storedSequence.instanceList[i].centerLocation - length / 2.0;
                seqSurveyDataView.Rows[i].Cells["survLength"].Value = length;
                seqSurveyDataView.Rows[i].Cells["hasDispData"].Value = formData.storedSequence.instanceList[i].hasDispersionData;
                if (!formData.storedSequence.instanceList[i].fromDispFile)
                {
                    Font font = new Font(seqSurveyDataView.DefaultCellStyle.Font, FontStyle.Bold);
                    seqSurveyDataView.Rows[i].DefaultCellStyle.Font = font;
                }
            }
        }

        private void readDispButton_Click(object sender, EventArgs e)
        {
            System.IO.Stream myStream = null;
            OpenFileDialog openFileDialog1 = new OpenFileDialog();

            //openFileDialog1.InitialDirectory = "c:\\";
            openFileDialog1.Filter = "DISP files (*.disp)|*.disp|All files (*.*)|*.*";
            openFileDialog1.FilterIndex = 0;
            openFileDialog1.RestoreDirectory = true;

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    using (myStream = openFileDialog1.OpenFile())
                    {
                        System.IO.StreamReader reader = new System.IO.StreamReader(myStream);
                        string line;
                        while ((line = reader.ReadLine()) != null)
                        {
                            if (line.Length == 0)
                            {
                                continue; //skip empty lines
                            }
                            string[] separators = { " " };
                            string[] commands = line.Split(separators, StringSplitOptions.RemoveEmptyEntries);

                            if (commands[0] == "AX") //header
                            {
                                continue;
                            }
                            else
                            { //values
                                //We'll hardcode the order this time
                                bool foundInstance = false;
                                string strippedInstanceName = commands[5];
                                if (strippedInstanceName.StartsWith("-")) strippedInstanceName = strippedInstanceName.Substring(1);
                                foreach (SurveyInstance inst in formData.storedSequence.instanceList)
                                {

                                    if (inst.instanceName == strippedInstanceName)
                                    {
                                        foundInstance = true;
                                        inst.hasDispersionData = true;
                                        inst.disp.alphaX = Convert.ToDouble(commands[0]);
                                        inst.disp.alphaY = Convert.ToDouble(commands[9]);
                                        inst.disp.betaX = Convert.ToDouble(commands[1]);
                                        inst.disp.betaY = Convert.ToDouble(commands[10]);
                                        inst.disp.etaX = Convert.ToDouble(commands[3]);
                                        inst.disp.etaPX = Convert.ToDouble(commands[4]);
                                        break;
                                    }
                                }
                                if (!foundInstance && dispInsertDriftsCheckbox.Checked)
                                {
                                    SurveyObject newDrift = new SurveyObject();

                                    newDrift.name = strippedInstanceName;
                                    double length = Convert.ToDouble(commands[6]);
                                    if (length > 0.0)
                                    {
                                        newDrift.type = "DRIFT";
                                        newDrift.properties.Add("L", length);
                                    }
                                    else
                                    {
                                        newDrift.type = "MARKER";
                                    }

                                    SurveyInstance newInstance = new SurveyInstance();
                                    newInstance.instanceName = strippedInstanceName;
                                    newInstance.surveyObject = newDrift;
                                    newInstance.centerLocation = Convert.ToDouble(commands[8]) + length / 2.0;
                                    newInstance.hasDispersionData = true;
                                    newInstance.fromDispFile = true;
                                    newInstance.disp.alphaX = Convert.ToDouble(commands[0]);
                                    newInstance.disp.alphaY = Convert.ToDouble(commands[9]);
                                    newInstance.disp.betaX = Convert.ToDouble(commands[1]);
                                    newInstance.disp.betaY = Convert.ToDouble(commands[10]);
                                    newInstance.disp.etaX = Convert.ToDouble(commands[3]);
                                    newInstance.disp.etaPX = Convert.ToDouble(commands[4]);
                                    int insertPos = formData.storedSequence.instanceList.Count;
                                    for (int i = 0; i < formData.storedSequence.instanceList.Count; i++)
                                    {
                                        if (newInstance.centerLocation < formData.storedSequence.instanceList[i].centerLocation)
                                        {
                                            insertPos = i;
                                            break;
                                        }
                                    }
                                    formData.storedSequence.instanceList.Insert(insertPos, newInstance);
                                }
                            }
                        }
                        reader.Close();
                    }
                    //Refresh sequence view
                    RefreshStoredSequenceView();
                    exportButton.Enabled = true;

                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error: " + ex.Message);
                }
            }
        }




        class StaticData
        {
            public int nameColumnId = -1;
            public int typeColumnId = -1;
            public int sColumnId = -1;
            public int dipoleStrengthColumnId = -1;
            public int quadStrengthColumnId = -1;

            public int betaXColumn = -1;
            public int betaYColumn = -1;
            public int etaXColumn = -1;
            public int etaPXColumn = -1;
            public int alphaXColumn = -1;
            public int alphaYColumn = -1;

            //public int datastartRowId = 0;
            public int referenceRowId = -1;

            public int sMode = 0;
            public int dipoleFormat = 0;
            public int quadFormat = 0;
            public bool quadStrengthSameColumn = false;
            public Vector3 refLocation = new Vector3(0, 0, 0);
            public Vector3 refDirection = new Vector3(0, 0, 1);  //Z
            //public double beamEnergy;

            public string[] driftNames = { "DRIFT", "SEXTUPOLE" };
            public string[] dipoleNames = { "LBEND", "RBEND" };
            public string[] quadNames = { "QUAD", "QUADRUPOLE" };

            public SurveySequence storedSequence;
            public Dictionary<string, double> storedValues;
            public Dictionary<string, SurveyObject> storedObjects;
        };

        class ElementProperties
        {
            //Input parameters
            //public string name;
            //ElementType type = ElementType.Unknown;
            //public double s; //starting point in beam coordinate
            // public double value; //strength in T or gradient in T/m
            public double betaX, betaY, etaX, etaPX, alphaX, alphaY;
            //Derived properties
            //public double length;
            //public Vector3 startLocation;
            // double startTheta;
        };

        enum ElementType
        {
            Unknown,
            Drift,
            Dipole,
            Quadrupole,
            Reference
        };

        class SurveyObject
        {
            public string name; //redundant, for display
            public string type;
            public Dictionary<string, double> properties;
            public SurveyObject()
            {
                properties = new Dictionary<string, double>();
            }
        };

        class SurveyBetaInstance
        {
            // public double s; //starting point in beam coordinate
            // public double betaX, betaY, etaX, etaPX, alphaX, alphaY;
        }

        class SurveyInstance
        {
            public string instanceName;
            public double centerLocation;
            public SurveyObject surveyObject;
            public ElementProperties disp;
            public bool hasDispersionData;
            public bool fromDispFile;
            public SurveyInstance()
            {
                hasDispersionData = false;
                fromDispFile = false;
                disp = new ElementProperties();
            }
        };

        class SurveySequence
        {
            public string name;
            public string referenceType;
            public double length;
            public List<SurveyInstance> instanceList;
        };

        private void exportBXYButton_Click(object sender, EventArgs e)
        {

            try
            {
                bool sMinLimit = false, sMaxLimit = false;
                double sMin = 0.0, sMax = 0.0;
                if (sMinText.Text.Length > 0)
                {
                    sMin = Convert.ToDouble(sMinText.Text);
                    sMinLimit = true;
                }
                if (sMaxText.Text.Length > 0)
                {
                    sMax = Convert.ToDouble(sMaxText.Text);
                    sMaxLimit = true;
                }

                analyzedView.Rows.Clear();

                bool sInitialized = false;
                double currentSposition = 0.0;

                foreach (SurveyInstance inst in formData.storedSequence.instanceList)
                {
                    if ((sMinLimit && sMin > inst.centerLocation) || (sMaxLimit && sMax < inst.centerLocation)) continue; //Out of export limits

                    Dictionary<string, double> prop = inst.surveyObject.properties;
                    double length;
                    if (prop.ContainsKey("L"))
                    {
                        length = Convert.ToDouble(prop["L"]);
                    }
                    else
                    { //Marker
                        length = 0;
                    }

                    if (createDriftsCheckbox.Checked) //Optionally insert drift
                    {
                        if (sInitialized) //Don't check at first element to export
                        {
                            double startPosition = inst.centerLocation - length / 2.0; //start point

                            if ((startPosition - currentSposition) > 1E-4)
                            { //Need to create drift
                                analyzedView.Rows.Add();
                                DataGridViewCellCollection driftCells = analyzedView.Rows[analyzedView.RowCount - 2].Cells;
                                driftCells = analyzedView.Rows[analyzedView.RowCount - 2].Cells;
                                driftCells["AVname"].Value = "DRIFT (auto)";
                                driftCells["length"].Value = (startPosition - currentSposition).ToString("G10");
                            }

                        }

                        currentSposition = inst.centerLocation + length / 2.0; //endpoint
                        sInitialized = true;
                    }

                    analyzedView.Rows.Add();
                    DataGridViewCellCollection cells = analyzedView.Rows[analyzedView.RowCount - 2].Cells;

                    cells["AVname"].Value = inst.surveyObject.name;
                    if (inst.surveyObject.name.StartsWith("B"))
                    {
                        analyzedView.Rows[analyzedView.RowCount - 2].DefaultCellStyle.BackColor = Color.FromArgb(230, 255, 230);
                    }
                    else if (inst.surveyObject.name.StartsWith("Q"))
                    {
                        analyzedView.Rows[analyzedView.RowCount - 2].DefaultCellStyle.BackColor = Color.FromArgb(230, 230, 255);
                    }

                    cells["length"].Value = length.ToString("G10");

                    if (prop.ContainsKey("K1"))
                    {
                        cells["K1L"].Value = length * prop["K1"];
                    }
                    if (prop.ContainsKey("ANGLE"))
                    {
                        cells["angle"].Value = prop["ANGLE"];
                    }
                    if (inst.hasDispersionData)
                    {
                        cells["betaX"].Value = inst.disp.betaX;
                        cells["betaY"].Value = inst.disp.betaY;
                        cells["etaX"].Value = inst.disp.etaX;
                        cells["etaPX"].Value = inst.disp.etaPX;
                        cells["alphaX"].Value = inst.disp.alphaX;
                        cells["alphaY"].Value = inst.disp.alphaY;
                    }
                }
                //RefreshRowHeaders_analyzedView();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message);
            }
            tabControl.SelectedIndex = 3;
        }
        /*
        private void pasteButton_new_Click(object sender, EventArgs e)
        {
            pasteTextBox.Text = Clipboard.GetText();
        }

        private void analyzeButton_new_Click(object sender, EventArgs e)
        {
            //string[] separators = { "\t" };
            string[] lines = pasteTextBox.Text.Split('\r');
            analyzedView.Rows.Clear();

            foreach (string line in lines)
            {

                string[] commands = line.Split('\t');
                if (!(commands.Length > 29)) continue;
                // if (!(commands[9].StartsWith("Q") || commands[9].StartsWith("B"))) continue; //only bends and quads
                analyzedView.Rows.Add();

                if (commands[0].StartsWith("\n")) commands[0] = commands[0].Substring(1);//Cut leading \n
                analyzedView.Rows[analyzedView.RowCount - 2].Cells["AVname"].Value = commands[0];
                //analyzedView.Rows[analyzedView.RowCount - 2].Cells["startX"].Value = Convert.ToDouble(commands[22])/100;
                //analyzedView.Rows[analyzedView.RowCount - 2].Cells["startZ"].Value = Convert.ToDouble(commands[23])/100;
                analyzedView.Rows[analyzedView.RowCount - 2].Cells["length"].Value = Convert.ToDouble(commands[2]);
                //analyzedView.Rows[analyzedView.RowCount - 2].Cells["zlimit"].Value = Convert.ToDouble(commands[24])/100;
                //analyzedView.Rows[analyzedView.RowCount - 2].Cells["thetaStart"].Value = Convert.ToDouble(commands[25]);

                //Beta functions:
                analyzedView.Rows[analyzedView.RowCount - 2].Cells["betaX"].Value = Convert.ToDouble(commands[13]);
                analyzedView.Rows[analyzedView.RowCount - 2].Cells["betaY"].Value = Convert.ToDouble(commands[14]);
                analyzedView.Rows[analyzedView.RowCount - 2].Cells["etaX"].Value = Convert.ToDouble(commands[15]);
                analyzedView.Rows[analyzedView.RowCount - 2].Cells["etaPX"].Value = Convert.ToDouble(commands[16]);
                analyzedView.Rows[analyzedView.RowCount - 2].Cells["alphaX"].Value = Convert.ToDouble(commands[17]);
                analyzedView.Rows[analyzedView.RowCount - 2].Cells["alphaY"].Value = Convert.ToDouble(commands[18]);

                if (commands[9].StartsWith("B"))
                {
                    analyzedView.Rows[analyzedView.RowCount - 2].Cells["B"].Value = commands[26];
                    analyzedView.Rows[analyzedView.RowCount - 2].DefaultCellStyle.BackColor = Color.FromArgb(230, 255, 230);
                }
                else if (commands[9].StartsWith("Q"))
                {
                    analyzedView.Rows[analyzedView.RowCount - 2].Cells["grad"].Value = commands[28];
                    analyzedView.Rows[analyzedView.RowCount - 2].DefaultCellStyle.BackColor = Color.FromArgb(230, 230, 255);
                }
            }
            RefreshRowHeaders();
        }
        */
        private void addLineButton_new_Click(object sender, EventArgs e)
        {
            if (analyzedView.SelectedRows.Count > 0)
            {
                analyzedView.Rows.Insert(Math.Min(analyzedView.SelectedRows[0].Index, analyzedView.SelectedRows[analyzedView.SelectedRows.Count - 1].Index), analyzedView.SelectedRows.Count);
            }
            RefreshRowHeaders();
        }

        private void remLineButton_new_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewRow row in analyzedView.SelectedRows)
            {
                if (analyzedView.SelectedRows[0].Index != analyzedView.Rows.Count - 1)
                {
                    analyzedView.Rows.Remove(row);
                }
            }
            RefreshRowHeaders();
        }

        private void pasteFromClipboardButton_new_Click(object sender, EventArgs e)
        {
            PasteInData(ref analyzedView);
        }

        private void resetTableButton_new_Click(object sender, EventArgs e)
        {
            analyzedView.Rows.Clear();
        }

        private void getRefRowButton_Click(object sender, EventArgs e)
        {
            int refRowId = SetAnalyzedRowId();
            if (refRowId != -1)
            {
                refRowTextBox.Text = (refRowId + 1).ToString();
                analyzedView.Rows[refRowId].Cells["startX"].Value = refXText.Text;
                analyzedView.Rows[refRowId].Cells["startZ"].Value = refZText.Text;
                analyzedView.Rows[refRowId].Cells["thetaStart"].Value = refThetaText.Text;
                analyzedView.Rows[refRowId].Cells["s"].Value = "0";
                try
                {   
                    CalcPositionsFromReference();
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error while calculating positions from reference row\n: " + ex.Message);
                }
            }
        }

        private void calcOthersButton_Click(object sender, EventArgs e)
        {
            CalcPositionsFromReference();
        }

        private void CalcPositionsFromReference()
        {
            int refRowId;
            try
            {
                refRowId = Convert.ToInt16(refRowTextBox.Text) - 1;
            }
            catch
            {
                MessageBox.Show("Set reference row");
                return;
            }

            if (refRowId == -1)
            {
                MessageBox.Show("Set reference row");
                return;
            }

            //Fill downwards
            int actualRowId = refRowId + 1;
            while (actualRowId < analyzedView.RowCount)
            {
                if (analyzedView.Rows[actualRowId].Cells["AVname"].Value == null) break; //Stop on empty line
                CalcPosition(actualRowId, 1);
                actualRowId++;
            }
            int lastValidRowId = actualRowId - 1;
            CalcPosition(lastValidRowId, 0); //z limit

            //Fill upwards
            actualRowId = refRowId - 1;
            while (actualRowId >= 0)
            {
                if (analyzedView.Rows[actualRowId].Cells["AVname"].Value == null) break; //Stop on empty line
                CalcPosition(actualRowId, -1);
                actualRowId--;
            }
        }

        private void bAngleButton_Click(object sender, EventArgs e)
        {
            try
            {
                foreach (DataGridViewRow row in analyzedView.Rows)
                {
                    ConvertBtoAngle(row.Cells);
                }
            }
            catch
            {

            }
        }

        private void browseButton_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog folderBrowserDialog1 = new FolderBrowserDialog();
            if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
            {
                dirTextBox.Text = folderBrowserDialog1.SelectedPath;
            }
        }

        private void browseBXYbutton_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog1 = new OpenFileDialog();

            //openFileDialog1.InitialDirectory = "c:\\";
            openFileDialog1.Filter = "BXY files (*.bxy)|*.bxy|All files (*.*)|*.*";
            openFileDialog1.FilterIndex = 0;
            openFileDialog1.RestoreDirectory = true;

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                bxyFileNameText.Text = Path.GetFileName(openFileDialog1.FileName);
            }
        }

        private void getStartLineButton_Click(object sender, EventArgs e)
        {
            int refRowId = SetRowId(analyzedView);
            if (refRowId != -1) startLineText.Text = (refRowId + 1).ToString();
        }

        private void getEndLineButton_Click(object sender, EventArgs e)
        {
            int refRowId = SetRowId(analyzedView);
            if (refRowId != -1) endLineText.Text = (refRowId + 1).ToString();
        }

        private void writeFilesButton_Click(object sender, EventArgs e)
        {
            if (dirTextBox.Text == "")
            {
                System.Windows.Forms.MessageBox.Show("Directory not defined");
                return;
            }
            int startLine = 0;
            int endLine = analyzedView.RowCount - 2;
            if (startLineText.Text != "") startLine = Convert.ToInt16(startLineText.Text);
            if (endLineText.Text != "") endLine = Convert.ToInt16(endLineText.Text);
            int index = 0;

            for (int r = Math.Max(0, startLine - 1); r <= Math.Min(analyzedView.RowCount - 2, endLine - 1); r++)
            {
                DataGridViewCellCollection cells = analyzedView.Rows[r].Cells;
                string fileName = cells["AVname"].Value.ToString();
                if (cells["angle"].Value == null && cells["K1L"].Value == null) continue; //skip drifts

                index++;
                fileName = ConvertSpecialCharToUnderscore(fileName);

                string prefix = "";
                if (prefixCheckbox.Checked)
                {
                    prefix = (index).ToString() + "_";
                    while (prefix.Length < 5) prefix = "0" + prefix;
                }

                string suffix = "";
                if (suffixCheckbox.Checked)
                {
                    suffix = "_s_" + cells["s"].Value.ToString();
                }

                if (cells["thetaStart"].Value == null)
                {
                    System.Windows.Forms.MessageBox.Show("Theta start value is empty. Set reference or click \"calc others\"");
                    return;
                }
                if (cells["K1L"].Value != null)
                { //Quad: param and mag
                    if (cells["grad"].Value == null && cells["K1L"].Value != null) //Calc gradient from K1L if not done yet
                    {
                        ConvertBtoAngle(cells);
                    }
                    using (StreamWriter writetext = new StreamWriter(dirTextBox.Text + "\\" + prefix + fileName + suffix + ".mag"))
                    {
                        string mag;
                        mag = (Convert.ToDouble(cells["startX"].Value) * 100.0).ToString("G10");
                        mag += " 0 ";
                        mag += (Convert.ToDouble(cells["startZ"].Value) * 100.0).ToString("G10");
                        mag += "\r\n0 ";
                        mag += cells["thetaStart"].Value.ToString(); //No conversion needed since we use inverse theta notation
                        mag += " 0\r\n";
                        mag += cells["grad"].Value.ToString();
                        mag += " ";
                        mag += (Convert.ToDouble(cells["length"].Value) * 100.0).ToString("G10");
                        mag += "\r\n";
                        writetext.WriteLine(mag);
                    }
                }
                using (StreamWriter writetext = new StreamWriter(dirTextBox.Text + "\\" + prefix + fileName + suffix + ".param"))
                {

                    string param;
                    param = "param_file_version: 3\r\n";
                    double startX = Convert.ToDouble(cells["startX"].Value);
                    double startY = 0.0; //Assuming reference orbit always in XZ plane
                    double startZ = Convert.ToDouble(cells["startZ"].Value);

                    //Convert local X,Y offset to global X,Y,Z coordinates
                    double offsetX = Convert.ToDouble(cells["xOffset"].Value) * Math.Cos(-Convert.ToDouble(cells["thetaStart"].Value));
                    double offsetY = Convert.ToDouble(cells["yOffset"].Value);
                    double offsetZ = Convert.ToDouble(cells["xOffset"].Value) * Math.Sin(-Convert.ToDouble(cells["thetaStart"].Value)); //inverse theta notation

                    //Apply offset
                    startX += offsetX;
                    startY += offsetY;
                    startZ += offsetZ;

                    double dirTheta = Convert.ToDouble(cells["thetaStart"].Value);
                    double dirAlpha = 0.0;

                    //Apply px, py offsets. Synrad angles are inverse (positive theta = negative X, positive alpha = negative Y)
                    double offsetTheta = -Math.Atan(Convert.ToDouble(cells["xAngle"].Value));
                    double offsetAlpha = -Math.Atan(Convert.ToDouble(cells["yAngle"].Value));

                    dirTheta += offsetTheta;
                    dirAlpha += offsetAlpha;

                    //Theta and alpha functions are the inverse of MADX (positive theta = negative X, positive alpha = negative Y)
                    //Note that COS is a pair function, so sign of angle doesn't matter
                    double dirX = Math.Cos(-dirAlpha) * Math.Sin(-dirTheta);
                    double dirY = Math.Sin(-dirAlpha);
                    double dirZ = Math.Cos(-dirAlpha) * Math.Cos(-dirTheta);

                    param += "startPos_cm: " + (startX * 100.0).ToString("G10") + " " + (startY * 100.0).ToString("G10") + " " + (startZ * 100.0).ToString("G10") + "\r\n";
                    param += "startDir_cm: " + dirX + " " + dirY + " " + dirZ + "\r\n";
                    param += "dL_cm: " + Convert.ToDouble(dLtext.Text) + "\r\n"; //1000 steps
                    param += "boundaries_cm: 1.00000000000000E+06 1.00000000000000E+06 " + (Convert.ToDouble(cells["zlimit"].Value) * 100.0) + "\r\n";
                    param += "particleMass_GeV: " + Convert.ToDouble(pMassText.Text) + "\r\n";
                    param += "beamEnergy_GeV: " + Convert.ToDouble(beamEText.Text) + "\r\n";
                    param += "beamCurrent_mA: " + Convert.ToDouble(currentText.Text) + "\r\n";
                    double coupling = Convert.ToDouble(emittYtext.Text) / Convert.ToDouble(emittXtext.Text);
                    double emittance = Convert.ToDouble(emittXtext.Text) * 100.0 * (1.0 + coupling);
                    param += "emittance_cm: " + emittance + "\r\n";
                    param += "energy_spread_percent: " + energySpreadText.Text + "\r\n";
                    param += "coupling_percent: " + 100.0 * coupling + "\r\n";
                    param += "const_beta_x_cm: -1.00000000000000E+00\r\n";
                    param += "BXYfileName: \"" + bxyFileNameText.Text + "\"\r\n";
                    param += "E_min_eV: " + Convert.ToDouble(eMinText.Text) + "\r\n";
                    param += "E_max_eV: " + Convert.ToDouble(eMaxText.Text) + "\r\n";
                    param += "enable_par_polarization: 1\r\n";
                    param += "enable_ort_polarization: 1\r\n";
                    param += "psiMax_X_Y_rad: 3.14159265358979E+00 3.14159265358979E+00\r\n";
                    if (cells["K1L"].Value != null) //Quad
                    {
                        param += "Bx_mode: 5\r\n";
                        param += "Bx_fileName: \"" + prefix + fileName + ".mag\"\r\n";
                    }
                    else //Dipole
                    {
                        param += "Bx_mode: 1\r\n";
                        param += "Bx_const_Tesla: 0.00000000000000E+00\r\n";
                    }
                    param += "By_mode: 1\r\n";
                    if (cells["K1L"].Value != null) //Quad
                    {
                        param += "By_const_Tesla: 0.00000000000000E+00\r\n";
                    }
                    else //Dipole
                    {
                        if (cells["B"].Value == null && cells["angle"].Value != null) //Calculate dipole gradient from angle if not done yet
                        {
                            ConvertBtoAngle(cells);
                        }
                        param += "By_const_Tesla: " + (Convert.ToDouble(cells["B"].Value)).ToString("G10") + "\r\n";
                    }
                    param += "Bz_mode: 1\r\n";
                    param += "Bz_const_Tesla: 0.00000000000000E+00\r\n";
                    writetext.WriteLine(param);
                }
            }
        }

        private static string ConvertSpecialCharToUnderscore(string fileName, bool convertDot = false)
        {
            StringBuilder newName = new StringBuilder(fileName);
            for (int i = 0; i < fileName.Length; i++)
            {
                if (fileName[i] == ' ' || (convertDot && fileName[i] == '.'))
                {
                    newName[i] = '_';
                }
            }
            fileName = newName.ToString();
            return fileName;
        }

        private void writeBXYfileButton_Click(object sender, EventArgs e)
        {
            if (dirTextBox.Text == "" || bxyFileNameText.Text == "")
            {
                System.Windows.Forms.MessageBox.Show("Directory or BXY filename not defined");
                return;
            }
            int startLine = 0;
            int endLine = analyzedView.RowCount - 2;
            if (startLineText.Text != "") startLine = Convert.ToInt16(startLineText.Text);
            if (endLineText.Text != "") endLine = Convert.ToInt16(endLineText.Text);

            bool zInitialized = false;
            double actZ = 0.0;

            List<List<double>> bxyEntries = new List<List<double>>();

            using (StreamWriter writetext = new StreamWriter(dirTextBox.Text + "\\" + bxyFileNameText.Text))
            {
                writetext.WriteLine("Z");
                for (int r = startLine; r <= endLine; r++)
                {
                    DataGridViewCellCollection cells = analyzedView.Rows[r].Cells;

                    if (cells["betaX"].Value != null && cells["betaY"].Value != null) //Skip lines where no disp data
                    {

                        double Z = Convert.ToDouble(cells["startZ"].Value);

                        if (!zInitialized || Math.Abs(Z - actZ) > 1E-4) //Skip duplicate entries
                        {

                            List<double> bxyEntry = new List<double>(7);

                            bxyEntry.Add(Z);
                            bxyEntry.Add(Convert.ToDouble(cells["betaX"].Value));
                            bxyEntry.Add(Convert.ToDouble(cells["betaY"].Value));

                            if (cells["etaX"].Value != null)
                            {
                                bxyEntry.Add(Convert.ToDouble(cells["etaX"].Value));
                            }
                            else
                            {
                                bxyEntry.Add(0.0);
                            }

                            if (cells["etaPX"].Value != null)
                            {
                                bxyEntry.Add(Convert.ToDouble(cells["etaPX"].Value));
                            }
                            else
                            {
                                bxyEntry.Add(0.0);
                            }

                            if (cells["alphaX"].Value != null)
                            {
                                bxyEntry.Add(Convert.ToDouble(cells["alphaX"].Value));
                            }
                            else
                            {
                                bxyEntry.Add(0.0);
                            }

                            if (cells["alphaY"].Value != null)
                            {
                                bxyEntry.Add(Convert.ToDouble(cells["alphaY"].Value));
                            }
                            else
                            {
                                bxyEntry.Add(0.0);
                            }
                            bxyEntries.Add(bxyEntry);
                        }
                        zInitialized = true;
                        actZ = Z;
                    }
                }
                List<List<double>> sortedBXY = bxyEntries.OrderBy(o => o[0]).ToList();
                foreach (var bxyEntry in sortedBXY)
                {
                    string line = "";
                    line += (bxyEntry[0] * 100.0).ToString("G10"); //Z
                    line += "\t";
                    line += (bxyEntry[1] * 100.0).ToString("G10"); //betaX
                    line += "\t";
                    line += (bxyEntry[2] * 100.0).ToString("G10"); //betaY
                    line += "\t";
                    line += (bxyEntry[3] * 100.0).ToString("G10"); //etaX
                    line += "\t";
                    line += (bxyEntry[4]).ToString("G10"); //etaPX
                    line += "\t";
                    line += (bxyEntry[5]).ToString("G10"); //alphaX
                    line += "\t";
                    line += (bxyEntry[6]).ToString("G10"); //alphaY
                    line += "\t";
                    writetext.WriteLine(line);
                }
            }
        }

        private void RefreshRowHeaders_analyzedView()
        {
            prg_new.Value = 0;
            prg_new.Maximum = analyzedView.Rows.Count;
            prg_new.Visible = true;

            for (int i = 0; i < analyzedView.Rows.Count; i++)
            {
                prg_new.Value = i;
                prg_new.PerformStep();
                string headerText;
                Font font = new Font(analyzedView.ColumnHeadersDefaultCellStyle.Font, FontStyle.Bold);


                {
                    headerText = "Row" + (i + 1).ToString();
                    font = new Font(font, FontStyle.Regular);
                }
                analyzedView.Rows[i].HeaderCell.Value = headerText;
                analyzedView.Rows[i].HeaderCell.Style.Font = font;
            }
            prg_new.Visible = false;
        }

        private void ConvertBtoAngle(DataGridViewCellCollection rowCells)
        {
            double chargeSign, E, length;
            try { 
                 chargeSign = Math.Sign(Convert.ToDouble(pMassText.Text));
            }
            catch (Exception ex)
            {
                throw new Exception("Parse particle mass \""+ pMassText+"\" failed:\n" + ex.Message);
            }
            try { 
             E = Convert.ToDouble(beamEText.Text);
            }
            catch (Exception ex)
            {
                throw new Exception("Parse energy \"" + beamEText + "\" failed:\n" + ex.Message);
            }
            try { 
             length = Convert.ToDouble(rowCells["length"].Value);
            }
            catch (Exception ex)
            {
                throw new Exception("Parse length \"" + rowCells["length"].Value + "\" failed:\n" + ex.Message);
            }

            //B <-> Angle
            if (NotEmptyCell(rowCells["angle"].Value))
            {
                double angle;
                try
                {
                    angle = Convert.ToDouble(rowCells["angle"].Value);
                }
                catch (Exception ex)
                {
                    throw new Exception("Parse angle \"" + rowCells["angle"].Value + "\" failed:\n" + ex.Message);
                }
                double B = chargeSign * E / 0.2998 * angle / length;
                rowCells["B"].Value = B.ToString("G10");
            }
            else if (NotEmptyCell(rowCells["B"].Value))
            {
                double B;
                B = Convert.ToDouble(rowCells["B"].Value);
                double angle = chargeSign * B * length * 0.2998 / E;
                rowCells["angle"].Value = angle.ToString("G10");
            }

            //Gradient <-> K1L
            if (NotEmptyCell(rowCells["grad"].Value))
            {
                double grad;
                try
                {
                    grad = Convert.ToDouble(rowCells["grad"].Value);
                }
                catch (Exception ex)
                {
                    throw new Exception("Parse gradient \"" + rowCells["grad"].Value + "\" failed:\n" + ex.Message);
                }
                double K1L = -chargeSign * grad * 0.2998 / E * length;
                rowCells["K1L"].Value = K1L.ToString("G10");
            }
            else if (NotEmptyCell(rowCells["K1L"].Value))
            {
                double K1L;
                try
                {
                    K1L = Convert.ToDouble(rowCells["K1L"].Value);
                }
                catch (Exception ex)
                {
                    throw new Exception("Parse K1L \"" + rowCells["K1L"].Value + "\" failed:\n" + ex.Message);
                }
                double grad = -chargeSign * K1L / length * E / 0.2998;
                rowCells["grad"].Value = grad.ToString("G10");
            }

        }

        private bool NotEmptyCell(object value)
        {
            return value != null && value != DBNull.Value && !String.IsNullOrWhiteSpace(value.ToString());
        }

        private void CalcPosition(int rowId, int direction)
        {
            int refRowId = rowId - direction;

            DataGridViewCellCollection refCells = analyzedView.Rows[refRowId].Cells;
            DataGridViewCellCollection currCells = analyzedView.Rows[rowId].Cells;

            double refX = Convert.ToDouble(refCells["startX"].Value);
            double refZ = Convert.ToDouble(refCells["startZ"].Value);
            double refTheta = Convert.ToDouble(refCells["thetaStart"].Value); //inverse in synrad
            double refS = Convert.ToDouble(refCells["s"].Value);

            double angle = 0.0;

            DataGridViewCellCollection angleCells = refCells;
            if (direction == -1)
            {
                angleCells = currCells;
            }

            double length = Convert.ToDouble(angleCells["length"].Value);
            // if (angleCells["angle"].Value == null && angleCells["B"].Value != null)
            // {
            ConvertBtoAngle(angleCells); //Always convert, maybe beam energy textbox value changed
                                         // }

            if (NotEmptyCell(angleCells["angle"].Value))
            {
                angle = Convert.ToDouble(angleCells["angle"].Value);
            } else
            {
                angle = 0.0;
            }

            double dZP, dXP;

            if (angle == 0.0)
            {
                dZP = length;
                dXP = 0;

            }
            else
            {
                dZP = length / angle * Math.Sin(angle);
                dXP = length / angle * (1.0 - Math.Cos(angle));
            }


            double dX = dZP * Math.Sin(direction * refTheta) + dXP * Math.Cos(direction * refTheta); //Inverse since we use inverse theta (compared to MADX) notation
            double dZ = dZP * Math.Cos(refTheta) - dXP * Math.Sin(refTheta);

            if (direction == 1)
            {
                refCells["zLimit"].Value = (refZ + dZ).ToString("G10");
                currCells["startX"].Value = (refX - dX).ToString("G10"); //Negative since our theta is the inverse of MADX (we want that a positive rotation increases theta)
                currCells["startZ"].Value = (refZ + dZ).ToString("G10");
                currCells["thetaStart"].Value = (refTheta + angle).ToString("G10");
                currCells["s"].Value = (refS + length).ToString("G10");
            }
            else if (direction == -1)
            {
                currCells["zLimit"].Value = (refZ).ToString("G10");
                currCells["startX"].Value = (refX - dX).ToString("G10");
                currCells["startZ"].Value = (refZ - dZ).ToString("G10");
                currCells["thetaStart"].Value = (refTheta - angle).ToString("G10");
                currCells["s"].Value = (refS - length).ToString("G10");
            }
            else
            {
                refCells["zLimit"].Value = (refZ + 1.0 * dZ).ToString("G10");
            }

        }

        private int SetAnalyzedRowId()
        {
            if (analyzedView.SelectedRows.Count != 1)
            {
                if (analyzedView.SelectedCells.Count != 1)
                {
                    System.Windows.Forms.MessageBox.Show("Select exactly one cell or one row");
                    return -1;
                }
                else
                {
                    return analyzedView.SelectedCells[0].RowIndex;
                }
            }
            else
            {
                return analyzedView.SelectedRows[0].Index;
            }

        }

        private void getResultButton_Click(object sender, EventArgs e)
        {

        }

        private void readTwissButton_Click(object sender, EventArgs e)
        {
            System.IO.Stream myStream = null;
            OpenFileDialog openFileDialog1 = new OpenFileDialog();

            //openFileDialog1.InitialDirectory = "c:\\";
            openFileDialog1.Filter = "All files (*.*)|*.*";
            openFileDialog1.FilterIndex = 0;
            openFileDialog1.RestoreDirectory = true;

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {

                    analyzedView.Rows.Clear();
                    analyzedView.Rows.Add();

                    using (myStream = openFileDialog1.OpenFile())
                    {
                        System.IO.StreamReader reader = new System.IO.StreamReader(myStream);
                        int
                                nameCol = -1,
                                keywordCol = -1,
                                sCol = -1,
                                lCol = -1,
                                angleCol = -1,
                                k1LCol = -1,
                                dxCol = -1,
                                dyCol = -1,
                                pxCol = -1,
                                pyCol = -1,
                                betXCol = -1,
                                betYCol = -1,
                                etaCol = -1,
                                etaPCol = -1,
                                alfXCol = -1,
                                alfYCol = -1;
                        bool columnsDefined = false;

                        string line;

                        while ((line = reader.ReadLine()) != null)
                        {


                            if (line.StartsWith("!")) continue; //skip comments
                            if (line.Length == 0) continue; //skip empty lines
                            if (line.StartsWith("$")) continue; //skip variable type
                            if (line.StartsWith("@")) //Parameter definition
                            {

                                string[] tokens = line.Split(new char[0], StringSplitOptions.RemoveEmptyEntries); //By whitespace
                                if (tokens[1] == "PARTICLE")
                                {
                                    switch (tokens[3])
                                    {
                                        case "ELECTRON":
                                            pMassText.Text = "-0.00051099891";
                                            break;
                                        case "POSITRON":
                                            pMassText.Text = "0.00051099891";
                                            break;
                                        case "PROTON":
                                            pMassText.Text = "0.9382720813";
                                            break;
                                    }
                                }
                                else if (tokens[2] == "%le")
                                {
                                    double value = Convert.ToDouble(tokens[3]);
                                    string paramName = tokens[1];
                                    switch (paramName)
                                    {
                                        case "ENERGY":
                                            beamEText.Text = value.ToString("G10");
                                            break;
                                        case "BCURRENT":
                                            currentText.Text = (value * 1000.0).ToString("G10");
                                            break;
                                        case "SIGE":
                                            energySpreadText.Text = (value / 100.0).ToString("G10");
                                            break;
                                        case "EX":
                                            emittXtext.Text = value.ToString("G10");
                                            break;
                                        case "EY":
                                            emittYtext.Text = value.ToString("G10");
                                            break;
                                    }
                                }
                            }
                            else if (line.StartsWith("*")) //Column definition
                            {
                                string[] tokens = line.Split(new char[0], StringSplitOptions.RemoveEmptyEntries); //By whitespace
                                for (int i = 0; i < tokens.Length; i++)
                                {
                                    switch (tokens[i])
                                    {
                                        case "NAME":
                                            nameCol = i - 1;
                                            break;
                                        case "KEYWORD":
                                            keywordCol = i - 1;
                                            break;
                                        case "S":
                                            sCol = i - 1;
                                            break;
                                        case "L":
                                            lCol = i - 1;
                                            break;
                                        case "ANGLE":
                                            angleCol = i - 1;
                                            break;
                                        case "K1L":
                                            k1LCol = i - 1;
                                            break;
                                        case "X":
                                            dxCol = i - 1;
                                            break;
                                        case "Y":
                                            dyCol = i - 1;
                                            break;
                                        case "PX":
                                            pxCol = i - 1;
                                            break;
                                        case "PY":
                                            pyCol = i - 1;
                                            break;
                                        case "BETX":
                                            betXCol = i - 1;
                                            break;
                                        case "BETY":
                                            betYCol = i - 1;
                                            break;
                                        case "DX":
                                            etaCol = i - 1;
                                            break;
                                        case "DPX":
                                            etaPCol = i - 1;
                                            break;
                                        case "ALFX":
                                            alfXCol = i - 1;
                                            break;
                                        case "ALFY":
                                            alfYCol = i - 1;
                                            break;
                                    }
                                }
                                if (nameCol == -1) throw new Exception("TWISS file doesn't have NAME column");
                                if (keywordCol == -1) throw new Exception("TWISS file doesn't have KEYWORD column");
                                if (sCol == -1) throw new Exception("TWISS file doesn't have S column");
                                if (lCol == -1) throw new Exception("TWISS file doesn't have L column");
                                if (angleCol == -1) throw new Exception("TWISS file doesn't have ANGLE column");
                                if (k1LCol == -1) throw new Exception("TWISS file doesn't have K1L column");
                                if (dxCol == -1) throw new Exception("TWISS file doesn't have X column");
                                if (dyCol == -1) throw new Exception("TWISS file doesn't have Y column");
                                if (pxCol == -1) throw new Exception("TWISS file doesn't have PX column");
                                if (pyCol == -1) throw new Exception("TWISS file doesn't have PY column");
                                if (betXCol == -1) throw new Exception("TWISS file doesn't have BETX column");
                                if (betYCol == -1) throw new Exception("TWISS file doesn't have BETY column");
                                if (etaCol == -1) throw new Exception("TWISS file doesn't have ETA column");
                                if (etaPCol == -1) throw new Exception("TWISS file doesn't have ETAP column");
                                if (alfXCol == -1) throw new Exception("TWISS file doesn't have ALFX column");
                                if (alfYCol == -1) throw new Exception("TWISS file doesn't have ALFY column");
                                columnsDefined = true;
                            }
                            else /*if (line.StartsWith(" "))*/ //Data row
                            {
                                if (!columnsDefined) throw new Exception("TWISS file: Data row reached before column definition");

                                string[] tokens = line.Split(new char[0], StringSplitOptions.RemoveEmptyEntries); //By whitespace
                                //Extract strings from quotation marks
                                for (int i = 0; i < tokens.Length; i++)
                                {
                                    var splitToken = tokens[i].Split('\"');
                                    if (splitToken.Length == 3) tokens[i] = splitToken[1];
                                }

                                analyzedView.Rows.Add();
                                DataGridViewCellCollection cells = analyzedView.Rows[analyzedView.RowCount - 3].Cells;
                                DataGridViewCellCollection nextCells = analyzedView.Rows[analyzedView.RowCount - 2].Cells;


                                cells["AVname"].Value = tokens[nameCol];
                                if (tokens[keywordCol] == "QUADRUPOLE")
                                {
                                    analyzedView.Rows[analyzedView.RowCount - 3].DefaultCellStyle.BackColor = Color.FromArgb(230, 230, 255);
                                }
                                else if (tokens[keywordCol] == "RBEND" || tokens[keywordCol] == "SBEND")
                                {
                                    analyzedView.Rows[analyzedView.RowCount - 3].DefaultCellStyle.BackColor = Color.FromArgb(230, 255, 230);
                                }

                                cells["length"].Value = Convert.ToDouble(tokens[lCol]).ToString("G10");

                                if (Convert.ToDouble(tokens[k1LCol]) != 0.0)
                                {
                                    cells["K1L"].Value = Convert.ToDouble(tokens[k1LCol]).ToString("G10");
                                }
                                if (Convert.ToDouble(tokens[angleCol]) != 0.0)
                                {
                                    cells["angle"].Value = Convert.ToDouble(tokens[angleCol]).ToString("G10");
                                }

                                cells["twissS"].Value = Convert.ToDouble(tokens[sCol]).ToString("G10");

                                nextCells["xOffset"].Value = Convert.ToDouble(tokens[dxCol]).ToString("G10");
                                nextCells["yOffset"].Value = Convert.ToDouble(tokens[dyCol]).ToString("G10");
                                nextCells["xAngle"].Value = Convert.ToDouble(tokens[pxCol]).ToString("G10");
                                nextCells["yAngle"].Value = Convert.ToDouble(tokens[pyCol]).ToString("G10");

                                nextCells["betaX"].Value = Convert.ToDouble(tokens[betXCol]).ToString("G10");
                                nextCells["betaY"].Value = Convert.ToDouble(tokens[betYCol]).ToString("G10");
                                nextCells["etaX"].Value = Convert.ToDouble(tokens[etaCol]).ToString("G10");
                                nextCells["etaPX"].Value = Convert.ToDouble(tokens[etaPCol]).ToString("G10");
                                nextCells["alphaX"].Value = Convert.ToDouble(tokens[alfXCol]).ToString("G10");
                                nextCells["alphaY"].Value = Convert.ToDouble(tokens[alfYCol]).ToString("G10");

                            }
                        } //end of readline block
                    }
                    try
                    {
                        foreach (DataGridViewRow row in analyzedView.Rows)
                        {
                            ConvertBtoAngle(row.Cells);
                        }
                    }
                    catch
                    {

                    }
                    //RefreshRowHeaders_analyzedView();
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error: " + ex.Message);
                }
            }
        }

        private void button7_Click(object sender, EventArgs e)
        {
            /*

            string conString = "User Id=" + usernameText.Text + "; password=" + passwordText.Text + ";" +
    "Data Source=" + serverText.Text + ";Pooling=false;";

            OracleConnection con = new OracleConnection();
            con.ConnectionString = conString;
            con.Open();

            //Create a command within the context of the connection
            //Use the command to display employee names and salary from the Employees table
            OracleCommand cmd = con.CreateCommand();
            cmd.CommandText = "select LAYOUT_EXPERT_NAME, S_START, S_END, SHAPE, INNER_DIAMETER_U, INNER_DIAMETER_V, ELLIPSE_PARAM_A, ELLIPSE_PARAM_B, ELLIPSE_PARAM_C, ELLIPSE_PARAM_D, NOTE from " + dbTableText.Text + " where machine_code='" + machineCodeCombo.SelectedItem + "' order by aperture_s, aperture_u, aperture_v";

            //Execute the command and use datareader to display the data
            OracleDataReader reader = cmd.ExecuteReader();
            if (reader.HasRows)
            {
                int nb = 0; queryView.Rows.Clear();
                while (reader.Read())
                {
                    queryView.Rows.Add();
                    DataGridViewCellCollection cells = queryView.Rows[queryView.RowCount - 2].Cells;
                    nb++;

                    if (!reader.IsDBNull(0)) cells["queryName"].Value = reader.GetString(0);
                    if (!reader.IsDBNull(1)) cells["querySstart"].Value = reader.GetValue(1);
                    if (!reader.IsDBNull(2)) cells["querySend"].Value = reader.GetValue(2);
                    if (!reader.IsDBNull(3)) cells["queryShape"].Value = reader.GetString(3);
                    if (!reader.IsDBNull(4)) cells["queryDiamU"].Value = reader.GetValue(4);
                    if (!reader.IsDBNull(5)) cells["queryDiamV"].Value = reader.GetValue(5);
                    if (!reader.IsDBNull(6)) cells["queryEllipseA"].Value = reader.GetValue(6);
                    if (!reader.IsDBNull(7)) cells["queryEllipseB"].Value = reader.GetValue(7);
                    if (!reader.IsDBNull(8)) cells["queryEllipseC"].Value = reader.GetValue(8);
                    if (!reader.IsDBNull(9)) cells["queryEllipseD"].Value = reader.GetValue(9);
                    if (!reader.IsDBNull(10)) cells["queryNote"].Value = reader.GetString(10);
                }
                queryResultText.Text = nb + " rows returned";
            }
            con.Close();
            */
        }

        private void button8_Click(object sender, EventArgs e)
        {
            /*
            string conString = "User Id=" + usernameText.Text + "; password=" + passwordText.Text + ";" +
                "Data Source=" + serverText.Text + ";Pooling=false;";

            OracleConnection con = new OracleConnection();
            con.ConnectionString = conString;
            con.Open();

            //Create a command within the context of the connection
            //Use the command to display employee names and salary from the Employees table
            OracleCommand cmd = con.CreateCommand();
            cmd.CommandText = "select distinct machine_code from " + dbTableText.Text + " order by machine_code";

            //Execute the command and use datareader to display the data
            OracleDataReader reader = cmd.ExecuteReader();
            if (reader.HasRows)
            {
                int nb = 0; machineCodeCombo.Items.Clear();
                while (reader.Read())
                {
                    machineCodeCombo.Items.Add(reader.GetString(0));
                    nb++;
                }
                connResultText.Text = nb + " machines loaded";
                if (nb > 0) queryButton.Enabled = true;
            }
            con.Close();
            */
        }

        private void searchBeginningButton_Click(object sender, EventArgs e)
        {
            SearchRow(true);
        }

        private void searchEndButton_Click(object sender, EventArgs e)
        {
            SearchRow(false);
        }

        private void SearchRow(bool beginning)
        {
            string searchTerm = searchTermTextbox.Text.ToLower();
            if (searchTerm.Length == 0)
            {
                searchResultLabel.Text = "Empty search term";
                return;
            }
            //Fill downwards
            int foundRowId = -1;
            for (int rowId = 0; foundRowId == -1 && rowId < analyzedView.RowCount; rowId++)
            {
                bool match;
                if (analyzedView.Rows[rowId].Cells["AVname"].Value == null)
                {
                    match = false;
                    searchTermTextbox.BackColor = Color.FromArgb(255, 255, 255);
                }
                else
                {
                    if (beginning) match = analyzedView.Rows[rowId].Cells["AVname"].Value.ToString().ToLower().StartsWith(searchTerm);
                    else match = analyzedView.Rows[rowId].Cells["AVname"].Value.ToString().ToLower().EndsWith(searchTerm);
                }
                if (match)
                {
                    foundRowId = rowId;
                }
            }
            if (foundRowId == -1)
            {
                searchResultLabel.Text = "Not found";
                searchTermTextbox.BackColor = Color.FromArgb(255, 220, 220);
                return;
            }
            else
            {
                searchResultLabel.Text = "Found at row " + (foundRowId + 1);
                searchTermTextbox.BackColor = Color.FromArgb(220, 255, 220);
                analyzedView.ClearSelection();
                analyzedView.Rows[foundRowId].Selected = true;
                analyzedView.FirstDisplayedScrollingRowIndex = foundRowId;
            }
        }

        private void searchTermTextbox_TextChanged(object sender, EventArgs e)
        {
            SearchRow(true);
        }

        private void browseGeomButton_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog1 = new OpenFileDialog();

            //openFileDialog1.InitialDirectory = "c:\\";
            openFileDialog1.Filter = "SYN files (*.syn)|*.syn|All files (*.*)|*.*";
            openFileDialog1.FilterIndex = 0;
            openFileDialog1.RestoreDirectory = true;

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                geomFileTextBox.Text = Path.GetFileName(openFileDialog1.FileName);
            }
        }

        private void readApertureButton_Click(object sender, EventArgs e)
        {
            System.IO.Stream myStream = null;
            OpenFileDialog openFileDialog1 = new OpenFileDialog();

            //openFileDialog1.InitialDirectory = "c:\\";
            openFileDialog1.Filter = "All files (*.*)|*.*";
            openFileDialog1.FilterIndex = 0;
            openFileDialog1.RestoreDirectory = true;

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    //Clear existing aperture data
                    for (int i = 0; i < analyzedView.RowCount; i++)
                    {
                        analyzedView.Rows[i].Cells["hasApertureInfo"].Value = false;
                        analyzedView.Rows[i].Cells["AptType"].Value = "";
                        analyzedView.Rows[i].Cells["Aperture1"].Value = "";
                        analyzedView.Rows[i].Cells["Aperture2"].Value = "";
                        analyzedView.Rows[i].Cells["Aperture3"].Value = "";
                        analyzedView.Rows[i].Cells["Aperture4"].Value = "";
                    }

                    using (myStream = openFileDialog1.OpenFile())
                    {
                        System.IO.StreamReader reader = new System.IO.StreamReader(myStream);
                        int sCol = -1;
                        int sMode = 0; //0=beginning 1=center 2=endpoint
                        int lengthCol = -1;
                        int apertureCol = -1;
                        int apertureMode = 0; //0=meter 1==millimeter
                        bool columnsDefined = false;

                        string line;
                        int lineNum = 0;
                        while ((line = reader.ReadLine()) != null)
                        {
                            lineNum++;

                            if (lineNum == 1) //Header
                            {
                                string[] tokens = line.Split(new char[0], StringSplitOptions.RemoveEmptyEntries); //By whitespace
                                for (int i = 0; i < tokens.Length; i++)
                                {
                                    switch (tokens[i])
                                    {
                                        case "Length":
                                            lengthCol = i;
                                            break;
                                        case "S_begin":
                                            sCol = i;
                                            sMode = 0;
                                            break;
                                        case "S_center":
                                            sCol = i;
                                            sMode = 1;
                                            break;
                                        case "S_end":
                                            sCol = i;
                                            sMode = 2;
                                            break;
                                        case "Aperture":
                                            apertureCol = i;
                                            apertureMode = 0;
                                            break;
                                        case "Aperture_m":
                                            apertureCol = i;
                                            apertureMode = 0;
                                            break;
                                        case "Aperture_mm":
                                            apertureCol = i;
                                            apertureMode = 1;
                                            break;
                                    }
                                }
                                if (sCol == -1) throw new Exception("Aperture file doesn't have S_begin, S_center or S_end column");
                                if (apertureCol == -1) throw new Exception("Aperture file doesn't Aperture, Aperture_m or Aperture_mm column");
                                if ((sMode == 1 || sMode == 2) && lengthCol == -1) throw new Exception("Aperture file doesn't have Length column. (Only necessary when using center or endpoint S coordinates)");
                                columnsDefined = true;
                            }
                            else
                            {
                                if (line.Length == 0) continue; //skip empty lines
                                if (!columnsDefined) throw new Exception("Aperture file: Data row reached before column definition (header)");

                                string[] tokens = line.Split(new char[0], StringSplitOptions.RemoveEmptyEntries); //By whitespace
                                //Extract strings from quotation marks
                                for (int i = 0; i < tokens.Length; i++)
                                {
                                    var splitToken = tokens[i].Split('\"');
                                    if (splitToken.Length == 3) tokens[i] = splitToken[1];
                                }

                                double S_end = Convert.ToDouble(tokens[sCol]);
                                if (sMode == 0)
                                {
                                    double L = Convert.ToDouble(tokens[lengthCol]);
                                    S_end += L;
                                }
                                if (sMode == 1)
                                {
                                    double L = Convert.ToDouble(tokens[lengthCol]);
                                    S_end += .5 * L;
                                }



                                //Fill downwards
                                int foundRowId = -1;
                                for (int rowId = 0; foundRowId == -1 && rowId < analyzedView.RowCount; rowId++)
                                {
                                    double rowTwissS = Convert.ToDouble(analyzedView.Rows[rowId].Cells["twissS"].Value);

                                    if (Math.Abs(rowTwissS - S_end) < 1E-4)
                                    {
                                        foundRowId = rowId;
                                        double aperture_m = Convert.ToDouble(tokens[apertureCol]);
                                        if (apertureMode == 1) aperture_m *= 0.001;
                                        analyzedView.Rows[rowId].Cells["hasApertureInfo"].Value = true;
                                        analyzedView.Rows[rowId].Cells["AptType"].Value = "Circle";
                                        analyzedView.Rows[rowId].Cells["Aperture1"].Value = aperture_m.ToString("G10");
                                    }
                                }
                                if (foundRowId == -1)
                                {
                                    MessageBox.Show("Not found: \n" + line);
                                    return;
                                }
                            }
                        } //end of readline block
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error: " + ex.Message);
                }
            }
        }

        private void suffixCheckbox_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void writeGeomButton_Click(object sender, EventArgs e)
        {
            if (dirTextBox.Text == "")
            {
                System.Windows.Forms.MessageBox.Show("Directory not defined");
                return;
            }
            if (refRowTextBox.Text == "")
            {
                System.Windows.Forms.MessageBox.Show("Reference row not set yet");
                return;
            }
            int curveSides;
            double curveStep;
            int mode;
            try
            {
                curveSides = Convert.ToUInt16(circleSidesText.Text);
                curveStep = Convert.ToDouble(curveStepText.Text);
                mode = taperCombo.SelectedIndex;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Couldn't interpret circle sides or curve step\n" + ex.Message);
                return;
            }

            int startLine = 0;
            int endLine = analyzedView.RowCount - 1; //last line shouldbe empty
            if (startLineText.Text != "") startLine = Convert.ToInt16(startLineText.Text);
            if (endLineText.Text != "") endLine = Convert.ToInt16(endLineText.Text);

            List<Aperture> apertures = new List<Aperture>();

            for (int rowId = Math.Max(0, startLine - 1); rowId <= Math.Min(analyzedView.RowCount, endLine - 1); rowId++)

            {
                try
                {
                    var cells = analyzedView.Rows[rowId].Cells;
                    double rowStartX = Convert.ToDouble(cells["startX"].Value);
                    double rowStartZ = Convert.ToDouble(cells["startZ"].Value);
                    double rowStartTheta = Convert.ToDouble(cells["thetaStart"].Value);
                    double rowLength = Convert.ToDouble(cells["length"].Value);
                    double rowAngle = Convert.ToDouble(cells["angle"].Value);

                    Aperture rowStartAperture = new Aperture();
                    rowStartAperture.x = rowStartX;
                    rowStartAperture.z = rowStartZ;
                    rowStartAperture.theta = rowStartTheta;

                    if (Convert.ToBoolean(cells["hasApertureInfo"].Value) == true)
                    {
                        switch (cells["AptType"].Value.ToString().ToLower())
                        {
                            case "circle":
                                rowStartAperture.type = ApertureType.Circle;
                                rowStartAperture.param1 = Convert.ToDouble(cells["Aperture1"].Value);
                                break;
                            case "racetrack":
                                rowStartAperture.type = ApertureType.Racetrack;
                                rowStartAperture.param1 = Convert.ToDouble(cells["Aperture1"].Value);
                                rowStartAperture.param2 = Convert.ToDouble(cells["Aperture2"].Value);
                                break;
                            default:
                                throw new Exception("Row " + (rowId + 1) + ": unknown aperture type " + cells["AptType"].Value);
                        }

                    }
                    else //No aperture info
                    {
                        if (apertures.Count > 0)
                        {
                            //Copy last aperture
                            rowStartAperture.type = apertures.Last().type;
                            rowStartAperture.param1 = apertures.Last().param1;
                            rowStartAperture.param2 = apertures.Last().param2;
                            rowStartAperture.param3 = apertures.Last().param3;
                            rowStartAperture.param4 = apertures.Last().param4;
                        }
                    }

                    //Start aperture determined, let's generate section
                    if (rowStartAperture.type != ApertureType.Undefined) //Undefined until first row with aperture info
                    {
                        List<Aperture> sectionApertures = GenerateSectionApertures(rowStartAperture, rowLength, rowAngle, curveStep);
                        AddSectionApertures(ref apertures, sectionApertures, mode); //Merges ends and adds new apertures
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Row " + (rowId + 1) + ": \n" + ex.Message);
                    return;
                }
                
            }
            (List<Vertex> vertices, List<Facet> facets) = GenerateVerticesAndFacets(apertures,curveSides);
            WriteTxtFile(geomFileTextBox.Text, vertices, facets);
        }

        private (List<Vertex>, List<Facet>) GenerateVerticesAndFacets(List<Aperture> apertures, int curveSides)
        {
            var newVertices = new List<Vertex>();
            var newFacets = new List<Facet>();

            for (int i=0;i<apertures.Count;i++)
            {
                Polygon2d aperture2dPoints;
                if (apertures[i].type == ApertureType.Circle)
                {
                    aperture2dPoints = new Polygon2d(curveSides, apertures[i].param1, false);
                } else if (apertures[i].type == ApertureType.Racetrack)
                {
                    aperture2dPoints = new Polygon2d(curveSides, apertures[i].param1, apertures[i].param2, false);
                } else
                {
                    throw new Exception("Aperture type not implemented");
                }
                CreateStep(newVertices, newFacets, aperture2dPoints, apertures[i].x, apertures[i].z, apertures[i].theta, i);
            }
                
            return (newVertices, newFacets);
        }

        private void AddSectionApertures(ref List<Aperture> apertures, List<Aperture> sectionApertures, int mode)
        {
            if (sectionApertures.Count == 0) return;
            if (apertures.Count == 0) apertures.AddRange(sectionApertures); //Add without any checks
            else
            {
                Aperture firstToAdd = sectionApertures.First();
                Aperture lastAdded = apertures.Last();
                if (mode==0 /* taper transition */ || ApertureEquals(firstToAdd, lastAdded))
                {
                    //Overwrite last aperture
                    apertures.RemoveAt(apertures.Count - 1);
                }
                    apertures.AddRange(sectionApertures);
                
            }
            return;
        }

        private static void AddToGeometry(List<Vertex> vertices, List<Facet> facets, List<Vertex> newVertices, List<Facet> newFacets)
        {
            int nbVertex_old = vertices.Count;
            vertices.AddRange(newVertices);
            foreach (Facet f in newFacets) //Renumber vertices
            {
                for (int i = 0; i < f.vertexIds.Count; i++)
                {
                    f.vertexIds[i] += nbVertex_old;
                }
            }
            facets.AddRange(newFacets);
        }

        private void WriteTxtFile(string fileName, List<Vertex> vertices, List<Facet> facets)
        {
            using (StreamWriter fw = new StreamWriter(dirTextBox.Text + "\\" + fileName))
            {
                fw.WriteLine("0");//Unused
                fw.WriteLine("0");//Hits
                fw.WriteLine("0"); //Leaks
                fw.WriteLine("0"); //Des
                fw.WriteLine("0"); //Des_limit
                fw.WriteLine(vertices.Count);
                fw.WriteLine(facets.Count);
                foreach (var vertex in vertices)
                {
                    fw.WriteLine(100.0 * vertex.x + " " + 100.0 * vertex.y + " " + 100.0 * vertex.z); //m->cm
                }
                foreach (var facet in facets)
                {
                    string line = facet.vertexIds.Count.ToString();
                    foreach (var vertexId in facet.vertexIds)
                    {
                        line += " " + (vertexId + 1);
                    }
                    fw.WriteLine(line);
                }
                foreach (var facet in facets)
                {
                    fw.WriteLine("0.0"); //sticking
                    fw.WriteLine("1.0"); //opacity
                    fw.WriteLine("0.0"); //area, unused
                    fw.WriteLine("0.0"); //nbDes, will override desType
                    fw.WriteLine("0.0"); //nbHit
                    fw.WriteLine("0.0");  //nbAbs
                    fw.WriteLine("0.0"); //desType, overridden
                    fw.WriteLine("1.0"); //reflection type, 1=specular
                    fw.WriteLine("0.0"); //unused
                }
                for (int i = 0; i < 100; i++) //Profiles
                    fw.WriteLine();
            }
        }

        private List<Aperture> GenerateSectionApertures(Aperture startAperture, double length, double angle, double curveStep)
        {
            List<Aperture> result = new List<Aperture>();

            int nbSteps = (int)(Math.Abs(angle) / curveStep) + 1;
            double stepAngle = angle / (double)nbSteps;
            double stepLength = length / (double)nbSteps;

            double actualX = startAperture.x;
            double actualZ = startAperture.z;
            double actualTheta = startAperture.theta;
            
            for (int i = 0; i < nbSteps; i++)
            {
                Aperture newAperture = new Aperture();
                newAperture.type = startAperture.type;
                newAperture.param1 = startAperture.param1;
                newAperture.param2 = startAperture.param2;
                newAperture.param3 = startAperture.param3;
                newAperture.param4 = startAperture.param4;

                newAperture.x = actualX;
                newAperture.z = actualZ;
                newAperture.theta = actualTheta;
                result.Add(newAperture);

                double dXP, dZP; //delta X,Z in coord system tangent to beam (as if theta was 0)
                if (angle == 0.0)
                {
                    dZP = stepLength;
                    dXP = 0;

                }
                else
                {
                    dZP = stepLength / stepAngle * Math.Sin(stepAngle);
                    dXP = stepLength / stepAngle * (1.0 - Math.Cos(stepAngle));
                }


                double dX = dZP * Math.Sin(actualTheta) + dXP * Math.Cos(actualTheta);
                double dZ = dZP * Math.Cos(actualTheta) - dXP * Math.Sin(actualTheta);

                actualZ += dZ;
                actualX -= dX;
                actualTheta += stepAngle;


            }
            if (length > 0.0)
            {
                //Last, closing step
                Aperture newAperture2 = new Aperture();
                newAperture2.type = startAperture.type;
                newAperture2.param1 = startAperture.param1;
                newAperture2.param2 = startAperture.param2;
                newAperture2.param3 = startAperture.param3;
                newAperture2.param4 = startAperture.param4;

                newAperture2.x = actualX;
                newAperture2.z = actualZ;
                newAperture2.theta = actualTheta;
                result.Add(newAperture2);
            }

            return result;
        }

        private (List<Vertex>, List<Facet>) generateSection(double xStart, double zStart, double thetaStart, double length, double angle, double curveStep, int circleSides, double aperture_m, int mode)
        {
            var newVertices = new List<Vertex>();
            var newFacets = new List<Facet>();

            Polygon2d apertureCircle = new Polygon2d(circleSides, aperture_m, false);

            int nbSteps = (int)(Math.Abs(angle) / curveStep) + 1;
            double stepAngle = angle / (double)nbSteps;
            double stepLength = length / (double)nbSteps;

            double actualTheta = thetaStart;
            double actualX = xStart;
            double actualZ = zStart;

            for (int i = 0; i < nbSteps; i++)
            {
                CreateStep(newVertices, newFacets, apertureCircle, actualTheta, actualX, actualZ, i);

                double dXP, dZP; //delta X,Z in coord system tangent to beam (as if theta was 0)
                if (angle == 0.0)
                {
                    dZP = stepLength;
                    dXP = 0;

                }
                else
                {
                    dZP = stepLength / stepAngle * Math.Sin(stepAngle);
                    dXP = stepLength / stepAngle * (1.0 - Math.Cos(stepAngle));
                }


                double dX = dZP * Math.Sin(actualTheta) + dXP * Math.Cos(actualTheta);
                double dZ = dZP * Math.Cos(actualTheta) - dXP * Math.Sin(actualTheta);

                actualZ += dZ;
                actualX -= dX;
                actualTheta += stepAngle;


            }
            //Last, closing step
            CreateStep(newVertices, newFacets, apertureCircle, actualTheta, actualX, actualZ, nbSteps);
            return (newVertices, newFacets);
        }

        private static void CreateStep(List<Vertex> newVertices, List<Facet> newFacets, Polygon2d aperture2dpolygon, double actualX, double actualZ, double actualTheta, int i)
        {
            int nbSides = aperture2dpolygon.vertices.Count;
            Polygon rotatedAperture = new Polygon(aperture2dpolygon.vertices, actualX, actualZ, actualTheta);
            newVertices.AddRange(rotatedAperture.vertices);
            if (i > 0) //Connect with previous
            {
                int previousSectionStartingVertexId = newVertices.Count - 2 * nbSides;
                int currentSectionStartingVertexId = newVertices.Count - nbSides;
                for (int sideId = 0; sideId < nbSides; sideId++)
                {
                    //Check whether simple connection is coplanar
                    Vertex plane1normal = (
                        newVertices[previousSectionStartingVertexId + sideId] - newVertices[currentSectionStartingVertexId + sideId]).CrossProductWith(
                        newVertices[currentSectionStartingVertexId + (sideId + 1) % nbSides] - newVertices[currentSectionStartingVertexId + sideId]
                        ).Normalized();
                    Vertex plane2normal = (
                        newVertices[currentSectionStartingVertexId + (sideId + 1) % nbSides] - newVertices[previousSectionStartingVertexId + (sideId + 1) % nbSides]).CrossProductWith(
                        newVertices[previousSectionStartingVertexId + sideId] - newVertices[previousSectionStartingVertexId + (sideId + 1) % nbSides]
                        ).Normalized();
                    bool triangulate = plane1normal.CrossProductWith(plane2normal).Norme() > 1E-5;
                    if (triangulate)
                    {
                        Facet newSide = new Facet();
                        //Right-hand rule rotation so normal points inwards
                        newSide.vertexIds.Add(previousSectionStartingVertexId + sideId);
                        newSide.vertexIds.Add(currentSectionStartingVertexId + sideId);
                        newSide.vertexIds.Add(currentSectionStartingVertexId + (sideId + 1) % nbSides);
                        newFacets.Add(newSide);
                        Facet newSide2 = new Facet();
                        //Right-hand rule rotation so normal points inwards
                        newSide2.vertexIds.Add(currentSectionStartingVertexId + (sideId + 1) % nbSides);
                        newSide2.vertexIds.Add(previousSectionStartingVertexId + (sideId + 1) % nbSides);
                        newSide2.vertexIds.Add(previousSectionStartingVertexId + sideId);
                        newFacets.Add(newSide2);
                    }
                    else
                    { //Rectangle
                        Facet newSide = new Facet();
                        //Right-hand rule rotation so normal points inwards
                        newSide.vertexIds.Add(previousSectionStartingVertexId + sideId);
                        newSide.vertexIds.Add(currentSectionStartingVertexId + sideId);
                        newSide.vertexIds.Add(currentSectionStartingVertexId + (sideId + 1) % nbSides);
                        newSide.vertexIds.Add(previousSectionStartingVertexId + (sideId + 1) % nbSides);
                        newFacets.Add(newSide);
                    }
                }
            }
        }

        private bool ApertureEquals(Aperture lhs, Aperture rhs)
        {
            return lhs.type == rhs.type &&
                ((lhs.param1 == 0.0 && rhs.param1 == 0.0) || (Math.Abs((lhs.param1 - rhs.param1) / lhs.param1) < 1E-4)) &&
                ((lhs.param2 == 0.0 && rhs.param2 == 0.0) || (Math.Abs((lhs.param2 - rhs.param2) / lhs.param2) < 1E-4)) &&
                ((lhs.param3 == 0.0 && rhs.param3 == 0.0) || (Math.Abs((lhs.param3 - rhs.param3) / lhs.param3) < 1E-4)) &&
                ((lhs.param4 == 0.0 && rhs.param4 == 0.0) || (Math.Abs((lhs.param4 - rhs.param4) / lhs.param4) < 1E-4));
        }
    }



}

class Vertex2d
{
    public double u, v;
    public Vertex2d(double U, double V)
    {
        u = U;v = V;
    }
};
class Vertex
{
    public double x, y, z;
    public Vertex(double X, double Y, double Z)
    {
        x = X; y = Y; z = Z;
    }
    public double Norme()
    {
        return Math.Sqrt(x * x + y * y + z * z);
    }
    public static Vertex operator +(Vertex a, Vertex b)
    {
        Vertex result = new Vertex(
            a.x + b.x,
            a.y + b.y,
            a.z + b.z);
        return result;
    }
    public static Vertex operator* (double s, Vertex v)
    {
        Vertex result = new Vertex(
            s * v.x,
            s * v.y,
            s * v.z);
        return result;
    }
    public static Vertex operator- (Vertex a, Vertex b)
    {
        return a + (-1.0*b);
    }
    public Vertex Normalized()
    {
        double norme = this.Norme();
        if (norme>0)
        {
            return (1.0 / norme) * this;
        } else
        {
            return 1.0*this;
        }
    }
    public Vertex CrossProductWith(Vertex b)
    {
        return new Vertex(
            this.y * b.z - this.z * b.y,
            this.z * b.x - this.x * b.z,
            this.x * b.y - this.y * b.x
            );
    }
};

class Facet
{
    public List<int> vertexIds;
    public Facet() => vertexIds = new List<int>();
};
class Polygon2d
{
    public List<Vertex2d> vertices;
    public Polygon2d(int nbSides, double radius, bool hiResSides) //Circle
    {
        vertices = new List<Vertex2d>();
        for (int i = 0; i < nbSides; i++)
        {
            double alpha = 2.0 * Math.PI * (double)i / (double)nbSides;
            Vertex2d point = new Vertex2d(
            radius * Math.Cos(alpha),
            radius * Math.Sin(alpha)
            );
            vertices.Add(point);
        }
    }
    public Polygon2d(int nbSides,
        double param1, //Radius or bounding box half-width
        double param2, //Bounding box half-height
        bool hiResSides) //Racetrack
    {
        vertices = new List<Vertex2d>();
        bool horizontal = param2 <= param1;
        double radius = (horizontal ? param1 : param2);
        if (param1 < 1E-8) throw new System.Exception("Aperture1 can't be 0");
        double cornerAngle;
        if (horizontal) cornerAngle = Math.Asin(param2 / param1);
        else cornerAngle = Math.Acos(param1 / param2);
        bool withinCurve = horizontal; //Horizontal racetrack: we start with curved side

        
        for (int i = 0; i < nbSides; i++)
        {
            double u, v;
            double alpha = 2.0 * Math.PI * (double)i / (double)nbSides;
            bool curvedPart = (alpha < cornerAngle || alpha > (2.0*Math.PI - cornerAngle)) || (alpha > (Math.PI - cornerAngle) && alpha < (Math.PI + cornerAngle));
            curvedPart = horizontal == curvedPart; //invert result if vertical
            if (curvedPart)
            {
                u = radius * Math.Cos(alpha);
                v = radius * Math.Sin(alpha);
            }
            else //straight side
            {
                if (horizontal)
                {
                    u = radius * Math.Cos(alpha);
                    v = param2; //height capped
                    if (alpha > Math.PI) v *= -1;
                } else
                {
                    u = param1; //width capped
                    if (alpha > 0.5 * Math.PI && alpha < 1.5 * Math.PI) u *= -1;
                    v = radius * Math.Sin(alpha);
                }
            }
            //Adjust corners
            if (curvedPart != withinCurve) //Just stepped out of curve or into curve
            {
                //Determine nearest corner angle
                double nearestCornerAngle;
                if (alpha < Math.PI / 2) nearestCornerAngle = cornerAngle;
                else if (alpha < Math.PI) nearestCornerAngle = Math.PI - cornerAngle;
                else if (alpha < 1.5 * Math.PI) nearestCornerAngle = Math.PI + cornerAngle;
                else nearestCornerAngle = 2.0 * Math.PI - cornerAngle;

                if (!curvedPart) //Stepped out of curve
                {
                    //Adjust actual (first straight) point to previous corner:
                    if (i>0 && Math.Abs(2.0 * Math.PI * (double)(i-1) / (double)nbSides - nearestCornerAngle) > 1e-3)
                    {
                        u = radius * Math.Cos(nearestCornerAngle);
                        v = radius * Math.Sin(nearestCornerAngle);
                    } else
                    { 
                       //No adjustment needed, last point was exactly the corner 
                    }
                } else //Stepped into curve
                {
                    //Adjust previous (last straight) point:
                    if (i > 0 && Math.Abs(2.0 * Math.PI * (double)(i - 1) / (double)nbSides - nearestCornerAngle) > 1e-3)
                    {
                        vertices[vertices.Count - 1].u = radius * Math.Cos(nearestCornerAngle);
                        vertices[vertices.Count - 1].v = radius * Math.Sin(nearestCornerAngle);
                    } else
                    {
                        //Leave last point as it is
                    }
                    //Current point unchanged:
                    u = radius * Math.Cos(alpha);
                    v = radius * Math.Sin(alpha);
                }
                withinCurve = curvedPart; //Update status
            }
           
            vertices.Add(new Vertex2d(u, v));
        }
    }
};

class Polygon
{
    public List<Vertex> vertices;
    public Polygon(List<Vertex2d> source2d, double xStart, double zStart, double theta)
    {
        vertices = new List<Vertex>();
        foreach (var vertex in source2d)
        {
            Vertex point = new Vertex(xStart, vertex.v, zStart);
            point.x += vertex.u * Math.Cos(theta);
            point.z += vertex.u * Math.Sin(theta);

            vertices.Add(point);
        }
    }
};

enum ApertureType
{
    Undefined,
    Circle,
    Racetrack
};

class Aperture
{
    public ApertureType type;
    public double param1, param2, param3, param4;
    public double x, z, theta;

};



﻿namespace OpticsBuilder
{
    partial class OpticsBuilderForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(OpticsBuilderForm));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            this.tabControl = new System.Windows.Forms.TabControl();
            this.exportTab = new System.Windows.Forms.TabPage();
            this.previewGeomButton = new System.Windows.Forms.Button();
            this.outputGroup = new System.Windows.Forms.GroupBox();
            this.dirComboBox = new System.Windows.Forms.ComboBox();
            this.browseButton = new System.Windows.Forms.Button();
            this.label30 = new System.Windows.Forms.Label();
            this.getStartLineButton = new System.Windows.Forms.Button();
            this.startLineLabel = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.startRowText = new System.Windows.Forms.TextBox();
            this.endRowText = new System.Windows.Forms.TextBox();
            this.getEndLineButton = new System.Windows.Forms.Button();
            this.paramTabcontrol = new System.Windows.Forms.TabControl();
            this.beamTab = new System.Windows.Forms.TabPage();
            this.beamGroupBox = new System.Windows.Forms.GroupBox();
            this.invertChargeButton = new System.Windows.Forms.Button();
            this.energySpreadText = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.emittXtext = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.beamEText = new System.Windows.Forms.TextBox();
            this.emittYtext = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.beamELabel = new System.Windows.Forms.Label();
            this.currentText = new System.Windows.Forms.TextBox();
            this.pMassText = new System.Windows.Forms.TextBox();
            this.betaGroupbox = new System.Windows.Forms.GroupBox();
            this.writeBXYfileButton = new System.Windows.Forms.Button();
            this.browseBXYbutton = new System.Windows.Forms.Button();
            this.bxyFileNameText = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.synradTab = new System.Windows.Forms.TabPage();
            this.filenameExampleLabel = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.customPrefixText = new System.Windows.Forms.TextBox();
            this.eMaxText = new System.Windows.Forms.TextBox();
            this.ignoreSameSCheckBox = new System.Windows.Forms.CheckBox();
            this.label17 = new System.Windows.Forms.Label();
            this.dLtext = new System.Windows.Forms.TextBox();
            this.eMinLabel = new System.Windows.Forms.Label();
            this.eMinText = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.writeFilesButton = new System.Windows.Forms.Button();
            this.prefixCheckbox = new System.Windows.Forms.CheckBox();
            this.suffixCheckbox = new System.Windows.Forms.CheckBox();
            this.geomTab = new System.Windows.Forms.TabPage();
            this.label4 = new System.Windows.Forms.Label();
            this.facetStickingTextbox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.apertureFacetsCombo = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.taperCombo = new System.Windows.Forms.ComboBox();
            this.label31 = new System.Windows.Forms.Label();
            this.geomFileTextBox = new System.Windows.Forms.TextBox();
            this.label34 = new System.Windows.Forms.Label();
            this.browseGeomButton = new System.Windows.Forms.Button();
            this.curveStepText = new System.Windows.Forms.TextBox();
            this.writeGeomButton = new System.Windows.Forms.Button();
            this.label33 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.circleSidesText = new System.Windows.Forms.TextBox();
            this.readApertureButton = new System.Windows.Forms.Button();
            this.searchResultLabel = new System.Windows.Forms.Label();
            this.searchEndButton = new System.Windows.Forms.Button();
            this.searchBeginningButton = new System.Windows.Forms.Button();
            this.searchTermTextbox = new System.Windows.Forms.TextBox();
            this.label28 = new System.Windows.Forms.Label();
            this.readTwissButton = new System.Windows.Forms.Button();
            this.bAngleButton = new System.Windows.Forms.Button();
            this.prg_new = new System.Windows.Forms.ProgressBar();
            this.resetTableButton_new = new System.Windows.Forms.Button();
            this.pasteFromClipboardButton_new = new System.Windows.Forms.Button();
            this.addLineButton_new = new System.Windows.Forms.Button();
            this.remLineButton_new = new System.Windows.Forms.Button();
            this.refThetaText = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.refZText = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.refXText = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.getRefRowButton = new System.Windows.Forms.Button();
            this.refRowTextBox = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.sequenceGrid = new System.Windows.Forms.DataGridView();
            this.AVname = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.length = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.angle = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.B = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.grad = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.K1L = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.twissS = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.s = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.startX = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.thetaStart = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.startZ = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.zlimit = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.xOffset = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.yOffset = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.xAngle = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.yAngle = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.betaX = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.betaY = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.etaX = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.etaPX = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.alphaX = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.alphaY = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AptType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Aperture1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Aperture2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Aperture3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Aperture4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.apertureTab = new System.Windows.Forms.TabPage();
            this.reloadApertures = new System.Windows.Forms.Button();
            this.apertureInfoButton = new System.Windows.Forms.Button();
            this.label35 = new System.Windows.Forms.Label();
            this.apertureGrid = new System.Windows.Forms.DataGridView();
            this.apertureName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FileType = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.FileLocation = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FileBrowse = new System.Windows.Forms.DataGridViewButtonColumn();
            this.FileStatus = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FileWidth = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FileHeight = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FilePlot = new System.Windows.Forms.DataGridViewButtonColumn();
            this.FileCopy = new System.Windows.Forms.DataGridViewButtonColumn();
            this.sessionTab = new System.Windows.Forms.TabPage();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.sessionGrid = new System.Windows.Forms.DataGridView();
            this.Filename = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Modified = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LoadColumn = new System.Windows.Forms.DataGridViewButtonColumn();
            this.Overwrite = new System.Windows.Forms.DataGridViewButtonColumn();
            this.Rename = new System.Windows.Forms.DataGridViewButtonColumn();
            this.Trash = new System.Windows.Forms.DataGridViewButtonColumn();
            this.reloadSessionsButton = new System.Windows.Forms.Button();
            this.openSessionDirButton = new System.Windows.Forms.Button();
            this.emptyTrashButton = new System.Windows.Forms.Button();
            this.openTrashButton = new System.Windows.Forms.Button();
            this.saveCurrentGroup = new System.Windows.Forms.GroupBox();
            this.label5 = new System.Windows.Forms.Label();
            this.newSessionNameBox = new System.Windows.Forms.TextBox();
            this.saveSessionAsNewButton = new System.Windows.Forms.Button();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.hideTooltipsCheckbox = new System.Windows.Forms.CheckBox();
            this.statusBox = new System.Windows.Forms.TextBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newSubmenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.loadSubmenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadRecentSubmenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.saveSubmenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveAsSubmenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.exitSubmenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.libraryHint = new System.Windows.Forms.Label();
            this.tabControl.SuspendLayout();
            this.exportTab.SuspendLayout();
            this.outputGroup.SuspendLayout();
            this.paramTabcontrol.SuspendLayout();
            this.beamTab.SuspendLayout();
            this.beamGroupBox.SuspendLayout();
            this.betaGroupbox.SuspendLayout();
            this.synradTab.SuspendLayout();
            this.geomTab.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.sequenceGrid)).BeginInit();
            this.apertureTab.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.apertureGrid)).BeginInit();
            this.sessionTab.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.sessionGrid)).BeginInit();
            this.saveCurrentGroup.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl
            // 
            this.tabControl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl.Controls.Add(this.exportTab);
            this.tabControl.Controls.Add(this.apertureTab);
            this.tabControl.Controls.Add(this.sessionTab);
            this.tabControl.Location = new System.Drawing.Point(6, 27);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(915, 701);
            this.tabControl.TabIndex = 4;
            this.tabControl.Resize += new System.EventHandler(this.tabControl_Resize);
            // 
            // exportTab
            // 
            this.exportTab.Controls.Add(this.previewGeomButton);
            this.exportTab.Controls.Add(this.outputGroup);
            this.exportTab.Controls.Add(this.paramTabcontrol);
            this.exportTab.Controls.Add(this.readApertureButton);
            this.exportTab.Controls.Add(this.searchResultLabel);
            this.exportTab.Controls.Add(this.searchEndButton);
            this.exportTab.Controls.Add(this.searchBeginningButton);
            this.exportTab.Controls.Add(this.searchTermTextbox);
            this.exportTab.Controls.Add(this.label28);
            this.exportTab.Controls.Add(this.readTwissButton);
            this.exportTab.Controls.Add(this.bAngleButton);
            this.exportTab.Controls.Add(this.prg_new);
            this.exportTab.Controls.Add(this.resetTableButton_new);
            this.exportTab.Controls.Add(this.pasteFromClipboardButton_new);
            this.exportTab.Controls.Add(this.addLineButton_new);
            this.exportTab.Controls.Add(this.remLineButton_new);
            this.exportTab.Controls.Add(this.refThetaText);
            this.exportTab.Controls.Add(this.label12);
            this.exportTab.Controls.Add(this.refZText);
            this.exportTab.Controls.Add(this.label1);
            this.exportTab.Controls.Add(this.refXText);
            this.exportTab.Controls.Add(this.label14);
            this.exportTab.Controls.Add(this.getRefRowButton);
            this.exportTab.Controls.Add(this.refRowTextBox);
            this.exportTab.Controls.Add(this.label15);
            this.exportTab.Controls.Add(this.sequenceGrid);
            this.exportTab.Location = new System.Drawing.Point(4, 22);
            this.exportTab.Name = "exportTab";
            this.exportTab.Padding = new System.Windows.Forms.Padding(3);
            this.exportTab.Size = new System.Drawing.Size(907, 675);
            this.exportTab.TabIndex = 2;
            this.exportTab.Text = "Export result to Synrad format";
            this.exportTab.UseVisualStyleBackColor = true;
            // 
            // previewGeomButton
            // 
            this.previewGeomButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.previewGeomButton.Location = new System.Drawing.Point(789, 350);
            this.previewGeomButton.Name = "previewGeomButton";
            this.previewGeomButton.Size = new System.Drawing.Size(103, 23);
            this.previewGeomButton.TabIndex = 135;
            this.previewGeomButton.Text = "Preview geometry";
            this.previewGeomButton.UseVisualStyleBackColor = true;
            this.previewGeomButton.Click += new System.EventHandler(this.previewGeomButton_Click);
            // 
            // outputGroup
            // 
            this.outputGroup.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.outputGroup.Controls.Add(this.dirComboBox);
            this.outputGroup.Controls.Add(this.browseButton);
            this.outputGroup.Controls.Add(this.label30);
            this.outputGroup.Controls.Add(this.getStartLineButton);
            this.outputGroup.Controls.Add(this.startLineLabel);
            this.outputGroup.Controls.Add(this.label13);
            this.outputGroup.Controls.Add(this.startRowText);
            this.outputGroup.Controls.Add(this.endRowText);
            this.outputGroup.Controls.Add(this.getEndLineButton);
            this.outputGroup.Location = new System.Drawing.Point(6, 414);
            this.outputGroup.Name = "outputGroup";
            this.outputGroup.Size = new System.Drawing.Size(892, 75);
            this.outputGroup.TabIndex = 112;
            this.outputGroup.TabStop = false;
            this.outputGroup.Text = "File output";
            // 
            // dirComboBox
            // 
            this.dirComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dirComboBox.FormattingEnabled = true;
            this.dirComboBox.Location = new System.Drawing.Point(79, 38);
            this.dirComboBox.Margin = new System.Windows.Forms.Padding(2);
            this.dirComboBox.Name = "dirComboBox";
            this.dirComboBox.Size = new System.Drawing.Size(549, 21);
            this.dirComboBox.TabIndex = 118;
            this.toolTip1.SetToolTip(this.dirComboBox, resources.GetString("dirComboBox.ToolTip"));
            this.dirComboBox.SelectedIndexChanged += new System.EventHandler(this.dirComboBox_SelectedIndexChanged);
            this.dirComboBox.TextUpdate += new System.EventHandler(this.dirComboBox_TextUpdate);
            // 
            // browseButton
            // 
            this.browseButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.browseButton.Location = new System.Drawing.Point(631, 36);
            this.browseButton.Name = "browseButton";
            this.browseButton.Size = new System.Drawing.Size(33, 23);
            this.browseButton.TabIndex = 61;
            this.browseButton.Text = "...";
            this.browseButton.UseVisualStyleBackColor = true;
            this.browseButton.Click += new System.EventHandler(this.browseButton_Click);
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(8, 40);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(56, 13);
            this.label30.TabIndex = 117;
            this.label30.Text = "Output dir:";
            // 
            // getStartLineButton
            // 
            this.getStartLineButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.getStartLineButton.Location = new System.Drawing.Point(783, 20);
            this.getStartLineButton.Name = "getStartLineButton";
            this.getStartLineButton.Size = new System.Drawing.Size(103, 21);
            this.getStartLineButton.TabIndex = 107;
            this.getStartLineButton.Text = "Get Selected";
            this.getStartLineButton.UseVisualStyleBackColor = true;
            this.getStartLineButton.Click += new System.EventHandler(this.getStartLineButton_Click);
            // 
            // startLineLabel
            // 
            this.startLineLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.startLineLabel.AutoSize = true;
            this.startLineLabel.Location = new System.Drawing.Point(686, 24);
            this.startLineLabel.Name = "startLineLabel";
            this.startLineLabel.Size = new System.Drawing.Size(52, 13);
            this.startLineLabel.TabIndex = 103;
            this.startLineLabel.Text = "Start row:";
            this.startLineLabel.Click += new System.EventHandler(this.startLineLabel_Click);
            // 
            // label13
            // 
            this.label13.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(686, 47);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(49, 13);
            this.label13.TabIndex = 104;
            this.label13.Text = "End row:";
            this.label13.Click += new System.EventHandler(this.label13_Click);
            // 
            // startRowText
            // 
            this.startRowText.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.startRowText.Location = new System.Drawing.Point(741, 21);
            this.startRowText.Name = "startRowText";
            this.startRowText.Size = new System.Drawing.Size(37, 20);
            this.startRowText.TabIndex = 105;
            this.toolTip1.SetToolTip(this.startRowText, "Only rows starting from this row id will be exported. Leave empty to use first li" +
        "ne.");
            this.startRowText.TextChanged += new System.EventHandler(this.startLineText_TextChanged);
            // 
            // endRowText
            // 
            this.endRowText.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.endRowText.Location = new System.Drawing.Point(741, 44);
            this.endRowText.Name = "endRowText";
            this.endRowText.Size = new System.Drawing.Size(37, 20);
            this.endRowText.TabIndex = 106;
            this.toolTip1.SetToolTip(this.endRowText, "Only rows up to this row id will be exported. Leave empty to use last line");
            this.endRowText.TextChanged += new System.EventHandler(this.endLineText_TextChanged);
            // 
            // getEndLineButton
            // 
            this.getEndLineButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.getEndLineButton.Location = new System.Drawing.Point(783, 43);
            this.getEndLineButton.Name = "getEndLineButton";
            this.getEndLineButton.Size = new System.Drawing.Size(103, 21);
            this.getEndLineButton.TabIndex = 108;
            this.getEndLineButton.Text = "Get Selected";
            this.getEndLineButton.UseVisualStyleBackColor = true;
            this.getEndLineButton.Click += new System.EventHandler(this.getEndLineButton_Click);
            // 
            // paramTabcontrol
            // 
            this.paramTabcontrol.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.paramTabcontrol.Controls.Add(this.beamTab);
            this.paramTabcontrol.Controls.Add(this.synradTab);
            this.paramTabcontrol.Controls.Add(this.geomTab);
            this.paramTabcontrol.Location = new System.Drawing.Point(6, 495);
            this.paramTabcontrol.Name = "paramTabcontrol";
            this.paramTabcontrol.SelectedIndex = 0;
            this.paramTabcontrol.Size = new System.Drawing.Size(896, 174);
            this.paramTabcontrol.TabIndex = 130;
            this.paramTabcontrol.SelectedIndexChanged += new System.EventHandler(this.paramTabcontrol_SelectedIndexChanged);
            // 
            // beamTab
            // 
            this.beamTab.Controls.Add(this.beamGroupBox);
            this.beamTab.Controls.Add(this.betaGroupbox);
            this.beamTab.Location = new System.Drawing.Point(4, 22);
            this.beamTab.Name = "beamTab";
            this.beamTab.Padding = new System.Windows.Forms.Padding(3);
            this.beamTab.Size = new System.Drawing.Size(888, 148);
            this.beamTab.TabIndex = 0;
            this.beamTab.Text = "Beam properties";
            this.beamTab.UseVisualStyleBackColor = true;
            // 
            // beamGroupBox
            // 
            this.beamGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.beamGroupBox.Controls.Add(this.invertChargeButton);
            this.beamGroupBox.Controls.Add(this.energySpreadText);
            this.beamGroupBox.Controls.Add(this.label23);
            this.beamGroupBox.Controls.Add(this.emittXtext);
            this.beamGroupBox.Controls.Add(this.label18);
            this.beamGroupBox.Controls.Add(this.label22);
            this.beamGroupBox.Controls.Add(this.beamEText);
            this.beamGroupBox.Controls.Add(this.emittYtext);
            this.beamGroupBox.Controls.Add(this.label19);
            this.beamGroupBox.Controls.Add(this.label21);
            this.beamGroupBox.Controls.Add(this.beamELabel);
            this.beamGroupBox.Controls.Add(this.currentText);
            this.beamGroupBox.Controls.Add(this.pMassText);
            this.beamGroupBox.Location = new System.Drawing.Point(6, 6);
            this.beamGroupBox.Name = "beamGroupBox";
            this.beamGroupBox.Size = new System.Drawing.Size(870, 71);
            this.beamGroupBox.TabIndex = 111;
            this.beamGroupBox.TabStop = false;
            this.beamGroupBox.Text = "Beam and particle properties";
            // 
            // invertChargeButton
            // 
            this.invertChargeButton.Location = new System.Drawing.Point(636, 19);
            this.invertChargeButton.Name = "invertChargeButton";
            this.invertChargeButton.Size = new System.Drawing.Size(228, 20);
            this.invertChargeButton.TabIndex = 91;
            this.invertChargeButton.Text = "Invert charge, bending angles and gradients";
            this.invertChargeButton.UseVisualStyleBackColor = true;
            this.invertChargeButton.Click += new System.EventHandler(this.invertChargeButton_Click);
            // 
            // energySpreadText
            // 
            this.energySpreadText.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.energySpreadText.Location = new System.Drawing.Point(318, 42);
            this.energySpreadText.Name = "energySpreadText";
            this.energySpreadText.Size = new System.Drawing.Size(100, 20);
            this.energySpreadText.TabIndex = 90;
            this.energySpreadText.Text = "0.1";
            this.toolTip1.SetToolTip(this.energySpreadText, "Energy spread of the particles. It\'s in percentage, so 1% = 0.01.\r\nAffects Synrad" +
        " beam size.\r\nIf you don\'t have this info, set to 0.");
            this.energySpreadText.TextChanged += new System.EventHandler(this.energySpreadText_TextChanged);
            // 
            // label23
            // 
            this.label23.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(23, 22);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(84, 13);
            this.label23.TabIndex = 79;
            this.label23.Text = "Emittance_X (m)";
            this.label23.Click += new System.EventHandler(this.label23_Click);
            // 
            // emittXtext
            // 
            this.emittXtext.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.emittXtext.Location = new System.Drawing.Point(113, 19);
            this.emittXtext.Name = "emittXtext";
            this.emittXtext.Size = new System.Drawing.Size(100, 20);
            this.emittXtext.TabIndex = 80;
            this.emittXtext.Text = "1E-9";
            this.toolTip1.SetToolTip(this.emittXtext, "Beam horizontal emittance in m.\r\n\r\nAffects Synrad beam size, if you don\'t know, s" +
        "et to 0 for ideal (0-width) beam.");
            this.emittXtext.TextChanged += new System.EventHandler(this.emittXtext_TextChanged);
            // 
            // label18
            // 
            this.label18.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(228, 45);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(91, 13);
            this.label18.TabIndex = 89;
            this.label18.Text = "energy spread (%)";
            this.label18.Click += new System.EventHandler(this.label18_Click);
            // 
            // label22
            // 
            this.label22.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(23, 45);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(84, 13);
            this.label22.TabIndex = 81;
            this.label22.Text = "Emittance_Y (m)";
            this.label22.Click += new System.EventHandler(this.label22_Click);
            // 
            // beamEText
            // 
            this.beamEText.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.beamEText.Location = new System.Drawing.Point(515, 43);
            this.beamEText.Name = "beamEText";
            this.beamEText.Size = new System.Drawing.Size(100, 20);
            this.beamEText.TabIndex = 88;
            this.beamEText.Text = "1";
            this.beamEText.TextChanged += new System.EventHandler(this.beamEText_TextChanged);
            // 
            // emittYtext
            // 
            this.emittYtext.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.emittYtext.Location = new System.Drawing.Point(113, 42);
            this.emittYtext.Name = "emittYtext";
            this.emittYtext.Size = new System.Drawing.Size(100, 20);
            this.emittYtext.TabIndex = 82;
            this.emittYtext.Text = "1E-9";
            this.toolTip1.SetToolTip(this.emittYtext, "Beam vertical emittance in m\r\n\r\nAffects Synrad beam size, if you don\'t know, set " +
        "to 0 for ideal (0-width) beam.");
            this.emittYtext.TextChanged += new System.EventHandler(this.emittYtext_TextChanged);
            // 
            // label19
            // 
            this.label19.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(228, 22);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(82, 13);
            this.label19.TabIndex = 85;
            this.label19.Text = "pMass(GeV/c2)";
            this.label19.Click += new System.EventHandler(this.label19_Click);
            // 
            // label21
            // 
            this.label21.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(425, 22);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(64, 13);
            this.label21.TabIndex = 83;
            this.label21.Text = "current (mA)";
            this.label21.Click += new System.EventHandler(this.label21_Click);
            // 
            // beamELabel
            // 
            this.beamELabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.beamELabel.AutoSize = true;
            this.beamELabel.Location = new System.Drawing.Point(425, 46);
            this.beamELabel.Name = "beamELabel";
            this.beamELabel.Size = new System.Drawing.Size(74, 13);
            this.beamELabel.TabIndex = 87;
            this.beamELabel.Text = "Beam E (GeV)";
            this.beamELabel.Click += new System.EventHandler(this.beamELabel_Click);
            // 
            // currentText
            // 
            this.currentText.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.currentText.Location = new System.Drawing.Point(515, 19);
            this.currentText.Name = "currentText";
            this.currentText.Size = new System.Drawing.Size(100, 20);
            this.currentText.TabIndex = 84;
            this.currentText.Text = "1";
            this.toolTip1.SetToolTip(this.currentText, "Beam current. Warning: it uses mA, not A");
            this.currentText.TextChanged += new System.EventHandler(this.currentText_TextChanged);
            // 
            // pMassText
            // 
            this.pMassText.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.pMassText.Location = new System.Drawing.Point(318, 19);
            this.pMassText.Name = "pMassText";
            this.pMassText.Size = new System.Drawing.Size(100, 20);
            this.pMassText.TabIndex = 86;
            this.pMassText.Text = "-0.00051099891";
            this.toolTip1.SetToolTip(this.pMassText, "Rest mass and charge of beam particle.\r\n\r\nelectron: -0.00051099895\r\npositron: 0.0" +
        "0051099895\r\nproton: 0.938272");
            this.pMassText.TextChanged += new System.EventHandler(this.pMassText_TextChanged);
            // 
            // betaGroupbox
            // 
            this.betaGroupbox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.betaGroupbox.Controls.Add(this.writeBXYfileButton);
            this.betaGroupbox.Controls.Add(this.browseBXYbutton);
            this.betaGroupbox.Controls.Add(this.bxyFileNameText);
            this.betaGroupbox.Controls.Add(this.label20);
            this.betaGroupbox.Location = new System.Drawing.Point(6, 93);
            this.betaGroupbox.Name = "betaGroupbox";
            this.betaGroupbox.Size = new System.Drawing.Size(870, 52);
            this.betaGroupbox.TabIndex = 110;
            this.betaGroupbox.TabStop = false;
            this.betaGroupbox.Text = "Lattice functions";
            // 
            // writeBXYfileButton
            // 
            this.writeBXYfileButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.writeBXYfileButton.Location = new System.Drawing.Point(636, 21);
            this.writeBXYfileButton.Name = "writeBXYfileButton";
            this.writeBXYfileButton.Size = new System.Drawing.Size(103, 23);
            this.writeBXYfileButton.TabIndex = 109;
            this.writeBXYfileButton.Text = "write BXY file";
            this.toolTip1.SetToolTip(this.writeBXYfileButton, "Write BXY file, overwrite if exists");
            this.writeBXYfileButton.UseVisualStyleBackColor = true;
            this.writeBXYfileButton.Click += new System.EventHandler(this.writeBXYfileButton_Click);
            // 
            // browseBXYbutton
            // 
            this.browseBXYbutton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.browseBXYbutton.Location = new System.Drawing.Point(591, 21);
            this.browseBXYbutton.Name = "browseBXYbutton";
            this.browseBXYbutton.Size = new System.Drawing.Size(39, 23);
            this.browseBXYbutton.TabIndex = 72;
            this.browseBXYbutton.Text = "...";
            this.browseBXYbutton.UseVisualStyleBackColor = true;
            this.browseBXYbutton.Click += new System.EventHandler(this.browseBXYbutton_Click);
            // 
            // bxyFileNameText
            // 
            this.bxyFileNameText.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.bxyFileNameText.Location = new System.Drawing.Point(110, 23);
            this.bxyFileNameText.Name = "bxyFileNameText";
            this.bxyFileNameText.Size = new System.Drawing.Size(471, 20);
            this.bxyFileNameText.TabIndex = 71;
            this.bxyFileNameText.Text = "beam.bxy";
            this.toolTip1.SetToolTip(this.bxyFileNameText, "Name of the BXY about to be exported. Overwrites if target exists");
            // 
            // label20
            // 
            this.label20.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(36, 26);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(68, 13);
            this.label20.TabIndex = 70;
            this.label20.Text = "bxy filename:";
            // 
            // synradTab
            // 
            this.synradTab.Controls.Add(this.filenameExampleLabel);
            this.synradTab.Controls.Add(this.label7);
            this.synradTab.Controls.Add(this.label6);
            this.synradTab.Controls.Add(this.customPrefixText);
            this.synradTab.Controls.Add(this.eMaxText);
            this.synradTab.Controls.Add(this.ignoreSameSCheckBox);
            this.synradTab.Controls.Add(this.label17);
            this.synradTab.Controls.Add(this.dLtext);
            this.synradTab.Controls.Add(this.eMinLabel);
            this.synradTab.Controls.Add(this.eMinText);
            this.synradTab.Controls.Add(this.label16);
            this.synradTab.Controls.Add(this.writeFilesButton);
            this.synradTab.Controls.Add(this.prefixCheckbox);
            this.synradTab.Controls.Add(this.suffixCheckbox);
            this.synradTab.Location = new System.Drawing.Point(4, 22);
            this.synradTab.Name = "synradTab";
            this.synradTab.Padding = new System.Windows.Forms.Padding(3);
            this.synradTab.Size = new System.Drawing.Size(888, 148);
            this.synradTab.TabIndex = 2;
            this.synradTab.Text = "Synrad param files";
            this.synradTab.UseVisualStyleBackColor = true;
            // 
            // filenameExampleLabel
            // 
            this.filenameExampleLabel.AutoSize = true;
            this.filenameExampleLabel.Location = new System.Drawing.Point(110, 107);
            this.filenameExampleLabel.Name = "filenameExampleLabel";
            this.filenameExampleLabel.Size = new System.Drawing.Size(53, 13);
            this.filenameExampleLabel.TabIndex = 133;
            this.filenameExampleLabel.Text = "Q1.param";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(14, 107);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(94, 13);
            this.label7.TabIndex = 132;
            this.label7.Text = "Filename example:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(127, 87);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(68, 13);
            this.label6.TabIndex = 131;
            this.label6.Text = "add to prefix:";
            // 
            // customPrefixText
            // 
            this.customPrefixText.Location = new System.Drawing.Point(195, 81);
            this.customPrefixText.Name = "customPrefixText";
            this.customPrefixText.Size = new System.Drawing.Size(100, 20);
            this.customPrefixText.TabIndex = 130;
            this.customPrefixText.TextChanged += new System.EventHandler(this.customPrefixText_TextChanged);
            // 
            // eMaxText
            // 
            this.eMaxText.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.eMaxText.Location = new System.Drawing.Point(309, 36);
            this.eMaxText.Name = "eMaxText";
            this.eMaxText.Size = new System.Drawing.Size(100, 20);
            this.eMaxText.TabIndex = 85;
            this.eMaxText.Text = "50E6";
            this.toolTip1.SetToolTip(this.eMaxText, resources.GetString("eMaxText.ToolTip"));
            // 
            // ignoreSameSCheckBox
            // 
            this.ignoreSameSCheckBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.ignoreSameSCheckBox.Checked = true;
            this.ignoreSameSCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ignoreSameSCheckBox.Location = new System.Drawing.Point(17, 62);
            this.ignoreSameSCheckBox.Name = "ignoreSameSCheckBox";
            this.ignoreSameSCheckBox.Size = new System.Drawing.Size(447, 17);
            this.ignoreSameSCheckBox.TabIndex = 129;
            this.ignoreSameSCheckBox.Text = "Ignore consecutive lines with same S coordinate (duplicate and 0-length elements)" +
    "";
            this.ignoreSameSCheckBox.UseVisualStyleBackColor = true;
            // 
            // label17
            // 
            this.label17.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(14, 14);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(82, 13);
            this.label17.TabIndex = 80;
            this.label17.Text = "step length (cm)";
            // 
            // dLtext
            // 
            this.dLtext.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.dLtext.Location = new System.Drawing.Point(104, 10);
            this.dLtext.Name = "dLtext";
            this.dLtext.Size = new System.Drawing.Size(100, 20);
            this.dLtext.TabIndex = 81;
            this.dLtext.Text = "1";
            this.toolTip1.SetToolTip(this.dLtext, "Synrad will calculate a point every step length. Aim for not more than a few thou" +
        "sand points per element.");
            // 
            // eMinLabel
            // 
            this.eMinLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.eMinLabel.AutoSize = true;
            this.eMinLabel.Location = new System.Drawing.Point(14, 40);
            this.eMinLabel.Name = "eMinLabel";
            this.eMinLabel.Size = new System.Drawing.Size(89, 13);
            this.eMinLabel.TabIndex = 82;
            this.eMinLabel.Text = "Photon Emin (eV)";
            // 
            // eMinText
            // 
            this.eMinText.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.eMinText.Location = new System.Drawing.Point(104, 36);
            this.eMinText.Name = "eMinText";
            this.eMinText.Size = new System.Drawing.Size(100, 20);
            this.eMinText.TabIndex = 83;
            this.eMinText.Text = "4";
            this.toolTip1.SetToolTip(this.eMinText, "Synrad will use this to generate the photon energy.\r\nBy default 4eV (lowest power" +
        " which can still generate photon stimulated gas desorption).");
            // 
            // label16
            // 
            this.label16.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(219, 40);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(92, 13);
            this.label16.TabIndex = 84;
            this.label16.Text = "Photon eMax (eV)";
            // 
            // writeFilesButton
            // 
            this.writeFilesButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.writeFilesButton.Location = new System.Drawing.Point(744, 107);
            this.writeFilesButton.Name = "writeFilesButton";
            this.writeFilesButton.Size = new System.Drawing.Size(138, 33);
            this.writeFilesButton.TabIndex = 63;
            this.writeFilesButton.Text = "write param file(s)";
            this.toolTip1.SetToolTip(this.writeFilesButton, "Write a param and if quadrupole a MAG file for each magnetic element.\r\nWill overw" +
        "rite files in destination folder.\r\n");
            this.writeFilesButton.UseVisualStyleBackColor = true;
            this.writeFilesButton.Click += new System.EventHandler(this.writeParamFilesButton_Click);
            // 
            // prefixCheckbox
            // 
            this.prefixCheckbox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.prefixCheckbox.Checked = true;
            this.prefixCheckbox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.prefixCheckbox.Location = new System.Drawing.Point(16, 86);
            this.prefixCheckbox.Name = "prefixCheckbox";
            this.prefixCheckbox.Size = new System.Drawing.Size(105, 17);
            this.prefixCheckbox.TabIndex = 86;
            this.prefixCheckbox.Text = "file number prefix";
            this.prefixCheckbox.UseVisualStyleBackColor = true;
            this.prefixCheckbox.CheckedChanged += new System.EventHandler(this.prefixCheckbox_CheckedChanged);
            // 
            // suffixCheckbox
            // 
            this.suffixCheckbox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.suffixCheckbox.Location = new System.Drawing.Point(309, 83);
            this.suffixCheckbox.Name = "suffixCheckbox";
            this.suffixCheckbox.Size = new System.Drawing.Size(105, 17);
            this.suffixCheckbox.TabIndex = 111;
            this.suffixCheckbox.Text = "s position suffix";
            this.suffixCheckbox.UseVisualStyleBackColor = true;
            this.suffixCheckbox.CheckedChanged += new System.EventHandler(this.suffixCheckbox_CheckedChanged);
            // 
            // geomTab
            // 
            this.geomTab.Controls.Add(this.label4);
            this.geomTab.Controls.Add(this.facetStickingTextbox);
            this.geomTab.Controls.Add(this.label3);
            this.geomTab.Controls.Add(this.apertureFacetsCombo);
            this.geomTab.Controls.Add(this.label2);
            this.geomTab.Controls.Add(this.taperCombo);
            this.geomTab.Controls.Add(this.label31);
            this.geomTab.Controls.Add(this.geomFileTextBox);
            this.geomTab.Controls.Add(this.label34);
            this.geomTab.Controls.Add(this.browseGeomButton);
            this.geomTab.Controls.Add(this.curveStepText);
            this.geomTab.Controls.Add(this.writeGeomButton);
            this.geomTab.Controls.Add(this.label33);
            this.geomTab.Controls.Add(this.label32);
            this.geomTab.Controls.Add(this.circleSidesText);
            this.geomTab.Location = new System.Drawing.Point(4, 22);
            this.geomTab.Name = "geomTab";
            this.geomTab.Padding = new System.Windows.Forms.Padding(3);
            this.geomTab.Size = new System.Drawing.Size(888, 148);
            this.geomTab.TabIndex = 1;
            this.geomTab.Text = "Synrad/Molflow Geometry";
            this.geomTab.UseVisualStyleBackColor = true;
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(457, 42);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(78, 13);
            this.label4.TabIndex = 132;
            this.label4.Text = "Sticking factor:";
            // 
            // facetStickingTextbox
            // 
            this.facetStickingTextbox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.facetStickingTextbox.Location = new System.Drawing.Point(539, 38);
            this.facetStickingTextbox.Name = "facetStickingTextbox";
            this.facetStickingTextbox.Size = new System.Drawing.Size(86, 20);
            this.facetStickingTextbox.TabIndex = 133;
            this.facetStickingTextbox.Text = "1.0";
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 118);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(82, 13);
            this.label3.TabIndex = 131;
            this.label3.Text = "Aperture facets:";
            // 
            // apertureFacetsCombo
            // 
            this.apertureFacetsCombo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.apertureFacetsCombo.FormattingEnabled = true;
            this.apertureFacetsCombo.Items.AddRange(new object[] {
            "None",
            "First and last (end caps)",
            "All but curve intermediate steps",
            "All"});
            this.apertureFacetsCombo.Location = new System.Drawing.Point(117, 115);
            this.apertureFacetsCombo.Name = "apertureFacetsCombo";
            this.apertureFacetsCombo.Size = new System.Drawing.Size(143, 21);
            this.apertureFacetsCombo.TabIndex = 130;
            this.apertureFacetsCombo.Text = "First and last (end caps)";
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 91);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(95, 13);
            this.label2.TabIndex = 129;
            this.label2.Text = "Aperture transition:";
            // 
            // taperCombo
            // 
            this.taperCombo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.taperCombo.FormattingEnabled = true;
            this.taperCombo.Items.AddRange(new object[] {
            "Continous (taper)",
            "Immediate (step)"});
            this.taperCombo.Location = new System.Drawing.Point(117, 88);
            this.taperCombo.Name = "taperCombo";
            this.taperCombo.Size = new System.Drawing.Size(143, 21);
            this.taperCombo.TabIndex = 128;
            this.taperCombo.Text = "Continous (taper)";
            // 
            // label31
            // 
            this.label31.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(3, 16);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(52, 13);
            this.label31.TabIndex = 118;
            this.label31.Text = "Filename:";
            // 
            // geomFileTextBox
            // 
            this.geomFileTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.geomFileTextBox.Location = new System.Drawing.Point(117, 12);
            this.geomFileTextBox.Name = "geomFileTextBox";
            this.geomFileTextBox.Size = new System.Drawing.Size(278, 20);
            this.geomFileTextBox.TabIndex = 119;
            this.geomFileTextBox.Text = "Geometry.txt";
            // 
            // label34
            // 
            this.label34.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(219, 65);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(41, 13);
            this.label34.TabIndex = 127;
            this.label34.Text = "radians";
            // 
            // browseGeomButton
            // 
            this.browseGeomButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.browseGeomButton.Location = new System.Drawing.Point(401, 10);
            this.browseGeomButton.Name = "browseGeomButton";
            this.browseGeomButton.Size = new System.Drawing.Size(32, 23);
            this.browseGeomButton.TabIndex = 120;
            this.browseGeomButton.Text = "...";
            this.browseGeomButton.UseVisualStyleBackColor = true;
            this.browseGeomButton.Click += new System.EventHandler(this.browseGeomButton_Click);
            // 
            // curveStepText
            // 
            this.curveStepText.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.curveStepText.Location = new System.Drawing.Point(117, 62);
            this.curveStepText.Name = "curveStepText";
            this.curveStepText.Size = new System.Drawing.Size(86, 20);
            this.curveStepText.TabIndex = 126;
            this.curveStepText.Text = "0.001";
            // 
            // writeGeomButton
            // 
            this.writeGeomButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.writeGeomButton.Location = new System.Drawing.Point(741, 109);
            this.writeGeomButton.Name = "writeGeomButton";
            this.writeGeomButton.Size = new System.Drawing.Size(141, 31);
            this.writeGeomButton.TabIndex = 121;
            this.writeGeomButton.Text = "Write geometry file";
            this.writeGeomButton.UseVisualStyleBackColor = true;
            this.writeGeomButton.Click += new System.EventHandler(this.writeGeomButton_Click);
            // 
            // label33
            // 
            this.label33.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(3, 65);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(86, 13);
            this.label33.TabIndex = 125;
            this.label33.Text = "curve step every";
            // 
            // label32
            // 
            this.label32.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(4, 42);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(63, 13);
            this.label32.TabIndex = 122;
            this.label32.Text = "Circle sides:";
            // 
            // circleSidesText
            // 
            this.circleSidesText.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.circleSidesText.Location = new System.Drawing.Point(117, 38);
            this.circleSidesText.Name = "circleSidesText";
            this.circleSidesText.Size = new System.Drawing.Size(86, 20);
            this.circleSidesText.TabIndex = 123;
            this.circleSidesText.Text = "36";
            // 
            // readApertureButton
            // 
            this.readApertureButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.readApertureButton.Location = new System.Drawing.Point(544, 321);
            this.readApertureButton.Name = "readApertureButton";
            this.readApertureButton.Size = new System.Drawing.Size(98, 23);
            this.readApertureButton.TabIndex = 124;
            this.readApertureButton.Text = "Read apertures";
            this.toolTip1.SetToolTip(this.readApertureButton, resources.GetString("readApertureButton.ToolTip"));
            this.readApertureButton.UseVisualStyleBackColor = true;
            this.readApertureButton.Click += new System.EventHandler(this.readApertureButton_Click);
            // 
            // searchResultLabel
            // 
            this.searchResultLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.searchResultLabel.AutoSize = true;
            this.searchResultLabel.Location = new System.Drawing.Point(546, 644);
            this.searchResultLabel.Name = "searchResultLabel";
            this.searchResultLabel.Size = new System.Drawing.Size(0, 13);
            this.searchResultLabel.TabIndex = 116;
            // 
            // searchEndButton
            // 
            this.searchEndButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.searchEndButton.Location = new System.Drawing.Point(427, 378);
            this.searchEndButton.Name = "searchEndButton";
            this.searchEndButton.Size = new System.Drawing.Size(115, 23);
            this.searchEndButton.TabIndex = 115;
            this.searchEndButton.Text = "Search end";
            this.toolTip1.SetToolTip(this.searchEndButton, "Jumps to the first row with text as end");
            this.searchEndButton.UseVisualStyleBackColor = true;
            this.searchEndButton.Click += new System.EventHandler(this.searchEndButton_Click);
            // 
            // searchBeginningButton
            // 
            this.searchBeginningButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.searchBeginningButton.Location = new System.Drawing.Point(309, 378);
            this.searchBeginningButton.Name = "searchBeginningButton";
            this.searchBeginningButton.Size = new System.Drawing.Size(112, 23);
            this.searchBeginningButton.TabIndex = 114;
            this.searchBeginningButton.Text = "Search beginning";
            this.toolTip1.SetToolTip(this.searchBeginningButton, "Jumps to the first row with text as beginning");
            this.searchBeginningButton.UseVisualStyleBackColor = true;
            this.searchBeginningButton.Click += new System.EventHandler(this.searchBeginningButton_Click);
            // 
            // searchTermTextbox
            // 
            this.searchTermTextbox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.searchTermTextbox.Location = new System.Drawing.Point(86, 381);
            this.searchTermTextbox.Name = "searchTermTextbox";
            this.searchTermTextbox.Size = new System.Drawing.Size(219, 20);
            this.searchTermTextbox.TabIndex = 113;
            this.searchTermTextbox.TextChanged += new System.EventHandler(this.searchTermTextbox_TextChanged);
            // 
            // label28
            // 
            this.label28.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(16, 384);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(64, 13);
            this.label28.TabIndex = 112;
            this.label28.Text = "Search text:";
            // 
            // readTwissButton
            // 
            this.readTwissButton.AccessibleDescription = "";
            this.readTwissButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.readTwissButton.Location = new System.Drawing.Point(427, 321);
            this.readTwissButton.Name = "readTwissButton";
            this.readTwissButton.Size = new System.Drawing.Size(115, 23);
            this.readTwissButton.TabIndex = 110;
            this.readTwissButton.Text = "Read TWISS file";
            this.toolTip1.SetToolTip(this.readTwissButton, "Reads a MADX-generated Twiss file and imports beam properties (if found) and sequ" +
        "ence of elements, along with lattice functions.\r\nSee the documentation on which " +
        "columns the Twiss file requires.\r\n");
            this.readTwissButton.UseVisualStyleBackColor = true;
            this.readTwissButton.Click += new System.EventHandler(this.readTwissButton_Click);
            // 
            // bAngleButton
            // 
            this.bAngleButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.bAngleButton.Location = new System.Drawing.Point(666, 377);
            this.bAngleButton.Name = "bAngleButton";
            this.bAngleButton.Size = new System.Drawing.Size(225, 24);
            this.bAngleButton.TabIndex = 102;
            this.bAngleButton.Text = "Convert B<->Angle and Grad<->K1L ";
            this.toolTip1.SetToolTip(this.bAngleButton, resources.GetString("bAngleButton.ToolTip"));
            this.bAngleButton.UseVisualStyleBackColor = true;
            this.bAngleButton.Click += new System.EventHandler(this.bAngleButton_Click);
            // 
            // prg_new
            // 
            this.prg_new.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.prg_new.Location = new System.Drawing.Point(648, 321);
            this.prg_new.Name = "prg_new";
            this.prg_new.Size = new System.Drawing.Size(123, 23);
            this.prg_new.TabIndex = 101;
            this.prg_new.Visible = false;
            // 
            // resetTableButton_new
            // 
            this.resetTableButton_new.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.resetTableButton_new.Location = new System.Drawing.Point(789, 321);
            this.resetTableButton_new.Name = "resetTableButton_new";
            this.resetTableButton_new.Size = new System.Drawing.Size(103, 23);
            this.resetTableButton_new.TabIndex = 100;
            this.resetTableButton_new.Text = "Reset table";
            this.toolTip1.SetToolTip(this.resetTableButton_new, "Clear all table contents");
            this.resetTableButton_new.UseVisualStyleBackColor = true;
            this.resetTableButton_new.Click += new System.EventHandler(this.resetTableButton_new_Click);
            // 
            // pasteFromClipboardButton_new
            // 
            this.pasteFromClipboardButton_new.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.pasteFromClipboardButton_new.Location = new System.Drawing.Point(222, 321);
            this.pasteFromClipboardButton_new.Name = "pasteFromClipboardButton_new";
            this.pasteFromClipboardButton_new.Size = new System.Drawing.Size(199, 23);
            this.pasteFromClipboardButton_new.TabIndex = 99;
            this.pasteFromClipboardButton_new.Text = "Paste from clipboard";
            this.toolTip1.SetToolTip(this.pasteFromClipboardButton_new, "Allows to paste tables into the main table above.\r\n\r\nPasting starts from the sele" +
        "cted cell, to the right and bottom. (The selected cell will be the top left of t" +
        "he paste area.)");
            this.pasteFromClipboardButton_new.UseVisualStyleBackColor = true;
            this.pasteFromClipboardButton_new.Click += new System.EventHandler(this.pasteFromClipboardButton_new_Click);
            // 
            // addLineButton_new
            // 
            this.addLineButton_new.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.addLineButton_new.Location = new System.Drawing.Point(14, 321);
            this.addLineButton_new.Name = "addLineButton_new";
            this.addLineButton_new.Size = new System.Drawing.Size(90, 23);
            this.addLineButton_new.TabIndex = 97;
            this.addLineButton_new.Text = "Add row(s)";
            this.toolTip1.SetToolTip(this.addLineButton_new, resources.GetString("addLineButton_new.ToolTip"));
            this.addLineButton_new.UseVisualStyleBackColor = true;
            this.addLineButton_new.Click += new System.EventHandler(this.addLineButton_new_Click);
            // 
            // remLineButton_new
            // 
            this.remLineButton_new.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.remLineButton_new.Location = new System.Drawing.Point(106, 321);
            this.remLineButton_new.Name = "remLineButton_new";
            this.remLineButton_new.Size = new System.Drawing.Size(110, 23);
            this.remLineButton_new.TabIndex = 98;
            this.remLineButton_new.Text = "Remove row(s)";
            this.toolTip1.SetToolTip(this.remLineButton_new, "Removes the selected row(s).\r\n\r\nUse CTRL and SHIFT to select multiple lines.");
            this.remLineButton_new.UseVisualStyleBackColor = true;
            this.remLineButton_new.Click += new System.EventHandler(this.remLineButton_new_Click);
            // 
            // refThetaText
            // 
            this.refThetaText.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.refThetaText.Location = new System.Drawing.Point(687, 352);
            this.refThetaText.Name = "refThetaText";
            this.refThetaText.Size = new System.Drawing.Size(54, 20);
            this.refThetaText.TabIndex = 95;
            this.refThetaText.Text = "0";
            // 
            // label12
            // 
            this.label12.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(603, 355);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(78, 13);
            this.label12.TabIndex = 94;
            this.label12.Text = "Ref.theta [rad]:";
            // 
            // refZText
            // 
            this.refZText.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.refZText.Location = new System.Drawing.Point(543, 352);
            this.refZText.Name = "refZText";
            this.refZText.Size = new System.Drawing.Size(54, 20);
            this.refZText.TabIndex = 93;
            this.refZText.Text = "0";
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(483, 355);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(54, 13);
            this.label1.TabIndex = 92;
            this.label1.Text = "Ref.Z [m]:";
            // 
            // refXText
            // 
            this.refXText.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.refXText.Location = new System.Drawing.Point(421, 352);
            this.refXText.Name = "refXText";
            this.refXText.Size = new System.Drawing.Size(54, 20);
            this.refXText.TabIndex = 91;
            this.refXText.Text = "0";
            // 
            // label14
            // 
            this.label14.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(361, 355);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(54, 13);
            this.label14.TabIndex = 90;
            this.label14.Text = "Ref.X [m]:";
            // 
            // getRefRowButton
            // 
            this.getRefRowButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.getRefRowButton.Location = new System.Drawing.Point(222, 349);
            this.getRefRowButton.Name = "getRefRowButton";
            this.getRefRowButton.Size = new System.Drawing.Size(104, 23);
            this.getRefRowButton.TabIndex = 89;
            this.getRefRowButton.Text = "Set sel. row as ref.";
            this.toolTip1.SetToolTip(this.getRefRowButton, resources.GetString("getRefRowButton.ToolTip"));
            this.getRefRowButton.UseVisualStyleBackColor = true;
            this.getRefRowButton.Click += new System.EventHandler(this.getRefRowButton_Click);
            // 
            // refRowTextBox
            // 
            this.refRowTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.refRowTextBox.Location = new System.Drawing.Point(137, 352);
            this.refRowTextBox.Name = "refRowTextBox";
            this.refRowTextBox.Size = new System.Drawing.Size(79, 20);
            this.refRowTextBox.TabIndex = 88;
            this.toolTip1.SetToolTip(this.refRowTextBox, "The row id of the element whose beginning serves as reference point.");
            // 
            // label15
            // 
            this.label15.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(11, 355);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(120, 13);
            this.label15.TabIndex = 87;
            this.label15.Text = "Reference element row:";
            // 
            // sequenceGrid
            // 
            this.sequenceGrid.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.Silver;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.sequenceGrid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.sequenceGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.sequenceGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.AVname,
            this.length,
            this.angle,
            this.B,
            this.grad,
            this.K1L,
            this.twissS,
            this.s,
            this.startX,
            this.thetaStart,
            this.startZ,
            this.zlimit,
            this.xOffset,
            this.yOffset,
            this.xAngle,
            this.yAngle,
            this.betaX,
            this.betaY,
            this.etaX,
            this.etaPX,
            this.alphaX,
            this.alphaY,
            this.AptType,
            this.Aperture1,
            this.Aperture2,
            this.Aperture3,
            this.Aperture4});
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle8.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.sequenceGrid.DefaultCellStyle = dataGridViewCellStyle8;
            this.sequenceGrid.Location = new System.Drawing.Point(8, 6);
            this.sequenceGrid.Name = "sequenceGrid";
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle9.BackColor = System.Drawing.Color.Silver;
            dataGridViewCellStyle9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle9.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.sequenceGrid.RowHeadersDefaultCellStyle = dataGridViewCellStyle9;
            this.sequenceGrid.RowHeadersWidth = 62;
            this.sequenceGrid.Size = new System.Drawing.Size(895, 309);
            this.sequenceGrid.TabIndex = 60;
            this.sequenceGrid.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.sequenceGrid_CellFormatting);
            // 
            // AVname
            // 
            this.AVname.Frozen = true;
            this.AVname.HeaderText = "Name";
            this.AVname.MinimumWidth = 8;
            this.AVname.Name = "AVname";
            this.AVname.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.AVname.ToolTipText = "Name of the element. If using the \"file number prefix\" export option, it doesn\'t " +
    "have to be unique.";
            this.AVname.Width = 150;
            // 
            // length
            // 
            this.length.Frozen = true;
            this.length.HeaderText = "length (m)";
            this.length.MinimumWidth = 8;
            this.length.Name = "length";
            this.length.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.length.Width = 150;
            // 
            // angle
            // 
            this.angle.HeaderText = "DIPOLE: Bending angle [rad]";
            this.angle.MinimumWidth = 8;
            this.angle.Name = "angle";
            this.angle.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.angle.Width = 150;
            // 
            // B
            // 
            this.B.HeaderText = "DIPOLE: Field (T)";
            this.B.MinimumWidth = 8;
            this.B.Name = "B";
            this.B.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.B.Width = 150;
            // 
            // grad
            // 
            this.grad.HeaderText = "QUADRUPOLE: Gradient (T/m)";
            this.grad.MinimumWidth = 8;
            this.grad.Name = "grad";
            this.grad.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.grad.Width = 150;
            // 
            // K1L
            // 
            this.K1L.HeaderText = "QUADRUPOLE: K1L";
            this.K1L.MinimumWidth = 8;
            this.K1L.Name = "K1L";
            this.K1L.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.K1L.Width = 150;
            // 
            // twissS
            // 
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.WhiteSmoke;
            this.twissS.DefaultCellStyle = dataGridViewCellStyle2;
            this.twissS.HeaderText = "Twiss s [m]";
            this.twissS.MinimumWidth = 8;
            this.twissS.Name = "twissS";
            this.twissS.ReadOnly = true;
            this.twissS.Width = 150;
            // 
            // s
            // 
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.WhiteSmoke;
            this.s.DefaultCellStyle = dataGridViewCellStyle3;
            this.s.HeaderText = "start s [m]";
            this.s.MinimumWidth = 8;
            this.s.Name = "s";
            this.s.ReadOnly = true;
            this.s.Width = 150;
            // 
            // startX
            // 
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.WhiteSmoke;
            this.startX.DefaultCellStyle = dataGridViewCellStyle4;
            this.startX.HeaderText = "start X (m)";
            this.startX.MinimumWidth = 8;
            this.startX.Name = "startX";
            this.startX.ReadOnly = true;
            this.startX.Width = 150;
            // 
            // thetaStart
            // 
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.WhiteSmoke;
            this.thetaStart.DefaultCellStyle = dataGridViewCellStyle5;
            this.thetaStart.HeaderText = "start Theta [rad]";
            this.thetaStart.MinimumWidth = 8;
            this.thetaStart.Name = "thetaStart";
            this.thetaStart.ReadOnly = true;
            this.thetaStart.Width = 150;
            // 
            // startZ
            // 
            dataGridViewCellStyle6.BackColor = System.Drawing.Color.WhiteSmoke;
            this.startZ.DefaultCellStyle = dataGridViewCellStyle6;
            this.startZ.HeaderText = "start Z (m)";
            this.startZ.MinimumWidth = 8;
            this.startZ.Name = "startZ";
            this.startZ.ReadOnly = true;
            this.startZ.Width = 150;
            // 
            // zlimit
            // 
            dataGridViewCellStyle7.BackColor = System.Drawing.Color.WhiteSmoke;
            this.zlimit.DefaultCellStyle = dataGridViewCellStyle7;
            this.zlimit.HeaderText = "Z end (m)";
            this.zlimit.MinimumWidth = 8;
            this.zlimit.Name = "zlimit";
            this.zlimit.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.zlimit.Width = 150;
            // 
            // xOffset
            // 
            this.xOffset.HeaderText = "dX [m]";
            this.xOffset.MinimumWidth = 8;
            this.xOffset.Name = "xOffset";
            this.xOffset.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.xOffset.ToolTipText = "Beam offset at start from startX position, perpendicular to local S direction";
            this.xOffset.Width = 150;
            // 
            // yOffset
            // 
            this.yOffset.HeaderText = "dY [m]";
            this.yOffset.MinimumWidth = 8;
            this.yOffset.Name = "yOffset";
            this.yOffset.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.yOffset.ToolTipText = "Beam horizontal offset. Without offset, the beam is always in the XZ plane.";
            this.yOffset.Width = 150;
            // 
            // xAngle
            // 
            this.xAngle.HeaderText = "dX/dS";
            this.xAngle.MinimumWidth = 8;
            this.xAngle.Name = "xAngle";
            this.xAngle.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.xAngle.ToolTipText = "Beam direction horizontal deviation from the S direction (in radians for small an" +
    "gles, otherwise dX/dS ratio)";
            this.xAngle.Width = 150;
            // 
            // yAngle
            // 
            this.yAngle.HeaderText = "dY/dS";
            this.yAngle.MinimumWidth = 8;
            this.yAngle.Name = "yAngle";
            this.yAngle.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.yAngle.ToolTipText = "Beam direction vertical deviation from the S direction (in radians for small angl" +
    "es, otherwise dY/dS ratio)";
            this.yAngle.Width = 150;
            // 
            // betaX
            // 
            this.betaX.HeaderText = "beta X [m]";
            this.betaX.MinimumWidth = 8;
            this.betaX.Name = "betaX";
            this.betaX.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.betaX.Width = 150;
            // 
            // betaY
            // 
            this.betaY.HeaderText = "beta Y [m]";
            this.betaY.MinimumWidth = 8;
            this.betaY.Name = "betaY";
            this.betaY.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.betaY.Width = 150;
            // 
            // etaX
            // 
            this.etaX.HeaderText = "eta X [m]";
            this.etaX.MinimumWidth = 8;
            this.etaX.Name = "etaX";
            this.etaX.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.etaX.ToolTipText = "Dispersion function, DX in MadX nomenclature";
            this.etaX.Width = 150;
            // 
            // etaPX
            // 
            this.etaPX.HeaderText = "eta prime X";
            this.etaPX.MinimumWidth = 8;
            this.etaPX.Name = "etaPX";
            this.etaPX.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.etaPX.ToolTipText = "Dispersion derivative function, DPX in MadX nomenclature";
            this.etaPX.Width = 150;
            // 
            // alphaX
            // 
            this.alphaX.HeaderText = "alpha X";
            this.alphaX.MinimumWidth = 8;
            this.alphaX.Name = "alphaX";
            this.alphaX.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.alphaX.Width = 150;
            // 
            // alphaY
            // 
            this.alphaY.HeaderText = "alpha Y";
            this.alphaY.MinimumWidth = 8;
            this.alphaY.Name = "alphaY";
            this.alphaY.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.alphaY.Width = 150;
            // 
            // AptType
            // 
            this.AptType.HeaderText = "Aperture Type";
            this.AptType.MinimumWidth = 8;
            this.AptType.Name = "AptType";
            this.AptType.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.AptType.ToolTipText = "Can be empty, \"circle\" for circle/ellipse, \"racetrack\", or a custom aperture name" +
    " defined in the Aperture list tab";
            this.AptType.Width = 150;
            // 
            // Aperture1
            // 
            this.Aperture1.HeaderText = "Aperture 1 [m]";
            this.Aperture1.MinimumWidth = 8;
            this.Aperture1.Name = "Aperture1";
            this.Aperture1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Aperture1.ToolTipText = "Circle radius, Racetrack half-width or custom aperture horizontal scale factor";
            this.Aperture1.Width = 150;
            // 
            // Aperture2
            // 
            this.Aperture2.HeaderText = "Aperture 2 [m]";
            this.Aperture2.MinimumWidth = 8;
            this.Aperture2.Name = "Aperture2";
            this.Aperture2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Aperture2.ToolTipText = "Empty for circle, ellipse half-height, Racetrack half-height or custom aperture v" +
    "ertical scale factor";
            this.Aperture2.Width = 150;
            // 
            // Aperture3
            // 
            this.Aperture3.HeaderText = "Aperture 3 [m]";
            this.Aperture3.MinimumWidth = 8;
            this.Aperture3.Name = "Aperture3";
            this.Aperture3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Aperture3.ToolTipText = "Custom aperture centerpoint\'s X offset";
            this.Aperture3.Width = 150;
            // 
            // Aperture4
            // 
            this.Aperture4.HeaderText = "Aperture 4 [m]";
            this.Aperture4.MinimumWidth = 8;
            this.Aperture4.Name = "Aperture4";
            this.Aperture4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Aperture4.ToolTipText = "Custom aperture centerpoint\'s Y offset";
            this.Aperture4.Width = 150;
            // 
            // apertureTab
            // 
            this.apertureTab.Controls.Add(this.reloadApertures);
            this.apertureTab.Controls.Add(this.apertureInfoButton);
            this.apertureTab.Controls.Add(this.label35);
            this.apertureTab.Controls.Add(this.apertureGrid);
            this.apertureTab.Location = new System.Drawing.Point(4, 22);
            this.apertureTab.Name = "apertureTab";
            this.apertureTab.Padding = new System.Windows.Forms.Padding(3);
            this.apertureTab.Size = new System.Drawing.Size(907, 675);
            this.apertureTab.TabIndex = 4;
            this.apertureTab.Text = "Aperture list";
            this.apertureTab.UseVisualStyleBackColor = true;
            // 
            // reloadApertures
            // 
            this.reloadApertures.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.reloadApertures.Location = new System.Drawing.Point(606, 643);
            this.reloadApertures.Name = "reloadApertures";
            this.reloadApertures.Size = new System.Drawing.Size(127, 23);
            this.reloadApertures.TabIndex = 3;
            this.reloadApertures.Text = "Reload apertures folder";
            this.reloadApertures.UseVisualStyleBackColor = true;
            this.reloadApertures.Click += new System.EventHandler(this.reloadApertures_Click);
            // 
            // apertureInfoButton
            // 
            this.apertureInfoButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.apertureInfoButton.Location = new System.Drawing.Point(787, 643);
            this.apertureInfoButton.Name = "apertureInfoButton";
            this.apertureInfoButton.Size = new System.Drawing.Size(112, 23);
            this.apertureInfoButton.TabIndex = 2;
            this.apertureInfoButton.Text = "Info on this tab";
            this.apertureInfoButton.UseVisualStyleBackColor = true;
            this.apertureInfoButton.Click += new System.EventHandler(this.apertureInfoButton_Click);
            // 
            // label35
            // 
            this.label35.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(6, 648);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(593, 13);
            this.label35.TabIndex = 1;
            this.label35.Text = "Files in the \"apertures\" subfolder will be auto-loaded here. Use .molflow.txt for" +
    " Molflow facets, .txt and .csv for coordinate lists";
            // 
            // apertureGrid
            // 
            this.apertureGrid.AllowUserToResizeRows = false;
            this.apertureGrid.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.apertureGrid.ColumnHeadersHeight = 50;
            this.apertureGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.apertureName,
            this.FileType,
            this.FileLocation,
            this.FileBrowse,
            this.FileStatus,
            this.FileWidth,
            this.FileHeight,
            this.FilePlot,
            this.FileCopy});
            this.apertureGrid.Location = new System.Drawing.Point(3, 3);
            this.apertureGrid.Name = "apertureGrid";
            this.apertureGrid.RowHeadersWidth = 62;
            this.apertureGrid.Size = new System.Drawing.Size(896, 634);
            this.apertureGrid.TabIndex = 0;
            this.apertureGrid.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            this.apertureGrid.RowsAdded += new System.Windows.Forms.DataGridViewRowsAddedEventHandler(this.apertureGrid_RowsAdded);
            this.apertureGrid.Resize += new System.EventHandler(this.apertureGrid_Resize);
            // 
            // apertureName
            // 
            this.apertureName.HeaderText = "Aperture name (as referenced in the sequence)";
            this.apertureName.MaxInputLength = 128;
            this.apertureName.MinimumWidth = 8;
            this.apertureName.Name = "apertureName";
            this.apertureName.ToolTipText = "Unique name of this aperture shape that you will refer to in the main table. Case" +
    "-sensitive, spaces allowed";
            this.apertureName.Width = 130;
            // 
            // FileType
            // 
            this.FileType.HeaderText = "File Type";
            this.FileType.Items.AddRange(new object[] {
            "Molflow txt",
            "Txt coord list",
            "Csv coord list"});
            this.FileType.MinimumWidth = 8;
            this.FileType.Name = "FileType";
            this.FileType.ToolTipText = "Allows to choose input file type (CSV, TXT or Synrad/Molflow-compatible TXT geome" +
    "try). See documentation for  details";
            this.FileType.Width = 80;
            // 
            // FileLocation
            // 
            this.FileLocation.HeaderText = "File Location (hover mouse for long paths)";
            this.FileLocation.MaxInputLength = 2048;
            this.FileLocation.MinimumWidth = 8;
            this.FileLocation.Name = "FileLocation";
            this.FileLocation.Width = 210;
            // 
            // FileBrowse
            // 
            this.FileBrowse.HeaderText = "Browse";
            this.FileBrowse.MinimumWidth = 8;
            this.FileBrowse.Name = "FileBrowse";
            this.FileBrowse.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.FileBrowse.Text = "...";
            this.FileBrowse.UseColumnTextForButtonValue = true;
            this.FileBrowse.Width = 48;
            // 
            // FileStatus
            // 
            dataGridViewCellStyle10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.FileStatus.DefaultCellStyle = dataGridViewCellStyle10;
            this.FileStatus.HeaderText = "Status";
            this.FileStatus.MinimumWidth = 8;
            this.FileStatus.Name = "FileStatus";
            this.FileStatus.ReadOnly = true;
            this.FileStatus.Width = 130;
            // 
            // FileWidth
            // 
            dataGridViewCellStyle11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.FileWidth.DefaultCellStyle = dataGridViewCellStyle11;
            this.FileWidth.HeaderText = "Width(m)";
            this.FileWidth.MinimumWidth = 8;
            this.FileWidth.Name = "FileWidth";
            this.FileWidth.ReadOnly = true;
            this.FileWidth.ToolTipText = "Default width, can be scaled (multiplied) by the Aperture1 parameter in the main " +
    "table";
            this.FileWidth.Width = 74;
            // 
            // FileHeight
            // 
            dataGridViewCellStyle12.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.FileHeight.DefaultCellStyle = dataGridViewCellStyle12;
            this.FileHeight.HeaderText = "Height(m)";
            this.FileHeight.MinimumWidth = 8;
            this.FileHeight.Name = "FileHeight";
            this.FileHeight.ReadOnly = true;
            this.FileHeight.ToolTipText = "Default height, can be scaled (multiplied) by the Aperture2 parameter in the main" +
    " table";
            this.FileHeight.Width = 77;
            // 
            // FilePlot
            // 
            this.FilePlot.HeaderText = "Plot";
            this.FilePlot.MinimumWidth = 8;
            this.FilePlot.Name = "FilePlot";
            this.FilePlot.ReadOnly = true;
            this.FilePlot.Text = "";
            this.FilePlot.Width = 40;
            // 
            // FileCopy
            // 
            this.FileCopy.HeaderText = "Copy to library";
            this.FileCopy.MinimumWidth = 8;
            this.FileCopy.Name = "FileCopy";
            this.FileCopy.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.FileCopy.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.FileCopy.Text = "Copy";
            this.FileCopy.ToolTipText = "Copies loaded file to the \"apertures\" folder, will be loaded on next program star" +
    "t";
            this.FileCopy.UseColumnTextForButtonValue = true;
            this.FileCopy.Width = 40;
            // 
            // sessionTab
            // 
            this.sessionTab.Controls.Add(this.libraryHint);
            this.sessionTab.Controls.Add(this.groupBox1);
            this.sessionTab.Controls.Add(this.saveCurrentGroup);
            this.sessionTab.Location = new System.Drawing.Point(4, 22);
            this.sessionTab.Name = "sessionTab";
            this.sessionTab.Padding = new System.Windows.Forms.Padding(3);
            this.sessionTab.Size = new System.Drawing.Size(907, 675);
            this.sessionTab.TabIndex = 5;
            this.sessionTab.Text = "Sessions library";
            this.sessionTab.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.sessionGrid);
            this.groupBox1.Controls.Add(this.reloadSessionsButton);
            this.groupBox1.Controls.Add(this.openSessionDirButton);
            this.groupBox1.Controls.Add(this.emptyTrashButton);
            this.groupBox1.Controls.Add(this.openTrashButton);
            this.groupBox1.Location = new System.Drawing.Point(6, 88);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(893, 566);
            this.groupBox1.TabIndex = 7;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Saved sessions in \"Sessions\" subfolder";
            // 
            // sessionGrid
            // 
            this.sessionGrid.AllowUserToAddRows = false;
            this.sessionGrid.AllowUserToDeleteRows = false;
            this.sessionGrid.AllowUserToResizeRows = false;
            this.sessionGrid.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.sessionGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.sessionGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Filename,
            this.Modified,
            this.LoadColumn,
            this.Overwrite,
            this.Rename,
            this.Trash});
            this.sessionGrid.Location = new System.Drawing.Point(6, 19);
            this.sessionGrid.Name = "sessionGrid";
            this.sessionGrid.RowHeadersWidth = 62;
            this.sessionGrid.Size = new System.Drawing.Size(881, 512);
            this.sessionGrid.TabIndex = 0;
            this.sessionGrid.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.sessionGrid_CellContentClick_1);
            this.sessionGrid.Resize += new System.EventHandler(this.sessionGrid_Resize);
            // 
            // Filename
            // 
            dataGridViewCellStyle13.BackColor = System.Drawing.SystemColors.Control;
            this.Filename.DefaultCellStyle = dataGridViewCellStyle13;
            this.Filename.HeaderText = "File name";
            this.Filename.MaxInputLength = 245;
            this.Filename.MinimumWidth = 8;
            this.Filename.Name = "Filename";
            this.Filename.Width = 460;
            // 
            // Modified
            // 
            dataGridViewCellStyle14.BackColor = System.Drawing.SystemColors.Control;
            this.Modified.DefaultCellStyle = dataGridViewCellStyle14;
            this.Modified.HeaderText = "Modified date";
            this.Modified.MinimumWidth = 8;
            this.Modified.Name = "Modified";
            this.Modified.Width = 130;
            // 
            // LoadColumn
            // 
            this.LoadColumn.HeaderText = "Load";
            this.LoadColumn.MinimumWidth = 8;
            this.LoadColumn.Name = "LoadColumn";
            this.LoadColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.LoadColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.LoadColumn.Width = 55;
            // 
            // Overwrite
            // 
            this.Overwrite.HeaderText = "Save current session";
            this.Overwrite.MinimumWidth = 8;
            this.Overwrite.Name = "Overwrite";
            this.Overwrite.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Overwrite.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.Overwrite.ToolTipText = "Saves the current session overwriting this file";
            this.Overwrite.Width = 57;
            // 
            // Rename
            // 
            this.Rename.HeaderText = "Rename";
            this.Rename.MinimumWidth = 8;
            this.Rename.Name = "Rename";
            this.Rename.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Rename.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.Rename.Width = 55;
            // 
            // Trash
            // 
            this.Trash.HeaderText = "To trash subfolder";
            this.Trash.MinimumWidth = 8;
            this.Trash.Name = "Trash";
            this.Trash.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Trash.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.Trash.Width = 55;
            // 
            // reloadSessionsButton
            // 
            this.reloadSessionsButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.reloadSessionsButton.Location = new System.Drawing.Point(122, 537);
            this.reloadSessionsButton.Name = "reloadSessionsButton";
            this.reloadSessionsButton.Size = new System.Drawing.Size(93, 23);
            this.reloadSessionsButton.TabIndex = 5;
            this.reloadSessionsButton.Text = "Reload sessions";
            this.reloadSessionsButton.UseVisualStyleBackColor = true;
            this.reloadSessionsButton.Click += new System.EventHandler(this.reloadSessionsButton_Click);
            // 
            // openSessionDirButton
            // 
            this.openSessionDirButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.openSessionDirButton.Location = new System.Drawing.Point(6, 537);
            this.openSessionDirButton.Name = "openSessionDirButton";
            this.openSessionDirButton.Size = new System.Drawing.Size(110, 23);
            this.openSessionDirButton.TabIndex = 1;
            this.openSessionDirButton.Text = "Open session folder";
            this.openSessionDirButton.UseVisualStyleBackColor = true;
            this.openSessionDirButton.Click += new System.EventHandler(this.openSessionDirButton_Click);
            // 
            // emptyTrashButton
            // 
            this.emptyTrashButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.emptyTrashButton.ForeColor = System.Drawing.Color.Red;
            this.emptyTrashButton.Location = new System.Drawing.Point(810, 537);
            this.emptyTrashButton.Name = "emptyTrashButton";
            this.emptyTrashButton.Size = new System.Drawing.Size(77, 23);
            this.emptyTrashButton.TabIndex = 4;
            this.emptyTrashButton.Text = "Empty trash";
            this.emptyTrashButton.UseVisualStyleBackColor = true;
            this.emptyTrashButton.Click += new System.EventHandler(this.emptyTrashButton_Click);
            // 
            // openTrashButton
            // 
            this.openTrashButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.openTrashButton.Location = new System.Drawing.Point(734, 537);
            this.openTrashButton.Name = "openTrashButton";
            this.openTrashButton.Size = new System.Drawing.Size(70, 23);
            this.openTrashButton.TabIndex = 3;
            this.openTrashButton.Text = "Open trash";
            this.openTrashButton.UseVisualStyleBackColor = true;
            this.openTrashButton.Click += new System.EventHandler(this.openTrashButton_Click);
            // 
            // saveCurrentGroup
            // 
            this.saveCurrentGroup.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.saveCurrentGroup.Controls.Add(this.label5);
            this.saveCurrentGroup.Controls.Add(this.newSessionNameBox);
            this.saveCurrentGroup.Controls.Add(this.saveSessionAsNewButton);
            this.saveCurrentGroup.Location = new System.Drawing.Point(6, 38);
            this.saveCurrentGroup.Name = "saveCurrentGroup";
            this.saveCurrentGroup.Size = new System.Drawing.Size(893, 44);
            this.saveCurrentGroup.TabIndex = 6;
            this.saveCurrentGroup.TabStop = false;
            this.saveCurrentGroup.Text = "Save current session as new";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(15, 19);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(76, 13);
            this.label5.TabIndex = 2;
            this.label5.Text = "Session name:";
            // 
            // newSessionNameBox
            // 
            this.newSessionNameBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.newSessionNameBox.Location = new System.Drawing.Point(97, 16);
            this.newSessionNameBox.MaxLength = 245;
            this.newSessionNameBox.Name = "newSessionNameBox";
            this.newSessionNameBox.Size = new System.Drawing.Size(573, 20);
            this.newSessionNameBox.TabIndex = 1;
            // 
            // saveSessionAsNewButton
            // 
            this.saveSessionAsNewButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.saveSessionAsNewButton.Location = new System.Drawing.Point(676, 14);
            this.saveSessionAsNewButton.Name = "saveSessionAsNewButton";
            this.saveSessionAsNewButton.Size = new System.Drawing.Size(211, 24);
            this.saveSessionAsNewButton.TabIndex = 0;
            this.saveSessionAsNewButton.Text = "Save as new";
            this.toolTip1.SetToolTip(this.saveSessionAsNewButton, "Save the form text, table and checkbox states as a file with the name on the left" +
        ".");
            this.saveSessionAsNewButton.UseVisualStyleBackColor = true;
            this.saveSessionAsNewButton.Click += new System.EventHandler(this.saveSessionAsNewButton_Click);
            // 
            // toolTip1
            // 
            this.toolTip1.AutomaticDelay = 100;
            this.toolTip1.AutoPopDelay = 32767;
            this.toolTip1.InitialDelay = 100;
            this.toolTip1.ReshowDelay = 20;
            this.toolTip1.UseFading = false;
            // 
            // hideTooltipsCheckbox
            // 
            this.hideTooltipsCheckbox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.hideTooltipsCheckbox.AutoSize = true;
            this.hideTooltipsCheckbox.Location = new System.Drawing.Point(833, 736);
            this.hideTooltipsCheckbox.Name = "hideTooltipsCheckbox";
            this.hideTooltipsCheckbox.Size = new System.Drawing.Size(84, 17);
            this.hideTooltipsCheckbox.TabIndex = 5;
            this.hideTooltipsCheckbox.Text = "Hide tooltips";
            this.hideTooltipsCheckbox.UseVisualStyleBackColor = true;
            this.hideTooltipsCheckbox.CheckedChanged += new System.EventHandler(this.hideTooltipsCheckbox_CheckedChanged);
            // 
            // statusBox
            // 
            this.statusBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.statusBox.Location = new System.Drawing.Point(10, 734);
            this.statusBox.Multiline = true;
            this.statusBox.Name = "statusBox";
            this.statusBox.ReadOnly = true;
            this.statusBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.statusBox.Size = new System.Drawing.Size(817, 77);
            this.statusBox.TabIndex = 6;
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(926, 24);
            this.menuStrip1.TabIndex = 7;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileMenuItem
            // 
            this.fileMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newSubmenuItem,
            this.toolStripSeparator3,
            this.loadSubmenuItem,
            this.loadRecentSubmenuItem,
            this.toolStripSeparator1,
            this.saveSubmenuItem,
            this.saveAsSubmenuItem,
            this.toolStripSeparator2,
            this.exitSubmenuItem});
            this.fileMenuItem.Name = "fileMenuItem";
            this.fileMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileMenuItem.Text = "File";
            // 
            // newSubmenuItem
            // 
            this.newSubmenuItem.Name = "newSubmenuItem";
            this.newSubmenuItem.Size = new System.Drawing.Size(179, 22);
            this.newSubmenuItem.Text = "New, empty session";
            this.newSubmenuItem.Click += new System.EventHandler(this.newSubmenuItem_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(176, 6);
            // 
            // loadSubmenuItem
            // 
            this.loadSubmenuItem.Name = "loadSubmenuItem";
            this.loadSubmenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
            this.loadSubmenuItem.Size = new System.Drawing.Size(179, 22);
            this.loadSubmenuItem.Text = "Load";
            this.loadSubmenuItem.Click += new System.EventHandler(this.loadSubmenuItem_Click);
            // 
            // loadRecentSubmenuItem
            // 
            this.loadRecentSubmenuItem.Name = "loadRecentSubmenuItem";
            this.loadRecentSubmenuItem.Size = new System.Drawing.Size(179, 22);
            this.loadRecentSubmenuItem.Text = "Load recent";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(176, 6);
            // 
            // saveSubmenuItem
            // 
            this.saveSubmenuItem.Name = "saveSubmenuItem";
            this.saveSubmenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.saveSubmenuItem.Size = new System.Drawing.Size(179, 22);
            this.saveSubmenuItem.Text = "Save";
            this.saveSubmenuItem.Click += new System.EventHandler(this.saveSubmenuItem_Click);
            // 
            // saveAsSubmenuItem
            // 
            this.saveAsSubmenuItem.Name = "saveAsSubmenuItem";
            this.saveAsSubmenuItem.Size = new System.Drawing.Size(179, 22);
            this.saveAsSubmenuItem.Text = "Save as";
            this.saveAsSubmenuItem.Click += new System.EventHandler(this.saveAsSubmenuItem_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(176, 6);
            // 
            // exitSubmenuItem
            // 
            this.exitSubmenuItem.Name = "exitSubmenuItem";
            this.exitSubmenuItem.Size = new System.Drawing.Size(179, 22);
            this.exitSubmenuItem.Text = "Exit";
            this.exitSubmenuItem.Click += new System.EventHandler(this.exitSubmenuItem_Click);
            // 
            // libraryHint
            // 
            this.libraryHint.AutoSize = true;
            this.libraryHint.Location = new System.Drawing.Point(21, 12);
            this.libraryHint.Name = "libraryHint";
            this.libraryHint.Size = new System.Drawing.Size(856, 13);
            this.libraryHint.TabIndex = 8;
            this.libraryHint.Text = "This tab shows the content of the Sessions folder in the app\'s directory. It is n" +
    "ow considered deprecated, as instead of this library, you can use the File menus" +
    "\'s Load/Save functions";
            // 
            // OpticsBuilderForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(926, 819);
            this.Controls.Add(this.statusBox);
            this.Controls.Add(this.hideTooltipsCheckbox);
            this.Controls.Add(this.tabControl);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "OpticsBuilderForm";
            this.Text = "Survey file to SynRad+ converter";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.OpticsBuilderForm_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.Resize += new System.EventHandler(this.Form1_Resize);
            this.tabControl.ResumeLayout(false);
            this.exportTab.ResumeLayout(false);
            this.exportTab.PerformLayout();
            this.outputGroup.ResumeLayout(false);
            this.outputGroup.PerformLayout();
            this.paramTabcontrol.ResumeLayout(false);
            this.beamTab.ResumeLayout(false);
            this.beamGroupBox.ResumeLayout(false);
            this.beamGroupBox.PerformLayout();
            this.betaGroupbox.ResumeLayout(false);
            this.betaGroupbox.PerformLayout();
            this.synradTab.ResumeLayout(false);
            this.synradTab.PerformLayout();
            this.geomTab.ResumeLayout(false);
            this.geomTab.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.sequenceGrid)).EndInit();
            this.apertureTab.ResumeLayout(false);
            this.apertureTab.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.apertureGrid)).EndInit();
            this.sessionTab.ResumeLayout(false);
            this.sessionTab.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.sessionGrid)).EndInit();
            this.saveCurrentGroup.ResumeLayout(false);
            this.saveCurrentGroup.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.TabPage exportTab;
        private System.Windows.Forms.Button writeBXYfileButton;
        private System.Windows.Forms.Button getEndLineButton;
        private System.Windows.Forms.Button getStartLineButton;
        private System.Windows.Forms.TextBox endRowText;
        private System.Windows.Forms.TextBox startRowText;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label startLineLabel;
        private System.Windows.Forms.Button bAngleButton;
        private System.Windows.Forms.ProgressBar prg_new;
        private System.Windows.Forms.Button resetTableButton_new;
        private System.Windows.Forms.Button pasteFromClipboardButton_new;
        private System.Windows.Forms.Button addLineButton_new;
        private System.Windows.Forms.Button remLineButton_new;
        private System.Windows.Forms.TextBox refThetaText;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox refZText;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox refXText;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Button getRefRowButton;
        private System.Windows.Forms.TextBox refRowTextBox;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.CheckBox prefixCheckbox;
        private System.Windows.Forms.TextBox eMaxText;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox eMinText;
        private System.Windows.Forms.Label eMinLabel;
        private System.Windows.Forms.TextBox dLtext;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.DataGridView sequenceGrid;
        private System.Windows.Forms.Button browseBXYbutton;
        private System.Windows.Forms.TextBox bxyFileNameText;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Button writeFilesButton;
        private System.Windows.Forms.Button browseButton;
        private System.Windows.Forms.Button readTwissButton;
        private System.Windows.Forms.CheckBox suffixCheckbox;
        private System.Windows.Forms.Button searchEndButton;
        private System.Windows.Forms.Button searchBeginningButton;
        private System.Windows.Forms.TextBox searchTermTextbox;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label searchResultLabel;
        private System.Windows.Forms.Button readApertureButton;
        private System.Windows.Forms.TextBox circleSidesText;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Button writeGeomButton;
        private System.Windows.Forms.Button browseGeomButton;
        private System.Windows.Forms.TextBox geomFileTextBox;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.TextBox curveStepText;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.ComboBox taperCombo;
        private System.Windows.Forms.TabPage apertureTab;
        private System.Windows.Forms.CheckBox ignoreSameSCheckBox;
        private System.Windows.Forms.DataGridView apertureGrid;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Button apertureInfoButton;
        private System.Windows.Forms.TabControl paramTabcontrol;
        private System.Windows.Forms.TabPage beamTab;
        private System.Windows.Forms.TextBox energySpreadText;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox beamEText;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label beamELabel;
        private System.Windows.Forms.TextBox pMassText;
        private System.Windows.Forms.TextBox currentText;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox emittYtext;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox emittXtext;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TabPage geomTab;
        private System.Windows.Forms.TabPage synradTab;
        private System.Windows.Forms.GroupBox beamGroupBox;
        private System.Windows.Forms.GroupBox betaGroupbox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox apertureFacetsCombo;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox outputGroup;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox facetStickingTextbox;
        private System.Windows.Forms.TabPage sessionTab;
        private System.Windows.Forms.DataGridView sessionGrid;
        private System.Windows.Forms.Button openSessionDirButton;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button reloadSessionsButton;
        private System.Windows.Forms.Button emptyTrashButton;
        private System.Windows.Forms.Button openTrashButton;
        private System.Windows.Forms.GroupBox saveCurrentGroup;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox newSessionNameBox;
        private System.Windows.Forms.Button saveSessionAsNewButton;
        private System.Windows.Forms.Button reloadApertures;
        private System.Windows.Forms.ComboBox dirComboBox;
        private System.Windows.Forms.Label filenameExampleLabel;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox customPrefixText;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Filename;
        private System.Windows.Forms.DataGridViewTextBoxColumn Modified;
        private System.Windows.Forms.DataGridViewButtonColumn LoadColumn;
        private System.Windows.Forms.DataGridViewButtonColumn Overwrite;
        private System.Windows.Forms.DataGridViewButtonColumn Rename;
        private System.Windows.Forms.DataGridViewButtonColumn Trash;
        private System.Windows.Forms.DataGridViewTextBoxColumn apertureName;
        private System.Windows.Forms.DataGridViewComboBoxColumn FileType;
        private System.Windows.Forms.DataGridViewTextBoxColumn FileLocation;
        private System.Windows.Forms.DataGridViewButtonColumn FileBrowse;
        private System.Windows.Forms.DataGridViewTextBoxColumn FileStatus;
        private System.Windows.Forms.DataGridViewTextBoxColumn FileWidth;
        private System.Windows.Forms.DataGridViewTextBoxColumn FileHeight;
        private System.Windows.Forms.DataGridViewButtonColumn FilePlot;
        private System.Windows.Forms.DataGridViewButtonColumn FileCopy;
        private System.Windows.Forms.CheckBox hideTooltipsCheckbox;
        private System.Windows.Forms.TextBox statusBox;
        private System.Windows.Forms.Button previewGeomButton;
        private System.Windows.Forms.DataGridViewTextBoxColumn AVname;
        private System.Windows.Forms.DataGridViewTextBoxColumn length;
        private System.Windows.Forms.DataGridViewTextBoxColumn angle;
        private System.Windows.Forms.DataGridViewTextBoxColumn B;
        private System.Windows.Forms.DataGridViewTextBoxColumn grad;
        private System.Windows.Forms.DataGridViewTextBoxColumn K1L;
        private System.Windows.Forms.DataGridViewTextBoxColumn twissS;
        private System.Windows.Forms.DataGridViewTextBoxColumn s;
        private System.Windows.Forms.DataGridViewTextBoxColumn startX;
        private System.Windows.Forms.DataGridViewTextBoxColumn thetaStart;
        private System.Windows.Forms.DataGridViewTextBoxColumn startZ;
        private System.Windows.Forms.DataGridViewTextBoxColumn zlimit;
        private System.Windows.Forms.DataGridViewTextBoxColumn xOffset;
        private System.Windows.Forms.DataGridViewTextBoxColumn yOffset;
        private System.Windows.Forms.DataGridViewTextBoxColumn xAngle;
        private System.Windows.Forms.DataGridViewTextBoxColumn yAngle;
        private System.Windows.Forms.DataGridViewTextBoxColumn betaX;
        private System.Windows.Forms.DataGridViewTextBoxColumn betaY;
        private System.Windows.Forms.DataGridViewTextBoxColumn etaX;
        private System.Windows.Forms.DataGridViewTextBoxColumn etaPX;
        private System.Windows.Forms.DataGridViewTextBoxColumn alphaX;
        private System.Windows.Forms.DataGridViewTextBoxColumn alphaY;
        private System.Windows.Forms.DataGridViewTextBoxColumn AptType;
        private System.Windows.Forms.DataGridViewTextBoxColumn Aperture1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Aperture2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Aperture3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Aperture4;
        private System.Windows.Forms.Button invertChargeButton;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileMenuItem;
        private System.Windows.Forms.ToolStripMenuItem loadSubmenuItem;
        private System.Windows.Forms.ToolStripMenuItem loadRecentSubmenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveAsSubmenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem saveSubmenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem exitSubmenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripMenuItem newSubmenuItem;
        private System.Windows.Forms.Label libraryHint;
    }
}


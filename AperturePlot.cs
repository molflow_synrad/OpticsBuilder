﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OpticsBuilder
{
    public partial class AperturePlot : Form
    {
        public AperturePlot()
        {
            InitializeComponent();
        }

        internal void DrawCoords(List<Vertex2d> tag)
        {
            foreach (var p in tag)
            {
                apertureChart.Series[0].Points.AddXY(p.u, p.v);
            }
            if (tag.Count > 2)
            {
                //Connect last point with first
                apertureChart.Series[0].Points.AddXY(tag[0].u, tag[0].v);
            }
        }

        private void apertureChart_MouseMove(object sender, MouseEventArgs e)
        {
            try
            {
                var sourceChart = sender as System.Windows.Forms.DataVisualization.Charting.Chart;
                //var result = sourceChart.HitTest(e.X, e.Y);
                var chartAreas = sourceChart.ChartAreas[0];


                chartAreas.CursorX.Position = chartAreas.AxisX.PixelPositionToValue(e.X);
                chartAreas.CursorY.Position = chartAreas.AxisY.PixelPositionToValue(e.Y);

                cursorCoord.Text = "X=" + chartAreas.CursorX.Position.ToString() + " Y=" + chartAreas.CursorY.Position.ToString();
            } catch 
            {
                return;
            }
        }

        private void apertureChart_Click(object sender, EventArgs e)
        {

        }
    }
}

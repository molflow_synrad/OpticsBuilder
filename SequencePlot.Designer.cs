﻿namespace OpticsBuilder
{
    partial class SequencePlot
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.cursorCoord = new System.Windows.Forms.Label();
            this.sequenceChart = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.displayNamesCheckbox = new System.Windows.Forms.CheckBox();
            this.resetZoomButton = new System.Windows.Forms.Button();
            this.zoomInfoLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.sequenceChart)).BeginInit();
            this.SuspendLayout();
            // 
            // cursorCoord
            // 
            this.cursorCoord.AutoSize = true;
            this.cursorCoord.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.cursorCoord.Location = new System.Drawing.Point(0, 437);
            this.cursorCoord.Name = "cursorCoord";
            this.cursorCoord.Size = new System.Drawing.Size(97, 13);
            this.cursorCoord.TabIndex = 2;
            this.cursorCoord.Text = "Mouse coordinates";
            this.cursorCoord.Click += new System.EventHandler(this.cursorCoord_Click);
            // 
            // sequenceChart
            // 
            chartArea1.AxisX.Title = "X [m]";
            chartArea1.AxisY.Title = "Z [m]";
            chartArea1.Name = "ChartArea1";
            this.sequenceChart.ChartAreas.Add(chartArea1);
            this.sequenceChart.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.sequenceChart.Dock = System.Windows.Forms.DockStyle.Fill;
            this.sequenceChart.Location = new System.Drawing.Point(0, 0);
            this.sequenceChart.Name = "sequenceChart";
            series1.BorderWidth = 2;
            series1.ChartArea = "ChartArea1";
            series1.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series1.MarkerColor = System.Drawing.Color.Red;
            series1.MarkerSize = 7;
            series1.MarkerStyle = System.Windows.Forms.DataVisualization.Charting.MarkerStyle.Circle;
            series1.Name = "Series1";
            series1.XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Double;
            series1.YValuesPerPoint = 2;
            series1.YValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Double;
            this.sequenceChart.Series.Add(series1);
            this.sequenceChart.Size = new System.Drawing.Size(800, 437);
            this.sequenceChart.TabIndex = 3;
            this.sequenceChart.Text = "sequence";
            this.sequenceChart.Click += new System.EventHandler(this.apertureChart_Click);
            this.sequenceChart.Paint += new System.Windows.Forms.PaintEventHandler(this.sequenceChart_Paint);
            this.sequenceChart.MouseDown += new System.Windows.Forms.MouseEventHandler(this.sequenceChart_MouseDown);
            this.sequenceChart.MouseMove += new System.Windows.Forms.MouseEventHandler(this.apertureChart_MouseMove_1);
            this.sequenceChart.MouseUp += new System.Windows.Forms.MouseEventHandler(this.sequenceChart_MouseUp);
            // 
            // displayNamesCheckbox
            // 
            this.displayNamesCheckbox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.displayNamesCheckbox.AutoSize = true;
            this.displayNamesCheckbox.Location = new System.Drawing.Point(584, 436);
            this.displayNamesCheckbox.Name = "displayNamesCheckbox";
            this.displayNamesCheckbox.Size = new System.Drawing.Size(216, 17);
            this.displayNamesCheckbox.TabIndex = 4;
            this.displayNamesCheckbox.Text = "Display element names and label tooltips";
            this.displayNamesCheckbox.UseVisualStyleBackColor = true;
            this.displayNamesCheckbox.CheckedChanged += new System.EventHandler(this.displayNamesCheckbox_CheckedChanged);
            // 
            // resetZoomButton
            // 
            this.resetZoomButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.resetZoomButton.Location = new System.Drawing.Point(725, 414);
            this.resetZoomButton.Name = "resetZoomButton";
            this.resetZoomButton.Size = new System.Drawing.Size(75, 23);
            this.resetZoomButton.TabIndex = 5;
            this.resetZoomButton.Text = "Reset zoom";
            this.resetZoomButton.UseVisualStyleBackColor = true;
            this.resetZoomButton.Click += new System.EventHandler(this.resetZoomButton_Click);
            // 
            // zoomInfoLabel
            // 
            this.zoomInfoLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.zoomInfoLabel.AutoSize = true;
            this.zoomInfoLabel.Location = new System.Drawing.Point(534, 419);
            this.zoomInfoLabel.Name = "zoomInfoLabel";
            this.zoomInfoLabel.Size = new System.Drawing.Size(185, 13);
            this.zoomInfoLabel.TabIndex = 6;
            this.zoomInfoLabel.Text = "You can zoom by drawing a rectangle";
            // 
            // SequencePlot
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.zoomInfoLabel);
            this.Controls.Add(this.resetZoomButton);
            this.Controls.Add(this.displayNamesCheckbox);
            this.Controls.Add(this.sequenceChart);
            this.Controls.Add(this.cursorCoord);
            this.Name = "SequencePlot";
            this.Text = "SequencePlot";
            ((System.ComponentModel.ISupportInitialize)(this.sequenceChart)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label cursorCoord;
        private System.Windows.Forms.DataVisualization.Charting.Chart sequenceChart;
        private System.Windows.Forms.CheckBox displayNamesCheckbox;
        private System.Windows.Forms.Button resetZoomButton;
        private System.Windows.Forms.Label zoomInfoLabel;
    }
}
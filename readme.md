OpticsBuilder is a Windows tool to generate SynRad+ geometries and magnetic regions from aperture lists, typically originating from MADX outputs.

For more information, see [its documentation](https://molflow.docs.cern.ch/guide/opticsbuilder/).

![](screenshot1.png)
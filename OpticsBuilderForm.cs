﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Numerics;
using System.IO;

using OfficeOpenXml;
using Microsoft.VisualBasic;
using System.Runtime.InteropServices;
using OfficeOpenXml.FormulaParsing.Excel.Functions.Engineering;

namespace OpticsBuilder
{
    public partial class OpticsBuilderForm : Form
    {
        StaticData formData;
        private string _mostRecentFile;

        public OpticsBuilderForm()
        {
            InitializeComponent();
            System.Globalization.CultureInfo customCulture = (System.Globalization.CultureInfo)System.Threading.Thread.CurrentThread.CurrentCulture.Clone();
            customCulture.NumberFormat.NumberDecimalSeparator = "."; //force DOT as decimal separator
            System.Threading.Thread.CurrentThread.CurrentCulture = customCulture;
        }

        private void Form1_Load(object sender, EventArgs e)
        {

            formData = new StaticData();
            sequenceGrid.RowHeadersWidth = 90;
            tabControl.SelectedIndex = 3;
            LoadApertureLibrary();
            LoadSavedSessionLibrary();
            if (File.Exists(@"Sessions\autoload.xlsx"))
            {
                LoadSession(@"Sessions\autoload.xlsx", true); //Load template, silently abandon on error
                Log("Found and loaded autoload.xlsx in Sessions folder.");
            } 
            else
            {
                Log("No autoload.xlsx in Sessions folder, filling form with default values.");
            }
            LoadRecentDirs();
            LoadRecentSessions();

        }

        private void LoadRecentDirs()
        {
            if (File.Exists("recentDirs.txt"))
            {
                string line;

                // Read the file and display it line by line.  
                using (System.IO.StreamReader file =
                    new System.IO.StreamReader("recentDirs.txt"))
                {
                    while ((line = file.ReadLine()) != null && dirComboBox.Items.Count < 20)
                    {
                        if (Directory.Exists(line)) AddToRecentDirs(line,true); //add to back
                    }

                    file.Close();
                    Log("Loaded " + dirComboBox.Items.Count + " recent directories from recentDirs.txt");
                }
            }
        }

        private void LoadApertureLibrary()
        {
            if (!Directory.Exists("apertures"))
            {
                //Called at Form_load, don't display error
                Log("\"apertures\" directory doesn't exist, will start with empty aperture library.");
                return;
            }
            string[] filePaths = Directory.GetFiles(@"apertures\", "*.txt", SearchOption.TopDirectoryOnly);
            try
            {

                foreach (var path in filePaths)
                {
                    apertureGrid.Rows.Add();
                    int lastRow = apertureGrid.RowCount - 2; //last comitted row
                    var row = apertureGrid.Rows[lastRow];
                    int first = path.IndexOf(@"apertures\") + @"apertures\".Length;
                    int last = path.LastIndexOf(".txt");
                    string name = path.Substring(first, last - first);
                    row.Cells["apertureName"].Value = name;
                    row.Cells["FileType"].Value = "Txt coord list";
                    row.Cells["FileLocation"].Value = path;
                    row.Tag = LoadFacetFile(path, "Txt coord list");
                    PostProcessAperture(lastRow);
                }
            }
            catch (Exception err)
            {
                MessageBox.Show("Error loading aperture cross-section library\n" + err.Message);
                return;
            }
            Log(String.Format("Loaded {0} apertures from \"apertures\" subfolder." , filePaths.Length));
        }

        private void Log(string v)
        {
            //string date = System.DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss");
            //string date = System.DateTime.Now.ToString();
            string dateString = System.DateTime.Now.ToString("HH:mm:ss");
            statusBox.AppendText(dateString + "   " + v + "\r\n");
            //statusBox.ScrollToCaret();
        }

        // PasteInData pastes clipboard data into the grid passed to it.
        private void PasteInData(ref DataGridView dgv)
        {

            char[] rowSplitter = { '\n', '\r' };  // Cr and Lf.
            char columnSplitter = '\t';         // Tab.
            char[] columnSplitters = { columnSplitter };

            IDataObject dataInClipboard = Clipboard.GetDataObject();

            var clipboardData = dataInClipboard.GetData(DataFormats.Text);
            if (clipboardData == null) return;
            string stringInClipboard = clipboardData.ToString();

            string[] rowsInClipboard = stringInClipboard.Split(rowSplitter,
                StringSplitOptions.RemoveEmptyEntries);
            string[] columnsInClipboard = rowsInClipboard[0].Split(columnSplitters/*,
                StringSplitOptions.RemoveEmptyEntries*/);

            int r = 0;
            int c = 0;

            if (dgv.SelectedCells.Count > 0)
            {
                r = dgv.SelectedCells[0].RowIndex;
                c = dgv.SelectedCells[0].ColumnIndex;
            }


            prg_new.Visible = true;
            prg_new.Minimum = 0;
            prg_new.Maximum = rowsInClipboard.Length;
            prg_new.Value = 0;

            if (dgv.Rows.Count < (r + rowsInClipboard.Length))
            {
                dgv.Rows.Add(r + rowsInClipboard.Length + 1 - dgv.Rows.Count);
                prg_new.Value += 1;
                prg_new.PerformStep();
            }
            if (dgv.Columns.Count < (c + columnsInClipboard.Length))
                for (int iCol = 0; iCol < (c + columnsInClipboard.Length + 1 - dgv.Columns.Count); iCol++)
                {
                    dgv.Columns.Add("", "");
                    dgv.Columns[dgv.Columns.Count - 1].SortMode = DataGridViewColumnSortMode.NotSortable;

                }

            // Loop through lines:

            int iRow = 0;
            prg_new.Value = 0;
            while (iRow < rowsInClipboard.Length)
            {
                // Split up rows to get individual cells:

                string[] valuesInRow =
                    rowsInClipboard[iRow].Split(columnSplitter);

                // Cycle through cells.
                // Assign cell value only if within columns of grid:

                int jCol = 0;
                while (jCol < valuesInRow.Length)
                {
                    if ((dgv.ColumnCount - 1) >= (c + jCol))
                    {
                        if (!String.IsNullOrEmpty(valuesInRow[jCol]))
                        {
                            if (dgv.Columns[c + jCol] is DataGridViewTextBoxColumn)
                            { //Prevent pasting into checkbox
                                dgv.Rows[r + iRow].Cells[c + jCol].Value =
                                valuesInRow[jCol];
                            }
                            else
                            {
                                if (valuesInRow[jCol].ToLower() == "true")
                                {
                                    dgv.Rows[r + iRow].Cells[c + jCol].Value = true;
                                }
                            }
                        }
                        else
                        {
                            dgv.Rows[r + iRow].Cells[c + jCol].Value = null; //Instead of empty string
                        }
                    }

                    jCol += 1;
                } // end while
                prg_new.Value = iRow;
                prg_new.PerformStep();
                iRow += 1;
            } // end while
            prg_new.Visible = false;
        } // PasteInData


        private void Form1_Resize(object sender, EventArgs e)
        {
            /*
            tabControl.Width = Width - 40;
            inputDataView.Width = inputTab.Width - 25;

            Form f1 = sender as Form;

            tabControl.Height = this.Height - 70;

            inputDataView.Height = inputTab.Height - paramsPanel.Height - 30;
            paramsPanel.Location = new Point(paramsPanel.Location.X, inputDataView.Height + 20);
            */
        }



        private int SetRowId(DataGridView dw)
        {
            if (dw.SelectedRows.Count != 1)
            {
                if (dw.SelectedCells.Count != 1)
                {
                    System.Windows.Forms.MessageBox.Show("Select exactly one cell or one row");
                    return -1;
                }
                else
                {
                    return dw.SelectedCells[0].RowIndex;
                }
            }
            else
            {
                return dw.SelectedRows[0].Index;
            }

        }





        private bool ContainsBeginning(string stringToAnalyze, string[] possibleBeginnings)
        {
            foreach (var beginning in possibleBeginnings)
            {
                if (stringToAnalyze.StartsWith(beginning)) return true;
            }
            return false;
        }

        class StaticData
        {
            public int nameColumnId = -1;
            public int typeColumnId = -1;
            public int sColumnId = -1;
            public int dipoleStrengthColumnId = -1;
            public int quadStrengthColumnId = -1;

            public int betaXColumn = -1;
            public int betaYColumn = -1;
            public int etaXColumn = -1;
            public int etaPXColumn = -1;
            public int alphaXColumn = -1;
            public int alphaYColumn = -1;

            //public int datastartRowId = 0;
            public int referenceRowId = -1;

            public int sMode = 0;
            public int dipoleFormat = 0;
            public int quadFormat = 0;
            public bool quadStrengthSameColumn = false;
            public Vector3 refLocation = new Vector3(0, 0, 0);
            public Vector3 refDirection = new Vector3(0, 0, 1);  //Z
            //public double beamEnergy;

            public string[] driftNames = { "DRIFT", "SEXTUPOLE" };
            public string[] dipoleNames = { "LBEND", "RBEND" };
            public string[] quadNames = { "QUAD", "QUADRUPOLE" };

            //public SurveySequence storedSequence;
            //public Dictionary<string, double> storedValues;
            //public Dictionary<string, SurveyObject> storedObjects;
        };

        /*
        class ElementProperties
        {
            //Input parameters
            //public string name;
            //ElementType type = ElementType.Unknown;
            //public double s; //starting point in beam coordinate
            // public double value; //strength in T or gradient in T/m
            public double betaX, betaY, etaX, etaPX, alphaX, alphaY;
            //Derived properties
            //public double length;
            //public Vector3 startLocation;
            // double startTheta;
        };
        */

        class SurveyObject
        {
            //public string name; //redundant, for display
            //public string type;
            public Dictionary<string, double> properties;
            public SurveyObject()
            {
                properties = new Dictionary<string, double>();
            }
        };

        class SurveyBetaInstance
        {
            // public double s; //starting point in beam coordinate
            // public double betaX, betaY, etaX, etaPX, alphaX, alphaY;
        }

        /*
        class SurveyInstance
        {
            public string instanceName;
            public double centerLocation;
            public SurveyObject surveyObject;
            public ElementProperties disp;
            public bool hasDispersionData;
            public bool fromDispFile;
            public SurveyInstance()
            {
                hasDispersionData = false;
                fromDispFile = false;
                disp = new ElementProperties();
            }
        };
        */

        /*
        class SurveySequence
        {
            public string name;
            public string referenceType;
            public double length;
            //public List<SurveyInstance> instanceList;
        };
        */

        private void addLineButton_new_Click(object sender, EventArgs e)
        {
            if (sequenceGrid.SelectedRows.Count > 0)
            {
                sequenceGrid.Rows.Insert(Math.Min(sequenceGrid.SelectedRows[0].Index, sequenceGrid.SelectedRows[sequenceGrid.SelectedRows.Count - 1].Index), sequenceGrid.SelectedRows.Count);
            }
        }

        private void remLineButton_new_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewRow row in sequenceGrid.SelectedRows)
            {
                if (sequenceGrid.SelectedRows[0].Index != sequenceGrid.Rows.Count - 1)
                {
                    sequenceGrid.Rows.Remove(row);
                }
            }
        }

        private void pasteFromClipboardButton_new_Click(object sender, EventArgs e)
        {
            PasteInData(ref sequenceGrid);
        }

        private void resetTableButton_new_Click(object sender, EventArgs e)
        {
            var confirmResult = MessageBox.Show("Clear all data?",
                                "Confirm table reset",
                                MessageBoxButtons.YesNo);
            if (confirmResult != DialogResult.Yes) return;
            sequenceGrid.Rows.Clear();
        }

        private void getRefRowButton_Click(object sender, EventArgs e)
        {
            int refRowId = SetAnalyzedRowId();
            if (refRowId != -1)
            {
                refRowTextBox.Text = (refRowId + 1).ToString();
                sequenceGrid.Rows[refRowId].Cells["startX"].Value = refXText.Text;
                sequenceGrid.Rows[refRowId].Cells["startZ"].Value = refZText.Text;
                sequenceGrid.Rows[refRowId].Cells["thetaStart"].Value = refThetaText.Text;
                sequenceGrid.Rows[refRowId].Cells["s"].Value = "0";
                try
                {
                    CalcPositionsFromReference();
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error while calculating positions from reference row\n: " + ex.Message);
                }
            }
        }

        private void calcOthersButton_Click(object sender, EventArgs e)
        {
            CalcPositionsFromReference();
        }

        private void CalcPositionsFromReference()
        {
            int refRowId;
            try
            {
                refRowId = Convert.ToInt16(refRowTextBox.Text) - 1;
            }
            catch
            {
                MessageBox.Show("Set reference row");
                return;
            }

            if (refRowId == -1)
            {
                MessageBox.Show("Set reference row");
                return;
            }

            //Fill downwards
            int actualRowId = refRowId + 1;
            while (actualRowId <= sequenceGrid.RowCount - 2)
            {
                if (sequenceGrid.Rows[actualRowId].Cells["Length"].Value == null) break; //Stop on empty line
                CalcPosition(actualRowId, 1);
                ColorRow(sequenceGrid.Rows[actualRowId]);
                actualRowId++;
            }
            int lastValidRowId = actualRowId - 1;
            CalcPosition(lastValidRowId, 0); //z limit

            //Fill upwards
            actualRowId = refRowId - 1;
            while (actualRowId >= 0)
            {
                if (sequenceGrid.Rows[actualRowId].Cells["Length"].Value == null) break; //Stop on empty line
                CalcPosition(actualRowId, -1);
                ColorRow(sequenceGrid.Rows[actualRowId]);
                actualRowId--;
            }
        }

        private void bAngleButton_Click(object sender, EventArgs e)
        {
            try
            {
                foreach (DataGridViewRow row in sequenceGrid.Rows)
                {
                    ConvertBtoAngle(row.Cells);
                    ColorRow(row);
                }
            }
            catch
            {

            }
        }

        private void browseButton_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog folderBrowserDialog1 = new FolderBrowserDialog();
            if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
            {
                dirComboBox.Text = folderBrowserDialog1.SelectedPath;
                if (ValidateDir(dirComboBox))
                {
                    AddToRecentDirs(dirComboBox.Text);
                    SaveRecentDirs();
                }
            }
        }

        private void SaveRecentDirs()
        {
            try
            {
                // Read the file and display it line by line.  
                using (System.IO.StreamWriter file =
                    new System.IO.StreamWriter("recentDirs.txt"))
                {
                    foreach (var item in dirComboBox.Items)
                    {
                        file.WriteLine(item);
                    }
                    file.Close();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Can't save recent Dirs:\n" + ex.Message); //Fail silently
            }
        }

        private bool ValidateDir(ComboBox source)
        {
            if (Directory.Exists(source.Text))
            {
                source.BackColor = Color.FromArgb(220, 255, 220);
                return true;
            }
            else
            {
                source.BackColor = Color.FromArgb(255, 220, 220);
                return false;
            }
        }

        private void browseBXYbutton_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog1 = new OpenFileDialog();

            //openFileDialog1.InitialDirectory = "c:\\";
            openFileDialog1.Filter = "BXY files (*.bxy)|*.bxy|All files (*.*)|*.*";
            openFileDialog1.FilterIndex = 0;
            openFileDialog1.RestoreDirectory = true;

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                bxyFileNameText.Text = Path.GetFileName(openFileDialog1.FileName);
            }
        }

        private void getStartLineButton_Click(object sender, EventArgs e)
        {
            int refRowId = SetRowId(sequenceGrid);
            if (refRowId != -1) startRowText.Text = (refRowId + 1).ToString();
        }

        private void getEndLineButton_Click(object sender, EventArgs e)
        {
            int refRowId = SetRowId(sequenceGrid);
            if (refRowId != -1) endRowText.Text = (refRowId + 1).ToString();
        }

        private void writeParamFilesButton_Click(object sender, EventArgs e)
        {
            if (dirComboBox.Text == "")
            {
                System.Windows.Forms.MessageBox.Show("Directory not defined");
                return;
            }
            if (!(
                CheckOutputDir() &&
                CheckTableFilled() &&
                CheckIfSCalculated() &&
                CheckStartStopValidity()
                    )) return;
            writeFilesButton.Enabled = false;
            int startLine, endLine;
            if (startRowText.Text != "") startLine = Convert.ToInt16(startRowText.Text) - 1;
            else startLine = 0;
            if (endRowText.Text != "") endLine = Convert.ToInt16(endRowText.Text) - 1;
            else endLine = sequenceGrid.RowCount - 2;
            int index = 0;

            int paramCount = 0;
            int magCount = 0;

            for (int r = Math.Max(0, startLine); r <= endLine; r++)
            {
                DataGridViewCellCollection cells = sequenceGrid.Rows[r].Cells;
                if (cells["angle"].Value == null && cells["K1L"].Value == null) continue; //skip drifts
                string fileName;
                try
                {
                    if (cells["AVname"].Value != null)
                    {
                        fileName = cells["AVname"].Value.ToString();
                    } else
                    {
                        fileName = "row_" + r;
                    }

                }
                catch (Exception ex)
                {
                    writeFilesButton.Enabled = true;
                    MessageBox.Show("Row " + (r + 1).ToString() + " is not a drift thus it needs a name:\n" + ex.Message);
                    return;
                }


                index++;
                fileName = ConvertSpecialCharToUnderscore(fileName);

                string prefix = "";
                if (prefixCheckbox.Checked)
                {
                    prefix = (index).ToString() + "_";
                    while (prefix.Length < 5) prefix = "0" + prefix;
                }

                string prefix2 = "";
                if (customPrefixText.Text.Length > 0) prefix2 = customPrefixText.Text + "_";

                string suffix = "";
                if (suffixCheckbox.Checked)
                {
                    suffix = "_s_" + cells["s"].Value.ToString();
                }

                string magFileName = prefix + prefix2 + fileName + suffix + ".mag";
                string paramFileName = prefix + prefix2 + fileName + suffix + ".param";

                if (cells["thetaStart"].Value == null)
                {
                    writeFilesButton.Enabled = true;
                    System.Windows.Forms.MessageBox.Show("Theta start value is empty. Set reference or click \"calc others\"");
                    return;
                }
                if (cells["K1L"].Value != null)
                { //Quad: param and mag
                    if (cells["grad"].Value == null && cells["K1L"].Value != null) //Calc gradient from K1L if not done yet
                    {
                        ConvertBtoAngle(cells);
                    }
                    using (StreamWriter writetext = new StreamWriter(dirComboBox.Text + "\\" + magFileName))
                    {
                        string mag;
                        mag = (Convert.ToDouble(cells["startX"].Value) * 100.0).ToString("G10");
                        mag += " 0 ";
                        mag += (Convert.ToDouble(cells["startZ"].Value) * 100.0).ToString("G10");
                        mag += "\r\n0 ";
                        mag += cells["thetaStart"].Value.ToString(); //No conversion needed since we use inverse theta notation
                        mag += " 0\r\n";
                        mag += cells["grad"].Value.ToString();
                        mag += " ";
                        mag += (Convert.ToDouble(cells["length"].Value) * 100.0).ToString("G10");
                        mag += "\r\n";
                        if (cells["angle"].Value != null) //Has dipole component, assuming curved quadrupole
                        {
                            if (cells["B"].Value == null && cells["angle"].Value != null) //Calculate dipole gradient from angle if not done yet
                            {
                                ConvertBtoAngle(cells);
                            }
                            mag += "0.0  " + (Convert.ToDouble(cells["B"].Value)).ToString("G10") + " 0.0\r\n"; //only Y component
                        }
                        writetext.WriteLine(mag);
                    }
                    magCount++;
                }
                using (StreamWriter writetext = new StreamWriter(dirComboBox.Text + "\\" + paramFileName))
                {
                    string param;
                    param = "param_file_version: 5\r\n"; //with startS and globalComponents
                    double startX = Convert.ToDouble(cells["startX"].Value);
                    double startY = 0.0; //Assuming reference orbit always in XZ plane
                    double startZ = Convert.ToDouble(cells["startZ"].Value);
                    double startS = Convert.ToDouble(cells["s"].Value);

                    //Convert local X,Y offset to global X,Y,Z coordinates
                    double offsetX = Convert.ToDouble(cells["xOffset"].Value) * Math.Cos(-Convert.ToDouble(cells["thetaStart"].Value));
                    double offsetY = Convert.ToDouble(cells["yOffset"].Value);
                    double offsetZ = Convert.ToDouble(cells["xOffset"].Value) * Math.Sin(-Convert.ToDouble(cells["thetaStart"].Value)); //inverse theta notation

                    //Apply offset
                    startX += offsetX;
                    startY += offsetY;
                    startZ += offsetZ;

                    double dirTheta = Convert.ToDouble(cells["thetaStart"].Value);
                    double dirAlpha = 0.0;

                    //Apply px, py offsets. Synrad angles are inverse (positive theta = negative X, positive alpha = negative Y)
                    double offsetTheta = -Math.Atan(Convert.ToDouble(cells["xAngle"].Value));
                    double offsetAlpha = -Math.Atan(Convert.ToDouble(cells["yAngle"].Value));

                    dirTheta += offsetTheta;
                    dirAlpha += offsetAlpha;

                    //Theta and alpha functions are the inverse of MADX (positive theta = negative X, positive alpha = negative Y)
                    //Note that COS is a pair function, so sign of angle doesn't matter
                    double dirX = Math.Cos(-dirAlpha) * Math.Sin(-dirTheta);
                    double dirY = Math.Sin(-dirAlpha);
                    double dirZ = Math.Cos(-dirAlpha) * Math.Cos(-dirTheta);

                    param += "startPos_cm: " + (startX * 100.0).ToString("G10") + " " + (startY * 100.0).ToString("G10") + " " + (startZ * 100.0).ToString("G10") + "\r\n";
                    param += "startDir_cm: " + dirX + " " + dirY + " " + dirZ + "\r\n";
                    param += "dL_cm: " + Convert.ToDouble(dLtext.Text) + "\r\n"; //1000 steps
                    param += "startS_m: " + startS + "\r\n"; //start S
                    param += "boundaries_cm: 1.00000000000000E+06 1.00000000000000E+06 " + (Convert.ToDouble(cells["zlimit"].Value) * 100.0) + "\r\n";
                    param += "particleMass_GeV: " + Convert.ToDouble(pMassText.Text) + "\r\n";
                    param += "beamEnergy_GeV: " + Convert.ToDouble(beamEText.Text) + "\r\n";
                    param += "beamCurrent_mA: " + Convert.ToDouble(currentText.Text) + "\r\n";
                    double coupling = Convert.ToDouble(emittYtext.Text) / Convert.ToDouble(emittXtext.Text);
                    double emittance = Convert.ToDouble(emittXtext.Text) * 100.0 * (1.0 + coupling);
                    param += "emittance_cm: " + emittance + "\r\n";
                    param += "energy_spread_percent: " + energySpreadText.Text + "\r\n";
                    param += "coupling_percent: " + 100.0 * coupling + "\r\n";
                    param += "const_beta_x_cm: -1.00000000000000E+00\r\n";
                    param += "BXYfileName: \"" + bxyFileNameText.Text + "\"\r\n";
                    param += "E_min_eV: " + Convert.ToDouble(eMinText.Text) + "\r\n";
                    param += "E_max_eV: " + Convert.ToDouble(eMaxText.Text) + "\r\n";
                    param += "enable_par_polarization: 1\r\n";
                    param += "enable_ort_polarization: 1\r\n";
                    param += "psiMax_X_Y_rad: 3.14159265358979E+00 3.14159265358979E+00\r\n";
                    param += "structureId:0\r\n";
                    param += "globalComponents:1\r\n";

                    if (cells["K1L"].Value != null) //Quad with optional dipole component
                    {
                        param += "Bx_mode: ";
                        if (cells["B"].Value == null)
                        {
                            param += 5; //Quad
                        }
                        else
                        {
                            param += 10; //Curved quadrupole
                        }
                        param += "\r\n";
                        param += "Bx_fileName: \"" + magFileName +"\"\r\n";
                    }
                    else //Dipole
                    {
                        param += "Bx_mode: 1\r\n";
                        param += "Bx_const_Tesla: 0.00000000000000E+00\r\n";
                    }
                    param += "By_mode: 1\r\n";
                    if (cells["K1L"].Value != null) //Quad
                    {
                        param += "By_const_Tesla: 0.00000000000000E+00\r\n";
                    }
                    else //Dipole
                    {
                        if (cells["B"].Value == null && cells["angle"].Value != null) //Calculate dipole gradient from angle if not done yet
                        {
                            ConvertBtoAngle(cells);
                        }
                        param += "By_const_Tesla: " + (Convert.ToDouble(cells["B"].Value)).ToString("G10") + "\r\n";
                    }
                    param += "Bz_mode: 1\r\n";
                    param += "Bz_const_Tesla: 0.00000000000000E+00"; //last line, no newline needed
                    writetext.WriteLine(param);
                }
                paramCount++;
            }
            writeFilesButton.Enabled = true;
            Log(String.Format("Written {0} .param and {1} .mag files to {2}",paramCount,magCount, dirComboBox.Text));
        }

        private bool CheckTableFilled()
        {
            if (sequenceGrid.RowCount==1)
            {
                MessageBox.Show("Table is empty (only one uncommitted line).");
                return false;
            }
            int actualRowId = 0; //Start from first line
            while (actualRowId <= sequenceGrid.RowCount-2)
            {
                if (sequenceGrid.Rows[actualRowId].Cells["Length"].Value == null)
                {
                    MessageBox.Show("Line " + (actualRowId + 1) + " has empty \"length\" cell.\nDelete all empty rows at the end of the table\n(except the very last allowing new element input, marked with *)\nYou can select the row header and click the \"Remove row(s)\" button.");
                    return false;
                }
                actualRowId++;
            }
            return true;
        }

        private bool CheckStartStopValidity()
        {
            int lastValidRowId = sequenceGrid.RowCount - 2;
            int startLine,endLine;
            if (startRowText.Text != "")
            {
                try
                {
                    startLine = Convert.ToInt16(startRowText.Text) - 1;
                } catch {
                    MessageBox.Show("Couldn't parse start row number \"" + startRowText.Text + "\".");
                    return false;
                }
                if (startLine < 0)
                {
                    MessageBox.Show("Start row (" + (startLine+1)+") must be positive or empty.");
                    return false;
                }
                if (startLine >= lastValidRowId )
                {
                    MessageBox.Show("Start row (" + (startLine+1)+") must be smaller than the committed row count ("+(lastValidRowId + 1)+").");
                    return false;
                }
            }
            if (endRowText.Text != "")
            {
                    try
                    {
                        endLine = Convert.ToInt16(endRowText.Text) - 1;
                    } catch {
                    MessageBox.Show("Couldn't parse start row number \"" + startRowText.Text + "\".");
                    return false;
                }
                if (endLine < 0)
                {
                    MessageBox.Show("End row (" + (endLine+1)+ ") must be positive or empty.");
                    return false;
                }
                if (endLine> lastValidRowId)
                {
                    MessageBox.Show("End row (" + (endLine + 1) + ") can't be larger than the committed row count (" + (lastValidRowId + 1) + ").");
                    return false;
                }
                if (startRowText.Text != "")
                {
                    startLine = Convert.ToInt16(startRowText.Text) - 1;
                    if (startLine >= endLine)
                    {
                        MessageBox.Show("Start row (" + (startLine + 1) + ") must be smaller than end row (" + (endLine + 1) + ").");
                        return false;
                    }
                } 
            }
            return true;
        }

        private static string ConvertSpecialCharToUnderscore(string fileName, bool convertDot = false)
        {
            StringBuilder newName = new StringBuilder(fileName);
            for (int i = 0; i < fileName.Length; i++)
            {
                if (fileName[i] == ' ' || (convertDot && fileName[i] == '.'))
                {
                    newName[i] = '_';
                }
            }
            fileName = newName.ToString();
            return fileName;
        }

        private void writeBXYfileButton_Click(object sender, EventArgs e) //Write bxy file
        {
            if (dirComboBox.Text == "" || bxyFileNameText.Text == "")
            {
                System.Windows.Forms.MessageBox.Show("Directory or BXY filename not defined");
                return;
            }
            if (!(
            CheckOutputDir() &&
            CheckTableFilled() &&
            CheckIfSCalculated() &&
            CheckStartStopValidity()
                )) return;

            int startLine, endLine;
            if (startRowText.Text != "") startLine = Convert.ToInt16(startRowText.Text) - 1;
            else startLine = 0;
            if (endRowText.Text != "") endLine = Convert.ToInt16(endRowText.Text) - 1;
            else endLine = sequenceGrid.RowCount - 2;
            if (sequenceGrid.Rows[startLine].Cells["betaX"].Value == null || sequenceGrid.Rows[startLine].Cells["betaY"].Value == null)
            {
                System.Windows.Forms.MessageBox.Show("The first exported line (number " + (startLine + 1).ToString() + " ) has to have a BetaX and BetaY value, as it will be kept until a new line defines a new Beta value.\nYou can write 0.01 as placeholder for near-ideal beam.");
                return;
            }

            bool sInitialized = false;
            double actS = 0.0; //position with BXY entry

            List<List<double>> bxyEntries = new List<List<double>>();

            string fullPath = dirComboBox.Text + "\\" + bxyFileNameText.Text;

            using (StreamWriter writetext = new StreamWriter(fullPath))
            {
                writetext.WriteLine("S"); //Synrad 1.4.31+ support S coord in BXY files
                for (int r = startLine; r <= endLine; r++)
                {
                    DataGridViewCellCollection cells = sequenceGrid.Rows[r].Cells;

                    if (cells["betaX"].Value != null && cells["betaY"].Value != null) //Skip lines where no disp data
                    {

                        double S = Convert.ToDouble(cells["s"].Value);

                        if (!sInitialized || Math.Abs(S - actS) > 1E-4 || !ignoreSameSCheckBox.Checked) //Skip duplicate entries
                        {

                            List<double> bxyEntry = new List<double>(7);

                            bxyEntry.Add(S);
                            bxyEntry.Add(Convert.ToDouble(cells["betaX"].Value));
                            bxyEntry.Add(Convert.ToDouble(cells["betaY"].Value));

                            if (cells["etaX"].Value != null)
                            {
                                bxyEntry.Add(Convert.ToDouble(cells["etaX"].Value));
                            }
                            else
                            {
                                bxyEntry.Add(0.0);
                            }

                            if (cells["etaPX"].Value != null)
                            {
                                bxyEntry.Add(Convert.ToDouble(cells["etaPX"].Value));
                            }
                            else
                            {
                                bxyEntry.Add(0.0);
                            }

                            if (cells["alphaX"].Value != null)
                            {
                                bxyEntry.Add(Convert.ToDouble(cells["alphaX"].Value));
                            }
                            else
                            {
                                bxyEntry.Add(0.0);
                            }

                            if (cells["alphaY"].Value != null)
                            {
                                bxyEntry.Add(Convert.ToDouble(cells["alphaY"].Value));
                            }
                            else
                            {
                                bxyEntry.Add(0.0);
                            }
                            bxyEntries.Add(bxyEntry);
                        }
                        sInitialized = true;
                        actS = S;
                    }
                }
                List<List<double>> sortedBXY = bxyEntries.OrderBy(o => o[0]).ToList(); //We have a range of lattice(S) values now.
                //Add a last entry for the exported region's last point with last user-defined BXY values
                double lastExportedPosS = Convert.ToDouble(sequenceGrid.Rows[endLine].Cells["s"].Value) + Convert.ToDouble(sequenceGrid.Rows[endLine].Cells["length"].Value);
                if ((lastExportedPosS - sortedBXY[sortedBXY.Count - 1][0]) > 1e-4) //If last BXY entry is not at end of exported region
                {
                    sortedBXY.Add(new List<double>(sortedBXY[sortedBXY.Count - 1])); //Duplicate last entry
                    sortedBXY[sortedBXY.Count - 1][0] = lastExportedPosS; //At last exported S position
                }

                //Write to BXY file
                foreach (var bxyEntry in sortedBXY)
                {
                    string line = "";
                    line += (bxyEntry[0]).ToString("G10"); //S, Synrad expects meters
                    line += "\t";
                    line += (bxyEntry[1] * 100.0).ToString("G10"); //betaX, cm
                    line += "\t";
                    line += (bxyEntry[2] * 100.0).ToString("G10"); //betaY, cm
                    line += "\t";
                    line += (bxyEntry[3] * 100.0).ToString("G10"); //etaX, cm
                    line += "\t";
                    line += (bxyEntry[4]).ToString("G10"); //etaPX, derivative so no unit
                    line += "\t";
                    line += (bxyEntry[5]).ToString("G10"); //alphaX
                    line += "\t";
                    line += (bxyEntry[6]).ToString("G10"); //alphaY
                    line += "\t";
                    writetext.WriteLine(line);
                }
                Log("Written " + fullPath + " with " + sortedBXY.Count + " lattice entries.");  
            }
        }
        private bool CheckOutputDir()
        {
            if (!ValidateDir(dirComboBox))
            {
                if (DialogResult.OK != MessageBox.Show("Target directory\n" + dirComboBox.Text + "\ndoesn't exist yet. Create it?", "Target folder", MessageBoxButtons.OKCancel)) return false;
                try
                {
                    Directory.CreateDirectory(dirComboBox.Text);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Can't create directory\n" + dirComboBox.Text + "\n" + ex.Message);
                    return false;
                }
            }
            AddToRecentDirs(dirComboBox.Text);
            SaveRecentDirs();
            return true;
        }

        private void AddToRecentDirs(string text, bool toBack=false)
        {
            string oldText = dirComboBox.Text; //Changing items can change text
            if (!toBack)
            {
                dirComboBox.Items.Insert(0, text); //So it's at top

                //Remove duplicate entries, starting from back
                for (int i = dirComboBox.Items.Count - 1; i > 0; i--)
                {
                    if (dirComboBox.Items[i].ToString() == text)
                    {
                        dirComboBox.Items.RemoveAt(i); //Remove duplicate entries
                    }
                }
            }
            else
            {
                dirComboBox.Items.Add(text); //So it's at back
                //Remove duplicate entries, starting from back
                for (int i = dirComboBox.Items.Count - 2; i >= 0; i--)
                {
                    if (dirComboBox.Items[i].ToString() == text)
                    {
                        dirComboBox.Items.RemoveAt(i); //Remove duplicate entries
                    }
                }
            }

            dirComboBox.Text = oldText; //Restore
        }

        private void RefreshRowHeaders_analyzedView()
        {
            prg_new.Value = 0;
            prg_new.Maximum = sequenceGrid.Rows.Count;
            prg_new.Visible = true;

            for (int i = 0; i < sequenceGrid.Rows.Count; i++)
            {
                prg_new.Value = i;
                prg_new.PerformStep();
                string headerText;
                Font font = new Font(sequenceGrid.ColumnHeadersDefaultCellStyle.Font, FontStyle.Bold);


                {
                    headerText = "Row" + (i + 1).ToString();
                    font = new Font(font, FontStyle.Regular);
                }
                sequenceGrid.Rows[i].HeaderCell.Value = headerText;
                sequenceGrid.Rows[i].HeaderCell.Style.Font = font;
            }
            prg_new.Visible = false;
        }

        private void ConvertBtoAngle(DataGridViewCellCollection rowCells)
        {
            double chargeSign, E, length;
            try
            {
                chargeSign = Math.Sign(Convert.ToDouble(pMassText.Text));
            }
            catch (Exception ex)
            {
                throw new Exception("Parse particle mass \"" + pMassText + "\" failed:\n" + ex.Message);
            }
            try
            {
                E = Convert.ToDouble(beamEText.Text);
            }
            catch (Exception ex)
            {
                throw new Exception("Parse energy \"" + beamEText + "\" failed:\n" + ex.Message);
            }
            try
            {
                length = Convert.ToDouble(rowCells["length"].Value);
            }
            catch (Exception ex)
            {
                throw new Exception("Parse length \"" + rowCells["length"].Value + "\" failed:\n" + ex.Message);
            }

            //B <-> Angle
            if (NotEmptyCell(rowCells["angle"].Value))
            {
                double angle;
                try
                {
                    angle = Convert.ToDouble(rowCells["angle"].Value);
                }
                catch (Exception ex)
                {
                    throw new Exception("Parse angle \"" + rowCells["angle"].Value + "\" failed:\n" + ex.Message);
                }
                double B = chargeSign * E / 0.2998 * angle / length;
                rowCells["B"].Value = B.ToString("G10");
            }
            else if (NotEmptyCell(rowCells["B"].Value))
            {
                double B;
                B = Convert.ToDouble(rowCells["B"].Value);
                double angle = chargeSign * B * length * 0.2998 / E;
                rowCells["angle"].Value = angle.ToString("G10");
            }

            //Gradient <-> K1L
            if (NotEmptyCell(rowCells["grad"].Value))
            {
                double grad;
                try
                {
                    grad = Convert.ToDouble(rowCells["grad"].Value);
                }
                catch (Exception ex)
                {
                    throw new Exception("Parse gradient \"" + rowCells["grad"].Value + "\" failed:\n" + ex.Message);
                }
                double K1L = -chargeSign * grad * 0.2998 / E * length;
                rowCells["K1L"].Value = K1L.ToString("G10");
            }
            else if (NotEmptyCell(rowCells["K1L"].Value))
            {
                double K1L;
                try
                {
                    K1L = Convert.ToDouble(rowCells["K1L"].Value);
                }
                catch (Exception ex)
                {
                    throw new Exception("Parse K1L \"" + rowCells["K1L"].Value + "\" failed:\n" + ex.Message);
                }
                double grad = -chargeSign * K1L / length * E / 0.2998;
                rowCells["grad"].Value = grad.ToString("G10");
            }

        }

        private void InvertChargeAndAngles()
        {

            double chargeSign, E, length;
            try
            {
                double charge = Convert.ToDouble(pMassText.Text);
                pMassText.Text = (-charge).ToString();
                chargeSign = Math.Sign(charge);
            }
            catch (Exception ex)
            {
                throw new Exception("Parse particle mass \"" + pMassText + "\" failed:\n" + ex.Message);
            }


            foreach (DataGridViewRow row in sequenceGrid.Rows)
            {
                var rowCells = row.Cells;
                try
                {
                    E = Convert.ToDouble(beamEText.Text);
                }
                catch (Exception ex)
                {
                    throw new Exception("Parse energy \"" + beamEText + "\" failed:\n" + ex.Message);
                }
                try
                {
                    length = Convert.ToDouble(rowCells["length"].Value);
                }
                catch (Exception ex)
                {
                    throw new Exception("Parse length \"" + rowCells["length"].Value + "\" failed:\n" + ex.Message);
                }

                //Invert angle. B doesn't change (inverse charge, fix B -> inverse angle)
                if (NotEmptyCell(rowCells["angle"].Value))
                {
                    double angle;
                    try
                    {
                        angle = Convert.ToDouble(rowCells["angle"].Value);
                        rowCells["angle"].Value = (-angle).ToString();
                    }
                    catch (Exception ex)
                    {
                        throw new Exception("Parse angle \"" + rowCells["angle"].Value + "\" failed:\n" + ex.Message);
                    }
                }

                //Invert gradient. K1L doesn't change (inverse charge, fix K1L -> inverse gradient)
                if (NotEmptyCell(rowCells["grad"].Value))
                {
                    double grad;
                    try
                    {
                        grad = Convert.ToDouble(rowCells["grad"].Value);
                        rowCells["grad"].Value = (-grad).ToString();
                    }
                    catch (Exception ex)
                    {
                        throw new Exception("Parse gradient \"" + rowCells["grad"].Value + "\" failed:\n" + ex.Message);
                    }
                }
            }
        }

        private bool NotEmptyCell(object value)
        {
            return value != null && value != DBNull.Value && !String.IsNullOrWhiteSpace(value.ToString());
        }

        private void CalcPosition(int rowId, int direction)
        {
            int refRowId = rowId - direction;

            DataGridViewCellCollection refCells = sequenceGrid.Rows[refRowId].Cells;
            DataGridViewCellCollection currCells = sequenceGrid.Rows[rowId].Cells;

            double refX = Convert.ToDouble(refCells["startX"].Value);
            double refZ = Convert.ToDouble(refCells["startZ"].Value);
            double refTheta = Convert.ToDouble(refCells["thetaStart"].Value); //inverse in synrad
            double refS = Convert.ToDouble(refCells["s"].Value);

            double angle = 0.0;

            DataGridViewCellCollection angleCells = refCells;
            if (direction == -1)
            {
                angleCells = currCells;
            }

            double length = Convert.ToDouble(angleCells["length"].Value);
            // if (angleCells["angle"].Value == null && angleCells["B"].Value != null)
            // {
            ConvertBtoAngle(angleCells); //Always convert, maybe beam energy textbox value changed
                                         // }

            if (NotEmptyCell(angleCells["angle"].Value))
            {
                angle = Convert.ToDouble(angleCells["angle"].Value);
            }
            else
            {
                angle = 0.0;
            }

            double dZP, dXP;

            if (angle == 0.0) //element has no bend
            {
                dZP = length;
                dXP = 0;

            }
            else //bend, initial dir: +Z, pos. bend towards +X;   center of rotation at X=+r,Z=0
            {
                double r = length / angle; //length=r*angle -> r=length/angle
                dZP = r * Math.Sin(angle); 
                dXP = r * (1.0 - Math.Cos(angle));
            }


            double dX = dZP * Math.Sin(direction * refTheta) + dXP * Math.Cos(direction * refTheta); //Inverse since we use inverse theta (compared to MADX) notation
            double dZ = dZP * Math.Cos(refTheta) - dXP * Math.Sin(refTheta);

            if (direction == 1)
            {
                refCells["zLimit"].Value = (refZ + dZ).ToString("G10");
                currCells["startX"].Value = (refX - dX).ToString("G10"); //Negative since our theta is the inverse of MADX (we want that a positive rotation increases theta)
                currCells["startZ"].Value = (refZ + dZ).ToString("G10");
                currCells["thetaStart"].Value = (refTheta + angle).ToString("G10");
                currCells["s"].Value = (refS + length).ToString("G10");
            }
            else if (direction == -1)
            {
                currCells["zLimit"].Value = (refZ).ToString("G10");
                currCells["startX"].Value = (refX - dX).ToString("G10");
                currCells["startZ"].Value = (refZ - dZ).ToString("G10");
                currCells["thetaStart"].Value = (refTheta - angle).ToString("G10");
                currCells["s"].Value = (refS - length).ToString("G10");
            }
            else
            {
                refCells["zLimit"].Value = (refZ + 1.0 * dZ).ToString("G10");
            }

        }

        private int SetAnalyzedRowId()
        {
            if (sequenceGrid.SelectedRows.Count != 1)
            {
                if (sequenceGrid.SelectedCells.Count != 1)
                {
                    System.Windows.Forms.MessageBox.Show("Select exactly one cell or one row");
                    return -1;
                }
                else
                {
                    return sequenceGrid.SelectedCells[0].RowIndex;
                }
            }
            else
            {
                return sequenceGrid.SelectedRows[0].Index;
            }

        }

        private void getResultButton_Click(object sender, EventArgs e)
        {

        }

        private void readTwissButton_Click(object sender, EventArgs e)
        {
            System.IO.Stream myStream = null;
            OpenFileDialog openFileDialog1 = new OpenFileDialog();

            //openFileDialog1.InitialDirectory = "c:\\";
            openFileDialog1.Filter = "All files (*.*)|*.*";
            openFileDialog1.FilterIndex = 0;
            openFileDialog1.RestoreDirectory = true;

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {

                    sequenceGrid.Rows.Clear();
                    sequenceGrid.Rows.Add();

                    using (myStream = openFileDialog1.OpenFile())
                    {
                        System.IO.StreamReader reader = new System.IO.StreamReader(myStream);
                        int
                                nameCol = -1,
                                keywordCol = -1,
                                sCol = -1,
                                lCol = -1,
                                angleCol = -1,
                                k1LCol = -1,
                                dxCol = -1,
                                dyCol = -1,
                                pxCol = -1,
                                pyCol = -1,
                                betXCol = -1,
                                betYCol = -1,
                                etaCol = -1,
                                etaPCol = -1,
                                alfXCol = -1,
                                alfYCol = -1;
                        bool columnsDefined = false;

                        string line;

                        while ((line = reader.ReadLine()) != null)
                        {
                            line = line.TrimStart(); //Remove starting whitespace
                            if (line.Length == 0) continue; //skip empty lines

                            if (line.StartsWith("!")) continue; //skip comments

                            if (line.StartsWith("$")) continue; //skip variable type
                            if (line.StartsWith("@")) //Parameter definition
                            {

                                string[] tokens = line.Split(new char[0], StringSplitOptions.RemoveEmptyEntries); //By whitespace
                                ExtractFromQuotationMarks(tokens);
                                if (tokens[1] == "PARTICLE")
                                {
                                    switch (tokens[3])
                                    {
                                        case "ELECTRON":
                                            pMassText.Text = "-0.00051099891";
                                            break;
                                        case "POSITRON":
                                            pMassText.Text = "0.00051099891";
                                            break;
                                        case "PROTON":
                                            pMassText.Text = "0.9382720813";
                                            break;
                                    }
                                }
                                else if (tokens[2] == "%le")
                                {
                                    double value = Convert.ToDouble(tokens[3]);
                                    string paramName = tokens[1];
                                    switch (paramName)
                                    {
                                        case "ENERGY":
                                            beamEText.Text = value.ToString("G10");
                                            break;
                                        case "BCURRENT":
                                            currentText.Text = (value * 1000.0).ToString("G10");
                                            break;
                                        case "SIGE":
                                            energySpreadText.Text = (value * 100.0).ToString("G10");
                                            break;
                                        case "EX":
                                            emittXtext.Text = value.ToString("G10");
                                            break;
                                        case "EY":
                                            emittYtext.Text = value.ToString("G10");
                                            break;
                                    }
                                }
                            }
                            else if (line.StartsWith("*")) //Column definition
                            {
                                string[] tokens = line.Split(new char[0], StringSplitOptions.RemoveEmptyEntries); //By whitespace
                                ExtractFromQuotationMarks(tokens);
                                for (int i = 0; i < tokens.Length; i++)
                                {
                                    switch (tokens[i])
                                    {
                                        case "NAME":
                                            nameCol = i - 1;
                                            break;
                                        case "KEYWORD":
                                            keywordCol = i - 1;
                                            break;
                                        case "S":
                                            sCol = i - 1;
                                            break;
                                        case "L":
                                            lCol = i - 1;
                                            break;
                                        case "ANGLE":
                                            angleCol = i - 1;
                                            break;
                                        case "K1L":
                                            k1LCol = i - 1;
                                            break;
                                        case "X":
                                            dxCol = i - 1;
                                            break;
                                        case "Y":
                                            dyCol = i - 1;
                                            break;
                                        case "PX":
                                            pxCol = i - 1;
                                            break;
                                        case "PY":
                                            pyCol = i - 1;
                                            break;
                                        case "BETX":
                                            betXCol = i - 1;
                                            break;
                                        case "BETY":
                                            betYCol = i - 1;
                                            break;
                                        case "DX":
                                            etaCol = i - 1;
                                            break;
                                        case "DPX":
                                            etaPCol = i - 1;
                                            break;
                                        case "ALFX":
                                            alfXCol = i - 1;
                                            break;
                                        case "ALFY":
                                            alfYCol = i - 1;
                                            break;
                                    }
                                }
                                if (nameCol == -1) throw new Exception("TWISS file doesn't have NAME column");
                                if (keywordCol == -1) throw new Exception("TWISS file doesn't have KEYWORD column");
                                if (sCol == -1) throw new Exception("TWISS file doesn't have S column");
                                if (lCol == -1) throw new Exception("TWISS file doesn't have L column");
                                if (angleCol == -1) throw new Exception("TWISS file doesn't have ANGLE column");
                                if (k1LCol == -1) throw new Exception("TWISS file doesn't have K1L column");
                                if (dxCol == -1) throw new Exception("TWISS file doesn't have X column");
                                if (dyCol == -1) throw new Exception("TWISS file doesn't have Y column");
                                if (pxCol == -1) throw new Exception("TWISS file doesn't have PX column");
                                if (pyCol == -1) throw new Exception("TWISS file doesn't have PY column");
                                if (betXCol == -1) throw new Exception("TWISS file doesn't have BETX column");
                                if (betYCol == -1) throw new Exception("TWISS file doesn't have BETY column");
                                if (etaCol == -1) throw new Exception("TWISS file doesn't have DX column");
                                if (etaPCol == -1) throw new Exception("TWISS file doesn't have DPX column");
                                if (alfXCol == -1) throw new Exception("TWISS file doesn't have ALFX column");
                                if (alfYCol == -1) throw new Exception("TWISS file doesn't have ALFY column");
                                columnsDefined = true;
                            }
                            else /*if (line.StartsWith(" "))*/ //Data row
                            {
                                if (!columnsDefined) throw new Exception("TWISS file: Data row reached before column definition");

                                string[] tokens = line.Split(new char[0], StringSplitOptions.RemoveEmptyEntries); //By whitespace
                                ExtractFromQuotationMarks(tokens);

                                sequenceGrid.Rows.Add();
                                DataGridViewRow currentRow = sequenceGrid.Rows[sequenceGrid.RowCount - 3];
                                DataGridViewCellCollection cells = currentRow.Cells;
                                DataGridViewCellCollection nextCells = sequenceGrid.Rows[sequenceGrid.RowCount - 2].Cells;
                                
                                cells["AVname"].Value = tokens[nameCol];

                                cells["length"].Value = Convert.ToDouble(tokens[lCol]).ToString("G10");

                                if (Convert.ToDouble(tokens[k1LCol]) != 0.0)
                                {
                                    cells["K1L"].Value = Convert.ToDouble(tokens[k1LCol]).ToString("G10");
                                }
                                if (Convert.ToDouble(tokens[angleCol]) != 0.0)
                                {
                                    cells["angle"].Value = Convert.ToDouble(tokens[angleCol]).ToString("G10");
                                }

                                cells["twissS"].Value = Convert.ToDouble(tokens[sCol]).ToString("G10");

                                nextCells["xOffset"].Value = Convert.ToDouble(tokens[dxCol]).ToString("G10");
                                nextCells["yOffset"].Value = Convert.ToDouble(tokens[dyCol]).ToString("G10");
                                nextCells["xAngle"].Value = Convert.ToDouble(tokens[pxCol]).ToString("G10");
                                nextCells["yAngle"].Value = Convert.ToDouble(tokens[pyCol]).ToString("G10");

                                nextCells["betaX"].Value = Convert.ToDouble(tokens[betXCol]).ToString("G10");
                                nextCells["betaY"].Value = Convert.ToDouble(tokens[betYCol]).ToString("G10");
                                nextCells["etaX"].Value = Convert.ToDouble(tokens[etaCol]).ToString("G10");
                                nextCells["etaPX"].Value = Convert.ToDouble(tokens[etaPCol]).ToString("G10");
                                nextCells["alphaX"].Value = Convert.ToDouble(tokens[alfXCol]).ToString("G10");
                                nextCells["alphaY"].Value = Convert.ToDouble(tokens[alfYCol]).ToString("G10");

                            }
                        } //end of readline block
                    }
                    try
                    {
                        foreach (DataGridViewRow row in sequenceGrid.Rows)
                        {
                            ConvertBtoAngle(row.Cells);
                            ColorRow(row);
                        }
                    }
                    catch
                    {

                    }
                    //RefreshRowHeaders_analyzedView();
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error: " + ex.Message);
                }
            }
        }

        private void ColorRow(DataGridViewRow row)
        {
            
            if (row.Cells["K1L"].Value != null)
            {
                row.DefaultCellStyle.BackColor = Color.FromArgb(230, 230, 255); //Quad or curved dipole, purple
            } else if (row.Cells["B"].Value != null)
            {
                row.DefaultCellStyle.BackColor = Color.FromArgb(230, 255, 230); //Dipole, green
            } else
            {
                row.DefaultCellStyle.BackColor = Color.White;
            }
        }

        private static void ExtractFromQuotationMarks(string[] tokens)
        {
            //Extract strings from quotation marks
            for (int i = 0; i < tokens.Length; i++)
            {
                var splitToken = tokens[i].Split('\"');
                if (splitToken.Length == 3) tokens[i] = splitToken[1];
            }
        }

        private void searchBeginningButton_Click(object sender, EventArgs e)
        {
            SearchRow(true);
        }

        private void searchEndButton_Click(object sender, EventArgs e)
        {
            SearchRow(false);
        }

        private void SearchRow(bool beginning)
        {
            string searchTerm = searchTermTextbox.Text.ToLower();
            if (searchTerm.Length == 0)
            {
                searchResultLabel.Text = "Empty search term";
                return;
            }
            //Fill downwards
            int foundRowId = -1;
            for (int rowId = 0; foundRowId == -1 && rowId <= sequenceGrid.RowCount-2; rowId++)
            {
                bool match;
                if (sequenceGrid.Rows[rowId].Cells["AVname"].Value == null)
                {
                    match = false;
                    searchTermTextbox.BackColor = Color.FromArgb(255, 255, 255);
                }
                else
                {
                    if (beginning) match = sequenceGrid.Rows[rowId].Cells["AVname"].Value.ToString().ToLower().StartsWith(searchTerm);
                    else match = sequenceGrid.Rows[rowId].Cells["AVname"].Value.ToString().ToLower().EndsWith(searchTerm);
                }
                if (match)
                {
                    foundRowId = rowId;
                }
            }
            if (foundRowId == -1)
            {
                searchResultLabel.Text = "Not found";
                searchTermTextbox.BackColor = Color.FromArgb(255, 220, 220);
                return;
            }
            else
            {
                searchResultLabel.Text = "Found at row " + (foundRowId + 1);
                searchTermTextbox.BackColor = Color.FromArgb(220, 255, 220);
                sequenceGrid.ClearSelection();
                sequenceGrid.Rows[foundRowId].Selected = true;
                sequenceGrid.FirstDisplayedScrollingRowIndex = foundRowId;
            }
        }

        private void searchTermTextbox_TextChanged(object sender, EventArgs e)
        {
            SearchRow(true);
        }

        private void browseGeomButton_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog1 = new OpenFileDialog();

            //openFileDialog1.InitialDirectory = "c:\\";
            openFileDialog1.Filter = "SYN files (*.syn)|*.syn|All files (*.*)|*.*";
            openFileDialog1.FilterIndex = 0;
            openFileDialog1.RestoreDirectory = true;

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                geomFileTextBox.Text = Path.GetFileName(openFileDialog1.FileName);
            }
        }

        private void readApertureButton_Click(object sender, EventArgs e)
        {
            System.IO.Stream myStream = null;
            OpenFileDialog openFileDialog1 = new OpenFileDialog();

            //openFileDialog1.InitialDirectory = "c:\\";
            openFileDialog1.Filter = "All files (*.*)|*.*";
            openFileDialog1.FilterIndex = 0;
            openFileDialog1.RestoreDirectory = true;

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    //Clear existing aperture data
                    for (int i = 0; i <= sequenceGrid.RowCount-2; i++)
                    {
                        sequenceGrid.Rows[i].Cells["AptType"].Value = "";
                        sequenceGrid.Rows[i].Cells["Aperture1"].Value = "";
                        sequenceGrid.Rows[i].Cells["Aperture2"].Value = "";
                        sequenceGrid.Rows[i].Cells["Aperture3"].Value = "";
                        sequenceGrid.Rows[i].Cells["Aperture4"].Value = "";
                    }

                    using (myStream = openFileDialog1.OpenFile())
                    {
                        System.IO.StreamReader reader = new System.IO.StreamReader(myStream);
                        int sCol = -1;
                        int sMode = 0; //0=beginning 1=center 2=endpoint
                        int lengthCol = -1;
                        int apertureCol = -1;
                        int apertureMode = 0; //0=meter 1==millimeter
                        bool columnsDefined = false;

                        string line;
                        int lineNum = 0;
                        while ((line = reader.ReadLine()) != null)
                        {
                            lineNum++;

                            if (lineNum == 1) //Header
                            {
                                string[] tokens = line.Split(new char[0], StringSplitOptions.RemoveEmptyEntries); //By whitespace
                                for (int i = 0; i < tokens.Length; i++)
                                {
                                    switch (tokens[i])
                                    {
                                        case "Length":
                                            lengthCol = i;
                                            break;
                                        case "S_begin":
                                            sCol = i;
                                            sMode = 0;
                                            break;
                                        case "S_center":
                                            sCol = i;
                                            sMode = 1;
                                            break;
                                        case "S_end":
                                            sCol = i;
                                            sMode = 2;
                                            break;
                                        case "Aperture":
                                            apertureCol = i;
                                            apertureMode = 0;
                                            break;
                                        case "Aperture_m":
                                            apertureCol = i;
                                            apertureMode = 0;
                                            break;
                                        case "Aperture_mm":
                                            apertureCol = i;
                                            apertureMode = 1;
                                            break;
                                    }
                                }
                                if (sCol == -1) throw new Exception("Aperture file doesn't have S_begin, S_center or S_end column");
                                if (apertureCol == -1) throw new Exception("Aperture file doesn't Aperture, Aperture_m or Aperture_mm column");
                                if ((sMode == 1 || sMode == 2) && lengthCol == -1) throw new Exception("Aperture file doesn't have Length column. (Only necessary when using center or endpoint S coordinates)");
                                columnsDefined = true;
                            }
                            else
                            {
                                if (line.Length == 0) continue; //skip empty lines
                                if (!columnsDefined) throw new Exception("Aperture file: Data row reached before column definition (header)");

                                string[] tokens = line.Split(new char[0], StringSplitOptions.RemoveEmptyEntries); //By whitespace
                                //Extract strings from quotation marks
                                for (int i = 0; i < tokens.Length; i++)
                                {
                                    var splitToken = tokens[i].Split('\"');
                                    if (splitToken.Length == 3) tokens[i] = splitToken[1];
                                }

                                double S_end = Convert.ToDouble(tokens[sCol]);
                                if (sMode == 0)
                                {
                                    double L = Convert.ToDouble(tokens[lengthCol]);
                                    S_end += L;
                                }
                                if (sMode == 1)
                                {
                                    double L = Convert.ToDouble(tokens[lengthCol]);
                                    S_end += .5 * L;
                                }



                                //Fill downwards
                                int foundRowId = -1;
                                for (int rowId = 0; foundRowId == -1 && rowId <= sequenceGrid.RowCount-2; rowId++)
                                {
                                    double rowTwissS = Convert.ToDouble(sequenceGrid.Rows[rowId].Cells["twissS"].Value);

                                    if (Math.Abs(rowTwissS - S_end) < 1E-4)
                                    {
                                        foundRowId = rowId;
                                        double aperture_m = Convert.ToDouble(tokens[apertureCol]);
                                        if (apertureMode == 1) aperture_m *= 0.001;
                                        sequenceGrid.Rows[rowId].Cells["AptType"].Value = "Circle";
                                        sequenceGrid.Rows[rowId].Cells["Aperture1"].Value = aperture_m.ToString("G10");
                                    }
                                }
                                if (foundRowId == -1)
                                {
                                    MessageBox.Show("Not found: \n" + line);
                                    return;
                                }
                            }
                        } //end of readline block
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error: " + ex.Message);
                }
            }
        }

        private void suffixCheckbox_CheckedChanged(object sender, EventArgs e)
        {
            FormatExampleFilename();
        }

        private void writeGeomButton_Click(object sender, EventArgs e)
        {
            //Form checks
            if (dirComboBox.Text == "")
            {
                System.Windows.Forms.MessageBox.Show("Directory not defined");
                return;
            }
            if (refRowTextBox.Text == "")
            {
                System.Windows.Forms.MessageBox.Show("Reference row not set yet");
                return;
            }
            if (!(
                CheckOutputDir() &&
                CheckTableFilled() &&
                CheckIfSCalculated() &&
                CheckStartStopValidity()
                    )) return;
            int curveSides; //For analytic apertures
            double curveStep; //Arc nb of steps
            double stickingFactor; //Default sticking factor for created facets
            int mode; //Taper or abrupt aperture change
            int facetMode; //0: Don't create facets, 1: end caps, 2: all facets
            try
            {
                curveSides = Convert.ToUInt16(circleSidesText.Text);
                curveStep = Convert.ToDouble(curveStepText.Text);
                stickingFactor = Convert.ToDouble(facetStickingTextbox.Text);
                mode = taperCombo.SelectedIndex;
                facetMode = apertureFacetsCombo.SelectedIndex;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Couldn't interpret circle sides or curve step or sticking factor\n" + ex.Message);
                return;
            }

            int startLine, endLine;
            if (startRowText.Text != "") startLine = Convert.ToInt16(startRowText.Text) - 1;
            else startLine = 0;
            if (endRowText.Text != "") endLine = Convert.ToInt16(endRowText.Text) - 1;
            else endLine = sequenceGrid.RowCount - 2;

            //Populate aperture list based on form. Aperture is 2d, x,z position and theta orientation
            List<Aperture> apertures = new List<Aperture>();

            for (int rowId = Math.Max(0, startLine); rowId <= endLine; rowId++)

            {
                try
                {
                    var cells = sequenceGrid.Rows[rowId].Cells;
                    double rowStartX = Convert.ToDouble(cells["startX"].Value);
                    double rowStartZ = Convert.ToDouble(cells["startZ"].Value);
                    double rowStartTheta = Convert.ToDouble(cells["thetaStart"].Value);
                    double rowLength = Convert.ToDouble(cells["length"].Value);
                    double rowAngle = Convert.ToDouble(cells["angle"].Value);

                    Aperture rowStartAperture = new Aperture();
                    rowStartAperture.x = rowStartX;
                    rowStartAperture.z = rowStartZ;
                    rowStartAperture.theta = rowStartTheta;

                    if (cells["AVName"].Value != null)
                    { //ignore sections without a name, typically last few lines
                        if (cells["AptType"].Value != null)
                        {
                            string aptType = cells["AptType"].Value.ToString();
                            switch (aptType.ToLower())
                            {
                                case "circle":
                                    rowStartAperture.type = ApertureType.Circle;
                                    rowStartAperture.param1 = Convert.ToDouble(cells["Aperture1"].Value); //radius
                                    if (cells["Aperture2"].Value == null)
                                    {
                                        rowStartAperture.param2 = rowStartAperture.param1; //circle
                                    }
                                    else
                                    {
                                        rowStartAperture.param2 = Convert.ToDouble(cells["Aperture2"].Value); //ellipse
                                    }
                                    break;
                                case "racetrack":
                                    rowStartAperture.type = ApertureType.Racetrack;
                                    rowStartAperture.param1 = Convert.ToDouble(cells["Aperture1"].Value);
                                    rowStartAperture.param2 = Convert.ToDouble(cells["Aperture2"].Value);
                                    break;
                                default: //look for custom aperture (imported)
                                    bool found = false;
                                    foreach (DataGridViewRow row in apertureGrid.Rows)
                                    {
                                        if (!found && row.Cells["apertureName"].Value != null && row.Cells["apertureName"].Value.ToString() == aptType)
                                        {
                                            found = true;
                                            rowStartAperture.type = ApertureType.User;
                                            rowStartAperture.vertexList = new List<Vertex2d>((List<Vertex2d>)row.Tag); //can be scaled/offset later in Calculate2dPoints
                                            if (cells["Aperture1"].Value == null) //width
                                            {
                                                rowStartAperture.param1 = 1.0; //scaleX
                                            }
                                            else
                                            {
                                                rowStartAperture.param1 = Convert.ToDouble(cells["Aperture1"].Value) / Convert.ToDouble(row.Cells["FileWidth"].Value);
                                            }
                                            if (cells["Aperture2"].Value == null) //height
                                            {
                                                rowStartAperture.param2 = 1.0; //scaleY
                                            }
                                            else
                                            {
                                                rowStartAperture.param2 = Convert.ToDouble(cells["Aperture2"].Value) / Convert.ToDouble(row.Cells["FileHeight"].Value);
                                            }
                                            if (cells["Aperture3"].Value == null) //x offset
                                            {
                                                rowStartAperture.param3 = 0.0;
                                            }
                                            else
                                            {
                                                rowStartAperture.param3 = Convert.ToDouble(cells["Aperture3"].Value);
                                            }
                                            if (cells["Aperture4"].Value == null) //y offset
                                            {
                                                rowStartAperture.param4 = 0.0;
                                            }
                                            else
                                            {
                                                rowStartAperture.param4 = Convert.ToDouble(cells["Aperture4"].Value);
                                            }
                                        }
                                    }
                                    //Not found
                                    if (!found) throw new Exception("Row " + (rowId + 1) + ": unknown aperture type " + cells["AptType"].Value);
                                    break;
                            }

                        }
                        else //No aperture info
                        {
                            if (apertures.Count > 0)
                            {
                                //Copy last aperture, but keep x,z,theta
                                rowStartAperture = new Aperture(apertures.Last());
                                rowStartAperture.x = rowStartX;
                                rowStartAperture.z = rowStartZ;
                                rowStartAperture.theta = rowStartTheta;
                            }
                        }


                        //Start aperture determined, let's generate section
                        if (rowStartAperture.type != ApertureType.Undefined) //Undefined until first row with aperture info
                        {
                            List<Aperture> sectionApertures = GenerateCurvedSectionApertures(rowStartAperture, rowLength, rowAngle, curveStep); //includes starting and ending apertures
                            AddSectionApertures(ref apertures, sectionApertures, mode); //Adds new apertures. If taper change, removes last, avoid duplicates
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Row " + (rowId + 1) + ": \n" + ex.Message);
                    return;
                }

            }
            //We've got a series of apertures, let's convert them to 3D points and connect them
            (List<Vertex3d> vertices, List<Facet> facets) = GenerateVerticesAndFacets(apertures, curveSides, facetMode);
            string path = dirComboBox.Text + "\\" + geomFileTextBox.Text;
            WriteTxtFile(path, vertices, facets);
            Log("Geometry saved to " + path);
        }

        private bool CheckIfSCalculated()
        {
            int actualRowId = 0; //Start from first line
            while (actualRowId <= sequenceGrid.RowCount-2)
            {
                if (sequenceGrid.Rows[actualRowId].Cells["s"].Value == null)
                {
                    MessageBox.Show("Line " + (actualRowId + 1) + " startS value is empty.\nTo calculate startS position for all rows, set reference row.");
                    return false;
                }
                    actualRowId++;
            }
            return true;
        }

        private (List<Vertex3d>, List<Facet>) GenerateVerticesAndFacets(List<Aperture> apertures, int curveSides, int facetMode)
        {

            List<Polygon2d> polygonList = Calculate2dApertures(apertures, curveSides); //Converts analytic aperture list to a list of Polgyon2d
            List<Polygon3d> aperturePoints3d = Convert2dAperturesTo3d(apertures, polygonList); //List of polygon2d to list of polygon3d

            List<Vertex3d> vertices = AggregateVertices(aperturePoints3d); //One aggregate list of vertices from list of 3d polygons
            List<Facet> facets = new List<Facet>();

            if (apertures.Count > 0 && (facetMode == 1 || facetMode == 2 || facetMode == 3))
            { //Write start facet in case of "end caps" or "all"
                var startCap = new Facet(Enumerable.Range(aperturePoints3d[0].firstVertexId, aperturePoints3d[0].vertices.Count).ToList());
                startCap.isTransparent = facetMode == 2 || facetMode == 3;
                facets.Add(startCap);
            }

            for (int i = 1; i < apertures.Count; i++)
            {
                //Preparing Molflow logic
                Facet Facet1 = new Facet(Enumerable.Range(aperturePoints3d[i - 1].firstVertexId, aperturePoints3d[i - 1].vertices.Count).ToList());
                Facet Facet2 = new Facet(Enumerable.Range(aperturePoints3d[i].firstVertexId, aperturePoints3d[i].vertices.Count).ToList());
                Polygon2d normalizedFacet1 = Polygon2d.NormalizeUV(polygonList[i - 1]);
                Polygon2d normalizedFacet2 = Polygon2d.NormalizeUV(polygonList[i]);
                facets.AddRange(CreateLoft(vertices, Facet1, Facet2, polygonList[i - 1], polygonList[i]));
                if (facetMode == 2 || facetMode == 3) //Create intermediate facets
                { //All aperture facets created
                    if (facetMode == 3 || apertures[i].isCurveStep == false)
                    {
                        Facet apertureFacet = new Facet(Enumerable.Range(aperturePoints3d[i].firstVertexId, aperturePoints3d[i].vertices.Count).ToList());
                        apertureFacet.isTransparent = true;
                        facets.Add(apertureFacet);
                    }
                }
                else if (facetMode == 1 && i == (apertures.Count - 1))
                { //End cap on last facet
                    var endCap = new Facet(Enumerable.Range(aperturePoints3d[i].firstVertexId, aperturePoints3d[i].vertices.Count).ToList());
                    endCap.isTransparent = facetMode == 2 || facetMode == 3;
                    endCap.vertexIds.Reverse(); //Swap normal
                    facets.Add(endCap);
                }
            }

            return (vertices, facets);
        }

        private List<Vertex3d> AggregateVertices(List<Polygon3d> aperturePoints3d)
        {
            List<Vertex3d> result = new List<Vertex3d>();
            foreach (Polygon3d p in aperturePoints3d)
            {
                foreach (Vertex3d v in p.vertices)
                {
                    result.Add(v);
                }
            }
            return result;
        }

        private List<Polygon3d> Convert2dAperturesTo3d(List<Aperture> apertures, List<Polygon2d> polygonList)
        {
            if (apertures.Count != polygonList.Count) throw new System.ArgumentException("aperture and polygonlist count must be equal");
            List<Polygon3d> result = new List<Polygon3d>();
            int vertexIndex = 0;
            for (int i = 0; i < apertures.Count; i++)
            {
                Polygon3d newPoly3d = new Polygon3d(apertures[i], polygonList[i]);
                newPoly3d.firstVertexId = vertexIndex;
                vertexIndex += newPoly3d.vertices.Count;
                result.Add(newPoly3d);
            }
            return result;
        }

        private static List<Polygon2d> Calculate2dApertures(List<Aperture> apertures, int curveSides)
        {
            List<Polygon2d> apt2dlist = new List<Polygon2d>();
            //Computes 2d points for each aperture
            foreach (var apt in apertures)
            {
                Polygon2d aperture2dPoints;
                if (apt.type == ApertureType.Circle)
                {
                    aperture2dPoints = Polygon2d.Ellipse(curveSides, apt.param1, apt.param2);
                }
                else if (apt.type == ApertureType.Racetrack)
                {
                    aperture2dPoints = Polygon2d.Racetrack(curveSides, apt.param1, apt.param2, false);
                }
                else if (apt.type == ApertureType.User)
                {
                    aperture2dPoints = Polygon2d.UserAperture(apt.vertexList, apt.param1, apt.param2, apt.param3, apt.param4);
                }
                else
                {
                    throw new Exception("Aperture type not implemented");
                }
                apt2dlist.Add(aperture2dPoints);
            }
            return apt2dlist;
        }

        private void AddSectionApertures(ref List<Aperture> apertures, List<Aperture> sectionApertures, int mode)
        {
            if (sectionApertures.Count == 0) return;
            if (apertures.Count == 0) apertures.AddRange(sectionApertures); //Add without any checks
            else
            {
                Aperture firstToAdd = sectionApertures.First();
                Aperture lastAdded = apertures.Last();
                if (mode == 0 /* taper transition */ || ApertureEquals(firstToAdd, lastAdded))
                {
                    //Overwrite last aperture
                    apertures.RemoveAt(apertures.Count - 1);
                }
                apertures.AddRange(sectionApertures);

            }
            return;
        }

        private static void AddToGeometry(List<Vertex3d> vertices, List<Facet> facets, List<Vertex3d> newVertices, List<Facet> newFacets)
        {
            int nbVertex_old = vertices.Count;
            vertices.AddRange(newVertices);
            foreach (Facet f in newFacets) //Renumber vertices
            {
                for (int i = 0; i < f.vertexIds.Count; i++)
                {
                    f.vertexIds[i] += nbVertex_old;
                }
            }
            facets.AddRange(newFacets);
        }

        private void WriteTxtFile(string path, List<Vertex3d> vertices, List<Facet> facets)
        {
            using (StreamWriter fw = new StreamWriter(path))
            {
                fw.WriteLine("0");//Unused
                fw.WriteLine("0");//Hits
                fw.WriteLine("0"); //Leaks
                fw.WriteLine("0"); //Des
                fw.WriteLine("0"); //Des_limit
                fw.WriteLine(vertices.Count);
                fw.WriteLine(facets.Count);
                foreach (var vertex in vertices)
                {
                    fw.WriteLine(100.0 * vertex.x + " " + 100.0 * vertex.y + " " + 100.0 * vertex.z); //m->cm
                }
                foreach (var facet in facets)
                {
                    string line = facet.vertexIds.Count.ToString();
                    foreach (var vertexId in facet.vertexIds)
                    {
                        line += " " + (vertexId + 1);
                    }
                    fw.WriteLine(line);
                }
                foreach (var facet in facets)
                {
                    fw.WriteLine(facetStickingTextbox.Text); //sticking (already checked if convertible to double)
                    fw.WriteLine(facet.isTransparent ? "0.0" : "1.0"); //opacity
                    fw.WriteLine("0.0"); //area, unused
                    fw.WriteLine("0.0"); //nbDes, will override desType
                    fw.WriteLine("0.0"); //nbHit
                    fw.WriteLine("0.0");  //nbAbs
                    fw.WriteLine("0.0"); //desType, overridden
                    fw.WriteLine("1.0"); //reflection type, 1=specular
                    fw.WriteLine("0.0"); //unused
                }
                for (int i = 0; i < 100; i++) //Profiles
                    fw.WriteLine();
            }
        }

        private List<Aperture> GenerateCurvedSectionApertures(Aperture startAperture, double length, double angle, double curveStep)
        {
            List<Aperture> result = new List<Aperture>();

            int nbSteps = (int)(Math.Abs(angle) / curveStep) + 1;
            double stepAngle = angle / (double)nbSteps;
            double stepLength = length / (double)nbSteps;

            double actualX = startAperture.x;
            double actualZ = startAperture.z;
            double actualTheta = startAperture.theta;

            for (int i = 0; i < nbSteps; i++)
            {
                Aperture newAperture = new Aperture(startAperture);

                newAperture.x = actualX;
                newAperture.z = actualZ;
                newAperture.theta = actualTheta;
                newAperture.isCurveStep = (i > 0 && i < (nbSteps - 1)); //Not first and not last
                result.Add(newAperture);

                double dXP, dZP; //delta X,Z in coord system tangent to beam (as if theta was 0)
                if (angle == 0.0)
                {
                    dZP = stepLength;
                    dXP = 0;

                }
                else
                {
                    dZP = stepLength / stepAngle * Math.Sin(stepAngle);
                    dXP = stepLength / stepAngle * (1.0 - Math.Cos(stepAngle));
                }


                double dX = dZP * Math.Sin(actualTheta) + dXP * Math.Cos(actualTheta);
                double dZ = dZP * Math.Cos(actualTheta) - dXP * Math.Sin(actualTheta);

                actualZ += dZ;
                actualX -= dX;
                actualTheta += stepAngle;


            }
            if (length > 0.0)
            {
                //Last, closing step
                Aperture newAperture2 = new Aperture(startAperture);

                newAperture2.x = actualX;
                newAperture2.z = actualZ;
                newAperture2.theta = actualTheta;
                result.Add(newAperture2);
            }

            return result;
        }

        /*
        private (List<Vertex>, List<Facet>) generateSection(double xStart, double zStart, double thetaStart, double length, double angle, double curveStep, int circleSides, double aperture_m1, double aperture_m2, int mode)
        {
            var newVertices = new List<Vertex>();
            var newFacets = new List<Facet>();

            Polygon2d apertureCircle = new Polygon2d(circleSides, aperture_m1,aperture_m2);

            int nbSteps = (int)(Math.Abs(angle) / curveStep) + 1;
            double stepAngle = angle / (double)nbSteps;
            double stepLength = length / (double)nbSteps;

            double actualTheta = thetaStart;
            double actualX = xStart;
            double actualZ = zStart;

            for (int i = 0; i < nbSteps; i++)
            {
                CreateStep(newVertices, newFacets, apertureCircle, actualTheta, actualX, actualZ, i);

                double dXP, dZP; //delta X,Z in coord system tangent to beam (as if theta was 0)
                if (angle == 0.0)
                {
                    dZP = stepLength;
                    dXP = 0;

                }
                else
                {
                    dZP = stepLength / stepAngle * Math.Sin(stepAngle);
                    dXP = stepLength / stepAngle * (1.0 - Math.Cos(stepAngle));
                }


                double dX = dZP * Math.Sin(actualTheta) + dXP * Math.Cos(actualTheta);
                double dZ = dZP * Math.Cos(actualTheta) - dXP * Math.Sin(actualTheta);

                actualZ += dZ;
                actualX -= dX;
                actualTheta += stepAngle;


            }
            //Last, closing step
            CreateStep(newVertices, newFacets, apertureCircle, actualTheta, actualX, actualZ, nbSteps);
            return (newVertices, newFacets);
        }
        */

        private static List<Facet> CreateLoft(List<Vertex3d> vertices, Facet Facet1, Facet Facet2, Polygon2d facet2d_1, Polygon2d facet2d_2)
        {
            //Creates transition (walls) between two aperture facets
            //Ported from C++ Molflow/Synrad Geometry::CreateLoft()

            /* //Commented out: we already know which two aperture to transition (Facet1 and Facet  2 passed as arguments)
            //Find first two selected facets
            int nbFound = 0;
            Facet* f1, *f2;
        for (size_t i = 0; nbFound< 2 && i<sh.nbFacet; i++) {
            if (facets[i]->selected) {
                if (nbFound == 0) f1 = facets[i];
                else f2 = facets[i];
                facets[i]->selected = false;
                nbFound++;
            }
    }
    if (nbFound != 2) return;
            */

            List<loftIndex> closestIndices1 = new List<loftIndex>(); //links starting from the indices of facet 1
            for (int i = 0; i < Facet1.vertexIds.Count; i++)
            {
                closestIndices1.Add(new loftIndex());
            }
            List<loftIndex> closestIndices2 = new List<loftIndex>(); //links starting from the indices of facet 2
            for (int i = 0; i < Facet2.vertexIds.Count; i++)
            {
                closestIndices2.Add(new loftIndex());
            }
            //std::vector<loftIndex> closestIndices2(FacetIds2.vertexIds.Count); //links starting from the indices of facet 2
            /*
            for (auto & closest : closestIndices1)
                closest.visited = false; //I don't trust C++ default values
            for (auto & closest : closestIndices2)
                closest.visited = false; //I don't trust C++ default values
            */

            //double u1Length = f1->sh.U.Norme();
            //double v1Length = f1->sh.V.Norme();
            //double u2Length = f2->sh.U.Norme();
            //double v2Length = f2->sh.V.Norme();

            //Vector2d center2Pos = ProjectVertex(f2->sh.center, f1->sh.U, f1->sh.V, f1->sh.O); //Project 2nd's center on 1st
            //Vector2d center1Pos = ProjectVertex(f1->sh.center, f1->sh.U, f1->sh.V, f1->sh.O); //Project 1st's center on 2nd
            //Vector2d centerOffset = center1Pos - center2Pos; //the centers will be aligned when looking for closest indices

            //We will loop through the indices on facet 1, and for each, we'll find the closest index on facet 2
            //We have to avoid cross-linking: whenever a link is established to facet 2, we have to reduce the search area on facet 2 to between the last established link and the first established link
            //bool normalsAligned = Dot(f1->sh.N, f2->sh.N) > 0.0; //The loop direction on facets 1 and 2 must be the same
            bool normalsAligned = true;
            int searchBeginIndex = normalsAligned ? 1 : Facet2.vertexIds.Count - 1; //Lower boundary of available region on facet 2 (used to avoid crossing)
            int searchEndIndex = 0; //Higher boundary of available region on facet 2
                                    //These two limits cover the whole facet2 in the beginning

            bool needInit = true; //Whole facet2 is available until the first connection is made
            bool leftFirst = false; //Allow to search on full facet2 until only one index is linked on facet 2

            for (int i1 = 0; i1 < Facet1.vertexIds.Count; i1++)
            { //Go through indices on facet 1
              //Find closest point on other facet
                double min = 9E99;
                int minPos = 0; //will be overwritten
                for (int i2 = searchBeginIndex; min > 8.9E99 || i2 != Next(searchEndIndex, Facet2.vertexIds.Count, !normalsAligned); i2 = Next(i2, Facet2.vertexIds.Count, !normalsAligned))
                {
                    //Loop through the available search area on facet2
                    //The loop direction depends on if the normals are pointing the same or the opposite direction
                    //Stop when the currently scanned index on facet2 would be the out of the search area (next or previous of searchEndIndex, depending on the loop direction)
                    //Vector2d projection = ProjectVertex(vertices3[Facet2.vertexIds[i2]], f1->sh.U, f1->sh.V, f1->sh.O);
                    //projection = projection + centerOffset;
                    Vertex2d projection = facet2d_2.vertices[i2]; //No need to project, we already have it on plane
                    double dist = Math.Pow(facet2d_2.vertices[i2].u - facet2d_1.vertices[i1].u, 2) + Math.Pow(facet2d_2.vertices[i2].v - facet2d_1.vertices[i1].v, 2); //We need the absolute distance
                    if (dist < min)
                    {
                        min = dist;
                        minPos = i2;
                    }
                }
                //Make pair with the closest index on facet 2
                closestIndices1[i1].index = minPos; //Link from facet1 to facet2
                closestIndices2[minPos].index = i1; //Link from facet2 to facet1
                closestIndices1[i1].visited = closestIndices2[minPos].visited = true; //Pair is complete

                if (needInit)
                { //If this is the first link, set search area from next index on facet 2 to this index on facet 2 (so the whole facet 2 is still searched)
                    searchBeginIndex = Next(minPos, Facet2.vertexIds.Count, !normalsAligned); //Next, depending on loop direction
                    searchEndIndex = minPos;
                    needInit = false;
                }
                else
                {
                    if (leftFirst || minPos != searchEndIndex)
                    {
                        //If it's not just a new link from facet1 to the first link destination on facet2
                        searchBeginIndex = minPos; //Reduce search region on facet2 to begin with the last established link
                        leftFirst = true;
                    }
                }
            }

            //Find boundaries of regions on the first facet that are the closest to the same point on facet 2
            for (int i = 0; i < closestIndices1.Count; i++)
            {
                int previousId = Previous(i, closestIndices1.Count);
                int nextId = Next(i, closestIndices1.Count);
                closestIndices1[i].boundary = (closestIndices1[i].index != closestIndices1[nextId].index) || (closestIndices1[i].index != closestIndices1[previousId].index);
            }

            //center2Pos = ProjectVertex(f2->sh.center, f2->sh.U, f2->sh.V, f2->sh.O);
            //center1Pos = ProjectVertex(f1->sh.center, f2->sh.U, f2->sh.V, f2->sh.O);
            //centerOffset = center2Pos - center1Pos;

            //Revisit those on f2 which aren't linked yet
            for (int i2 = 0; i2 < Facet2.vertexIds.Count; i2++)
            {
                //Find closest point on other facet
                if (!closestIndices2[i2].visited)
                {
                    double min = 9E99;
                    int minPos = 0; //Will be overwritten

                    //To avoid crossing, narrow search area on facet2 ranging...
                    // ...from the previous facet2 index that is already linked
                    // ...to the next facet2 index that is already linked

                    //Searching for the previous facet2 index that is already linked
                    int i2_lookup = i2;
                    do
                    {
                        i2_lookup = Previous(i2_lookup, Facet2.vertexIds.Count);
                    } while (!closestIndices2[i2_lookup].visited /*&& i2_lookup != i2*/); //Commented out, I don't see how i2_lookup could be the same as i2

                    //At this point, we know that closestIndices2[i2_lookup].index is connected to i2_lookup, however we need to find the last facet1 index (in the loop direction) that is connected to the same i2_lookup index (otherwise we would still create a crossing)
                    int i1index_1 = closestIndices2[i2_lookup].index;
                    do
                    {
                        i1index_1 = Next(i1index_1, Facet1.vertexIds.Count, !normalsAligned);
                    } while (closestIndices1[i1index_1].index == i2_lookup);

                    i1index_1 = Previous(i1index_1, Facet1.vertexIds.Count, !normalsAligned); //step back one after loop exited

                    //Now searching for the next facet2 index that is already linked
                    i2_lookup = i2;
                    do
                    {
                        i2_lookup = Next(i2_lookup, Facet2.vertexIds.Count); //next index
                    } while (!closestIndices2[i2_lookup].visited /*&& i2_lookup != i2*/); //Commented out

                    //At this point, we know that closestIndices2[i2_lookup].index is connected to i2_lookup, however we need to find the last facet1 index that is connected to the same i2_lookup index
                    int i1index_2 = closestIndices2[i2_lookup].index;
                    do
                    {
                        i1index_2 = Previous(i1index_2, Facet1.vertexIds.Count, !normalsAligned);
                    } while (closestIndices1[i1index_2].index == i2_lookup);

                    i1index_2 = Next(i1index_2, Facet1.vertexIds.Count, !normalsAligned); //step back one after loop exited

                    int i1index_lower = Math.Min(i1index_1, i1index_2);
                    int i1index_higher = Math.Max(i1index_1, i1index_2);

                    //Look for the closest among [i1index_lower .. i1index_higher]. These can be safely connected to, without creating a crossing

                    for (int i1 = i1index_lower; i1 <= i1index_higher; i1++)
                    {
                        //Vector2d projection = ProjectVertex(vertices3[Facet1.vertexIds[i1]], f2->sh.U, f2->sh.V, f2->sh.O);
                        //projection = projection + centerOffset;
                        //double dist = Sqr(u2Length * (projection.u - f2->vertices2[i2].u)) + Sqr(v2Length * (projection.v - f2->vertices2[i2].v)); //We need the absolute distance
                        double dist = Math.Pow(facet2d_2.vertices[i2].u - facet2d_1.vertices[i1].u, 2) + Math.Pow(facet2d_2.vertices[i2].v - facet2d_1.vertices[i1].v, 2); //We need the absolute distance
                        if (!closestIndices1[i1].boundary) dist += 1E6; //penalty -> try to connect with boundaries

                        if (dist < min)
                        {
                            min = dist;
                            minPos = i1;
                        }
                    }
                    //Make pair
                    closestIndices2[i2].index = minPos;
                    //closestIndices1[minPos].index = i2; //Commented out as all indices on facet1 already have links
                    closestIndices2[i2].visited = true;
                }
            }

            //Detect boundaries on facet 2
            for (int i = 0; i < closestIndices2.Count; i++)
            {
                int previousId = Previous(i, closestIndices2.Count);
                int nextId = Next(i, closestIndices2.Count);
                closestIndices2[i].boundary = (closestIndices2[i].index != closestIndices2[nextId].index) || (closestIndices2[i].index != closestIndices2[previousId].index);
                closestIndices2[i].visited = false; //Reset this flag, will use to make sure we don't miss anything on facet2
            }

            //Links created, now we have to build the facets
            List<Facet> newFacets = new List<Facet>();
            for (int i1 = 0; i1 < Facet1.vertexIds.Count; i1++)
            { //Cycle through all facet1 indices to create triangles and rectangles

                for (int i2 = Next(closestIndices1[i1].index, Facet2.vertexIds.Count); closestIndices2[i2].index == i1; i2 = Next(i2, Facet2.vertexIds.Count))
                {
                    //In increasing direction, cycle through those indices on facet2 that are also linked with the actual facet1 index.
                    //When two consecutive indices on facet 2 are connected with the same facet1 index, create the following triangle:

                    Facet newFacet2 = new Facet();
                    newFacet2.vertexIds.Add(Facet1.vertexIds[i1]); //actual facet1 index 
                    newFacet2.vertexIds.Add(Facet2.vertexIds[i2]); closestIndices2[i2].visited = true; //next facet2 index (i2 already incremented in for cycle head)
                    newFacet2.vertexIds.Add(Facet2.vertexIds[Previous(i2, Facet2.vertexIds.Count)]); closestIndices2[Previous(i2, Facet2.vertexIds.Count)].visited = true; //actual facet2 index
                    //newFacet->selected = true;
                    //if (viewStruct != -1) newFacet->sh.superIdx = viewStruct;
                    newFacet2.vertexIds.Reverse();
                    newFacets.Add(newFacet2);
                }


                for (int i2 = Previous(closestIndices1[i1].index, Facet2.vertexIds.Count); closestIndices2[i2].index == i1; i2 = Previous(i2, Facet2.vertexIds.Count))
                {
                    //In decreasing direction, cycle through those indices on facet2 that are also linked with the actual facet1 index.
                    //When two consecutive indices on facet 2 are connected with the same facet1 index, create the following triangle:

                    Facet newFacet2 = new Facet();
                    newFacet2.vertexIds.Add(Facet1.vertexIds[i1]); //actual facet1 index
                    newFacet2.vertexIds.Add(Facet2.vertexIds[Next(i2, Facet2.vertexIds.Count)]); closestIndices2[Next(i2, Facet2.vertexIds.Count)].visited = true; //actual facet2 index
                    newFacet2.vertexIds.Add(Facet2.vertexIds[i2]); closestIndices2[i2].visited = true; //previous facet2 index (i2 already decremented in for cycle head)

                    //newFacet->selected = true;
                    //if (viewStruct != -1) newFacet->sh.superIdx = viewStruct;
                    newFacet2.vertexIds.Reverse();
                    newFacets.Add(newFacet2);
                }

                //Trivial triangle connections created. Now we're up to create rectangles. Later, if those rectangles wouldn't be planar, we'll split them to two triangles
                bool triangle = closestIndices1[Next(i1, Facet1.vertexIds.Count)].index == closestIndices1[i1].index; //If the current (i1) and also the next index on facet1 are connected with the same index on facet2, then create a triangle. Else a rectangle.
                Facet newFacet = new Facet();
                newFacet.vertexIds.Add(Facet1.vertexIds[i1]); //Actual facet1 index
                newFacet.vertexIds.Add(Facet1.vertexIds[Next(i1, Facet1.vertexIds.Count)]); //Next facet1 index

                //Find last vertex on other facet that's linked to us
                //int incrementDir = (Dot(f1->sh.N, f2->sh.N) > 0) ? -1 : 1; //In some procedures we have to go in the same direction on both facets
                int incrementDir = -1; //normals always aligned
                int increment; //not size_t, allow negative values
                for (increment = 0;
                    closestIndices2[
                        IDX((int)closestIndices1[Next(i1, Facet1.vertexIds.Count)].index + increment + incrementDir, Facet2.vertexIds.Count) //Cycles through the indices on facet2
                    ].index ==
                    (Next(i1, Facet1.vertexIds.Count)); // is connected to the NEXT index on facet1
                    increment += incrementDir) ;
                //Basically, look at the next connection. Fix the connection node as the next index on facet1, then step through all the indices on facet2 that are still connected to the same (the next) index on facet1

                int lastIndexOnFacet2ThatConnectsToTheNextOnFacet1 = IDX((int)closestIndices1[Next(i1, Facet1.vertexIds.Count)].index + increment, Facet2.vertexIds.Count);
                newFacet.vertexIds.Add(Facet2.vertexIds[lastIndexOnFacet2ThatConnectsToTheNextOnFacet1]);
                closestIndices2[lastIndexOnFacet2ThatConnectsToTheNextOnFacet1].visited = true;

                if (!triangle)
                {
                    int nextAfterLastConnected = IDX((int)lastIndexOnFacet2ThatConnectsToTheNextOnFacet1 + incrementDir, Facet2.vertexIds.Count);
                    newFacet.vertexIds.Add(Facet2.vertexIds[nextAfterLastConnected]);
                    closestIndices2[nextAfterLastConnected].visited = true;
                }
                //if (incrementDir == -1) newFacet->SwapNormal();
                if (incrementDir == -1) newFacet.vertexIds.Reverse(); //always -1, since they are aligned

                //Check whether simple connection is coplanar
                if (!triangle)
                {
                    Vertex3d v0 = vertices[newFacet.vertexIds[0]];
                    Vertex3d v1 = vertices[newFacet.vertexIds[1]];
                    Vertex3d v2 = vertices[newFacet.vertexIds[2]];
                    Vertex3d v3 = vertices[newFacet.vertexIds[3]];

                    Vertex3d plane1normal = (v1 - v0).CrossProductWith(v2 - v0).Normalized();
                    Vertex3d plane2normal = (v2 - v0).CrossProductWith(v3 - v0).Normalized();
                    bool triangulate = plane1normal.CrossProductWith(plane2normal).Norme() > 1E-5;

                    //CalculateFacetParams(newFacet);
                    if (triangulate)
                    {
                        //Split to two triangles
                        //int ind4[] = { newFacet->indices[0], newFacet->indices[1], newFacet->indices[2], newFacet->indices[3] };
                        List<int> oriIndices = newFacet.vertexIds.GetRange(0, 4);
                        //delete newFacet;
                        newFacet = new Facet();
                        Vertex3d diff_0_2 = vertices[oriIndices[0]] - vertices[oriIndices[2]];
                        Vertex3d diff_1_3 = vertices[oriIndices[1]] - vertices[oriIndices[3]];
                        bool connect_0_2 = diff_0_2.Norme() < diff_1_3.Norme(); //Split rectangle to two triangles along shorter side. To do: detect which split would create larger total surface and use that
                        newFacet.vertexIds.Add(oriIndices[0]);
                        newFacet.vertexIds.Add(oriIndices[1]);
                        newFacet.vertexIds.Add(oriIndices[connect_0_2 ? 2 : 3]);
                        //newFacet->selected = true;
                        //newFacet.vertexIds.Reverse();
                        newFacets.Add(newFacet);

                        newFacet = new Facet();
                        newFacet.vertexIds.Add(oriIndices[connect_0_2 ? 0 : 1]);
                        newFacet.vertexIds.Add(oriIndices[2]);
                        newFacet.vertexIds.Add(oriIndices[3]);
                    }
                }
                //newFacet.vertexIds.Reverse();
                //newFacet->selected = true;
                //if (viewStruct != -1) newFacet->sh.superIdx = viewStruct;
                newFacets.Add(newFacet);
            }
            //Go through leftover vertices on facet 2
            for (int i2 = 0; i2 < Facet2.vertexIds.Count; i2++)
            {
                if (closestIndices2[i2].visited == false)
                {
                    int targetIndex = closestIndices2[Previous(i2, Facet2.vertexIds.Count)].index; //Previous node

                    do
                    {
                        //Connect with previous
                        Facet newFacet2 = new Facet();
                        newFacet2.vertexIds.Add(Facet1.vertexIds[targetIndex]);
                        newFacet2.vertexIds.Add(Facet2.vertexIds[i2]); closestIndices2[i2].visited = true;
                        newFacet2.vertexIds.Add(Facet2.vertexIds[Previous(i2, Facet2.vertexIds.Count)]); closestIndices2[Previous(i2, Facet2.vertexIds.Count)].visited = true;
                        //newFacet->selected = true;
                        //if (viewStruct != -1) newFacet->sh.superIdx = viewStruct;
                        newFacet2.vertexIds.Reverse();
                        newFacets.Add(newFacet2);
                        i2 = Next(i2, Facet2.vertexIds.Count);
                    } while (closestIndices2[i2].visited == false);
                    //last
                    //Connect with next for the last unvisited
                    Facet newFacet = new Facet();
                    newFacet.vertexIds.Add(Facet1.vertexIds[targetIndex]);
                    newFacet.vertexIds.Add(Facet2.vertexIds[i2]); closestIndices2[i2].visited = true;
                    newFacet.vertexIds.Add(Facet2.vertexIds[Previous(i2, Facet2.vertexIds.Count)]); closestIndices2[Previous(i2, Facet2.vertexIds.Count)].visited = true;
                    //newFacet->selected = true;
                    //if (viewStruct != -1) newFacet->sh.superIdx = viewStruct;
                    newFacet.vertexIds.Reverse();
                    newFacets.Add(newFacet);
                }
            }

            /*
            //Register new facets
            if (newFacets.Count > 0) facets = (Facet**)realloc(facets, sizeof(Facet*) * (sh.nbFacet + newFacets.Count));
            for (int i = 0; i < newFacets.Count; i++)
                facets[sh.nbFacet + i] = newFacets[i];
            sh.nbFacet += newFacets.Count;
            */
            return newFacets;

        }

        private bool ApertureEquals(Aperture lhs, Aperture rhs)
        {
            bool equal = lhs.type == rhs.type &&
                ((lhs.param1 == 0.0 && rhs.param1 == 0.0) || (Math.Abs((lhs.param1 - rhs.param1) / lhs.param1) < 1E-5)) &&
                ((lhs.param2 == 0.0 && rhs.param2 == 0.0) || (Math.Abs((lhs.param2 - rhs.param2) / lhs.param2) < 1E-5)) &&
                ((lhs.param3 == 0.0 && rhs.param3 == 0.0) || (Math.Abs((lhs.param3 - rhs.param3) / lhs.param3) < 1E-5)) &&
                ((lhs.param4 == 0.0 && rhs.param4 == 0.0) || (Math.Abs((lhs.param4 - rhs.param4) / lhs.param4) < 1E-5));
            if (lhs.type == ApertureType.User)
            {
                equal = equal && (lhs.vertexList.Count == rhs.vertexList.Count);
                for (int i = 0; equal && (i < lhs.vertexList.Count); i++)
                {
                    equal = equal && ((lhs.vertexList[i].u == 0.0 && rhs.vertexList[i].u == 0.0) || (Math.Abs((lhs.vertexList[i].u - rhs.vertexList[i].u) / lhs.vertexList[i].u) < 1E-5));
                    equal = equal && ((lhs.vertexList[i].v == 0.0 && rhs.vertexList[i].v == 0.0) || (Math.Abs((lhs.vertexList[i].v - rhs.vertexList[i].v) / lhs.vertexList[i].v) < 1E-5));
                }
            }
            return equal;
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 3)
            { //Browse button
                int rowIndex = e.RowIndex;
                OpenFileDialog openFileDialog1 = new OpenFileDialog();
                var fileType = apertureGrid.Rows[rowIndex].Cells["FileType"].Value;
                string filter;
                if (fileType != null)
                {
                    string fileTypeString = fileType.ToString();
                    if (fileTypeString == "Molflow txt export" || fileTypeString == "TXT 2d coord list") filter = "TXT files (*.txt)|*.txt|All files (*.*)|*.*";
                    else if (fileTypeString == "CSV 2d coord list") filter = "CSV files (*.csv)|*.csv|All files (*.*)|*.*";
                    else filter = "All files(*.*) | *.*";
                }
                else
                {
                    filter = "All files(*.*) | *.*";
                }

                openFileDialog1.Filter = filter;
                openFileDialog1.FilterIndex = 0;
                openFileDialog1.RestoreDirectory = true;
                if (apertureGrid.Rows[rowIndex].Cells["FileType"].Value == null)
                {
                    MessageBox.Show("Select file type first");
                    return;
                }

                if (openFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    var fileName = openFileDialog1.FileName;
                    apertureGrid.Rows[rowIndex].Cells["FileLocation"].Value = fileName;
                    apertureGrid.Rows[rowIndex].Cells["FileLocation"].ToolTipText = fileName; //For long paths

                    try
                    {
                        apertureGrid.Rows[rowIndex].Tag = LoadFacetFile(fileName, apertureGrid.Rows[rowIndex].Cells["FileType"].Value.ToString());
                    }
                    catch (Exception err)
                    {
                        MessageBox.Show("Error loading facet file:\n" + err.Message);
                        return;
                    }

                    PostProcessAperture(rowIndex);
                }
            }
            else if (e.ColumnIndex == 7) //Plot
            {
                if (apertureGrid.Rows[e.RowIndex].Tag != null)
                {
                    var plotForm = new AperturePlot();
                    var name = apertureGrid.Rows[e.RowIndex].Cells["apertureName"].Value;
                    if (name != null) plotForm.Text = name.ToString();
                    plotForm.Show();
                    plotForm.DrawCoords((List<Vertex2d>)apertureGrid.Rows[e.RowIndex].Tag);
                }
            }
            else if (e.ColumnIndex == 8) //Copy to local folder
            {
                if (apertureGrid.Rows[e.RowIndex].Tag == null)
                {
                    MessageBox.Show("First load a file to parse it");
                    return;
                }
                if (apertureGrid.Rows[e.RowIndex].Cells["apertureName"].Value == null)
                {
                    MessageBox.Show("Give the aperture a name first (it will determine file name.)");
                    return;
                }
                string apertureName = apertureGrid.Rows[e.RowIndex].Cells["apertureName"].Value.ToString();
                string targetName = "apertures\\" + apertureName + ".txt";
                if (File.Exists(targetName))
                {
                    var answer = MessageBox.Show("Target file\n" + targetName + "\nalready exists. Overwrite it?", "Copy to local library", MessageBoxButtons.OKCancel);
                    if (answer != DialogResult.OK) return;
                }
                if (!Directory.Exists("apertures")) Directory.CreateDirectory("apertures");
                using (var writer = new StreamWriter(targetName))
                {
                    foreach (var v in (List<Vertex2d>)apertureGrid.Rows[e.RowIndex].Tag)
                    {
                        writer.WriteLine(v.u.ToString() + " " + v.v.ToString());
                    }
                }
                apertureGrid.Rows[e.RowIndex].Cells["FileLocation"].Value = targetName;
            }
        }

        private void PostProcessAperture(int rowIndex)
        {
            var vertices = (List<Vertex2d>)apertureGrid.Rows[rowIndex].Tag;
            apertureGrid.Rows[rowIndex].Cells["FileStatus"].Value = "Loaded " + vertices.Count + " vertices";
            var bounds = GetBounds(vertices);

            apertureGrid.Rows[rowIndex].Cells["FileWidth"].Value = (bounds.uMax - bounds.uMin).ToString();
            apertureGrid.Rows[rowIndex].Cells["FileHeight"].Value = (bounds.vMax - bounds.vMin).ToString();

            apertureGrid.Rows[rowIndex].Cells["FilePlot"].Value = "Plot";
        }

        private (double uMin, double uMax, double vMin, double vMax) GetBounds(List<Vertex2d> vertices)
        {
            double uMin = 0.0;
            double uMax = 0.0;
            double vMin = 0.0;
            double vMax = 0.0; //Center is always part of BB
            foreach (var vertex in vertices)
            {
                uMin = Math.Min(uMin, vertex.u);
                uMax = Math.Max(uMax, vertex.u);
                vMin = Math.Min(vMin, vertex.v);
                vMax = Math.Max(vMax, vertex.v);
            }
            return (uMin, uMax, vMin, vMax);
        }

        /// <summary>
        /// Loads a file and returns a list of u,v coordinates read from it. Can throw an error.
        /// </summary>
        /// <param name="fileName">The file to open</param>
        /// <param name="fileType">File type as string. "Molflow txt" or "Txt coord list" or "Csv coord list"</param>
        /// <returns>Retruns a List<Vertex2d> object of the read coordinates.</Vertex2d></returns>
        private List<Vertex2d> LoadFacetFile(string fileName, string fileType)
        {
            List<Vertex2d> vertexList = new List<Vertex2d>();

            using (var reader = new System.IO.StreamReader(fileName))
            {
                if (fileType == "Txt coord list" || fileType == "Csv coord list")
                {
                    int lineNum = 0;
                    while (!reader.EndOfStream)
                    {
                        lineNum++;
                        double u, v;
                        string line = reader.ReadLine();
                        string[] tokens = line.Split(((fileType == "Txt coord list") ? null : new char[] { ',' }), StringSplitOptions.RemoveEmptyEntries); //By whitespace
                        if (tokens.Length != 2) throw new Exception("Line " + lineNum + ": 2 numbers expected, " + tokens.Length + " found");
                        try
                        {
                            u = Convert.ToDouble(tokens[0]);
                        }
                        catch (Exception e)
                        {
                            throw new Exception("Line " + lineNum + " first token \"" + tokens[0] + "\": can't convert to a number:\n" + e.Message);
                        }
                        try
                        {
                            v = Convert.ToDouble(tokens[1]);
                        }
                        catch (Exception e)
                        {
                            throw new Exception("Line " + lineNum + " second token \"" + tokens[1] + "\": can't convert to a number:\n" + e.Message);
                        }
                        vertexList.Add(new Vertex2d(u, v));
                    }
                }
                else if (fileType == "Molflow txt")
                {
                    reader.ReadLine(); //Unused
                    reader.ReadLine(); //nbHit
                    reader.ReadLine(); //nbLeak
                    reader.ReadLine(); //nbDes
                    reader.ReadLine(); //maxDes
                    string line = "";
                    int nbVertex, nbFacet;
                    try
                    {
                        line = reader.ReadLine();
                        nbVertex = Convert.ToInt32(line.Split(new char[0], StringSplitOptions.RemoveEmptyEntries)[0]); //nbVertex
                    }
                    catch (Exception err)
                    {
                        throw new Exception("Can't parse number of vertices (line 6) \n\"" + line + "\":\n" + err.Message);
                    }
                    try
                    {
                        line = reader.ReadLine();
                        nbFacet = Convert.ToInt32(line.Split(new char[0], StringSplitOptions.RemoveEmptyEntries)[0]); //nbFacet
                    }
                    catch (Exception err)
                    {
                        throw new Exception("Can't parse number of facets (line 7) \n\"" + line + "\":\n" + err.Message);
                    }
                    if (nbFacet != 1)
                    {
                        string errMsg = "The file should contain exactly 1 facet\n";
                        errMsg += "(It contains " + nbFacet.ToString() + ")\n";
                        errMsg += "In Molflow/Synrad, use File->Export selected facet(s) with a single facet in the XY plane selected.";
                        throw new Exception(errMsg);
                    }

                    //Read vertices
                    int lineNum = 7;
                    var molflowVertexList = new List<Vertex3d>(nbVertex);
                    for (int i = 0; i < nbVertex; i++)
                    {
                        try
                        {
                            lineNum++;
                            line = reader.ReadLine();
                            var vertexCoords = line.Split(new char[0], StringSplitOptions.RemoveEmptyEntries);
                            double X = Convert.ToDouble(vertexCoords[0]) * 0.01; //cm to m
                            double Y = Convert.ToDouble(vertexCoords[1]) * 0.01; //cm to m
                            double Z = Convert.ToDouble(vertexCoords[2]) * 0.01; //cm to m
                            var vertex = new Vertex3d(X, Y, Z);
                            molflowVertexList.Add(vertex);
                        }
                        catch (Exception err)
                        {
                            throw new Exception("Can't parse vertex " + (i + 1).ToString() + " coordinates (line " + lineNum + ") \n\"" + line + "\":\n" + err.Message);
                        }
                    }

                    int nbVertexFacet;
                    try
                    {
                        lineNum++;
                        line = reader.ReadLine();
                        var tokens = line.Split(new char[0], StringSplitOptions.RemoveEmptyEntries);
                        nbVertexFacet = Convert.ToInt32(tokens[0]);

                        for (int i = 0; i < nbVertexFacet; i++)
                        {
                            int ind = Convert.ToInt32(tokens[i + 1]) - 1; //indices are numbered 1 to N
                            Vertex2d vert2 = new Vertex2d(molflowVertexList[ind].x, molflowVertexList[ind].y); //Ignore Z coord
                            vertexList.Add(vert2);
                        }
                    }
                    catch (Exception err)
                    {
                        throw new Exception("Can't parse facet1 vertices (line " + lineNum + ") \n\"" + line + "\":\n" + err.Message);
                    }
                }
            }

            return vertexList;
        }

        private void apertureGrid_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            // apertureGrid.Rows[e.RowIndex].Cells["FileBrowse"].Value = "...";
        }

        private void apertureInfoButton_Click(object sender, EventArgs e)
        {
            string infoText =
                @"By default, you can use a few predefined shapes of the previous tab's aperture type field
                Here you can import custom cross-sections, which can be referenced from the sequence list
                
                3 types are supported:
                 - Molflow/Synrad TXT file: Create a facet in these programs' GUI in the XY plane (Z coords are discarded),
                   then save the geometry (or use File/Export selected facets) as a TXT file (a legacy format)
                 - TXT coordinate list: a list of 2d coordinates (in meters), one line per vertex, the x,y coordinates separated by whitespace
                 - CSV coordinate list: same as above, coordinates separated by commas (whitespace ignored)
                
                To reference these cross-sections, use the name (first column) which serves as an identifier.
                If no aperture parameters are given, the coordinates will be applied as-is.
                Param1 (target width) and Param2 (tagert height) allow you to rescale the (bounding box) width and height,
                and Param3 and Param4 allows to set an X,Y offset (meters, after scaling) of the cross-section, relative to the beam reference.
                
                Apertures (in TXT coordinate list format) will be auto-loaded here on startup.";
            MessageBox.Show(infoText);
        }

        private void tabControl_Resize(object sender, EventArgs e)
        {

        }

        private void apertureGrid_Resize(object sender, EventArgs e)
        {

            int nbCol = apertureGrid.Columns.Count;
            int othersWidth = 0; //All fields but file location
            for (int i = 0; i < nbCol; i++)
            {
                if (apertureGrid.Columns["FileLocation"] == apertureGrid.Columns[i]) continue; //Don't count this column
                int width = apertureGrid.Columns[i].Width;
                othersWidth += width;
            }
            apertureGrid.Columns["FileLocation"].Width = this.Width - 110 - othersWidth;
        }

        private static int IDX(int i, int nb)
        {
            //Return circular index restrained within 0..nb, allows negative index (Python logics: -1=last)
            int ret = i % nb;
            return (ret >= 0) ? (ret) : (ret + nb);
        }


        private static int Next(int i, int nb, bool inverseDir = false)
        {
            //Returns the next element of a circular index (next of last is first)
            //inverseDir is a helper: when true, returns the previous
            if (!inverseDir)
            {
                int next = i + 1;
                if (next == nb) next = 0;
                return next;
            }
            else return Previous(i, nb, false);
        }

        private static int Previous(int i, int nb, bool inverseDir = false)
        {
            //Returns the previous element of a circular index (previous of first is last)
            //inverseDir is a helper: when true, returns the next
            if (!inverseDir)
            {
                if (i == 0) return nb - 1;
                else return i - 1;
            }
            else return Next(i, nb, false);
        }

        private void label23_Click(object sender, EventArgs e)
        {

        }

        private void label22_Click(object sender, EventArgs e)
        {

        }

        private void emittYtext_TextChanged(object sender, EventArgs e)
        {

        }

        private void emittXtext_TextChanged(object sender, EventArgs e)
        {

        }

        private void label19_Click(object sender, EventArgs e)
        {

        }

        private void label18_Click(object sender, EventArgs e)
        {

        }

        private void energySpreadText_TextChanged(object sender, EventArgs e)
        {

        }

        private void pMassText_TextChanged(object sender, EventArgs e)
        {

        }

        private void label21_Click(object sender, EventArgs e)
        {

        }

        private void beamELabel_Click(object sender, EventArgs e)
        {

        }

        private void beamEText_TextChanged(object sender, EventArgs e)
        {

        }

        private void currentText_TextChanged(object sender, EventArgs e)
        {

        }

        private void saveButton_Click(object sender, EventArgs e)
        {
        }

        private void sessionGrid_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            int rowIndex = e.RowIndex;
            if (e.ColumnIndex == 2)
            {
                //Load
            }
            else if (e.ColumnIndex == 3)
            {
                //Overwrite with current
            }
            else if (e.ColumnIndex == 4)
            {
                //To trash
            }
        }

        private void reloadApertures_Click(object sender, EventArgs e)
        {
            apertureGrid.Rows.Clear();
            LoadApertureLibrary();
        }

        private void openSessionDirButton_Click(object sender, EventArgs e)
        {
            if (Directory.Exists("Sessions"))
            {
                System.Diagnostics.Process.Start("explorer.exe", "Sessions");
            }
            else
            {
                MessageBox.Show("There is no Sessions folder at:\n" + Directory.GetCurrentDirectory().ToString() + @"\Sessions");
            }
        }

        private void openTrashButton_Click(object sender, EventArgs e)
        {
            if (Directory.Exists(@"Sessions\Trash"))
            {
                System.Diagnostics.Process.Start("explorer.exe", @"Sessions\Trash");
            }
            else
            {
                MessageBox.Show("There is no trash folder at:\n" + Directory.GetCurrentDirectory().ToString() + @"\Sessions\Trash");
            }
        }

        private void emptyTrashButton_Click(object sender, EventArgs e)
        {
            if (Directory.Exists(@"Sessions\Trash"))
            {
                string[] filePaths = Directory.GetFiles(@"Sessions\Trash", "*.xlsx", SearchOption.TopDirectoryOnly);
                if (filePaths.Length == 0)
                {
                    MessageBox.Show("There are no .xlsx files in\n" + Directory.GetCurrentDirectory().ToString() + @"\Sessions\Trash");
                    return;
                }
                else
                {
                    if (DialogResult.OK == MessageBox.Show(filePaths.Length.ToString() + " files will be deleted.", "Empty trash", MessageBoxButtons.OKCancel))
                    {
                        foreach (var path in filePaths)
                        {
                            try
                            {
                                File.Delete(path);
                            }
                            catch (Exception err)
                            {
                                MessageBox.Show("Error deleting " + path.ToString() + "\n" + err.Message);
                                return;
                            }
                        }
                    }
                }
            }
            else
            {
                MessageBox.Show("There is no trash folder at:\n" + Directory.GetCurrentDirectory().ToString() + @"\Sessions\Trash");
            }
        }

        private void reloadSessionsButton_Click(object sender, EventArgs e)
        {
            sessionGrid.Rows.Clear();
            LoadSavedSessionLibrary();
        }

        private void LoadSavedSessionLibrary()
        {
            if (Directory.Exists(@"Sessions"))
            {
                string[] filePaths = Directory.GetFiles(@"Sessions", "*.xlsx", SearchOption.TopDirectoryOnly);

                try
                {

                    foreach (var path in filePaths)
                    {
                        sessionGrid.Rows.Add();
                        int lastRow = sessionGrid.RowCount - 1; //uncommitted row (user can't add rows)
                        var row = sessionGrid.Rows[lastRow];
                        int first = path.IndexOf(@"Sessions\") + @"Sessions\".Length;
                        int last = path.LastIndexOf(".xlsx");
                        string name = path.Substring(first, last - first);
                        row.Cells["Filename"].Value = name;
                        DateTime lastModified = System.IO.File.GetLastWriteTime(path);
                        row.Cells["Modified"].Value = lastModified.ToString();
                        row.Cells["LoadColumn"].Value = "Load";
                        row.Cells["Overwrite"].Value = "Overwrite";
                        row.Cells["Rename"].Value = "Rename";
                        row.Cells["Trash"].Value = "To trash";
                    }
                }
                catch (Exception err)
                {
                    MessageBox.Show("Error loading saved sessions library\n" + err.Message);
                    return;
                }
                Log("Loaded " + filePaths.Length + " saved sessions from the Sessions subfolder");
            }
        }

        private void sessionGrid_CellContentClick_1(object sender, DataGridViewCellEventArgs e)
        {
            int rowIndex = e.RowIndex;
            if (rowIndex == -1) return; //header click
            var cells = sessionGrid.Rows[rowIndex].Cells;
            if (e.ColumnIndex == 2)
            {
                //Load
                var value = cells["Filename"].Value;
                if (value == null) { MessageBox.Show("File name box empty, can't load. Reload sessions (button at the bottom)"); return; }
                string fileName = @"Sessions\" + value.ToString() + ".xlsx";
                if (File.Exists(fileName))
                {
                    if (sequenceGrid.RowCount <= 1 || DialogResult.OK == MessageBox.Show("Will overwrite current form data with\n" + fileName, "Confirm load", MessageBoxButtons.OKCancel))
                    {
                        LoadSession(fileName);
                        _mostRecentFile = null;
                        UpdateFormWindowText("");
                    }
                    else
                    {
                        return;
                    }
                }
                else
                {
                    MessageBox.Show("File " + fileName + " doesn't exist. Reload sessions (button at the bottom)"); return;
                }
            }
            else if (e.ColumnIndex == 3)
            {
                //Overwrite
                var value = cells["Filename"].Value;
                if (value == null) { MessageBox.Show("File name box empty, can't save. Reload sessions (button at the bottom)"); return; }
                string fileName = @"Sessions\" + value.ToString() + ".xlsx";
                if (File.Exists(fileName)) { if (DialogResult.OK != MessageBox.Show("File " + fileName + " will be overwritten with current form data.", "Confirm overwrite", MessageBoxButtons.OKCancel)) return; }
                try
                {
                    if (File.Exists("tempSave.xlsx")) File.Delete("tempSave.xlsx"); //Clean up previous unsuccesful attempt
                    SaveSession("tempSave.xlsx");
                    File.Copy("tempSave.xlsx", fileName, true); //Only overwrite if save successful
                    File.Delete("tempSave.xlsx"); //Only delete temp if copy succesful
                    sessionGrid.Rows.Clear();
                    LoadSavedSessionLibrary();
                }
                catch (Exception err)
                {
                    MessageBox.Show("Failed to save form data file:\n" + err.Message);
                    return;
                }

            }
            else if (e.ColumnIndex == 4)
            {
                //Rename
                var value = cells["Filename"].Value;
                if (value == null) { MessageBox.Show("File name box empty, can't rename. Reload sessions (button at the bottom)"); return; }
                string fileName = @"Sessions\" + value.ToString() + ".xlsx";
                if (!File.Exists(fileName)) { MessageBox.Show("File " + fileName + " doesn't exist, can't rename. Reload sessions (button at the bottom)"); return; }

                string newName = Interaction.InputBox("New session name:", "Rename session", value.ToString());
                if (newName.Length > 0) //User didn't click Cancel
                {
                    string targetName = @"Sessions\" + newName + ".xlsx";
                    bool ok = true;
                    if (File.Exists(targetName))
                    {
                        ok = (DialogResult.OK == MessageBox.Show(targetName + " already exists. Overwrite?", "Confirm overwrite", MessageBoxButtons.OKCancel));
                    }
                    if (ok) //Target doesn't exist or user confirmed overwrite
                    {
                        try
                        {
                            File.Copy(fileName, targetName, true);
                            File.Delete(fileName); //Executed only if copy was successful
                            sessionGrid.Rows.Clear();
                            LoadSavedSessionLibrary();
                        }
                        catch (Exception err)
                        {
                            MessageBox.Show("Failed to rename\n" + fileName + "\nto\n" + targetName + "\n\n" + err.Message);
                            return;
                        }
                    }
                    else return;
                }
            }
            else if (e.ColumnIndex == 5)
            {
                //Trash
                var value = cells["Filename"].Value;
                if (value == null) { MessageBox.Show("File name box empty, can't rename. Reload sessions (button at the bottom)"); return; }
                string fileName = @"Sessions\" + value.ToString() + ".xlsx";
                if (!File.Exists(fileName)) { MessageBox.Show("File " + fileName + " doesn't exist, can't rename. Reload sessions (button at the bottom)"); return; }
                if (DialogResult.OK != MessageBox.Show("Put session \"" + value.ToString() + "\" to Sessions\\Trash folder?", "Confirm delete", MessageBoxButtons.OKCancel)) return;
                try
                {
                    if (!Directory.Exists(@"Sessions\Trash")) Directory.CreateDirectory(@"Sessions\Trash");
                    string targetNameNoExt_ori = @"Sessions\Trash\" + value.ToString();
                    string targetNameNoExt = targetNameNoExt_ori;
                    bool exists = File.Exists(targetNameNoExt + ".xlsx");
                    int i;
                    for (i = 1; exists && i < 100; i++)
                    {
                        targetNameNoExt = targetNameNoExt_ori + "_" + i.ToString("00");
                        exists = File.Exists(targetNameNoExt + ".xlsx");
                    }
                    string targetName = targetNameNoExt + ".xlsx";
                    File.Copy(fileName, targetName);
                    File.Delete(fileName);
                    sessionGrid.Rows.Clear();
                    LoadSavedSessionLibrary();
                }
                catch (Exception err)
                {
                    MessageBox.Show("Failed to move \n" + fileName + "\nto trash.\n\n" + err.Message);
                    return;
                }

                sessionGrid.Rows.Clear();
                LoadSavedSessionLibrary();
            }
        }

        private bool LoadSession(string fileName, bool silent=false)
        {
            ExcelPackage.LicenseContext = OfficeOpenXml.LicenseContext.NonCommercial;
            FileInfo excelFile = new FileInfo(fileName);
            using (ExcelPackage excel = new ExcelPackage(excelFile))
            {
                var sequenceSheet = excel.Workbook.Worksheets["Sequence"];
                if (sequenceSheet != null)
                {
                    if (sequenceSheet.Dimension != null)
                    {
                        int rowCount = sequenceSheet.Dimension.Rows;
                        int columnCount = sequenceSheet.Dimension.Columns;
                        sequenceGrid.Rows.Clear();
                        if (rowCount > 1) sequenceGrid.Rows.Add(rowCount - 1);
                        for (int r = 0; r < rowCount; r++)
                        {
                            string hexaColor = sequenceSheet.Row(r + 2).Style.Fill.BackgroundColor.Rgb;
                            if (hexaColor != null)
                            {
                                sequenceGrid.Rows[r].DefaultCellStyle.BackColor = System.Drawing.ColorTranslator.FromHtml("#" + hexaColor);
                            }
                            for (int c = 0; c < Math.Min(columnCount,sequenceGrid.ColumnCount); c++)
                            {
                                sequenceGrid.Rows[r].Cells[c].Value = sequenceSheet.Cells[r + 2, c + 1].Value;
                            }
                        }
                    }
                } else if (!silent)
                {
                    MessageBox.Show(fileName + "\ndoesn't have a Sequence named worksheet", "Can't load");
                    return false;
                }
                var interfaceSheet = excel.Workbook.Worksheets["Interface settings"];
                if (interfaceSheet != null)
                {
                    var cells = interfaceSheet.Cells;
                    if (interfaceSheet.Dimension != null)
                    {
                        int rowCount = interfaceSheet.Dimension.Rows;
                        for (int r = 1; r <= rowCount; r++)
                        {
                            var cellValue = cells[r, 1].Value;
                            if (cellValue != null)
                            {
                                string paramName = cellValue.ToString();
                                var cell2Value = cells[r, 2].Value;
                                if (cell2Value != null)
                                {
                                    string paramValue = cell2Value.ToString();
                                    if (paramName == "referenceRowId") refRowTextBox.Text = paramValue;
                                    if (paramName == "refX") refXText.Text = paramValue;
                                    if (paramName == "refZ") refZText.Text = paramValue;
                                    if (paramName == "refTheta") refThetaText.Text = paramValue;
                                    if (paramName == "startRowId") startRowText.Text = paramValue;
                                    if (paramName == "endRowId") endRowText.Text = paramValue;
                                    if (paramName == "searchTerm") searchTermTextbox.Text = paramValue;
                                    if (paramName == "outputDir") dirComboBox.Text = paramValue;
                                    if (paramName == "hideTooltips") hideTooltipsCheckbox.Checked = paramValue.ToUpper() == "TRUE";
                                }
                            }
                        }
                    }
                } else if (!silent)
                {
                    MessageBox.Show(fileName + "\ndoesn't have an \"Interface settings\" named worksheet", "Can't load");
                    return false;
                }

                var beamSheet = excel.Workbook.Worksheets["Beam parameters"];
                if (beamSheet!=null) { 
                    var cells = beamSheet.Cells;
                    if (beamSheet.Dimension != null)
                    {
                        int rowCount = beamSheet.Dimension.Rows;
                        for (int r = 1; r <= rowCount; r++)
                        {
                            var cellValue = cells[r, 1].Value;
                            if (cellValue != null)
                            {
                                string paramName = cellValue.ToString();
                                var cell2Value = cells[r, 2].Value;
                                if (cell2Value != null)
                                {
                                    string paramValue = cell2Value.ToString();
                                    if (paramName == "emittanceX_m") emittXtext.Text = paramValue;
                                    if (paramName == "emittanceY_m") emittYtext.Text = paramValue;
                                    if (paramName == "pMass") pMassText.Text = paramValue;
                                    if (paramName == "energySpread_percent") energySpreadText.Text = paramValue;
                                    if (paramName == "current_mA") currentText.Text = paramValue;
                                    if (paramName == "energy_GeV") beamEText.Text = paramValue;
                                    if (paramName == "bxyFile") bxyFileNameText.Text = paramValue;
                                }
                            }
                        }
                    }
                }
                else if (!silent)
                {
                    MessageBox.Show(fileName + "\ndoesn't have an \"Interface settings\" named worksheet", "Can't load");
                    return false;
                }

                var paramSheet = excel.Workbook.Worksheets["Param files"];
                if (paramSheet!=null) { 
                    var cells = paramSheet.Cells;
                    if (paramSheet.Dimension != null)
                    {
                        int rowCount = paramSheet.Dimension.Rows;
                        for (int r = 1; r <= rowCount; r++)
                        {
                            var cellValue = cells[r, 1].Value;
                            if (cellValue != null)
                            {
                                string paramName = cellValue.ToString();
                                var cell2Value = cells[r, 2].Value;
                                if (cell2Value != null)
                                {
                                    string paramValue = cell2Value.ToString();
                                    if (paramName == "stepLength_cm") dLtext.Text = paramValue;
                                    if (paramName == "photonEmin_eV") eMinText.Text = paramValue;
                                    if (paramName == "photonEmax_eV") eMaxText.Text = paramValue;
                                    if (paramName == "ignoreConsecutive") ignoreSameSCheckBox.Checked = paramValue.ToUpper() == "TRUE";
                                    if (paramName == "fileNamePrefix") prefixCheckbox.Checked = paramValue.ToUpper() == "TRUE";
                                    if (paramName == "fileNamePrefix2") customPrefixText.Text = paramValue;
                                    if (paramName == "fileNameSuffix") suffixCheckbox.Checked = paramValue.ToUpper() == "TRUE";
                                }
                            }
                        }
                    }
                }
                else if (!silent)
                {
                    MessageBox.Show(fileName + "\ndoesn't have an \"Param files\" named worksheet", "Can't load");
                    return false;
                }

                var geomSheet = excel.Workbook.Worksheets["Synrad geometry"];
                if (geomSheet != null)
                {
                    var cells = geomSheet.Cells;
                    if (geomSheet.Dimension != null)
                    {
                        int rowCount = geomSheet.Dimension.Rows;
                        for (int r = 1; r <= rowCount; r++)
                        {
                            var cellValue = cells[r, 1].Value;
                            if (cellValue != null)
                            {
                                string paramName = cellValue.ToString();
                                var cell2Value = cells[r, 2].Value;
                                if (cell2Value != null)
                                {
                                    string paramValue = cell2Value.ToString();
                                    if (paramName == "geomFilename") geomFileTextBox.Text = paramValue;
                                    if (paramName == "circleSides") circleSidesText.Text = paramValue;
                                    if (paramName == "curveStep_rad") curveStepText.Text = paramValue;
                                    if (paramName == "stickingFactor") facetStickingTextbox.Text = paramValue;
                                    if (paramName == "apertureTransition") taperCombo.Text = paramValue;
                                    FindSelectedIndex(taperCombo);
                                    if (paramName == "apertureFacets") apertureFacetsCombo.Text = paramValue;
                                    FindSelectedIndex(apertureFacetsCombo);
                                }
                            }
                        }
                    }
                }
                else if (!silent)
                {
                    MessageBox.Show(fileName + "\ndoesn't have an \"Synrad geometry\" named worksheet", "Can't load");
                    return false;
                }

                return true;
            }
        }

        private void FindSelectedIndex(ComboBox combo)
        {
            for (int i = 0; i < combo.Items.Count; i++)
            {
                if (combo.Text == combo.Items[i].ToString())
                {
                    combo.SelectedIndex = i;
                    return;
                }
            }
        }

        private void SaveSession(string fileName)
        {
            ExcelPackage.LicenseContext = OfficeOpenXml.LicenseContext.NonCommercial;
            using (ExcelPackage excel = new ExcelPackage())
            {

                var sequenceSheet = excel.Workbook.Worksheets.Add("Sequence");

                int nbCol = apertureGrid.Columns.Count;
                List<string> headerRow = new List<string>();
                foreach (DataGridViewColumn col in sequenceGrid.Columns)
                {
                    headerRow.Add(col.HeaderText);
                }

                // Determine the header range (e.g. A1:E1)

                // Populate header row data
                for (int c = 0; c < headerRow.Count; c++)
                {
                    sequenceSheet.Cells[1, c + 1].Value = headerRow[c];
                }

                for (int r = 0; r <= sequenceGrid.RowCount - 2; r++)
                {
                    Color rowColor = sequenceGrid.Rows[r].DefaultCellStyle.BackColor;
                    bool nofill = rowColor.R == 0 && rowColor.G == 0 && rowColor.B == 0;
                    if (!nofill)
                    {
                        sequenceSheet.Row(r + 2).Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                        sequenceSheet.Row(r + 2).Style.Fill.BackgroundColor.SetColor(rowColor);
                    }
                    for (int c = 0; c < sequenceGrid.ColumnCount; c++)
                    {
                        var value = sequenceGrid.Rows[r].Cells[c].Value;
                        if (value != null)
                        {
                            sequenceSheet.Cells[r + 2, c + 1].Value = value.ToString();
                        }
                    }
                }
                for (int c = 0; c < headerRow.Count; c++)
                {
                    sequenceSheet.Column(c + 1).AutoFit();
                }


                var interfaceSheet = excel.Workbook.Worksheets.Add("Interface settings");

                var cells = interfaceSheet.Cells;
                int actualRow = 1;
                cells[actualRow, 1].Value = "referenceRowId"; cells[actualRow++, 2].Value = refRowTextBox.Text;
                cells[actualRow, 1].Value = "refX"; cells[actualRow++, 2].Value = refXText.Text;
                cells[actualRow, 1].Value = "refZ"; cells[actualRow++, 2].Value = refZText.Text;
                cells[actualRow, 1].Value = "refTheta"; cells[actualRow++, 2].Value = refThetaText.Text;
                actualRow++;
                cells[actualRow, 1].Value = "startRowId"; cells[actualRow++, 2].Value = startRowText.Text;
                cells[actualRow, 1].Value = "endRowId"; cells[actualRow++, 2].Value = endRowText.Text;
                actualRow++;
                cells[actualRow, 1].Value = "searchTerm"; cells[actualRow++, 2].Value = searchTermTextbox.Text;
                actualRow++;
                cells[actualRow, 1].Value = "outputDir"; cells[actualRow++, 2].Value = dirComboBox.Text;
                cells[actualRow, 1].Value = "hideTooltips"; cells[actualRow++, 2].Value = hideTooltipsCheckbox.Checked;
                interfaceSheet.Column(1).AutoFit();
                interfaceSheet.Column(2).AutoFit();


                var beamSheet = excel.Workbook.Worksheets.Add("Beam parameters");

                cells = beamSheet.Cells;
                actualRow = 1;
                cells[actualRow, 1].Value = "emittanceX_m"; cells[actualRow++, 2].Value = emittXtext.Text;
                cells[actualRow, 1].Value = "emittanceY_m"; cells[actualRow++, 2].Value = emittYtext.Text;
                cells[actualRow, 1].Value = "pMass"; cells[actualRow++, 2].Value = pMassText.Text;
                cells[actualRow, 1].Value = "energySpread_percent"; cells[actualRow++, 2].Value = energySpreadText.Text;
                cells[actualRow, 1].Value = "current_mA"; cells[actualRow++, 2].Value = currentText.Text;
                cells[actualRow, 1].Value = "energy_GeV"; cells[actualRow++, 2].Value = beamEText.Text;
                actualRow++;
                cells[actualRow, 1].Value = "bxyFile"; cells[actualRow++, 2].Value = bxyFileNameText.Text;
                beamSheet.Column(1).AutoFit();
                beamSheet.Column(2).AutoFit();

                var paramSheet = excel.Workbook.Worksheets.Add("Param files");

                cells = paramSheet.Cells;
                actualRow = 1;
                cells[actualRow, 1].Value = "stepLength_cm"; cells[actualRow++, 2].Value = dLtext.Text;
                cells[actualRow, 1].Value = "photonEmin_eV"; cells[actualRow++, 2].Value = eMinText.Text;
                cells[actualRow, 1].Value = "photonEmax_eV"; cells[actualRow++, 2].Value = eMaxText.Text;
                actualRow++;
                cells[actualRow, 1].Value = "ignoreConsecutive"; cells[actualRow++, 2].Value = ignoreSameSCheckBox.Checked;
                cells[actualRow, 1].Value = "fileNamePrefix"; cells[actualRow++, 2].Value = prefixCheckbox.Checked;
                cells[actualRow, 1].Value = "fileNamePrefix2"; cells[actualRow++, 2].Value = customPrefixText.Text; 
                cells[actualRow, 1].Value = "fileNameSuffix"; cells[actualRow++, 2].Value = suffixCheckbox.Checked;
                paramSheet.Column(1).AutoFit();
                paramSheet.Column(2).AutoFit();

                var geomSheet = excel.Workbook.Worksheets.Add("Synrad geometry");

                cells = geomSheet.Cells;
                actualRow = 1;
                cells[actualRow, 1].Value = "geomFilename"; cells[actualRow++, 2].Value = geomFileTextBox.Text;
                cells[actualRow, 1].Value = "circleSides"; cells[actualRow++, 2].Value = circleSidesText.Text;
                cells[actualRow, 1].Value = "curveStep_rad"; cells[actualRow++, 2].Value = curveStepText.Text;
                cells[actualRow, 1].Value = "stickingFactor"; cells[actualRow++, 2].Value = facetStickingTextbox.Text;
                actualRow++;
                cells[actualRow, 1].Value = "apertureTransition"; cells[actualRow++, 2].Value = taperCombo.Text;
                cells[actualRow, 1].Value = "apertureFacets"; cells[actualRow++, 2].Value = apertureFacetsCombo.Text;

                geomSheet.Column(1).AutoFit();
                geomSheet.Column(2).AutoFit();


                FileInfo excelFile = new FileInfo(fileName);
                excel.SaveAs(excelFile);                
            }
        }

        private void sessionGrid_Resize(object sender, EventArgs e)
        {
            int nbCol = sessionGrid.Columns.Count;
            int othersWidth = 0; //All fields but file name
            for (int i = 0; i < nbCol; i++)
            {
                if (sessionGrid.Columns["Filename"] == sessionGrid.Columns[i]) continue; //Don't count this column
                int width = sessionGrid.Columns[i].Width;
                othersWidth += width;
            }
            sessionGrid.Columns["Filename"].Width = this.Width - 130 - othersWidth;
        }

        private void saveSessionAsNewButton_Click(object sender, EventArgs e)
        {
            string value = newSessionNameBox.Text;
            if (value.Length == 0) { MessageBox.Show("Session name can't be empty."); return; }
            string sessionName = value.ToString();
            if (sessionName.EndsWith(".xlsx")) sessionName = sessionName.Substring(0, sessionName.Length - 5); //Cut down extension
            string fileName = @"Sessions\" + sessionName + ".xlsx";
            if (File.Exists(fileName)) { MessageBox.Show(fileName + "\nalready exists. Use the Overwrite button if that's what you want."); return; }
            try
            {
                if (!Directory.Exists("Sessions")) Directory.CreateDirectory("Sessions"); //First run
                SaveSession(fileName);
                sessionGrid.Rows.Clear();
                LoadSavedSessionLibrary();
            }
            catch (Exception err)
            {
                MessageBox.Show("Failed to save form as\n" + fileName + "\n\n" + err.Message);
                return;
            }
        }

        private void OpticsBuilderForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (sequenceGrid.RowCount > 1)
            {
                var result = MessageBox.Show("Sequnce grid not empty. Are you sure you want to quit?", "Confirm quit", MessageBoxButtons.OKCancel);
                if (result != DialogResult.OK) e.Cancel = true;
            }
            if (loadRecentSubmenuItem.DropDownItems.Count > 0)
            {
                SaveRecentSessions();
            }

        }

        private void dirComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            ValidateDir((System.Windows.Forms.ComboBox)sender);
        }

        private void dirComboBox_TextUpdate(object sender, EventArgs e)
        {
            ValidateDir((System.Windows.Forms.ComboBox)sender);
        }

        private void startLineLabel_Click(object sender, EventArgs e)
        {

        }

        private void startLineText_TextChanged(object sender, EventArgs e)
        {

        }

        private void label13_Click(object sender, EventArgs e)
        {

        }

        private void endLineText_TextChanged(object sender, EventArgs e)
        {

        }

        private void removeFromRecents_Click(object sender, EventArgs e)
        {

        }

        private void customPrefixText_TextChanged(object sender, EventArgs e)
        {
            FormatExampleFilename();
        }
        
        private void FormatExampleFilename()
        {
            string prefixExample = prefixCheckbox.Checked ? "0017_" : "";
            string prefixExample2 = customPrefixText.Text.Length > 0 ? (customPrefixText.Text + "_") : "";
            string filenameExample = "Q1_dipole.2";
            string suffixExample = suffixCheckbox.Checked ? "_617.842" : "";
            string extensionExample = ".param";
            filenameExampleLabel.Text = prefixExample + prefixExample2 + filenameExample + suffixExample + extensionExample;
        }

        private void prefixCheckbox_CheckedChanged(object sender, EventArgs e)
        {
            FormatExampleFilename();
        }

        private void paramTabcontrol_SelectedIndexChanged(object sender, EventArgs e)
        {
            FormatExampleFilename();
        }

        private void hideTooltipsCheckbox_CheckedChanged(object sender, EventArgs e)
        {
            toolTip1.Active = !hideTooltipsCheckbox.Checked;
        }

        private void previewGeomButton_Click(object sender, EventArgs e)
        {
            if (CheckTableFilled() &&
                CheckIfSCalculated() &&
                CheckStartStopValidity()) {
                int startLine, endLine;
                if (startRowText.Text != "") startLine = Convert.ToInt16(startRowText.Text) - 1;
                else startLine = 0;
                if (endRowText.Text != "") endLine = Convert.ToInt16(endRowText.Text) - 1;
                else endLine = sequenceGrid.RowCount - 2;

                var previewForm = new SequencePlot();
                previewForm.Text = "Sequence preview";
                previewForm.Show();
                previewForm.DrawSequence(sequenceGrid,startLine,endLine);
            }
        }

        private void sequenceGrid_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            (sender as DataGridView).Rows[e.RowIndex].HeaderCell.Value = (e.RowIndex + 1).ToString();
        }

        private void sequenceGrid_ColumnSortModeChanged(object sender, DataGridViewColumnEventArgs e)
        {
        }

        private void sequenceGrid_Sorted(object sender, EventArgs e)
        {
            
        }

        private void invertChargeButton_Click(object sender, EventArgs e)
        {
            var answer = MessageBox.Show("You are about to invert the particle mass (charge) and all bending angles and quadrupole gradients in the table"+
                " (useful for beam2 calculation on e-p machines). After, you'll have to manually recalculate the X,Z coordinates using the reference row.",
                "Charge and angle invert", MessageBoxButtons.OKCancel);
            if (answer == DialogResult.OK)
            {
                InvertChargeAndAngles();
            }
        }

        private void sessionCountLabel_Click(object sender, EventArgs e)
        {

        }

        private void UpdateFormWindowText(string text)
        {
            this.Text = "Survey file to SynRad+ converter" + text;
        }

        private void RemoveFromRecentSessions(string path)
        {
            // Remove all sessions at the specified path
            for (int i = 0; i < loadRecentSubmenuItem.DropDownItems.Count; i++)
            {
                if (loadRecentSubmenuItem.DropDownItems[i].Text == path)
                {
                    loadRecentSubmenuItem.DropDownItems.RemoveAt(i);
                }
            }
        }

        private void AddToRecentSessions(string path, bool toBack=false)
        {
            ToolStripMenuItem recentSubmenuItem = new ToolStripMenuItem(path);
            recentSubmenuItem.Click += loadRecentSubmenuItem_Click;

            // Remove if already exists, to avoid duplicates
            RemoveFromRecentSessions(path);
            if (!toBack)
            {
                loadRecentSubmenuItem.DropDownItems.Insert(0, recentSubmenuItem); //So it's at top
            }
            else
            {
                loadRecentSubmenuItem.DropDownItems.Add(recentSubmenuItem); //So it's at back
            }
        }

        private void LoadRecentSessions()
        {
            loadRecentSubmenuItem.DropDownItems.Clear();
            if (File.Exists("recentSessions.txt"))
            {
                string line;

                using (System.IO.StreamReader file =
                    new System.IO.StreamReader("recentSessions.txt"))
                {
                    while ((line = file.ReadLine()) != null && loadRecentSubmenuItem.DropDownItems.Count < 20)
                    {
                        AddToRecentSessions(line, true); //add to back
                    }

                    file.Close();
                    Log("Loaded " + loadRecentSubmenuItem.DropDownItems.Count + " recent sessions from recentSessions.txt");
                }
            }
        }

        private void SaveRecentSessions()
        {
            try
            {
                // Read the file and display it line by line.  
                using (System.IO.StreamWriter file =
                    new System.IO.StreamWriter("recentSessions.txt"))
                {
                    foreach (ToolStripMenuItem item in loadRecentSubmenuItem.DropDownItems)
                    {
                        file.WriteLine(item.Text);
                    }
                    file.Close();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Can't save recent sessions:\n" + ex.Message); //Fail silently
            }
        }

        private void newSubmenuItem_Click(object sender, EventArgs e)
        {
            var confirmResult = MessageBox.Show(
                "Any unsaved changes to the current session will be lost. Are you sure you want to start a new, empty session?",
                "Confirm new, empty session",
                MessageBoxButtons.YesNo);

            if (confirmResult == DialogResult.Yes)
            {
                _mostRecentFile = null;
                UpdateFormWindowText("");

                // clear sequence table
                sequenceGrid.Rows.Clear();

                // clear interface settings
                refRowTextBox.Text = string.Empty;
                refXText.Text = "0";
                refZText.Text = "0";
                refThetaText.Text = "0";
                startRowText.Text = string.Empty;
                endRowText.Text = string.Empty;
                searchTermTextbox.Text = string.Empty;
                searchTermTextbox.BackColor = Color.FromArgb(255, 255, 255);
                dirComboBox.Text = string.Empty;
                dirComboBox.BackColor = Color.FromArgb(255, 255, 255);
                hideTooltipsCheckbox.Checked = false;

                // set default beam properties
                emittXtext.Text = "1E-9";
                emittYtext.Text = "1E-9";
                pMassText.Text = "-0.00051099891";
                energySpreadText.Text = "0.1";
                currentText.Text = "1";
                beamEText.Text = "1";
                bxyFileNameText.Text = "beam.bxy";

                // set default synrad param files
                dLtext.Text = "1";
                eMinText.Text = "4";
                eMaxText.Text = "50E6";
                ignoreSameSCheckBox.Checked = true;
                prefixCheckbox.Checked = true;
                customPrefixText.Text = string.Empty;
                suffixCheckbox.Checked = false;

                // set default synrad/molflow geometry
                geomFileTextBox.Text = "Geometry.txt";
                circleSidesText.Text = "36";
                curveStepText.Text = "0.001";
                facetStickingTextbox.Text = "1.0";
                taperCombo.Text = "Continous (taper)";
                FindSelectedIndex(taperCombo);
                apertureFacetsCombo.Text = "First and last (end caps)";
                FindSelectedIndex(apertureFacetsCombo);


                if (File.Exists(@"Sessions\autoload.xlsx"))
                {
                    LoadSession(@"Sessions\autoload.xlsx", true); //Load template, silently abandon on error
                    Log("Found and loaded autoload.xlsx in Sessions folder.");
                }
                else
                {
                    Log("No autoload.xlsx in Sessions folder, filling form with default values.");
                }
            }
        }

        private void loadSubmenuItem_Click(object sender, EventArgs e)
        {
            var confirmResult = MessageBox.Show(
                "Any unsaved changes to the current session will be lost. Are you sure you want to load the selected session?",
                "Confirm new, empty session",
                MessageBoxButtons.YesNo);
            if (confirmResult == DialogResult.Yes)
            {
                OpenFileDialog openFileDialog1 = new OpenFileDialog();
                var filter = "XLSX files (*.xlsx)|*.xlsx|All files (*.*) | *.*";
                openFileDialog1.Filter = filter;

                if (openFileDialog1.ShowDialog() == DialogResult.OK)
                {

                    var path = openFileDialog1.FileName;
                    if (Path.GetExtension(path) != ".xlsx")
                    {
                        MessageBox.Show(path + "has invalid file extension. Only xslx supported.",
                            "Error loading session",
                            MessageBoxButtons.OK,
                            MessageBoxIcon.Error);
                    }
                    else
                    {
                        if (LoadSession(path))
                        {
                            AddToRecentSessions(path);
                            _mostRecentFile = path;
                            UpdateFormWindowText(" - " + Path.GetFileName(path));
                        }
                    }
                }
            }
        }

        private void saveAsSubmenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveFileDialog1 = new SaveFileDialog();
            var filter = "XLSX files (*.xlsx)|*.xlsx|Supported types (*.xlsx) | *.xlsx";
            saveFileDialog1.Filter = filter;

            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                var path = saveFileDialog1.FileName;

                try
                {
                    if (File.Exists("tempSave.xlsx")) File.Delete("tempSave.xlsx"); //Clean up previous unsuccesful attempt
                    SaveSession("tempSave.xlsx");
                    File.Copy("tempSave.xlsx", path, true); //Only overwrite if save successful
                    File.Delete("tempSave.xlsx");

                    AddToRecentSessions(path);
                    _mostRecentFile = path;
                    UpdateFormWindowText(" - " + Path.GetFileName(path));
                }
                catch (Exception err)
                {
                    MessageBox.Show("Failed to save form data file:\n" + err.Message);
                    return;
                }
            }
        }

        private void loadRecentSubmenuItem_Click(object sender, EventArgs e)
        {
            var confirmResult = MessageBox.Show(
                "Any unsaved changes to the current session will be lost. Are you sure you want to load the selected session?",
                "Confirm new, empty session",
                MessageBoxButtons.YesNo);

            if (confirmResult == DialogResult.Yes)
            {
                ToolStripMenuItem menuItem = sender as ToolStripMenuItem;
                var path = menuItem.Text;
                if (!File.Exists(path))
                {
                    confirmResult = MessageBox.Show(
                        path + " does not exist. Remove it from the Recent files menu?",
                        "File not found",
                        MessageBoxButtons.YesNo);
                    if (confirmResult == DialogResult.Yes)
                    {
                        RemoveFromRecentSessions(path);
                    }
                }
                else
                {
                    if (LoadSession(path))
                    {
                        AddToRecentSessions(path);
                        _mostRecentFile = path;
                        UpdateFormWindowText(" - " + Path.GetFileName(path));
                    }
                    else
                    {
                        confirmResult = MessageBox.Show(
                            path + " could not be loaded. Remove it from the Recent files menu?",
                            "Error loading session",
                            MessageBoxButtons.YesNo);
                        if (confirmResult == DialogResult.Yes)
                        {
                            RemoveFromRecentSessions(path);
                        }
                    }
                }
            }


        }

        private void saveSubmenuItem_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(_mostRecentFile))
            {
                saveAsSubmenuItem_Click(sender, e);
            }
            else
            {
                try
                {
                    if (File.Exists("tempSave.xlsx")) File.Delete("tempSave.xlsx"); //Clean up previous unsuccesful attempt
                    SaveSession("tempSave.xlsx");
                    File.Copy("tempSave.xlsx", _mostRecentFile, true); //Only overwrite if save successful
                    File.Delete("tempSave.xlsx");
                }
                catch (Exception err)
                {
                    MessageBox.Show("Failed to save form data file:\n" + err.Message);
                    return;
                }
            }
        }

        private void exitSubmenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

    }
}

class Vertex2d
{
    public double u, v;
    public Vertex2d(double U, double V)
    {
        u = U; v = V;
    }
};
class Vertex3d
{
    public double x, y, z;
    public Vertex3d(double X, double Y, double Z)
    {
        x = X; y = Y; z = Z;
    }
    public double Norme()
    {
        return Math.Sqrt(x * x + y * y + z * z);
    }
    public static Vertex3d operator +(Vertex3d a, Vertex3d b)
    {
        Vertex3d result = new Vertex3d(
            a.x + b.x,
            a.y + b.y,
            a.z + b.z);
        return result;
    }
    public static Vertex3d operator *(double s, Vertex3d v)
    {
        Vertex3d result = new Vertex3d(
            s * v.x,
            s * v.y,
            s * v.z);
        return result;
    }
    public static Vertex3d operator -(Vertex3d a, Vertex3d b)
    {
        return a + (-1.0 * b);
    }
    public Vertex3d Normalized()
    {
        double norme = this.Norme();
        if (norme > 0)
        {
            return (1.0 / norme) * this;
        }
        else
        {
            return 1.0 * this;
        }
    }
    public Vertex3d CrossProductWith(Vertex3d b)
    {
        return new Vertex3d(
            this.y * b.z - this.z * b.y,
            this.z * b.x - this.x * b.z,
            this.x * b.y - this.y * b.x
            );
    }
};

class Facet
{
    //Synrad logic
    public List<int> vertexIds;
    public bool isTransparent; //For export, used for aperture cross sections
    public Facet() => vertexIds = new List<int>();
    public Facet(List<int> idList)
    {
        vertexIds = new List<int>(idList);
    }
};
class Polygon2d //List<Vertex2d> with helper functions and constructors for analytic shapes
{
    public List<Vertex2d> vertices;
    public Polygon2d() => vertices = new List<Vertex2d>();

    public static Polygon2d NormalizeUV(Polygon2d sourcePoly)
    {
        //Calculates bounding box and puts all coordinates within 0..1
        Polygon2d result = new Polygon2d();
        double uMin = 9e99;
        double uMax = -9E99;
        double vMin = 9e99;
        double vMax = -9e99;
        foreach (Vertex2d v in sourcePoly.vertices) //First pass: search
        {
            uMin = Math.Min(v.u, uMin);
            uMax = Math.Max(v.u, uMax);
            vMin = Math.Min(v.v, vMin);
            vMax = Math.Max(v.v, vMax);
        }
        double uSpan = uMax - uMin; if (uSpan == 0.0) uSpan = 1.0;
        double vSpan = vMax - vMin; if (vSpan == 0.0) vSpan = 1.0;
        foreach (Vertex2d v in sourcePoly.vertices) //Second pass: normalize
        {
            Vertex2d newVertex = new Vertex2d((v.u - uMin) / uSpan, (v.v - vMin) / vSpan);
            result.vertices.Add(newVertex);
        }
        return result;
    }
    public static Polygon2d Ellipse(int nbSides, double radius1, double radius2) //Circle
    {
        Polygon2d result = new Polygon2d();
        for (int i = 0; i < nbSides; i++)
        {
            double alpha = 2.0 * Math.PI * (double)i / (double)nbSides;
            Vertex2d point = new Vertex2d(
            radius1 * Math.Cos(alpha),
            radius2 * Math.Sin(alpha)
            );
            result.vertices.Add(point);
        }
        return result;
    }
    public static Polygon2d Racetrack(int nbSides,
        double param1, //Radius or bounding box half-width
        double param2, //Bounding box half-height
        bool hiResSides)
    {
        Polygon2d result = new Polygon2d();
        bool horizontal = param2 <= param1;
        double radius = (horizontal ? param1 : param2);
        if (param1 < 1E-8) throw new System.Exception("Aperture1 can't be 0");
        double cornerAngle;
        if (horizontal) cornerAngle = Math.Asin(param2 / param1);
        else cornerAngle = Math.Acos(param1 / param2);

        //Construct vertices counterclockwise
        double angle_left_right = 2.0 * cornerAngle; //Left and Right side angles
        double angle_top_bottom = 2.0 * (Math.PI / 2.0 - cornerAngle); //Top and bottom side angles
        int nbIntermediate_left_right = (int)(angle_left_right / (2.0 * Math.PI) * ((double)nbSides - 4.0)); //intervals on the left and right
        int nbIntermediate_top_bottom = (int)(angle_top_bottom / (2.0 * Math.PI) * ((double)nbSides - 4.0)); //intervals on the left and right
        double angleStep_left_right = angle_left_right / (double)nbIntermediate_left_right;
        double angleStep_top_bottom = angle_top_bottom / (double)nbIntermediate_top_bottom;

        List<double> vertexAngles = new List<double>(); //Angles where vertices are created

        //Add four corners
        vertexAngles.Add(cornerAngle); //top right

        for (int i = 0; i < nbIntermediate_top_bottom - 1; i++)
        {
            vertexAngles.Add(cornerAngle + (double)(i + 1) * angleStep_top_bottom);
        }

        vertexAngles.Add(Math.PI - cornerAngle); //top left

        for (int i = 0; i < nbIntermediate_left_right - 1; i++)
        {
            vertexAngles.Add(Math.PI - cornerAngle + (double)(i + 1) * angleStep_left_right);
        }

        vertexAngles.Add(Math.PI + cornerAngle); //bottom left
        for (int i = 0; i < nbIntermediate_top_bottom - 1; i++)
        {
            vertexAngles.Add(Math.PI + cornerAngle + (double)(i + 1) * angleStep_top_bottom);
        }

        vertexAngles.Add(2.0 * Math.PI - cornerAngle); //bottom right
        for (int i = 0; i < nbIntermediate_left_right - 1; i++)
        {
            double angle = 2.0 * Math.PI - cornerAngle + (double)(i + 1) * angleStep_left_right;
            if (angle >= 2.0 * Math.PI) angle -= 2.0 * Math.PI; //Normalize to 0..2PI
            vertexAngles.Add(angle);
        }


        for (int i = 0; i < vertexAngles.Count; i++)
        {
            double u, v;
            double alpha = vertexAngles[i];
            bool curvedPart = (alpha < cornerAngle || alpha > (2.0 * Math.PI - cornerAngle)) || (alpha > (Math.PI - cornerAngle) && alpha < (Math.PI + cornerAngle));
            curvedPart = horizontal == curvedPart; //invert result if vertical
            if (curvedPart)
            {
                u = radius * Math.Cos(alpha);
                v = radius * Math.Sin(alpha);
            }
            else //straight side
            {
                if (horizontal)
                {
                    u = radius * Math.Cos(alpha);
                    v = param2; //height capped
                    if (alpha > Math.PI) v *= -1;
                }
                else
                {
                    u = param1; //width capped
                    if (alpha > 0.5 * Math.PI && alpha < 1.5 * Math.PI) u *= -1;
                    v = radius * Math.Sin(alpha);
                }
            }

            /*
            //Adjust corners
            if (curvedPart != withinCurve) //Just stepped out of curve or into curve
            {
                //Determine nearest corner angle
                double nearestCornerAngle;
                if (alpha < Math.PI / 2) nearestCornerAngle = cornerAngle;
                else if (alpha < Math.PI) nearestCornerAngle = Math.PI - cornerAngle;
                else if (alpha < 1.5 * Math.PI) nearestCornerAngle = Math.PI + cornerAngle;
                else nearestCornerAngle = 2.0 * Math.PI - cornerAngle;

                if (!curvedPart) //Stepped out of curve
                {
                    //Adjust actual (first straight) point to previous corner:
                    if (i > 0 && Math.Abs(2.0 * Math.PI * (double)(i - 1) / (double)nbSides - nearestCornerAngle) > 1e-3)
                    {
                        u = radius * Math.Cos(nearestCornerAngle);
                        v = radius * Math.Sin(nearestCornerAngle);
                    }
                    else
                    {
                        //No adjustment needed, last point was exactly the corner 
                    }
                }
                else //Stepped into curve
                {
                    //Adjust previous (last straight) point:
                    if (i > 0 && Math.Abs(2.0 * Math.PI * (double)(i - 1) / (double)nbSides - nearestCornerAngle) > 1e-3)
                    {
                        result.vertices[result.vertices.Count - 1].u = radius * Math.Cos(nearestCornerAngle);
                        result.vertices[result.vertices.Count - 1].v = radius * Math.Sin(nearestCornerAngle);
                    }
                    else
                    {
                        //Leave last point as it is
                    }
                    //Current point unchanged:
                    u = radius * Math.Cos(alpha);
                    v = radius * Math.Sin(alpha);
                }
                withinCurve = curvedPart; //Update status
            }

            */

            result.vertices.Add(new Vertex2d(u, v));
        }

        return result;
    }

    public static Polygon2d UserAperture(List<Vertex2d> vertexList, double param1, double param2, double param3, double param4)
    {
        Polygon2d result = new Polygon2d();
        foreach (var vert in vertexList)
        {
            double u = vert.u * param1 + param3;
            double v = vert.v * param2 + param4;
            result.vertices.Add(new Vertex2d(u, v));
        }
        return result;
    }

};

class Polygon3d //List<Vertex2d> with constructor from Polygon2d
{
    public List<Vertex3d> vertices;
    public int firstVertexId; //BuildVertices marks where in the vertex list "vertices" is the first vertex
    public Polygon3d(List<Vertex2d> source2d, double xStart, double zStart, double theta)
    {
        vertices = new List<Vertex3d>();
        foreach (var vertex in source2d)
        {
            Vertex3d point = new Vertex3d(xStart, vertex.v, zStart);
            point.x += vertex.u * Math.Cos(theta);
            point.z += vertex.u * Math.Sin(theta);

            vertices.Add(point);
        }
    }

    public Polygon3d(Aperture sourceAperture, Polygon2d sourcePoly)
    {
        vertices = new List<Vertex3d>();
        foreach (var vertex in sourcePoly.vertices)
        {
            Vertex3d point = new Vertex3d(sourceAperture.x, vertex.v, sourceAperture.z);
            point.x += vertex.u * Math.Cos(sourceAperture.theta);
            point.z += vertex.u * Math.Sin(sourceAperture.theta);

            vertices.Add(point);
        }
    }
};

enum ApertureType
{
    Undefined,
    Circle,
    Racetrack,
    User
};

class Aperture
{
    public ApertureType type;
    public double param1, param2, param3, param4;
    public double x, z, theta;
    public bool isCurveStep = false; //Is it an intermediate step of a curve (option to not export it)
    public List<Vertex2d> vertexList; //2d list of user defined vertices
    //private Aperture startAperture;

    public Aperture()
    {
    }

    public Aperture(Aperture rhs)
    {
        this.type = rhs.type;
        this.param1 = rhs.param1;
        this.param2 = rhs.param2;
        this.param3 = rhs.param3;
        this.param4 = rhs.param4;
        this.vertexList = rhs.vertexList; //shallow copy
        this.x = rhs.x;
        this.z = rhs.z;
        this.theta = rhs.theta;
    }
};

class loftIndex
{
    public int index;
    public bool visited = false;
    public bool boundary; //false if in the middle of a series of indices connecting to the same index on the other facet
};



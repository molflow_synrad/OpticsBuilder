# 2024-11-08

- Add File menu with the following features:
    - Save sessions to arbitrary locations
    - Load sessions from arbitrary locations
    - Quick reload of recently used sessions
- The use of Session library is now deprecated
- Color row backgrounds on setting reference point

# 2023-10-17

- Fix wrong labels on sequence plotter
- Add zoom capability to sequence plotter
- Add option to invert charge, bending angles and gradients
- Added row numbers
- Disable sorting of columns where no physical sense

# 2023-01-25

- Added rudimentary visualization (preview) for sequence
- Lots of sanity checks:
    - Are positions calculated from reference?
    - Are there empty lines?
    - Are start and end lines valid?
- Write BXY file entry at end of exported region